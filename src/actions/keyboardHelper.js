import { SET_CURRENT_REF, SET_INPUT_REFS, RESET_KEYBOARD_HELPER } from '../config/constants/actionsTypes';

export const setInputRefs = inputRefs => ({
  type: SET_INPUT_REFS,
  data: inputRefs,
});

export const setCurrentRef = current => ({
  type: SET_CURRENT_REF,
  data: current,
});

export const resetKeyboardHelper = params => ({
  type: RESET_KEYBOARD_HELPER,
  data: params,
});


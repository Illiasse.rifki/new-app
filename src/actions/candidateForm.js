export const SET_FIELD = 'SET_FIELD';
export const FLUSH_FORM = 'FLUSH_FORM';

export const setField = (key, value) => ({
  type: SET_FIELD,
  key,
  value,
});

export const flushForm = () => ({
  type: FLUSH_FORM,
});

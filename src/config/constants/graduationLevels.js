
const graduationLevels = [
  { text: '< Bac +2', value: '1' },
  { text: 'Bac+2/3', value: '2' },
  { text: 'Bac+4/5', value: '3' },
  { text: '> Bac +5', value: '4' },
];

export { graduationLevels };
const tags = [
  { value: 'Animation', key: 'animation' },
  { value: 'B to B', key: 'btob' },
  { value: 'B to C', key: 'btoc' },
  { value: 'Business Dev', key: 'businessdev' },
  { value: 'Chasseur', key: 'headhunter' },
  { value: 'Conseil', key: 'conseil' },
  { value: 'Gestion client', key: 'client-management' },
  { value: 'Grands compte', key: 'key-account' },
  { value: 'Indépendant', key: 'independant' },
  { value: 'Itinérant', key: 'itinerant' },
  { value: 'Management', key: 'management' },
  { value: 'Prospection', key: 'prospection' },
  { value: 'Sédentaire', key: 'sedentary' },
  { value: 'Support client', key: 'customer-support' },
];

export { tags };
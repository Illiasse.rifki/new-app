const sectors = [
  { value: 'Activités des organisations associatives et administration publique', key: 'associative-orgs-public-administration' },
  { value: 'Activités informatiques', key: 'it-activity' },
  { value: 'Activités juridiques et comptables', key: 'legal-accounting' },
  { value: 'Agroalimentaire', key: 'food' },
  { value: 'Automobile, aéronautique et autres matériels de transport', key: 'automobile-aviation' },
  { value: 'Autres', key: 'others' },
  { value: 'Banque et Assurances', key: 'bank-insurance' },
  { value: 'Bois - Papier - Imprimerie', key: 'wood-paper-print' },
  { value: 'Chimie - Caoutchouc - Plastique', key: 'chemistry-rubber-plastic' },
  { value: 'Commerce interentreprises', key: 'inter-company-commerce' },
  { value: 'Communication et médias', key: 'communication-media' },
  { value: 'Conseil et gestion des entreprises', key: 'business-management' },
  { value: 'Construction', key: 'construction' },
  { value: 'Distribution généraliste et spécialisée', key: 'distribution' },
  { value: 'Énergies', key: 'energies' },
  { value: 'Équipements électriques et électroniques', key: 'electrical' },
  { value: 'Formation initiale et continue', key: 'training' },
  { value: 'Gestion des déchets', key: 'waste-management' },
  { value: 'Hôtellerie - Restauration - Loisirs', key: 'hotel-catering-leisure' },
  { value: 'Immobilier', key: 'real-estate' },
  { value: 'Industrie pharmaceutique', key: 'pharmaceutical' },
  { value: 'Ingénierie - R et D', key: 'engineering' },
  { value: 'Intermédiaires du recrutement', key: 'recruitment' },
  { value: 'Mécanique - Métallurgie', key: 'mechanical' },
  { value: 'Meuble, Textile et autres industries manufacturières', key: 'furniture-textile' },
  { value: 'Santé - action sociale', key: 'health-social' },
  { value: 'Services divers aux entreprises', key: 'business-services' },
  { value: 'Télécommunications', key: 'telecommunications' },
  { value: 'Transports et logistique', key: 'transports-logistic' },
];

const workedSectors = [
  { value: 'Je n\'ai jamais travaillé', key: 'never-work' },
  ...sectors,
];

const trendySectors = [
  { value: 'Banque & Assurances', key: 'bank-insurance' },
  { value: 'Conseil & gestion des entreprises', key: 'business-management' },
  { value: 'Télécommunications', key: 'telecommunications' },
  { value: 'Transports et logistique', key: 'transports-logistic' },
  { value: 'Activités informatiques', key: 'it-activity' },
  { value: 'Automobile, aéronautique et autres matériels de transport', key: 'automobile-aviation' },
];

export { sectors, workedSectors, trendySectors }
export const JobSituationStatus = [
  { label: 'En poste', value: 'onDuty' },
  { label: "En recherche d'emploi", value: 'jobLooking' },
  { label: 'En recherche de stage/alternance', value: 'internshipLooking' },
];

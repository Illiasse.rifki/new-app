
const languages = [
  { code: 'de', language: 'Allemand' },
  { code: 'en', language: 'Anglais' },
  { code: 'ar', language: 'Arabe' },
  { code: 'bn', language: 'Bengali' },
  { code: 'zh', language: 'Chinois' },
  { code: 'ko', language: 'Coréen' },
  { code: 'es', language: 'Espagnol' },
  { code: 'fi', language: 'Finnois' },
  { code: 'fr', language: 'Français' },
  { code: 'hi', language: 'Hindi' },
  { code: 'id', language: 'Indonésien' },
  { code: 'it', language: 'Italien' },
  { code: 'ja', language: 'Japonais' },
  { code: 'ms', language: 'Malais' },
  { code: 'nl', language: 'Néerlandais' },
  { code: 'no', language: 'Norvégien' },
  { code: 'ur', language: 'Ourdou' },
  { code: 'pt', language: 'Portugais' },
  { code: 'ru', language: 'Russe' },
  { code: 'sv', language: 'Suédois' },
];

const levels = [
  { code: 1, label: 'Notions élémentaires' },
  { code: 2, label: 'Compétence professionelle limitée' },
  { code: 3, label: 'Compétence professionelle' },
  { code: 4, label: 'Compétence professionelle complète' },
  { code: 5, label: 'Bilingue ou langue natale' },
];

export { languages, levels };
export default languages;

const companySize = [
  { text: '1 à 20', value: '1-20' },
  { text: '21 à 50', value: '21-50' },
  { text: '51 à 250', value: '51-250' },
  { text: '+ de 250', value: '250+' },
];

export { companySize };
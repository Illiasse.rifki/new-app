import { StyleSheet, Dimensions, Platform } from 'react-native';

// const localhost = 'localhost';

export const URI = 'https://api.bizzeo.co/graphql';
export const CHAT = 'https://chat.bizzeo.co/';
// export const URI = `http://${localhost}:8080/graphql`;
// export const CHAT = `http://${localhost}:4000/`;
export const LINKEDIN =
  'https://linkedin.com/oauth/v2/authorization?response_type=code&client_id=777yvzri8ut0jj&redirect_uri=https://api.bizzeo.co/linkedin&state=monTest&scope=r_basicprofile%20r_emailaddress';
export const googleAPIKEY = 'AIzaSyBRKxMDFqHxl_IpaK0BcO5jaks092mLOBA';
export const GA_TRACKING_ID = 'UA-118814091-1';
export const ONE_SIGNAL_APP_ID = '851f95a4-f869-4d29-8a33-c07344fd4b5d';

const x = Dimensions.get('window').width;
const y = Dimensions.get('window').height;
export const window = { x, y };

const primaryColor = '#33cbcc';
const secondaryColor = '#ffffff';
const fontBlack = '#1c1c1c';

const Header = () => {
  if (Platform.OS === 'ios') {
    return {
      backgroundColor: primaryColor,
      paddingTop: 46,
      paddingBottom: 27,
      borderBottomWidth: 0,
    };
  }
  return {
    backgroundColor: primaryColor,
  };
};

export const themeElements = StyleSheet.create({
  header: Header(),
  headerTitle: {
    color: secondaryColor,
    width: 250,
    fontSize: 18,
    fontWeight: '500',
    lineHeight: 24,
  },
  title: {
    lineHeight: 24,
    fontSize: 18,
    fontWeight: '500',
    color: fontBlack,
  },
  bigTitle: {
    lineHeight: 40,
    fontSize: 28,
    fontWeight: 'bold',
    color: fontBlack,
  },
  formInputText: {
    color: fontBlack,
    width: '100%',
    fontSize: 16,
    marginBottom: -5,
  },
  formInputContainer: {
    marginTop: 16,
    marginLeft: 0,
    marginRight: 0,
  },
  validateButton: {
    borderRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { width: 0.5, height: 4 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    width: 310,
    height: 40,
    alignSelf: 'center',
  },
  validateSmallButton: {
    borderRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { width: 0.5, height: 4 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    height: 40,
    alignSelf: 'center',
  },
  bottomPageButton: {
    backgroundColor: primaryColor,
    shadowColor: 'rgba(0, 0, 0, 0.16)',
    shadowOffset: { width: 0, height: 1.5 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    paddingTop: 13.5,
    paddingBottom: 13.5,
  },
  bottomPageButtonContainer: {
    marginLeft: 0,
    marginRight: 0,
  },
  bottomPageButtonText: {
    color: secondaryColor,
    fontSize: 18,
    letterSpacing: 0.5,
    fontWeight: '500',
  },
});

export const theme = {
  primaryColor,
  secondaryColor,
  green: '#33cc80',
  gray: '#afafaf',
  grey: '#afafaf',
  red: '#cc4733',
  fontBlack,
};

export { languages } from './languages';
export { levels } from './languages';
export { sectors } from './sectors';
export { workedSectors } from './sectors';
export { trendySectors } from './sectors';
export { tags } from './tags';
export { qualities } from './qualities';
export { graduationLevels } from './graduationLevels';
export { companySize } from './companySize';
export { default as mapStyle } from './mapStyle';

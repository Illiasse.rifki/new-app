
const qualities = [
  { value: 'Autonomie', key: 'autonomy', icon: { name: 'md-compass', type: 'ionicon' } },
  { value: 'Convainquant', key: 'convincing', icon: { name: 'black-tie', type: 'font-awesome' } },
  { value: 'Communication', key: 'communication', icon: { name: 'commenting', type: 'font-awesome' } },
  { value: 'Créativité', key: 'creativity', icon: { name: 'lightbulb', type: 'foundation' } },
  { value: 'Curiosité', key: 'curiosity', icon: { name: 'search', type: 'material-icons' } },
  { value: 'Dynamique', key: 'dynamic', icon: { name: 'bolt', type: 'font-awesome' } },
  { value: 'Esprit d\'analyse', key: 'analytical-mind', icon: { name: 'gears', type: 'font-awesome' } },
  { value: 'Esprit d\'équipe', key: 'team-spirit', icon: { name: 'users', type: 'font-awesome' } },
  { value: 'Entrepreneuriat', key: 'entrepreneurship', icon: { name: 'trophy', type: 'font-awesome' } },
  { value: 'Flexible', key: 'flexible', icon: { name: 'random', type: 'font-awesome' } },
  { value: 'Responsable', key: 'responsible', icon: { name: 'user', type: 'entypo' } },
  { value: 'Rigoureux', key: 'rigorous', icon: { name: 'calendar-check', type: 'material-community' } },
];

export { qualities };

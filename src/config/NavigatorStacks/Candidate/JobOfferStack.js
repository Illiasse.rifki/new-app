import { StackNavigator, TabNavigator, TabBarTop } from 'react-navigation';
import JobOfferList, { PertinenceJobOffersList, FavoriteJobOffersList } from '../../../screens/job-offer/JobOffersList';
import JobOffer from '../../../screens/job-offer/JobOffer';
import { theme } from '../../constants/index';
import JobOffersRegistrationsList from '../../../screens/job-offer/JobOffersRegistrationsList';

const JobOffersFilter = TabNavigator(
  {
    NewJobOffers: {
      screen: JobOfferList,
      navigationOptions: {
        tabBarLabel: 'Nouvelles',
      },
    },
    PertinenceJobOffers: {
      screen: PertinenceJobOffersList,
      navigationOptions: {
        tabBarLabel: 'Pertinentes',
      },
    },
    FavoriteJobOffersList: {
      screen: FavoriteJobOffersList,
      navigationOptions: {
        title: 'Vos offres favorites',
        tabBarLabel: 'Favoris',
      },
    },
    JobOffersRegistrationsList: {
      screen: JobOffersRegistrationsList,
      navigationOptions: {
        tabBarLabel: 'Candidatures',
      },
    },
  },
  {
    lazy: true,
    tabBarComponent: TabBarTop,
    tabBarPosition: 'top',
    animationEnabled: false,
    swipeEnabled: false,
    tabBarOptions: {
      indicatorStyle: {
        backgroundColor: theme.primaryColor,
        height: 4,
      },
      showIcon: false,
      activeTintColor: theme.fontBlack,
      inactiveTintColor: theme.gray,
      upperCaseLabel: false,
      labelStyle: {
        fontSize: 12,
        marginTop: 0,
        marginLeft: 0,
        marginRight: 0,
      },
      iconStyle: {
        marginBottom: 0,
        color: theme.gray,
      },
      tabStyle: {
        justifyContent: 'flex-end',
        paddingTop: 15.5,
      },
      style: {
        justifyContent: 'center',
        borderTopColor: 'transparent',
        borderTopWidth: 0,
        backgroundColor: 'white',
        paddingLeft: 0,
        paddingRight: 0,
        height: 49,
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 0, height: 1.5 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
      },
    },
    initialRouteName: 'NewJobOffers',
  },
);

const JobOfferStack = StackNavigator({
  JobOffersFilter: {
    screen: JobOffersFilter,
  },
}, {
  headerMode: 'none',
  initialRouteName: 'JobOffersFilter',
});

export default JobOfferStack;

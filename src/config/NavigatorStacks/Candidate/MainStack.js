import React from "react";
import { Platform, TouchableOpacity } from "react-native";
import {
  DrawerNavigator,
  HeaderBackButton,
  TabNavigator,
} from "react-navigation";

import { Icon } from "./../../../components/Pure/react-native-elements";
import Home from "../../../screens/Home";
import Chats from "../../../screens/chat/Chats";
import SponsoredCompaniesList from "../../../screens/SponsoredCompaniesList";
import { theme } from "../../constants";
import CandidateEventsStack from "./EventsStack";
import CandidateJobOfferStack from "./JobOfferStack";
import Drawer from "../../../components/Content/Drawer";
import QRCodeCandidate from "../../../screens/profile/QRCodeCandidate";
import LegalMentions from "../../../screens/profile/LegalMentions";
import Settings from "../../../screens/profile/Settings";
import AccountInfos from "../../../screens/profile/AccountInfos";
import FAQCandidate from "../../../screens/profile/FAQCandidate";
import Profile from "../../../screens/profile/Profile";
import ImportantFieldModification from "../../../screens/profile/ImportantFieldModification";
import CandidateProfile from "../../../screens/profile/CandidateProfile";

const headerLeft = (navigation) =>
  navigation.state.index === 0 ? (
    <Drawer.Icon onPress={() => navigation.navigate("DrawerOpen")} />
  ) : (
    <TouchableOpacity
      hitSlop={{
        top: 20,
        bottom: 20,
        left: 50,
        right: 50,
      }}
      onPress={() => navigation.goBack(null)}
    >
      <HeaderBackButton
        tintColor={theme.secondaryColor}
        onPress={() => navigation.goBack(null)}
      />
    </TouchableOpacity>
  );

const TabStack = TabNavigator(
  {
    Home: {
      screen: Home,
    },
    EventsList: {
      screen: CandidateEventsStack,
      navigationOptions: ({ navigation }) => ({
        headerLeft: headerLeft(navigation),
        tabBarLabel: "Events",
        tabBarIcon: ({ tintColor }) => (
          <Icon
            size={20}
            name="event"
            type="material-icons"
            color={tintColor || theme.gray}
          />
        ),
      }),
    },
    SponsoredCompanies: {
      screen: SponsoredCompaniesList,
    },
    // JobOffers: {
    //   screen: CandidateJobOfferStack,
    //   navigationOptions: ({ navigation }) => ({
    //     headerLeft: headerLeft(navigation),
    //     tabBarLabel: 'Offres',
    //     tabBarIcon: ({ tintColor }) => (
    //       <Icon size={20} name="business-center" type="material-icons" color={tintColor || theme.gray} />
    //     ),
    //   }),
    // },
  },
  {
    headerMode: "float",
    tabBarPosition: "bottom",
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: theme.primaryColor,
      inactiveTintColor: theme.gray,
      labelStyle: {
        fontSize: 12,
        marginTop: Platform.OS === "android" ? 3 : 0,
      },
      iconStyle: {
        marginBottom: 0,
      },
      style: {
        borderTopColor: "transparent",
        backgroundColor: "white",
        paddingBottom: 8,
        height: 58,
        shadowColor: "rgba(0, 0, 0, 0.08)",
        shadowOffset: { width: 0, height: -1.5 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
      },
      showIcon: true,
    },
    initialRouteName: "Home",
  }
);

const CandidateDrawer = DrawerNavigator(
  {
    Main: {
      screen: TabStack,
      path: "event",
    },
    // ProfileInfos: {
    //   screen: CandidateProfile,
    //   navigationOptions: ({ navigation }) => ({
    //     title: 'Mon profil',
    //     headerLeft: <Drawer.Icon
    //       onPress={() => navigation.navigate('DrawerOpen')}
    //     />,
    //   }),
    // },
    SearchCriteria: {
      screen: Profile,
      navigationOptions: ({ navigation }) => ({
        title: "Poste recherché",
        headerLeft: (
          <Drawer.Icon onPress={() => navigation.navigate("DrawerOpen")} />
        ),
      }),
    },
    QRCodeCandidate: {
      screen: QRCodeCandidate,
    },
    LegalMentions: {
      screen: LegalMentions,
    },
    CandidateSettings: {
      screen: Settings,
    },
    CandidateAccountInfos: {
      screen: AccountInfos,
      navigationOptions: ({ navigation }) => ({
        title: "Identifiants et coordonnées",
        headerLeft: (
          <Drawer.Icon onPress={() => navigation.navigate("DrawerOpen")} />
        ),
      }),
    },
    FAQCandidate: {
      screen: FAQCandidate,
    },
    ImportantFieldModification: {
      screen: ImportantFieldModification,
      path: "important-modification/:field",
    },
  },
  {
    initialRouteName: "Main",
    contentComponent: Drawer,
    headerMode: "none",
  }
);

export default CandidateDrawer;

import { TabNavigator, TabBarTop, StackNavigator } from 'react-navigation';

import CandidateEvent from '../../../screens/events/candidate/Event';
import EventAbout from '../../../screens/events/EventAbout';
import EventConcept from '../../../screens/events/EventConcept';
import EventFaq from '../../../screens/events/EventFaq';

import CandidateEventsList from '../../../screens/events/candidate/EventsList';
import { theme } from '../../constants/index';

// const EventsListStack = TabNavigator(
//   {
//     EventsFutur: {
//       screen: CandidateEventsList,
//       navigationOptions: {
//         tabBarLabel: 'À venir',
//       },
//     },
//     EventsPast: {
//       screen: CandidateEventsList,
//       navigationOptions: {
//         tabBarLabel: 'Passés',
//       },
//     },
//   },
//   {
//     headerMode: 'none',
//     tabBarComponent: TabBarTop,
//     tabBarPosition: 'top',
//     animationEnabled: false,
//     swipeEnabled: false,
//     tabBarOptions: {
//       indicatorStyle: {
//         backgroundColor: theme.primaryColor,
//         height: 4,
//       },
//       showIcon: false,
//       activeTintColor: theme.fontBlack,
//       inactiveTintColor: theme.gray,
//       upperCaseLabel: false,
//       labelStyle: {
//         fontSize: 14,
//         marginTop: 0,
//       },
//       iconStyle: {
//         marginBottom: 0,
//         color: theme.gray,
//       },
//       tabStyle: {
//         justifyContent: 'flex-end',
//         paddingTop: 15.5,
//       },
//       style: {
//         justifyContent: 'center',
//         borderTopColor: 'transparent',
//         borderTopWidth: 0,
//         backgroundColor: 'white',
//         height: 49,
//         shadowColor: 'rgba(0, 0, 0, 0.08)',
//         shadowOffset: { width: 0, height: 1.5 },
//         shadowOpacity: 0.8,
//         shadowRadius: 5,
//       },
//     },
//     lazy: true,
//     initialRouteName: 'EventsFutur',
//   },
// );

const createEventTabs = route =>
  TabNavigator(
    {
      About: {
        screen: EventAbout,
        navigationOptions: {
          tabBarLabel: 'À propos',
        },
      },
      Concept: {
        screen: EventConcept,
        navigationOptions: {
          tabBarLabel: 'Concept',
        },
      },
      Faq: {
        screen: EventFaq,
        navigationOptions: {
          tabBarLabel: 'FAQ',
        },
      },
    },
    {
      headerMode: 'none',
      tabBarComponent: TabBarTop,
      tabBarPosition: 'top',
      animationEnabled: false,
      swipeEnabled: false,
      tabBarOptions: {
        indicatorStyle: {
          backgroundColor: theme.primaryColor,
          height: 4,
        },
        showIcon: false,
        activeTintColor: theme.fontBlack,
        inactiveTintColor: theme.gray,
        upperCaseLabel: false,
        labelStyle: {
          fontSize: 14,
          marginTop: 0,
        },
        iconStyle: {
          marginBottom: 0,
          color: theme.gray,
        },
        tabStyle: {
          justifyContent: 'flex-end',
          paddingTop: 15.5,
        },
        style: {
          justifyContent: 'center',
          borderTopColor: 'transparent',
          borderTopWidth: 0,
          backgroundColor: 'white',
          height: 49,
          shadowColor: 'rgba(0, 0, 0, 0.08)',
          shadowOffset: { width: 0, height: 1.5 },
          shadowOpacity: 0.8,
          shadowRadius: 5,
        },
      },
      lazy: true,
      initialRouteName: route || 'About',
    },
  );

const EventsStack = StackNavigator(
  {
    CandidateEventsList: {
      screen: CandidateEventsList,
      navigationOptions: {
        title: 'Liste des événements',
        headerTruncatedBackTitle: 'Events',
        headerLeft: null,
      },
    },
    CandidateEvent: {
      screen: CandidateEvent,
    },
  },
  {
    lazy: true,
    headerMode: 'none',
  },
);

export { createEventTabs };
export default EventsStack;

// import React from 'react';
// import { StackNavigator, DrawerNavigator, TabNavigator, TabBarTop } from 'react-navigation';
// import Profile from '../../../screens/profile/Profile';
// import Account from '../../../screens/profile/Account';
// import LegalMentions from '../../../screens/profile/LegalMentions';
// import Settings from '../../../screens/profile/Settings';
// import AccountInfos from '../../../screens/profile/AccountInfos';
// import ImportantFieldModification from '../../../screens/profile/ImportantFieldModification';
// import QRCodeCandidate from '../../../screens/profile/QRCodeCandidate';
// import { theme } from '../../constants/index';
// import FAQCandidate from '../../../screens/profile/FAQCandidate';
// import Drawer from '../../../components/Content/Drawer';
//
// const ProfileStack = DrawerNavigator({
//   // AccountStack: {
//   //   screen: AccountStack,
//   // },
//   // QRCodeCandidate: {
//   //   screen: QRCodeCandidate,
//   // },
//   // LegalMentions: {
//   //   screen: LegalMentions,
//   // },
//   // CandidateSettings: {
//   //   screen: Settings,
//   // },
//   CandidateAccountInfos: {
//     screen: AccountInfos,
//     navigationOptions: ({ navigation }) => ({
//       title: 'Identifiants et coordonnées',
//       headerLeft: <Drawer.Icon
//         onPress={() => navigation.navigate('DrawerOpen')}
//       />,
//     }),
//   },
//   // FAQCandidate: {
//   //   screen: FAQCandidate,
//   // },
//   // ImportantFieldModification: {
//   //   screen: ImportantFieldModification,
//   //   path: 'important-modification/:field',
//   // },
// }, {
//   headerMode: 'float',
// });
//
// export default ProfileStack;

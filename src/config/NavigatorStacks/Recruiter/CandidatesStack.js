// import React from 'react';
// import { StyleSheet, Dimensions } from 'react-native';
// import { TabNavigator, TabBarTop } from 'react-navigation';
// import TabRouter from 'react-navigation/src/routers/TabRouter';
// import { Icon } from 'react-native-elements';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import CandidatesTreat from '../../../screens/candidates/Treat';
// import CandidatesAnnotated from '../../../screens/candidates/Annotated';
// import CandidatesPresent from '../../../screens/candidates/Present';
// import CandidatesMissing from '../../../screens/candidates/Missing';
// import CandidatesSeen from '../../../screens/candidates/Seen';
// import CandidatesContacted from '../../../screens/candidates/Contacted';
// import CandidatesSelected from '../../../screens/candidates/Selected';

// import { theme } from '../../constants/index';
// import Wrapper, { FiltersContext } from '../../../screens/candidates/Wrapper';
// import Filters from '../../../screens/candidates/Filters';

// const styles = StyleSheet.create({
//   icon: {
//     marginRight: 16,
//     padding: 8,
//     alignItems: 'center',
//     backgroundColor: 'transparent',
//   },
// });

// // Not your typical wrapper
// const Contextualizer = Screen => props => (
//   <FiltersContext.Consumer>{filters => <Screen {...props} {...filters} />}</FiltersContext.Consumer>
// );
// const C = Contextualizer;

// const stackOptions = initialRouteName => ({
//   tabBarComponent: TabBarTop,
//   tabBarPosition: 'top',
//   animationEnabled: false,
//   swipeEnabled: false,
//   navigationOptions: ({
//     navigation: {
//       navigate,
//       state: { params },
//     },
//   }) => ({
//     headerRight: (
//       <Icon
//         size={28}
//         iconStyle={styles.icon}
//         color="white"
//         type="font-awesome"
//         name="qrcode"
//         onPress={() => navigate('Scan', params)}
//       />
//     ),
//   }),
//   tabBarOptions: {
//     scrollEnabled: true,
//     indicatorStyle: {
//       backgroundColor: theme.primaryColor,
//       height: 4,
//     },
//     allowFontScaling: false,
//     showIcon: false,
//     activeTintColor: theme.fontBlack,
//     inactiveTintColor: theme.gray,
//     upperCaseLabel: false,
//     labelStyle: {
//       // width: '100%',
//       // minWidth: 80,
//       // maxWidth: 120,
//       // width: 'auto',
//       fontSize: 12,
//       // transform: [{ rotate: '-30deg' }],
//       margin: 0,
//       padding: 0,
//     },
//     iconStyle: {
//       marginBottom: 0,
//       color: theme.gray,
//     },
//     tabStyle: {
//       // minWidth: 80,
//       // maxWidth: 120,
//       // width: 'auto',
//       justifyContent: 'center',
//       alignItems: 'center',
//       paddingTop: 15.5,
//     },
//     style: {
//       justifyContent: 'center',
//       // alignItems: 'center',
//       borderTopColor: 'transparent',
//       borderTopWidth: 0,
//       backgroundColor: 'white',
//       height: 49,
//       shadowColor: 'rgba(0, 0, 0, 0.08)',
//       shadowOffset: { width: 0, height: 1.5 },
//       shadowOpacity: 0.8,
//       shadowRadius: 5,
//     },
//   },
//   lazy: true,
//   initialRouteName,
// });

// const Annotated = {
//   screen: C(CandidatesAnnotated),
//   navigationOptions: {
//     title: 'Candidats annotés',
//     tabBarLabel: 'Annotés',
//   },
// };

// const Present = {
//   screen: C(CandidatesPresent),
//   navigationOptions: {
//     title: 'Candidats présents',
//     tabBarLabel: 'Présents',
//   },
// };

// const Missing = {
//   screen: C(CandidatesMissing),
//   navigationOptions: {
//     title: 'Candidats absents',
//     tabBarLabel: 'Absents',
//   },
// };

// const Contacted = {
//   screen: C(CandidatesContacted),
//   navigationOptions: {
//     title: 'Candidats contactés',
//     tabBarLabel: 'Contactés',
//   },
// };

// const Seen = {
//   screen: C(CandidatesSeen),
//   navigationOptions: {
//     title: 'Candidats vus',
//     tabBarLabel: 'Vus',
//   },
// };

// const Selected = {
//   screen: C(CandidatesSelected),
//   navigationOptions: {
//     title: 'Candidats sélectionnés',
//     tabBarLabel: 'Sélectionnés',
//   },
// };

// const Treat = {
//   screen: C(CandidatesTreat),
//   navigationOptions: {
//     title: 'Candidats préselectionnés',
//     tabBarLabel: 'Tous',
//   },
// };

// const PastStack = {
//   Present,
//   Seen,
//   Selected,
//   Annotated,
//   Contacted,
//   Treat,
// };

// const FuturStack = {
//   Treat,
//   Contacted,
// };

// const Router = TabRouter({
//   ...PastStack,
//   ...FuturStack,
// });

// /**
//  * A destination de toutes personnes chargées de maintenir ce projet.
//  *
//  *    A ce jour, le 21/01/2019, react-navigation ne supporte pas les TabNavigator conditionnels.
//  *    Du à une gestion de la navigation propre au context de react-navigation basé sur une static
//  *    (admirez le magnigique `static router = Router` déclaré quelque ligne plus bas).
//  *    Un grand pouvoir impliquant de grandes responsabilités, le JS ne nous permet pas encore
//  *    de modifier le context d'une function au runtime, au grand désespoir des développeurs les plus
//  *    aventureux de notre ère.
//  *
//  *    Mais comment faire un TabNavigator conditionnels ?
//  *    La subtilité réside dans la réecriture du contexte de navigation passé en paramètre
//  *    du component enscapsulant la stack de navigation.
//  *    Malheureusement, l'index de navigation reste propagé par le context static du router.
//  *
//  *    Merci de pensez à changer les valeurs en dure dans les ternaires en cas de modifications.
//  *
//  *    TL;DR
//  *    Sensitive code, please be careful
//  *    https://i.imgflip.com/2roxgy.jpg
//  */

// class ContextTabNavigator extends React.Component {
//   static router = TabNavigator(
//     {
//       ...PastStack,
//       ...FuturStack,
//     },
//     stackOptions(''),
//   ).router;

//   props: {
//     data: {
//       event: {
//         passed: Boolean,
//       },
//     },
//     navigation: Object,
//   };

//   static orderFutureTab = ['Treat', 'Contacted'];

//   static orderPastTab = ['Present', 'Seen', 'Selected', 'Annotated', 'Contacted', 'Treat'];

//   conditionalTabNavigationCreator = () => {
//     const { navigation, data } = this.props;
//     const { passed } = data.event;
//     const stack = passed ? PastStack : FuturStack;
//     const o = passed ? ContextTabNavigator.orderPastTab : ContextTabNavigator.orderFutureTab;
//     const sort = (a, b) => (o.indexOf(a.routeName) > o.indexOf(b.routeName) ? 1 : -1);
//     const enhancedRoutes = navigation.state.routes
//       .filter(e => Object.keys(stack).indexOf(e.routeName) !== -1)
//       .sort(sort);
//     // I'm a disgrace
//     const awfulBooleanContacted =
//       navigation.state.index === 4 &&
//       navigation.state.routes[4].routeName === 'Contacted' &&
//       passed === false;
//     // Feel bad man
//     const awfulBooleanTreat =
//       navigation.state.index === 5 &&
//       navigation.state.routes[5].routeName === 'Treat' &&
//       passed === false;
//     return {
//       ...navigation,
//       state: {
//         ...navigation.state,
//         params: { ...navigation.state.params, passed },
//         routes: enhancedRoutes,
//         index: awfulBooleanTreat // eslint-disable-line
//           ? 0
//           : awfulBooleanContacted
//             ? 1
//             : navigation.state.index,
//       },
//     };
//   };

//   render() {
//     if (!this.props.data || !this.props.data.event) return null;
//     const { data } = this.props;
//     const { passed } = data.event;

//     const Stack = TabNavigator(
//       { ...PastStack, ...FuturStack },
//       stackOptions(passed ? 'Present' : 'Treat'),
//     );
//     return (
//       <Wrapper>
//         <Stack navigation={this.conditionalTabNavigationCreator()} />
//         <Filters />
//       </Wrapper>
//     );
//   }
// }

// const EVENT_QUERY = gql`
//   query event($event_id: String!) {
//     event: event_by_id(event_id: $event_id) {
//       passed
//     }
//   }
// `;

// const Candidates = graphql(EVENT_QUERY, {
//   skip: ownProps =>
//     !ownProps.navigation ||
//     !ownProps.navigation.state ||
//     !ownProps.navigation.state.params ||
//     !ownProps.navigation.state.params.event_id,
//   options: ownProps => ({
//     variables: {
//       event_id: ownProps.navigation.state.params.event_id,
//     },
//   }),
// })(ContextTabNavigator);

// export default Candidates;

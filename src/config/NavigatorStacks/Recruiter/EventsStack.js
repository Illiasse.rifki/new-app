// import { TabNavigator, TabBarTop, StackNavigator } from "react-navigation";

// // import RecruiterEvent from '../../../screens/events/recruiter/Event';
// import EventAbout from "../../../screens/events/EventAbout";
// import EventConcept from "../../../screens/events/EventConcept";
// import EventFaq from "../../../screens/events/EventFaq";
// import EventsList from "../../../screens/events/recruiter/EventsList";
// import EventRegistrationRecruiter from "../../../screens/events/recruiter/EventRegistrationRecruiter";
// import CandidatesStack from "./CandidatesStack";

// import { theme } from "../../constants/index";

// const createEventTabs = (route) =>
//   TabNavigator(
//     {
//       About: {
//         screen: EventAbout,
//         navigationOptions: {
//           tabBarLabel: "À propos",
//         },
//       },
//       Concept: {
//         screen: EventConcept,
//         navigationOptions: {
//           tabBarLabel: "Concept",
//         },
//       },
//       Faq: {
//         screen: EventFaq,
//         navigationOptions: {
//           tabBarLabel: "FAQ",
//         },
//       },
//     },
//     {
//       headerMode: "none",
//       tabBarComponent: TabBarTop,
//       tabBarPosition: "top",
//       animationEnabled: false,
//       swipeEnabled: false,
//       tabBarOptions: {
//         indicatorStyle: {
//           backgroundColor: theme.primaryColor,
//           height: 4,
//         },
//         showIcon: false,
//         activeTintColor: theme.fontBlack,
//         inactiveTintColor: theme.gray,
//         upperCaseLabel: false,
//         labelStyle: {
//           fontSize: 12,
//           marginTop: 0,
//           marginLeft: 0,
//           marginRight: 0,
//         },
//         iconStyle: {
//           marginBottom: 0,
//           color: theme.gray,
//         },
//         tabStyle: {
//           justifyContent: "flex-end",
//           paddingTop: 15.5,
//         },
//         style: {
//           justifyContent: "center",
//           borderTopColor: "transparent",
//           borderTopWidth: 0,
//           backgroundColor: "white",
//           paddingLeft: 0,
//           paddingRight: 0,
//           height: 49,
//           shadowColor: "rgba(0, 0, 0, 0.08)",
//           shadowOffset: { width: 0, height: 1.5 },
//           shadowOpacity: 0.8,
//           shadowRadius: 5,
//         },
//       },
//       lazy: true,
//       initialRouteName: route || "About",
//     }
//   );

// const EventsStack = StackNavigator(
//   {
//     RecruiterEventsList: {
//       screen: EventsList,
//       navigationOptions: () => ({
//         title: "Liste des événements",
//         headerTruncatedBackTitle: "Events",
//       }),
//     },
//     RecruiterEvent: {
//       screen: RecruiterEvent,
//     },
//     EventRegistrationRecruiter: {
//       screen: EventRegistrationRecruiter,
//     },
//     CandidatesStack: {
//       screen: CandidatesStack,
//     },
//   },
//   {
//     lazy: true,
//     headerMode: "none",
//     // initialRouteName: "RecruiterEventsList",
//   }
// );

// export { createEventTabs };
// export default EventsStack;

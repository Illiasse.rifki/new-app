import { StackNavigator } from 'react-navigation';
import RecruiterJobOffersList from '../../../screens/job-offer/RecruiterJobOffersList';
import RecruiterJobOffer from '../../../screens/job-offer/RecruiterJobOffer';
import RecruiterJobOfferRegistrationStack from './JobOfferRegistrationStack';
import JobOfferVisits from '../../../screens/job-offer/JobOfferVisits';
import RecruiterJobOfferVisitProfile from '../../../screens/job-offer/RecruiterJobOfferVisitProfile';


const JobOfferStack = StackNavigator({
  RecruiterJobOffersList: {
    screen: RecruiterJobOffersList,
  },
  RecruiterJobOffer: {
    screen: RecruiterJobOffer,
  },
  JobOfferVisits: {
    screen: JobOfferVisits,
  },
  RecruiterJobOfferVisitProfile: {
    screen: RecruiterJobOfferVisitProfile,
  },
  RecruiterJobOfferRegistrationStack: {
    screen: RecruiterJobOfferRegistrationStack,
  },
}, {
  headerMode: 'none',
  initialRouteName: 'RecruiterJobOffersList',
});

export default JobOfferStack;

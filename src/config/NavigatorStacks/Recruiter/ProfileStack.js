import { StackNavigator, TabNavigator, TabBarTop } from "react-navigation";
import ProfileRecruiter from "../../../screens/profile/ProfileRecruiter";
// import AccountRecruiter from "../../../screens/profile/AccountRecruiter";
// import LegalMentions from "../../../screens/profile/LegalMentions";
// import Settings from "../../../screens/profile/SettingsRecruiter";
// import AccountInfosRecruiter from "../../../screens/profile/AccountInfosRecruiter";
// import ImportantFieldModificationRecruiter from "../../../screens/profile/ImportantFieldModificationRecruiter";
// import FAQRecruiter from "../../../screens/profile/FAQRecruiter";
import { theme } from "../../constants/index";

const AccountStack = TabNavigator(
  {
    ProfileInfos: {
      screen: ProfileRecruiter,
    },
    // Account: {
    //   screen: AccountRecruiter,
    // },
  },
  {
    lazy: true,
    tabBarComponent: TabBarTop,
    tabBarPosition: "top",
    animationEnabled: false,
    swipeEnabled: false,
    tabBarOptions: {
      indicatorStyle: {
        backgroundColor: theme.primaryColor,
        height: 4,
      },
      showIcon: false,
      activeTintColor: theme.fontBlack,
      inactiveTintColor: theme.gray,
      upperCaseLabel: false,
      labelStyle: {
        fontSize: 14,
        marginTop: 0,
      },
      iconStyle: {
        marginBottom: 0,
        color: theme.gray,
      },
      tabStyle: {
        justifyContent: "flex-end",
        paddingTop: 15.5,
      },
      style: {
        justifyContent: "center",
        borderTopColor: "transparent",
        borderTopWidth: 0,
        backgroundColor: "white",
        height: 49,
        shadowColor: "rgba(0, 0, 0, 0.08)",
        shadowOffset: { width: 0, height: 1.5 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
      },
    },
    // initialRouteName: "ProfileInfos",
  }
);

const ProfileStack = StackNavigator(
  {
    AccountStack: {
      screen: AccountStack,
    },
    // LegalMentions: {
    //   screen: LegalMentions,
    // },
    // SettingsRecruiter: {
    //   screen: Settings,
    // },
    // AccountInfosRecruiter: {
    //   screen: AccountInfosRecruiter,
    // },
    // FAQRecruiter: {
    //   screen: FAQRecruiter,
    // },
    // ImportantFieldModificationRecruiter: {
    //   screen: ImportantFieldModificationRecruiter,
    //   path: "important-modification-recruiter/:field",
    // },
  },
  {
    lazy: true,
    headerMode: "none",
    initialRouteName: "AccountStack",
  }
);

export default ProfileStack;

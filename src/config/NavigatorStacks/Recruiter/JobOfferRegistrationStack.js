import { TabNavigator, TabBarTop } from 'react-navigation';

import {
  RecruiterJobOfferRegistrationListTreat,
  RecruiterJobOfferRegistrationListPreselected,
  RecruiterJobOfferRegistrationList,
} from '../../../screens/job-offer/RecruiterJobOfferRegistrationListGeneric';

import { theme } from '../../../config/constants/index';

const JobOfferRegistrationStack = TabNavigator(
  {
    RecruiterJobOfferRegistrationListTreat: {
      screen: RecruiterJobOfferRegistrationListTreat,
      navigationOptions: {
        tabBarLabel: 'À traiter',
      },
    },
    RecruiterJobOfferRegistrationListPreselected: {
      screen: RecruiterJobOfferRegistrationListPreselected,
      navigationOptions: {
        tabBarLabel: 'Préselectionnées',
      },
    },
    RecruiterJobOfferRegistrationList: {
      screen: RecruiterJobOfferRegistrationList,
      navigationOptions: {
        tabBarLabel: 'Toutes',
      },
    },
  },
  {
    navigationOptions: {
      title: 'Liste des candidatures',
    },
    lazy: true,
    tabBarComponent: TabBarTop,
    tabBarPosition: 'top',
    animationEnabled: false,
    swipeEnabled: false,
    tabBarOptions: {
      indicatorStyle: {
        backgroundColor: theme.primaryColor,
        height: 4,
      },
      showIcon: false,
      activeTintColor: theme.fontBlack,
      inactiveTintColor: theme.gray,
      upperCaseLabel: false,
      labelStyle: {
        fontSize: 12,
        marginTop: 0,
        marginLeft: 0,
        marginRight: 0,
      },
      iconStyle: {
        marginBottom: 0,
        color: theme.gray,
      },
      tabStyle: {
        justifyContent: 'flex-end',
        paddingTop: 15.5,
      },
      style: {
        justifyContent: 'center',
        borderTopColor: 'transparent',
        borderTopWidth: 0,
        backgroundColor: 'white',
        paddingLeft: 0,
        paddingRight: 0,
        height: 49,
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 0, height: 1.5 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
      },
    },
    headerMode: 'none',
    initialRouteName: 'RecruiterJobOfferRegistrationListTreat',
  },
);

export default JobOfferRegistrationStack;

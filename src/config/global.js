import AuthStockage from './../utils/AuthStockage';

const authStockage = new AuthStockage();
global.authStockage = authStockage;
module.exports = {
  authStockage,
};

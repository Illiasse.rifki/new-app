// @flow
import React from "react";
import { TouchableOpacity, Platform, Text } from "react-native";
import {
  StackNavigator,
  TabNavigator,
  HeaderBackButton,
  // DrawerNavigator,
  // withNavigation,
} from "react-navigation";
import CardStackStyleInterpolator from "react-navigation/src/views/CardStack/CardStackStyleInterpolator";
import { Icon } from "../components/Pure/react-native-elements";
import { theme, themeElements } from "./constants/index";

// import CandidateProfileStack from "./NavigatorStacks/Candidate/ProfileStack";
import RecruiterProfileStack from "./NavigatorStacks/Recruiter/ProfileStack";
// import RecruiterEventsStack from "./NavigatorStacks/Recruiter/EventsStack";

import OnBoarding from "../screens/OnBoarding/index";
// import Login from "../screens/Login";
// import RetrievePassword from "../screens/RetrievePassword";
import HomeRecruiter from "../screens/HomeRecruiter";

// import EventRegistrationCompleted from "../screens/EventRegistrationCompleted";
// import SignUpType from "../screens/SignUpType";

// import RecruiterAccountRequest from "../screens/RecruiterAccountRequest";
// import CandidateForm from "../screens/candidate-form/CandidateForm";
// import CandidateFormTags from "../screens/candidate-form/CandidateFormTags";
// import CandidateFormQualities from "../screens/candidate-form/CandidateFormQualities";
// import CandidateFormExperience from "../screens/candidate-form/CandidateFormExperience";
// import CandidateFormActivitySector from "../screens/candidate-form/CandidateFormActivitySector";
// import CandidateFormLookingFor from "../screens/candidate-form/CandidateFormLookingFor";
// import CandidateFormActivitySectorWanted from "../screens/candidate-form/CandidateFormActivitySectorWanted";
// import CandidateFormSalary from "../screens/candidate-form/CandidateFormSalary";
// import CandidateFormCV from "../screens/candidate-form/CandidateFormCV";
// import CandidateFormPreview from "../screens/candidate-form/CandidateFormPreview";

// import Validation from "../screens/Validation";
// import ValidationChoice from "../screens/ValidationChoice";

// import Chats from "../screens/chat/Chats";
// import Chat from "../screens/chat/Chat";

// import CandidateProfile from "../screens/profile/CandidateProfile";
// import CandidateProfileExperiences from "../screens/profile/CandidateProfileExperiences";
// import CandidateProfileAddExperience from "../screens/profile/CandidateProfileAddExperience";
// import CandidateProfileEditExperience from "../screens/profile/CandidateProfileEditExperience";
// import CandidateProfileLanguages from "../screens/profile/CandidateProfileLanguages";
// import CandidateProfileAddLanguage from "../screens/profile/CandidateProfileAddLanguage";
// import CandidateProfileEditLanguage from "../screens/profile/CandidateProfileEditLanguage";
// import CandidateProfileEducation from "../screens/profile/CandidateProfileEducation";
// import CandidateProfileAddEducation from "../screens/profile/CandidateProfileAddEducation";
// import CandidateProfileEditEducation from "../screens/profile/CandidateProfileEditEducation";

// import AccountType from "../screens/AccountType";

// import CompanyTags from "../screens/profile/CompanyTags";
// import CompanyQualities from "../screens/profile/CompanyQualities";
// import CompanyDescription from "../screens/profile/CompanyDescription";
// import CompanyVisitors from "../screens/profile/CompanyVisitors";

// import Comment from "../screens/candidates/Comment";
// import Scan from "../screens/candidates/Scan";
// import EventCandidate from "../screens/candidates/Candidate";

// import CompanyProfile from "../screens/profile/CompanyProfile";

// import RecruiterJobOfferStack from "./NavigatorStacks/Recruiter/JobOfferStack";
// import PostulateJobOffer from "../screens/job-offer/PostulateJobOffer";
// import CandidateDrawer from "./NavigatorStacks/Candidate/MainStack";
// import RecruiterJobOfferRegistration from "../screens/job-offer/RecruiterJobOfferRegistration";

// import JobOfferListFilter from "../screens/job-offer/JobOfferListFilter";
// import JobOffer from "../screens/job-offer/JobOffer";
// import UpdateProfileForJobOffer from "../screens/job-offer/UpdateProfileForJobOffer";

// import AccountInfos from "../screens/profile/AccountInfos";
// import CandidateFormLastEductation from "../screens/candidate-form/CandidateFormLastEducation";
// import CandidateFormLastExperience from "../screens/candidate-form/CandidateFormLastExperience";

const noTransition = (index, position) => {
  const translateX = 0;
  const translateY = 0;

  const opacity = position.interpolate({
    inputRange: [index - 0.9, index, index + 0.9],
    outputRange: [1, 1, 1],
  });
  return {
    opacity,
    transform: [{ translateX }, { translateY }],
  };
};

const tabBarIcon = ({
  tintColor,
  name,
  type,
}: {
  tintColor: string,
  name: string,
  type: string,
}) => (
  <Icon size={20} name={name} type={type} color={tintColor || theme.gray} />
);

class HeaderLeftBase extends React.Component {
  props: {
    navigation: Object,
  };
  state = {
    transitionRunning: false,
  };

  constructor(props) {
    super(props);
    const { navigation } = props;
    this.handleTransitionEnd = navigation.addListener("didFocus", (payload) => {
      /*
       * So.
       * Basically, this setState will trigger a rerender and kill the weird chat bug
       */
      this.setState({ transitionRunning: false });
    });
  }

  componentWillUnmount() {
    if (this.handleTransitionEnd) {
      this.handleTransitionEnd.remove();
    }
  }

  onGoBack = (e, nb = null) => {
    e.stopPropagation();
    const { navigation } = this.props;
    if (!this.state.transitionRunning) {
      if (nb) {
        navigation.pop(nb);
      }
      navigation.goBack(nb);
    }
  };

  render() {
    return null;
  }
}

class headerLeft extends HeaderLeftBase {
  render() {
    const { navigation } = this.props;
    if (navigation && navigation.state && navigation.state.index === 0)
      return null;
    return (
      <TouchableOpacity
        hitSlop={{
          top: 20,
          bottom: 20,
          left: 50,
          right: 50,
        }}
        onPress={this.onGoBack}
      >
        <HeaderBackButton
          tintColor={theme.secondaryColor}
          onPress={this.onGoBack}
        />
      </TouchableOpacity>
    );
  }
}
export class headerLeftNormal extends HeaderLeftBase {
  render() {
    const { nbBack } = this.props;
    return (
      <TouchableOpacity
        hitSlop={{
          top: 20,
          bottom: 20,
          left: 50,
          right: 50,
        }}
        onPress={(e) => this.onGoBack(e, nbBack)}
      >
        <HeaderBackButton
          tintColor={theme.secondaryColor}
          onPress={(e) => this.onGoBack(e, nbBack)}
        />
      </TouchableOpacity>
    );
  }
}

const tabViewsOption = {
  activeTintColor: theme.primaryColor,
  inactiveTintColor: theme.gray,
  labelStyle: {
    fontSize: 12,
    marginTop: Platform.OS === "android" ? 3 : 0,
  },
  iconStyle: {
    marginBottom: 0,
    //    color: theme.gray,
  },
  style: {
    borderTopColor: "transparent",
    backgroundColor: "white",
    paddingBottom: 8,
    height: 58,
    shadowColor: "rgba(0, 0, 0, 0.08)",
    shadowOffset: { width: 0, height: -1.5 },
    shadowOpacity: 0.8,
    shadowRadius: 5,
  },
  showIcon: true,
};

export { headerLeft, tabViewsOption, tabBarIcon };

const RecruiterTabStack = TabNavigator(
  {
    HomeRecruiter: {
      screen: HomeRecruiter,
    },
    // RecruiterEventsStack: {
    //   screen: RecruiterEventsStack,
    //   path: "event/recruiter",
    //   navigationOptions: ({ navigation }) => ({
    //     headerLeft,
    //     tabBarLabel: "Events",
    //     tabBarIcon: ({ tintColor }) =>
    //       tabBarIcon({ tintColor, name: "event", type: "material-icons" }),
    //   }),
    // },
    // RecruiterJobOffers: {
    //   screen: RecruiterJobOfferStack,
    //   navigationOptions: ({ navigation }) => ({
    //     headerLeft,
    //     tabBarLabel: 'Offres',
    //     tabBarIcon: ({ tintColor }) => (
    //       <Icon size={20} name="business-center" type="material-icons" color={tintColor || theme.gray} />
    //     ),
    //   }),
    // },
    Profile: {
      screen: RecruiterProfileStack,
      navigationOptions: ({ navigation }) => ({
        headerLeft,
        tabBarLabel: "Profil",
        tabBarIcon: ({ tintColor }) =>
          tabBarIcon({ tintColor, name: "user-o", type: "font-awesome" }),
      }),
    },
  },
  {
    headerMode: "float",
    tabBarPosition: "bottom",
    animationEnabled: true,
    tabBarOptions: tabViewsOption,
    initialRouteName: "HomeRecruiter",
  }
);

const appScenes = {
  OnBoarding: {
    screen: OnBoarding,
  },
  // Login: {
  //   screen: Login,
  // },
  // RetrievePassword: {
  //   screen: RetrievePassword,
  // },
  // SignUpType: {
  //   screen: SignUpType,
  // },
  // RecruiterAccountRequest: {
  //   screen: RecruiterAccountRequest,
  // },
  // CandidateForm: {
  //   screen: CandidateForm,
  // },
  // CandidateFormTags: {
  //   screen: CandidateFormTags,
  // },
  // CandidateFormQualities: {
  //   screen: CandidateFormQualities,
  // },
  // CandidateFormExperience: {
  //   screen: CandidateFormExperience,
  // },
  // CandidateFormActivitySector: {
  //   screen: CandidateFormActivitySector,
  // },
  // CandidateFormLookingFor: {
  //   screen: CandidateFormLookingFor,
  // },
  // CandidateFormActivitySectorWanted: {
  //   screen: CandidateFormActivitySectorWanted,
  // },
  // CandidateFormSalary: {
  //   screen: CandidateFormSalary,
  // },
  // CandidateFormCV: {
  //   screen: CandidateFormCV,
  // },
  // CandidateFormPreview: {
  //   screen: CandidateFormPreview,
  // },
  // CandidateProfileExperiences: {
  //   screen: CandidateProfileExperiences,
  // },
  // CandidateFormLastEducation: {
  //   screen: CandidateFormLastEductation,
  // },
  // CandidateFormLastExperience: {
  //   screen: CandidateFormLastExperience,
  // },
  // CandidateProfileAddExperience: {
  //   screen: CandidateProfileAddExperience,
  // },
  // CandidateProfileEditExperience: {
  //   screen: CandidateProfileEditExperience,
  // },
  // CandidateProfileEducation: {
  //   screen: CandidateProfileEducation,
  // },
  // CandidateProfileAddEducation: {
  //   screen: CandidateProfileAddEducation,
  // },
  // CandidateProfileEditEducation: {
  //   screen: CandidateProfileEditEducation,
  // },
  // CandidateProfileLanguages: {
  //   screen: CandidateProfileLanguages,
  // },
  // CandidateProfileAddLanguage: {
  //   screen: CandidateProfileAddLanguage,
  // },
  // CandidateProfileEditLanguage: {
  //   screen: CandidateProfileEditLanguage,
  // },
  // CandidateProfile: {
  //   screen: CandidateProfile,
  // },
  // TabViews: {
  //   screen: CandidateDrawer,
  // },
  // RecruiterTabViews: {
  //   screen: RecruiterTabStack,
  // },
  // Validation: {
  //   screen: Validation,
  // },
  // ValidationChoice: {
  //   screen: ValidationChoice,
  // },
  // Chat: {
  //   screen: Chat,
  // },
  // Chats: {
  //   screen: Chats,
  //   path: 'chats',
  // },
  // EventRegistrationCompleted: {
  //   screen: EventRegistrationCompleted,
  // },
  // AccountType: {
  //   screen: AccountType,
  // },
  // CompanyTags: {
  //   screen: CompanyTags,
  // },
  // CompanyQualities: {
  //   screen: CompanyQualities,
  // },
  // CompanyDescription: {
  //   screen: CompanyDescription,
  // },
  // Comment: {
  //   screen: Comment,
  // },
  // Scan: {
  //   screen: Scan,
  // },
  // EventCandidate: {
  //   screen: EventCandidate,
  // },
  // CompanyProfile: {
  //   screen: CompanyProfile,
  // },
  // PostulateJobOffer: {
  //   screen: PostulateJobOffer,
  // },
  // RecruiterJobOfferRegistration: {
  //   screen: RecruiterJobOfferRegistration,
  // },
  // JobOfferListFilter: {
  //   screen: JobOfferListFilter,
  // },
  // JobOffer: {
  //   screen: JobOffer,
  // },
  // UpdateProfileForJobOffer: {
  //   screen: UpdateProfileForJobOffer,
  // },
  // CandidateEditProfil: {
  //   screen: AccountInfos,
  //   navigationOptions: () => ({
  //     title: 'Identifiants et coordonnées',
  //   }),
  // },
  // CompanyVisitors: {
  //   screen: CompanyVisitors,
  // },
};

const transitionConfiguration = () => ({
  screenInterpolator: (sceneProps) => {
    const { position, scene } = sceneProps;
    const { index, route } = scene;
    const params = route.params || {};
    const transition = params.transition || "default";
    return {
      noTransition: noTransition(index, position),
      default: CardStackStyleInterpolator.forHorizontal(sceneProps),
    }[transition];
  },
});

const switchInitialRouteName = (signedIn: boolean, signedAs: string) => {
  if (signedIn && signedAs === "candidate") return "TabViews";
  if (signedIn && signedAs === "recruiter") return "RecruiterTabViews";
  return "OnBoarding";
};

const createRootNavigator = (signedIn = false, signedAs = "") =>
  StackNavigator(appScenes, {
    navigationOptions: ({ navigation, navigationOptions }) => {
      const {
        headerLeft: HeaderLeft,
        headerStyle,
        headerTitleStyle,
      } = navigationOptions;
      const hStyle = {
        backgroundColor: theme.primaryColor,
        paddingTop: Platform.OS === "ios" ? 20 : 0,
        paddingBottom: Platform.OS === "ios" ? 12 : 0,
      };
      let hl = React.createElement(headerLeftNormal, { navigation });
      //  if (navigation.state.)
      if (React.isValidElement(HeaderLeft)) {
        hl = HeaderLeft;
      } else if (HeaderLeft === null) {
        hl = null;
      } else if (HeaderLeft !== undefined) {
        hl = React.createElement(HeaderLeft, { navigation });
      }
      return {
        ...navigationOptions,
        headerLeft: hl,
        headerStyle: {
          ...hStyle,
          ...headerStyle,
        },
        headerTitleStyle: headerTitleStyle || themeElements.headerTitle,
        headerTintColor: theme.secondaryColor,
      };
    },
    transitionConfig: transitionConfiguration,
    lazy: true,
    initialRouteName: switchInitialRouteName(signedIn, signedAs),
  });

export default createRootNavigator;

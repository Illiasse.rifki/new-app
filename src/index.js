// @flow
import React, { Fragment } from "react";
// import { ApolloProvider } from "react-apollo";
// import { ApolloClient } from "apollo-client";
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  gql,
} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";

// import { setContext } from "apollo-link-context";
// import { InMemoryCache } from "apollo-cache-inmemory";
import { StatusBar, Platform, YellowBox } from "react-native";
import { onError } from "@apollo/client/link/error";
import { createUploadLink } from "apollo-upload-client";
import { Provider } from "react-redux";
import Expo from "expo";
import { Analytics, PageHit, ScreenHit } from "expo-analytics";

import global from "./config/global";
import createRootNavigator from "./config/routes";
import { URI, GA_TRACKING_ID } from "./config/constants/index";
import store from "./config/store";
import { AlertProvider } from "./components/Alert";
import createChatClient, { client as chatClient } from "./utils/Chat";
import SetupPlayerId from "./components/PlayerId/SetupPlayerId";

YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader",
  "Module RCTOneSignalEventEmitter",
  "Module RNDocumentPicker",
]); // todo check sur react navigation si solution

const analytics = new Analytics(GA_TRACKING_ID);

let that;
let client;

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.map(({ message, locations, path }) =>
      console.log(
        `[GraphQL error]: Message: ${message}, Location: ${JSON.stringify(
          locations
        )}, Path: ${path}`
      )
    );
  }
  if (networkError) {
    console.log(`[Network error]: ${networkError}`);
    if (networkError.toString().search("Unauthorized") !== -1) {
      console.log("Unauthorized");

      if (that && that.state.signedIn) {
        global.authStockage.removeToken();
        global.authStockage.load([
          "access-token",
          "recruiter-access-token",
          "signed-as",
        ]);
        that.resetBizzeoApp();
        if (chatClient && chatClient.destroy) {
          chatClient.destroy();
        }
        client.resetStore();
        that.setState({
          signedIn: false,
          isInit: true,
          signedAs: "",
          route: "",
        });
      }
      return "Unauthorized";
    }
  }
  return "";
});

const prefix = Platform.Os === "android" ? "bizzeo://bizzeo/" : "bizzeo://";

const authLink = setContext(async (_, { headers }) => {
  const token = global.authStockage.getToken("access-token");
  const recruiterToken = global.authStockage.getToken("recruiter-access-token");

  return {
    headers: {
      ...headers,
      "access-token": token,
      "recruiter-access-token": recruiterToken,
      "user-agent": await Expo.Constants.getWebViewUserAgentAsync(),
    },
  };
});

const uploadLink = createUploadLink({
  uri: URI,
});

client = new ApolloClient({
  link: errorLink.concat(authLink.concat(uploadLink)),
  cache: new InMemoryCache(),
  defaultOptions: {
    query: {
      fetchPolicy: "network-only",
      errorPolicy: "all",
    },
  },
});

StatusBar.setBarStyle("light-content", true);

export default class App extends React.Component {
  state = {
    signedIn: false,
    isInit: false,
    signedAs: "",
    route: "",
  };

  componentWillMount() {
    //  OneSignal.init(ONE_SIGNAL_APP_ID);
    that = this;
    let res = false;
    global.authStockage
      .load(["access-token", "recruiter-access-token", "signed-as"])
      .then(() => {
        const accessToken = global.authStockage.getToken("access-token");
        const recruiterAccessToken = global.authStockage.getToken(
          "recruiter-access-token"
        );
        const signedAs = global.authStockage.getToken("signed-as");
        if (accessToken || recruiterAccessToken) {
          res = true;
        }
        this.BizzeoApp = createRootNavigator(res, signedAs);
        this.setState({ signedIn: res, isInit: true, signedAs });
        // connect chat if user already logged after restart app
        // client.set();
        // createChatClient({ accessToken, recruiterAccessToken });
      });
  }

  resetBizzeoApp = () => {
    this.BizzeoApp = createRootNavigator();
  };

  getCurrentRouteName = (navigationState) => {
    if (!navigationState) {
      return null;
    }
    const route = navigationState.routes[navigationState.index];
    // dive into nested navigators
    if (route.routes) {
      return this.getCurrentRouteName(route);
    }
    return route.routeName;
  };

  onNavigationStateChange = (prev, next) => {
    const route = this.getCurrentRouteName(next);
    const prevRoute = this.getCurrentRouteName(prev);
    if (route !== prevRoute) {
      if (analytics) {
        const routeString = route.toString();
        analytics.hit(new ScreenHit(routeString));
      }
    }
  };

  render() {
    if (!this.state.isInit) {
      return null;
    }

    const { BizzeoApp } = this;
    const { signedAs } = this.state;
    const navigationPersistenceKey = __DEV__ ? "NavigationStateDEV" : null;
    return (
      <Provider store={store}>
        <ApolloProvider client={client}>
          <AlertProvider>
            <Fragment>
              <SetupPlayerId signedAs={signedAs} />
              <BizzeoApp
                persistenceKey={navigationPersistenceKey}
                screenProps={{ route: this.state.route }}
                onNavigationStateChange={this.onNavigationStateChange}
                uriPrefix={prefix}
              />
            </Fragment>
          </AlertProvider>
        </ApolloProvider>
      </Provider>
    );
  }
}

// import React, { Component } from 'react';
// import { View, Text, StyleSheet } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql, compose } from 'react-apollo';
// import { Button } from 'react-native-elements';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// import { theme, themeElements, companySize } from '../../config/constants/index';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import connectAlert from '../../components/Alert/connectAlert';
// import Drawer from '../../components/Content/Drawer';
// import ProfilePicture from '../../components/GQLLinked/ProfilePicture';
// import Card from '../../components/Card/CardEditable';
// import FormGenerator from '../../components/FormGenerator';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//     paddingBottom: 24,
//     paddingTop: 24,
//   },
//   section: {
//     backgroundColor: 'white',
//     alignItems: 'center',
//     paddingBottom: 24,
//     paddingTop: 32,
//     marginBottom: 16,
//   },
//   bigSection: {
//     backgroundColor: 'white',
//     paddingBottom: 35,
//     paddingTop: 37.5,
//     paddingLeft: 32,
//     paddingRight: 32,
//     marginBottom: 48,
//   },
//   button: {
//     width: 'auto',
//     paddingLeft: 15,
//     paddingRight: 15,
//   },
//   profilePictureView: {
//     marginBottom: 14,
//   },
//   validateView: {
//     marginTop: 30,
//   },
//   fullNameView: {
//     marginBottom: 14,
//   },
//   personalInfosName: {
//     fontSize: 28,
//     lineHeight: 40,
//     fontWeight: 'bold',
//     color: theme.fontBlack,
//   },
//   personalInfosRole: {
//     fontSize: 18,
//     lineHeight: 24,
//     fontWeight: '500',
//     color: theme.fontBlack,
//     textAlign: 'center',
//   },
//   personalInfosCity: {
//     fontSize: 14,
//     lineHeight: 24,
//     color: theme.gray,
//   },
//   italic: {
//     fontStyle: 'italic',
//   },
// });

// const VIEWER_CANDIDATE = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         firstname
//         lastname
//         headline
//         wish_raw_salary
//         wish_variable_salary
//         company_size
//         wish_activity
//         work_city {
//           work_city_id
//           name
//         }
//       }
//     }
//   }
// `;

// class Profile extends Component {
//   static navigationOptions = ({ navigation }) => ({
//     title: 'Votre profil',
//     headerTruncatedBackTitle: 'Profil',
//     headerLeft: <Drawer.Icon
//       onPress={() => navigation.navigate('DrawerOpen')}
//     />,
//   });

//   constructor(props) {
//     super(props);
//     this.state = {};
//   }

//   props: {
//     alertWithType: Function,
//     navigation: {
//       navigate: Function,
//     },
//     editInfos: Function,
//     dispatch: Function,
//     data: {
//       error: string,
//       viewer: {
//         id: string,
//         candidate: {
//           firstname: string,
//           lastname: string,
//           wish_raw_salary: string,
//           wish_variable_salary: string,
//           work_cities: [],
//           company_size: [],
//           wish_activity: [],
//         },
//       },
//       loading: boolean,
//     },
//   };

//   // shouldComponentUpdate(nextProps) {
//   //   const { route } = nextProps.screenProps;
//   //   const { routeName } = this.props.navigation.state;
//   //   return route === routeName;
//   // }

//   componentDidUpdate() {
//     const { route } = this.props.screenProps;
//     const { routeName } = this.props.navigation.state;
//     if (route === routeName) {
//       this.props.data.refetch().catch(e => console.log('catch profile.js refetch', e));
//     }
//   }

//   getCompanySizesFromValue = (values) => companySize.slice().filter(s => (values.indexOf(s.value) !== -1));

//   handleValidateModifications = () => {
//     this.props
//       .editInfos({
//         refetchQueries: [
//           {
//             query: VIEWER_CANDIDATE,
//           },
//         ],
//         variables: {
//           user_id: this.props.data.viewer.id,
//           wish_raw_salary:
//             this.state.wishRawSalary || this.props.data.viewer.candidate.wish_raw_salary,
//           wish_variable_salary:
//             this.state.wishVariableSalary || this.props.data.viewer.candidate.wish_variable_salary,
//           company_size: this.state.companySize ? this.state.companySize.map(s => s.value) : this.props.data.viewer.candidate.company_size,
//         },
//       })
//       .then((r) => {
//         if (r.data.editCandidate.error) {
//           this.props.alertWithType('error', 'Erreur', r.data.editCandidate.error.message);
//           return;
//         }
//         this.props.alertWithType('success', 'Modifications effectuées', 'Les modifications ont bien été prises en compte');
//       })
//       .catch((e) => {
//         this.props.alertWithType('error', 'Erreur', e.message);
//       });
//   };

//   selectCompanySize = (companySize) => {
//     this.setState({ companySize });
//   };

//   getFieldsWithProps = () => {
//     const { viewer } = this.props.data;
//     const { candidate } = viewer;
//     const { navigate } = this.props.navigation;

//     return [
//       {
//         key: 'search-work-cities',
//         title: 'Où recherchez-vous ?',
//         type: 'work-cities',
//         fieldStyle: { marginTop: 16 },
//         props: {
//           userId: viewer.id,
//           searchCities: candidate.work_city,
//           searchRef: (instance) => {
//             this.googlePlacesRef = instance;
//           },
//           onAddCity: () => {
//             this.googlePlacesRef.setAddressText('');
//           },
//         },
//       },
//       {
//         key: 'company-type',
//         type: 'title',
//         fieldStyle: { marginTop: 24 },
//         props: {
//           title: 'Le type de votre entreprise ?',
//         },
//       },
//       {
//         key: 'activity-sector-picker',
//         type: 'activity-sector-picker',
//         props: {
//           activities: candidate.wish_activity,
//           onPress: () => {
//             navigate('CandidateFormActivitySectorWanted', { edit: true });
//           },
//         },
//       },
//       {
//         key: 'company-size-picker',
//         type: 'company-size-picker',
//         fieldStyle: { marginTop: 16 },
//         props: {
//           label: 'Dans quelle taille d\'entreprise souhaitez-vous travailler ?',
//           info: '(en nombre d\'employés, plusieurs choix possibles)',
//           onCompanySizesChange: this.selectCompanySize,
//           selectedSizes: this.state.companySize || this.getCompanySizesFromValue(candidate.company_size),
//         },
//       },
//       {
//         key: 'wish-salaries',
//         type: 'title',
//         fieldStyle: { marginTop: 32 },
//         props: {
//           title: 'Votre salaire attendu ?',
//           subtitle: 'Cette donnée restera confidentielle',
//         },
//       },
//       {
//         key: 'wish-salary-raw',
//         type: 'number',
//         props: {
//           label: 'Salaire brut fixe annuel en €',
//           placeholder: 'Votre salaire annuel brut fixe',
//           onChangeText: (wishRawSalary) => {
//             this.setState({ wishRawSalary });
//           },
//           value: this.state.wishRawSalary !== undefined
//             ? this.state.wishRawSalary
//             : candidate.wish_raw_salary,
//         },
//       },
//       {
//         key: 'wish-salary-variable',
//         type: 'number',
//         props: {
//           label: 'Salaire brut variable annuel en €',
//           placeholder: 'Votre salaire annuel brut variable',
//           onChangeText: (wishVariableSalary) => {
//             this.setState({ wishVariableSalary });
//           },
//           value: this.state.wishVariableSalary !== undefined
//             ? this.state.wishVariableSalary
//             : candidate.wish_variable_salary,
//         },
//       },
//     ];
//   };

//   render() {
//     const { navigate } = this.props.navigation;

//     if (this.props.data.error) {
//       return <ErrorComponent message="Problème lors de la récupération du profil." />;
//     } else if (this.props.data.loading) {
//       return <LoadingComponent />;
//     }

//     if (!this.props.data.viewer) {
//       return null;
//     }
//     const { candidate } = this.props.data.viewer;
//     if (!candidate) {
//       return null;
//     }
//     return (
//       <KeyboardAwareScrollView
//         style={styles.background}
//         keyboardShouldPersistTaps="always"
//         showsVerticalScrollIndicator={false}
//         showsHorizontalScrollIndicator={false} >
//         <View style={styles.section}>
//           <ProfilePicture
//             alertWithType={this.props.alertWithType}
//             containerStyle={styles.profilePictureView}
//             userId={!this.props.data.loading ? this.props.data.viewer.id : null}
//           />
//           <Text style={styles.personalInfosName}>{`${candidate.firstname} ${candidate.lastname || ''}`}</Text>
//           <Text style={styles.personalInfosRole}>{candidate.headline}</Text>
//           <Text style={styles.personalInfosCity}>
//             {
//               candidate.work_city ? candidate.work_city.reduce((s, city, i) => `${s} ${i > 0 ? '-' : ''} ${city.name}`, '')
//                 : null
//             }
//           </Text>
//         </View>
//         <Card
//           title="Vos critères"
//           containerStyle={styles.bigSection}
//           titleStyle={{ marginLeft: 0, marginTop: 0 }}
//         >
//           <FormGenerator buildFields={this.getFieldsWithProps()} />
//           <Button
//             containerViewStyle={styles.validateView}
//             title="Valider les modifications"
//             color={theme.secondaryColor}
//             backgroundColor={theme.primaryColor}
//             buttonStyle={themeElements.validateSmallButton}
//             onPress={this.handleValidateModifications}
//           />
//         </Card>
//       </KeyboardAwareScrollView>
//     );
//   }
// }

// const EDIT_INFOS = gql`
//   mutation(
//     $user_id: String!
//     $wish_raw_salary: String!
//     $wish_variable_salary: String!
//     $company_size: [String!]
//   ) {
//     editCandidate(
//       user_id: $user_id
//       wish_raw_salary: $wish_raw_salary
//       wish_variable_salary: $wish_variable_salary
//       company_size: $company_size
//     ) {
//       candidate {
//         user_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// export default compose(
//   graphql(VIEWER_CANDIDATE),
//   graphql(EDIT_INFOS, { name: 'editInfos' }),
// )(connectAlert(Profile));

// import React, { Component } from 'react';
// import {
//   View,
//   StyleSheet,
//   ScrollView,
// } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';
// import 'moment/locale/fr';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import Profile from '../../components/GQLLinked/Profile';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//   },
// });

// const VIEWER = gql`
//   query {
//     viewer {
//       id
//     }
//   }
// `;

// class CandidateProfile extends Component {
//   static navigationOptions = ({ navigation }) => ({
//     title: (navigation.state.params && navigation.state.params.userId) ? 'Aperçu de profil' : 'Aperçu de votre profil',
//     headerTruncatedBackTitle: '',
//   });

//   props: {
//     navigation: {
//       navigate: Function,
//     },
//     mutate: Function,
//     dispatch: Function,
//     data: {
//       error: string,
//       viewer: {
//         id: string,
//         candidate: {
//           firstname: string,
//           lastname: string,
//           wish_raw_salary: string,
//           wish_variable_salary: string,
//           work_city: [],
//           company_size: [],
//         },
//       },
//       loading: boolean,
//     },
//   };

//   render() {
//     if (this.props.data.error) {
//       return (<ErrorComponent message="Problème lors de la récupération du profil." />);
//     } else if (this.props.data.loading) {
//       return (<LoadingComponent />);
//     }

//     const { params } = this.props.navigation.state;
//     const userId = params && params.userId ? params.userId : this.props.data.viewer.id;
//     return (
//       <View style={styles.background}>
//         <ScrollView
//           showsVerticalScrollIndicator={false}
//           showsHorizontalScrollIndicator={false}
//         >
//           <Profile userId={userId} viewer={this.props.data.viewer.id} navigate={this.props.navigation.navigate} />
//         </ScrollView>
//       </View>
//     );
//   }
// }

// export default graphql(VIEWER)(CandidateProfile);

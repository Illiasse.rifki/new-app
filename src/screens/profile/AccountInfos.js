// import React, { Component } from 'react';
// import { StyleSheet, View, Platform } from 'react-native';
// import moment from 'moment';
// import DatePicker from 'react-native-datepicker';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';
// import { FormLabel, Button, FormInput } from 'react-native-elements';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// import Card from '../../components/Card/CardEditable';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import SquareButtonField from '../../components/Design/SquareButtonField';
// import { theme, themeElements } from '../../config/constants/index';
// import connectAlert from '../../components/Alert/connectAlert';
// import UserCurrentCity from '../../components/GQLLinked/UserCurrentCity';
// import ProfilePicture from '../../components/GQLLinked/ProfilePicture';
// import JobSituation from '../../components/Candidate/Register/JobSituation';
// import ExperimentCheckbox from '../../components/Candidate/Register/ExperimentCheckbox';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//     paddingTop: 24,
//   },
//   section: {
//     paddingTop: 30,
//     paddingBottom: 45,
//   },
//   sectionImage: {
//     paddingTop: 30,
//     paddingBottom: 45,
//     alignItems: 'center',
//   },
//   lastSection: {
//     paddingTop: 30,
//     paddingBottom: 45,
//     marginBottom: 50,
//   },
//   fieldView: {
//     marginTop: 5,
//     paddingLeft: 8,
//     paddingRight: 8,
//   },
//   formLabelContainer: {
//     marginLeft: 0,
//     marginRight: 0,
//     marginTop: 17,
//   },
//   formLabelContent: {
//     marginTop: 0,
//     marginRight: 0,
//     marginLeft: 0,
//     color: '#afafaf',
//     letterSpacing: 0.5,
//     fontSize: 14,
//     fontWeight: '500',
//   },
//   formInputContainer: {
//     marginTop: -5,
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   infosView: {
//     marginBottom: 25,
//   },
//   birthContainer: {
//     width: '100%',
//     marginTop: -10,
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   profilePictureView: {
//     marginVertical: 14,
//     alignSelf: 'center',
//     flex: 1,
//   },
// });

// const VIEWER_CANDIDATE = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         email
//         contact_email
//         password
//         firstname
//         lastname
//         jobSituation
//         years_experience
//         phone
//         birthday
//         headline
//         current_city {
//           current_city_id
//         }
//       }
//     }
//   }
// `;

// const EDIT_INFOS = gql`
//   mutation(
//     $user_id: String!
//     $firstname: String!
//     $lastname: String!
//     $phone: String
//     $jobSituation: String
//     $birthday: String
//     $years_experience: String
//     $contact_email: String
//     $headline: String
//   ) {
//     editCandidate(
//       user_id: $user_id
//       firstname: $firstname
//       lastname: $lastname
//       phone: $phone
//       birthday: $birthday
//       contact_email: $contact_email
//       headline: $headline
//       years_experience: $years_experience
//       jobSituation: $jobSituation
//     ) {
//       candidate {
//         user_id
//       }
//     }
//   }
// `;

// const QUERY_PROFILE_CANDIDATE = gql`
//   query SingleCandidateQuery($user_id: String) {
//     singleCandidate(user_id: $user_id) {
//       editable
//       firstname
//       lastname
//       tags
//       qualities
//       jobSituation
//       years_experience
//       headline
//       work_city {
//         name
//       }
//       experience(order_by_date: true) {
//         experience_id
//         role
//       }
//       education(order_by_date: true) {
//         education_id
//       }
//       language {
//         language_id
//       }
//     }
//   }
// `;

// class AccountInfos extends Component {
//   state = {};

//   props: {
//     editInfos: Function,
//     navigation: {
//       navigate: Function,
//       goBack: Function,
//     },
//     data: {
//       viewer: {
//         id: string,
//         candidate: {
//           email: string,
//           jobSituation: string,
//           firstname: string,
//           years_experience: string,
//           lastname: string,
//           phone: string,
//           birthday: string,
//           current_city: {
//             current_city_id: string,
//           },
//         },
//       },
//       error: boolean,
//       loading: boolean,
//       alertWithType: Function,
//     },
//   };

//   handleValidateModifications = () => {
//     const variables = {
//       user_id: this.props.data.viewer.id,
//       firstname: this.state.firstname || this.props.data.viewer.candidate.firstname,
//       lastname: this.state.lastname || this.props.data.viewer.candidate.lastname,
//       phone: this.state.phone || this.props.data.viewer.candidate.phone,
//       years_experience:
//         this.state.experimented !== undefined
//           ? this.state.experimented
//             ? '3'
//             : '0'
//           : this.props.data.viewer.candidate.years_experience,
//       jobSituation: this.state.jobSituation || this.props.data.viewer.candidate.jobSituation,
//       contact_email: this.state.contactEmail || this.props.data.viewer.candidate.contact_email,
//       headline: this.state.headline || this.props.data.viewer.candidate.headline,
//     };
//     if (this.state.birthday) {
//       variables.birthday = this.state.birthday
//         ? moment(this.state.birthday, 'DD-MM-YYYY').toDate()
//         : moment(new Date(this.props.data.viewer.candidate.birthday)).toDate();
//     }
//     this.props
//       .editInfos({
//         refetchQueries: [
//           {
//             query: VIEWER_CANDIDATE,
//           },
//           {
//             query: QUERY_PROFILE_CANDIDATE,
//             variables: {
//               user_id: this.props.data.viewer.id,
//             },
//           },
//         ],
//         variables,
//       })
//       .then(() => {
//         this.props.alertWithType(
//           'success',
//           'Succès',
//           'Les modifications ont bien été prises en compte',
//           'Sauvegarde',
//         );
//         this.props.navigation.goBack(null);
//       })
//       .catch((e) => {
//         this.props.alertWithType('error', 'Erreur', e.message);
//       });
//   };

//   render() {
//     const legalBirth = moment().subtract(18, 'years');

//     if (this.props.data.error) {
//       return <ErrorComponent message="Problème lors de la récupération du compte." />;
//     } else if (this.props.data.loading) {
//       return <LoadingComponent />;
//     }

//     const { experimented } = this.state;

//     const { navigate } = this.props.navigation;
//     const { candidate } = this.props.data.viewer;
//     return (
//       <KeyboardAwareScrollView
//         style={styles.background}
//         keyboardShouldPersistTaps="always"
//         showsVerticalScrollIndicator={false}
//         showsHorizontalScrollIndicator={false}
//       >
//         <Card title="Modifier photo de profil" containerStyle={styles.sectionImage}>
//           <ProfilePicture
//             alertWithType={this.props.alertWithType}
//             containerStyle={styles.profilePictureView}
//             userId={this.props.data.viewer.id}
//             edit
//           />
//         </Card>
//         <Card title="Votre poste" containerStyle={styles.section}>
//           <View style={styles.fieldView}>
//             <FormLabel
//               labelStyle={styles.formLabelContent}
//               containerStyle={styles.formLabelContainer}
//             >
//               Poste actuel
//             </FormLabel>
//             <FormInput
//               placeholder={'Poste'}
//               inputStyle={themeElements.formInputText}
//               containerStyle={styles.formInputContainer}
//               onChangeText={(headline) => {
//                 this.setState({ headline });
//               }}
//               value={this.state.headline !== undefined ? this.state.headline : candidate.headline}
//               autoCorrect={false}
//               underlineColorAndroid="#979797"
//             />
//             <FormLabel
//               labelStyle={styles.formLabelContent}
//               containerStyle={styles.formLabelContainer}
//             >
//               Situation actuel
//             </FormLabel>
//             <JobSituation
//               value={
//                 this.state.jobSituation !== undefined
//                   ? this.state.jobSituation
//                   : candidate.jobSituation
//               }
//               onValueChange={(key, value) => this.setState({ jobSituation: value })}
//             />
//             <FormLabel
//               labelStyle={styles.formLabelContent}
//               containerStyle={styles.formLabelContainer}
//             >
//               Experience
//             </FormLabel>
//             <ExperimentCheckbox
//               checked={
//                 experimented !== undefined
//                   ? experimented
//                   : candidate.years_experience && parseInt(candidate.years_experience, 10) > 2
//               }
//               onPress={() =>
//                 this.setState({
//                   experimented:
//                     experimented !== undefined
//                       ? !experimented
//                       : !(
//                         candidate.years_experience && parseInt(candidate.years_experience, 10) > 2
//                       ),
//                 })
//               }
//             />
//           </View>
//         </Card>
//         <Card title="Vos identifiants" containerStyle={styles.section}>
//           {/* <SquareButtonField
//             containerStyle={styles.fieldView}
//             label="E-mail de connexion"
//             title={candidate.email}
//             onPress={() => {
//               navigate('ImportantFieldModification', {
//                 field: 'email',
//                 display: 'E-mail de connexion',
//                 secure: false,
//               });
//             }}
//           /> */}
//           <SquareButtonField
//             containerStyle={styles.fieldView}
//             label="Mot de passe"
//             title="Changez votre mot de passe"
//             onPress={() => {
//               navigate('ImportantFieldModification', {
//                 field: 'password',
//                 display: 'Mot de passe',
//                 secure: true,
//               });
//             }}
//           />
//         </Card>
//         <Card title="Vos coordonnées" containerStyle={styles.lastSection}>
//           <View style={styles.infosView}>
//             <View style={styles.fieldView}>
//               <FormLabel
//                 labelStyle={styles.formLabelContent}
//                 containerStyle={styles.formLabelContainer}
//               >
//                 NOM
//               </FormLabel>
//               <FormInput
//                 placeholder={'Nom'}
//                 inputStyle={themeElements.formInputText}
//                 containerStyle={styles.formInputContainer}
//                 onChangeText={(lastname) => {
//                   this.setState({ lastname });
//                 }}
//                 value={this.state.lastname !== undefined ? this.state.lastname : candidate.lastname}
//                 autoCorrect={false}
//                 underlineColorAndroid="#979797"
//               />
//             </View>
//             <View style={styles.fieldView}>
//               <FormLabel
//                 labelStyle={styles.formLabelContent}
//                 containerStyle={styles.formLabelContainer}
//               >
//                 {'Prénom'.toUpperCase()}
//               </FormLabel>
//               <FormInput
//                 placeholder={'Prénom'}
//                 inputStyle={themeElements.formInputText}
//                 containerStyle={styles.formInputContainer}
//                 onChangeText={(firstname) => {
//                   this.setState({ firstname });
//                 }}
//                 value={
//                   this.state.firstname !== undefined ? this.state.firstname : candidate.firstname
//                 }
//                 autoCorrect={false}
//                 underlineColorAndroid="#979797"
//               />
//             </View>
//             <View style={styles.fieldView}>
//               <FormLabel
//                 labelStyle={styles.formLabelContent}
//                 containerStyle={styles.formLabelContainer}
//               >
//                 {'E-mail de contact'.toUpperCase()}
//               </FormLabel>
//               <FormInput
//                 placeholder={'E-mail de contact'}
//                 inputStyle={themeElements.formInputText}
//                 containerStyle={styles.formInputContainer}
//                 onChangeText={(contactEmail) => {
//                   this.setState({ contactEmail });
//                 }}
//                 value={
//                   this.state.contactEmail !== undefined
//                     ? this.state.contactEmail
//                     : candidate.contact_email
//                 }
//                 autoCorrect={false}
//                 underlineColorAndroid="#979797"
//               />
//             </View>
//             <View style={styles.fieldView}>
//               <FormLabel
//                 labelStyle={styles.formLabelContent}
//                 containerStyle={styles.formLabelContainer}
//               >
//                 {'Date de naissance'.toUpperCase()}
//               </FormLabel>
//               <DatePicker
//                 placeholder="Date de naissance"
//                 style={styles.birthContainer}
//                 onDateChange={(birthday) => {
//                   this.setState({ birthday });
//                 }}
//                 date={
//                   this.state.birthday !== undefined
//                     ? this.state.birthday
//                     : moment(new Date(candidate.birthday)).format('DD-MM-YYYY')
//                 }
//                 format="DD-MM-YYYY"
//                 minDate="01-01-1917"
//                 maxDate={legalBirth.format('DD-MM-YYYY')}
//                 confirmBtnText="Confirmer"
//                 cancelBtnText="Annuler"
//                 customStyles={{
//                   dateInput: {
//                     borderRightWidth: 0,
//                     borderLeftWidth: 0,
//                     borderTopColor: 'transparent',
//                     borderLeftColor: 'transparent',
//                     borderRightColor: 'transparent',
//                     borderBottomColor: Platform.OS === 'ios' ? '#c9c9c9' : '#979797',
//                   },
//                   dateText: {
//                     fontSize: 16,
//                     width: '100%',
//                     color: theme.fontBlack,
//                     marginBottom: -8,
//                     textAlign: 'left',
//                   },
//                   placeholderText: {
//                     fontSize: 16,
//                     width: '100%',
//                     marginBottom: -8,
//                     textAlign: 'left',
//                   },
//                 }}
//                 showIcon={false}
//               />
//             </View>
//             <View style={styles.fieldView}>
//               <FormLabel
//                 labelStyle={styles.formLabelContent}
//                 containerStyle={styles.formLabelContainer}
//               >
//                 {'Adresse'.toUpperCase()}
//               </FormLabel>
//               <UserCurrentCity
//                 currentCityId={candidate.current_city.current_city_id}
//                 userId={this.props.data.viewer.id}
//               />
//             </View>
//             <View style={styles.fieldView}>
//               <FormLabel
//                 labelStyle={styles.formLabelContent}
//                 containerStyle={styles.formLabelContainer}
//               >
//                 {'Téléphone'.toUpperCase()}
//               </FormLabel>
//               <FormInput
//                 placeholder={'Téléphone'}
//                 keyboardType="phone-pad"
//                 returnKeyType="done"
//                 inputStyle={themeElements.formInputText}
//                 containerStyle={styles.formInputContainer}
//                 onChangeText={(phone) => {
//                   this.setState({ phone });
//                 }}
//                 value={this.state.phone !== undefined ? this.state.phone : candidate.phone}
//                 autoCorrect={false}
//                 underlineColorAndroid="#979797"
//               />
//             </View>
//           </View>
//           <Button
//             containerViewStyle={styles.validateView}
//             title="Valider les modifications"
//             color={theme.secondaryColor}
//             backgroundColor={theme.primaryColor}
//             buttonStyle={themeElements.validateSmallButton}
//             onPress={this.handleValidateModifications}
//           />
//         </Card>
//       </KeyboardAwareScrollView>
//     );
//   }
// }

// export default compose(
//   graphql(VIEWER_CANDIDATE),
//   graphql(EDIT_INFOS, { name: 'editInfos' }),
// )(connectAlert(AccountInfos));

// import React, { Component } from 'react';
// import {
//   View,
//   StyleSheet,
//   ScrollView,
// } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';
// import 'moment/locale/fr';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import FullProfileCompany from '../../components/Company/FullProfileCompany';
// import CompanyChatIcon from '../../components/Company/CompanyChatIcon';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//   },
//   rightIconStyle: { marginTop: 4, marginRight: 24 },
// });

// class CompanyProfile extends Component {
//   static navigationOptions = ({ navigation }) => ({
//     title: 'Entreprise',
//     headerTruncatedBackTitle: '',
//     // headerRight:
//     //   <CompanyChatIcon
//     //     containerStyle={styles.rightIconStyle}
//     //     navigation={navigation}
//     //     companyId={navigation.state.params.companyId}
//     //   />,
//   });

//   props: {
//     navigation: {
//       navigate: Function,
//       state: {
//         params: {
//           companyId: string,
//         },
//       },
//     },
//     data: {
//       loading: boolean,
//       error: string,
//       loading: boolean,
//     },
//   };

//   render() {
//     if (this.props.data.error) {
//       return (<ErrorComponent message="Problème lors de la récupération du profil de l'entreprise" />);
//     } if (this.props.data.loading) {
//       return (<LoadingComponent />);
//     }

//     const { state, navigate } = this.props.navigation;
//     return (
//       <View style={styles.background}>
//         <ScrollView
//           showsVerticalScrollIndicator={false}
//           showsHorizontalScrollIndicator={false}
//         >
//           <FullProfileCompany navigate={navigate} companyId={state.params.companyId} />
//         </ScrollView>
//       </View>
//     );
//   }
// }

// const QUERY_COMPANY_NAME = gql`
//   query CompanyQuery($company_id: String!) {
//     visitCompany(company_id: $company_id) {
//       name
//     }
//   }
// `;

// export default graphql(QUERY_COMPANY_NAME, {
//   options: ownProps => ({
//     variables: {
//       company_id: ownProps.navigation.state.params.companyId,
//     },
//     fetchPolicy: 'network-only',
//   }),
// })(CompanyProfile);

import React, { Component } from 'react';
import { StyleSheet, Platform } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import LanguageEdit from '../../components/GQLLinked/LanguageEdit';
import { CANDIDATE_LANGUAGES } from './CandidateProfileLanguages';

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: 'white',
    paddingBottom: 16,
    paddingTop: Platform.OS === 'ios' ? 4 : 20,
    paddingLeft: 8.25,
    paddingRight: 8.25,
  },
});

class CandidateProfileAddLanguage extends Component {
  static navigationOptions = {
    title: "Ajout d'une langue",
    headerTruncatedBackTitle: '',
  };

  props: {
    navigation: {
      goBack: Function,
      state: {
        params: {
          userId: string,
        },
      },
    },
  };

  handleAddLanguageButton = () => {
    const { goBack } = this.props.navigation;
    goBack(null);
  };

  render() {
    const { state } = this.props.navigation;

    return (
      <KeyboardAwareScrollView style={styles.background}>
        <LanguageEdit
          userId={state.params.userId}
          refetchQueries={[{
            query: CANDIDATE_LANGUAGES,
            variables: { user_id: state.params.userId },
          }]}
          onAddLanguage={this.handleAddLanguageButton}
        />
      </KeyboardAwareScrollView>
    );
  }
}

export default CandidateProfileAddLanguage;

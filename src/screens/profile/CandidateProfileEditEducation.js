// import React, { Component } from 'react';
// import { StyleSheet } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import EducationEdit from '../../components/GQLLinked/EducationEdit';
// import { CANDIDATE_EDUCATION } from './CandidateProfileEducation';
// import { QUERY_PROFILE_CANDIDATE } from '../../components/GQLLinked/Profile';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: 'white',
//     paddingBottom: 16,
//     paddingTop: 34,
//     paddingLeft: 8.25,
//     paddingRight: 8.25,
//   },
// });

// class CandidateProfileEditEducation extends Component {
//   static navigationOptions = {
//     title: "Modification d'une formation",
//     headerTruncatedBackTitle: '',
//   };

//   constructor(props) {
//     super(props);
//     this.state = {};
//   }

//   props: {
//     navigation: {
//       goBack: Function,
//       state: {
//         params: {
//           experienceId: string,
//         }
//       }
//     },
//     data: {
//       loading: boolean,
//       error: boolean,
//       viewer: {
//         id: string,
//       }
//     }
//   };

//   handleButton = () => {
//     const { goBack } = this.props.navigation;
//     goBack(null);
//   };

//   render() {
//     if (this.props.data.error) {
//       return (<ErrorComponent message="Problème lors de la récupération du profil." />);
//     } else if (this.props.data.loading) {
//       return (<LoadingComponent />);
//     }

//     const { state } = this.props.navigation;
//     return (
//       <KeyboardAwareScrollView
//         style={styles.background}
//         keyboardShouldPersistTaps="always"
//       >
//         <EducationEdit
//           educationId={state.params.educationId}
//           refetchQueries={[{
//             query: CANDIDATE_EDUCATION,
//             variables: { user_id: state.params.userId },
//           }, {
//             query: QUERY_PROFILE_CANDIDATE,
//             variables: { user_id: state.params.userId },
//           }]}
//           onEditEducation={this.handleButton}
//           onDeleteEducation={this.handleButton}
//         />

//       </KeyboardAwareScrollView>
//     );
//   }
// }

// const VIEWER_CANDIDATE = gql`
//   query {
//     viewer {
//       id
//     }
//   }
// `;

// export default graphql(VIEWER_CANDIDATE)(CandidateProfileEditEducation);

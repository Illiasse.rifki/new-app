// import React, { Component } from 'react';
// import { View, Text, StyleSheet, ScrollView } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';
// import { Icon } from 'react-native-elements';
// import { themeElements } from '../../config/constants/index';
// import CardEditable from '../../components/Card/CardEditable';
// import EducationDisplay from '../../components/GQLLinked/EducationDisplay';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//     paddingBottom: 16,
//     paddingTop: 16,
//   },
//   scrollViewFix: {
//     marginBottom: 32,
//   },
//   titleView: {
//     flexDirection: 'row',
//     paddingLeft: 32,
//     paddingRight: 19,
//     marginTop: 22,
//     marginBottom: 30,
//   },
//   mainTitle: {
//     flex: 16,
//   },
//   addIcon: {
//     alignItems: 'flex-end',
//     flex: 2,
//   },
//   section: {
//     flex: 1,
//     position: 'relative',
//     backgroundColor: 'white',
//     paddingLeft: 32,
//     paddingRight: 32,
//     paddingTop: 49,
//     paddingBottom: 51,
//     marginBottom: 16,
//   },
// });

// class CandidateProfileEducation extends Component {
//   static navigationOptions = {
//     title: 'Vos formations',
//     headerTruncatedBackTitle: '',
//   };

//   props: {
//     navigation: {
//       navigate: Function,
//       state: {},
//     },
//     data: {
//       singleCandidate: {
//         education: {
//           education_id: string,
//         }
//       },
//       error: String,
//       loading: boolean,
//     }
//   };

//   render() {
//     if (this.props.data.error) {
//       return (<ErrorComponent message='Problème lors de la récupération du profil.' />);
//     } else if (this.props.data.loading) {
//       return (<LoadingComponent />);
//     }

//     const { navigate, state } = this.props.navigation;
//     const candidate = this.props.data.singleCandidate;
//     const hasEducation = candidate.education.length;
//     return (
//       <ScrollView
//         style={styles.background}
//         showsVerticalScrollIndicator={false}
//         showsHorizontalScrollIndicator={false} >
//         <View style={styles.titleView}>
//           <Text style={[themeElements.bigTitle, styles.mainTitle]}>Formation</Text>
//           <Icon
//             name="plus"
//             type="entypo"
//             size={40}
//             containerStyle={styles.addIcon}
//             onPress={() => {
//               navigate('CandidateProfileAddEducation', { userId: state.params.userId });
//             }}
//           />
//         </View>
//         <View>
//           {hasEducation ? (
//             candidate.education.map(education => (
//               <CardEditable
//                 key={education.education_id}
//                 containerStyle={styles.section}
//                 editable
//                 onPress={() => {
//                   navigate('CandidateProfileEditEducation', { userId: state.params.userId, educationId: education.education_id });
//                 }}
//               >
//                 <EducationDisplay educationId={education.education_id} />
//               </CardEditable>
//             ))
//           ) : (
//             <CardEditable>
//               <Text>Aucune formation ajouté</Text>
//             </CardEditable>
//           )}
//           <View style={styles.scrollViewFix} />
//         </View>
//       </ScrollView>
//     );
//   }
// }

// const CANDIDATE_EDUCATION = gql`
//   query SingleCandidateQuery($user_id: String) {
//     singleCandidate(user_id: $user_id) {
//       education {
//         education_id
//       }
//     }
//   }
// `;

// export default graphql(CANDIDATE_EDUCATION, {
//   options: ownProps => ({
//     variables: {
//       user_id: ownProps.navigation.state.params.userId,
//     },
//   }),
// })(CandidateProfileEducation);
// export { CANDIDATE_EDUCATION };

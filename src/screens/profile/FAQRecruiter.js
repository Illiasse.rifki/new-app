import React, { Component } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import FAQ from '../../components/Content/FAQ';

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: '#f5f5f5',
    paddingTop: 24,
  },
  faqContainer: {
    paddingTop: 0,
    paddingBottom: 0,
    marginBottom: 25,
  },
  questionsContainer: {
    marginTop: 0,
    borderTopColor: '#f5f5f5',
    borderTopWidth: 4,
  },
  questionContainerStyle: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 25.5,
    paddingBottom: 25.5,
    borderBottomColor: '#f5f5f5',
    borderBottomWidth: 4,
  },
  questionTextStyle: {
    fontSize: 18,
    fontWeight: '500',
    lineHeight: 24,
  },
});

class FAQRecruiter extends Component {
  // static navigationOptions = {
  //   title: 'Foire Aux Questions',
  //   headerTruncatedBackTitle: 'FAQ',
  // };

  static questions = [
    {
      question: 'Comment avoir accès aux personnes ayant postulé à notre événement ?',
      answer: 'Dans l’accueil, vous aurez directement en visuel un onglet « candidatures à traiter ». Sous ce titre figureront les différents candidats ayant postulé pour ces évènements. Vous pourrez cliquer sur le profil de chaque candidat et ainsi traiter leur candidature très facilement.',
    },
    {
      question: 'J’ai trouvé un profil intéressant, comment le contacter directement ?',
      answer: 'Il vous suffit de vous rendre sur le profil du candidat en question sur lequel apparaîtra un bouton « chat » pour le contacter. Une conversation privée sera alors engagée avec ce candidat et vous serez notifié dès qu’il vous répondra.',
    },
    {
      question: 'Comment déposer mon offre d’emploi sur l’application ?',
      answer: 'Pour déposer une offre d’emploi il vous faudra aller sur le site internet Bizzeo et non sur l’application. Depuis ce site, vous allez avoir la possibilité en quelque clics de créer une offre et ensuite de la déposer sur notre plateforme. Elle apparaîtra immédiatement sur l’application pour les éventuels candidats.',
    },
    {
      question: 'Comment avoir accès aux personnes ayant postulé à mes offres d’emploi ?',
      answer: 'Lorsque vous vous connectez sur votre profil recruteur, les offres et événements vous concernant sont affichés. Il vous suffit de cliquer sur cette offre/cet événement puis sur « liste des candidatures » pour avoir accès aux personnes ayant postulé. Vous pouvez également retrouver vos candidatures depuis votre espace Recruteur sur le site internet.',
    },
    {
      question: 'Comment suis-je notifié qu’un candidat à postulé à une offre que j’ai déposée ?',
      answer: 'Vous allez avoir la possibilité de voir toutes les personnes ayant postulé à vos offres d’emploi en cliquant sur cette même offre depuis l’application (les offres proposées par votre entreprise apparaissent directement dans le menu principal). En fonction de vos réglages, vous pourrez également recevoir une notification et/ou un email.',
    },
    {
      question: 'Vous organisez un événement auquel notre entreprise souhaite participer, comment faire ?',
      answer: 'Dans le menu principal vous pouvez cliquer sur chaque événement. En cliquant dessus, vous aurez accès à toutes les informations de cet événement (date, lieu, nombre d’entreprise, conditions, etc.) et aussi à un bouton « je veux participer » qui vous mènera directement vers la plateforme pour participer à cet événement.',
    },
  ];

  render() {
    return (
      <ScrollView
        style={styles.background}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false} >
        <FAQ
          faq={FAQRecruiter.questions}
          containerStyle={styles.faqContainer}
          questionsContainerStyle={styles.questionsContainer}
          questionContainerStyle={styles.questionContainerStyle}
          questionTextStyle={styles.questionTextStyle}
        />
      </ScrollView>
    );
  }
}

export default FAQRecruiter;

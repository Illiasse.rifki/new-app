import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import EducationEdit from '../../components/GQLLinked/EducationEdit';
import { CANDIDATE_EDUCATION } from './CandidateProfileEducation';
import { QUERY_PROFILE_CANDIDATE } from '../../components/GQLLinked/Profile';
import Loader from '../../components/Loader/Loader';

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: 'white',
    paddingBottom: 16,
    paddingTop: 34,
    paddingLeft: 8.25,
    paddingRight: 8.25,
  },
});

class CandidateProfileAddEducation extends Component {
  static navigationOptions = {
    title: "Ajout d'une formation",
    headerTruncatedBackTitle: '',
  };

  props: {
    navigation: {
      goBack: Function,
      navigate: Function,
      state: {
        params: {
          register: boolean,
          userId: string,
        },
      },
    },
  };

  handleAddEducationButton = () => {
    const { goBack, navigate, state } = this.props.navigation;
    if (state.params.register) {
      navigate('CandidateFormTags', { transition: 'noTransition', userId: state.params.userId });
    } else { goBack(null); }
  };

  render() {
    const { state } = this.props.navigation;

    return (
      <KeyboardAwareScrollView
        style={{ flex: 1, backgroundColor: 'white' }}
        keyboardShouldPersistTaps="always"
      >
        {
          this.props.register &&
          <Loader
            width={this.props.progress}
            start={this.props.progress}
          />
        }
        <View style={styles.background}>
          <EducationEdit
            userId={state.params.userId}
            refetchQueries={[{
              query: CANDIDATE_EDUCATION,
              variables: { user_id: state.params.userId },
            }, {
              query: QUERY_PROFILE_CANDIDATE,
              variables: { user_id: state.params.userId },
            }]}
            onAddEducation={this.handleAddEducationButton}
          />
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

export default CandidateProfileAddEducation;

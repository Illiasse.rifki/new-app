import React, { Component } from "react";
// import { StyleSheet, ScrollView } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import ProfileCompany from '../../components/GQLLinked/ProfileCompany';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//     paddingBottom: 24,
//     paddingTop: 24,
//   },
//   profileCompany: {
//     marginBottom: 40,
//   },
// });

// const VIEWER = gql`
//   query {
//     viewer {
//       id
//       recruiter {
//         company {
//           company_id
//         }
//       }
//     }
//   }
// `;

// class ProfileRecruiter extends Component {
//   static navigationOptions = {
//     title: 'Votre profil',
//     headerTruncatedBackTitle: 'Profil',
//     headerLeft: null,
//     tabBarLabel: 'Mes informations',
//   };

//   constructor(props) {
//     super(props);
//     this.state = {};
//   }

//   // Why ?
//   // shouldComponentUpdate(nextProps) {
//   //   const { route } = nextProps.screenProps;
//   //   const { routeName } = this.props.navigation.state;
//   //   return route === routeName;
//   // }

//   props: {
//     navigation: {
//       navigate: Function,
//     },
//     data: {
//       error: string,
//       viewer: {
//         recruiter: {
//           company: {
//             company_id: string,
//           },
//         },
//       },
//       loading: boolean,
//     },
//   };

//   render() {
//     const { navigate } = this.props.navigation;

//     if (this.props.data.error) {
//       return <ErrorComponent message="Problème lors de la récupération du profil." />;
//     } else if (this.props.data.loading) {
//       return <LoadingComponent />;
//     }
//     if (!this.props.data.viewer) { return null; }
//     const { company } = this.props.data.viewer.recruiter;
//     return (
//       <ScrollView style={styles.background} keyboardShouldPersistTaps="always">
//         <ProfileCompany
//           companyId={company.company_id}
//           navigate={navigate}
//           containerStyle={styles.profileCompany}
//         />
//       </ScrollView>
//     );
//   }
// }

// export default graphql(VIEWER)(ProfileRecruiter);

class ProfileRecruiter extends Component {
  static navigationOptions = {
    title: "Votre profil",
    headerTruncatedBackTitle: "Profil",
    headerLeft: null,
    tabBarLabel: "Mes informations",
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  // Why ?
  // shouldComponentUpdate(nextProps) {
  //   const { route } = nextProps.screenProps;
  //   const { routeName } = this.props.navigation.state;
  //   return route === routeName;
  // }

  // props: {
  //   navigation: {
  //     navigate: Function,
  //   },
  //   data: {
  //     error: string,
  //     viewer: {
  //       recruiter: {
  //         company: {
  //           company_id: string,
  //         },
  //       },
  //     },
  //     loading: boolean,
  //   },
  // };

  render() {
    return <View>SLAUT</View>;
  }
}

export default ProfileRecruiter;

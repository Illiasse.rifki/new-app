// import React, { Component } from 'react';
// import { StyleSheet, View, Text } from 'react-native';
// import styled from 'styled-components';
// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';

// import QRCode from 'react-native-qrcode';
// import Drawer from '../../components/Content/Drawer';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';

// const InformationsContainer = styled(View)`
//   width: 256;
//   margin-top: 24px;
//   height: auto;
//   `;

// const Instructions = styled(Text)`
//   font-size: 16px;
//   font-weight: bold;
// `;

// const Mandatory = styled(Text)`
//   font-size: 16px;
//   font-weight: normal;
//   margin-top: 8px;
// `;

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//     paddingBottom: 48,
//     paddingTop: 48,
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   qr: {
//     flex: 2,
//     width: '100%',
//     paddingBottom: 24,
//     paddingTop: 24,
//   },
// });

// const VIEWER = gql`
//   query {
//     viewer {
//       id
//     }
//   }
// `;

// class QRCodeCandidate extends Component {
//   static navigationOptions = ({ navigation }) => ({
//     title: 'Votre QRCode',
//     headerTruncatedBackTitle: 'QRCode',
//     headerLeft: <Drawer.Icon
//       onPress={() => navigation.navigate('DrawerOpen')}
//     />,
//   });

//   constructor(props) {
//     super(props);
//     this.state = {};
//   }

//   props: {
//     navigation: {
//       navigate: Function,
//     },
//     data: {
//       error: string,
//       viewer: {
//         id: string,
//       },
//       loading: boolean,
//     },
//   };

//   instruction = 'Pour pouvoir être scanné(e) par un recruteur, vous devez:'
//   mandatory = [
//     'Avoir une inscription valide',
//     'Avoir validé votre présence auprès de nos organisateurs',
//   ]

//   render() {
//     const { navigate } = this.props.navigation;

//     if (this.props.data.error) {
//       return <ErrorComponent message="Problème lors de la récupération du profil." />;
//     } else if (this.props.data.loading) {
//       return <LoadingComponent />;
//     }
//     const { id } = this.props.data.viewer;
//     return (
//       <View style={styles.background}>
//         <View styles={styles.qr}>
//           <QRCode size={256} value={id} />
//         </View>
//         <InformationsContainer>
//           <Instructions>
//             {`${this.instruction}\n`}
//             {
//               this.mandatory.map((text, i) => <Mandatory key={`mandatory-${i}`}>{`  \u2022\t${text}\n`}</Mandatory>)
//             }
//           </Instructions>
//         </InformationsContainer>
//       </View>
//     );
//   }
// }

// export default graphql(VIEWER)(QRCodeCandidate);

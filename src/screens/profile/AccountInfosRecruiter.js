// import React, { Component } from 'react';
// import {
//   StyleSheet,
//   View,
// } from 'react-native';
// import moment from 'moment';
// import gql from 'graphql-tag';
// import { graphql, compose } from 'react-apollo';
// import { FormLabel, Button, FormInput } from 'react-native-elements';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import Card from '../../components/Card/CardEditable';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import SquareButtonField from '../../components/Design/SquareButtonField';
// import { theme, themeElements } from '../../config/constants/index';
// import connectAlert from '../../components/Alert/connectAlert';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//     paddingTop: 24,
//   },
//   section: {
//     paddingTop: 30,
//     paddingBottom: 45,
//   },
//   lastSection: {
//     paddingTop: 30,
//     paddingBottom: 45,
//     marginBottom: 50,
//   },
//   fieldView: {
//     marginTop: 5,
//     paddingLeft: 8,
//     paddingRight: 8,
//   },
//   formLabelContainer: {
//     marginLeft: 0,
//     marginRight: 0,
//     marginTop: 17,
//   },
//   formLabelContent: {
//     marginTop: 0,
//     marginRight: 0,
//     marginLeft: 0,
//     color: '#afafaf',
//     letterSpacing: 0.5,
//     fontSize: 14,
//     fontWeight: '500',
//   },
//   formInputContainer: {
//     marginTop: -5,
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   infosView: {
//     marginBottom: 25,
//   },
//   birthContainer: {
//     width: '100%',
//     marginTop: -10,
//     marginLeft: 0,
//     marginRight: 0,
//   },
// });

// const VIEWER_RECRUITER = gql`
//   query {
//     viewer {
//       id
//       recruiter {
//         email
//         contact_email
//         firstname
//         lastname
//         phone
//         address
//       }
//     }
//   }
// `;

// const EDIT_INFOS = gql`
//   mutation editRecruiter(
//     $user_id: String
//     $firstname: String
//     $lastname: String
//     $phone: String
//     $address: String
//     $contact_email: String,
//   ) {
//     editRecruiter(
//       user_id: $user_id
//       firstname: $firstname
//       lastname: $lastname
//       phone: $phone
//       address: $address
//       contact_email: $contact_email
//     ) {
//       error {
//         message
//       }
//     }
//   }
// `;

// class AccountInfosRecruiter extends Component {
//   static navigationOptions = {
//     title: 'Identifiants et coordonnées',
//     headerTruncatedBackTitle: '',
//   };

//   constructor(props) {
//     super(props);
//     this.state = {};
//   }

//   props: {
//     editInfos: Function,
//     navigation: {
//       navigate: Function,
//       goBack: Function,
//     },
//     alertWithType: Function,
//     data: {
//       viewer: {
//         id: string,
//         recruiter: {
//           email: string,
//           firstname: string,
//           lastname: string,
//           phone: string,
//           birthday: string,
//           current_city: {
//             current_city_id: string,
//           }
//         },
//       },
//       error: boolean,
//       loading: boolean,
//     },
//   };

//   handleValidateModifications = () => {
//     const { recruiter } = this.props.data.viewer;
//     this.props.editInfos({
//       refetchQueries: [{
//         query: VIEWER_RECRUITER,
//       }],
//       variables: {
//         user_id: this.props.data.viewer.id,
//         firstname: this.state.firstname || recruiter.firstname,
//         contact_email: this.state.contactEmail || recruiter.contact_email,
//         lastname: this.state.lastname || recruiter.lastname,
//         phone: this.state.phone || recruiter.phone,
//         address: this.state.address || recruiter.address,
//       },
//     }).then((res) => {
//       if (res && res.data && res.data.editRecruiter && res.data.editRecruiter.error) throw res.data.editRecruiter.error;
//       this.props.alertWithType('success', 'Modifications effectuées', 'Les modifications ont bien été prises en compte');
//       this.props.navigation.goBack(null);
//     }).catch((e) => {
//       this.props.alertWithType('error', 'Erreur', e.message);
//     });
//   };

//   render() {
//     const legalBirth = moment().subtract(18, 'years');

//     if (this.props.data.error) {
//       return (<ErrorComponent message="Problème lors de la récupération du compte." />);
//     } else if (this.props.data.loading) {
//       return (<LoadingComponent />);
//     }

//     const { navigate } = this.props.navigation;
//     const { recruiter } = this.props.data.viewer;
//     return (
//       <KeyboardAwareScrollView
//         style={styles.background}
//         keyboardShouldPersistTaps="always"
//         showsVerticalScrollIndicator={false}
//         showsHorizontalScrollIndicator={false}
//       >
//         <Card
//           title="Vos identifiants"
//           containerStyle={styles.section}
//         >
//           <SquareButtonField
//             containerStyle={styles.fieldView}
//             label="E-mail de connexion"
//             title={recruiter.email}
//             onPress={() => { navigate('ImportantFieldModificationRecruiter', { field: 'email', display: 'E-mail de connexion', secure: false }); }}
//           />
//           <SquareButtonField
//             containerStyle={styles.fieldView}
//             label="Mot de passe"
//             title="Changez votre mot de passe"
//             onPress={() => { navigate('ImportantFieldModificationRecruiter', { field: 'password', display: 'Mot de passe', secure: true }); }}
//           />
//         </Card>
//         <Card
//           title="Vos coordonnées"
//           containerStyle={styles.lastSection}
//         >
//           <View style={styles.infosView}>
//             <View
//               style={styles.fieldView}
//             >
//               <FormLabel
//                 labelStyle={styles.formLabelContent}
//                 containerStyle={styles.formLabelContainer}
//               >
//                 NOM
//               </FormLabel>
//               <FormInput
//                 placeholder={'Nom'}
//                 inputStyle={themeElements.formInputText}
//                 containerStyle={styles.formInputContainer}
//                 onChangeText={(lastname) => { this.setState({ lastname }); }}
//                 value={this.state.lastname !== undefined ?
//                   this.state.lastname :
//                   recruiter.lastname}
//                 autoCorrect={false}
//                 underlineColorAndroid='#979797'
//               />
//             </View>
//             <View
//               style={styles.fieldView}
//             >
//               <FormLabel
//                 labelStyle={styles.formLabelContent}
//                 containerStyle={styles.formLabelContainer}
//               >
//                 {'Prénom'.toUpperCase()}
//               </FormLabel>
//               <FormInput
//                 placeholder={'Prénom'}
//                 inputStyle={themeElements.formInputText}
//                 containerStyle={styles.formInputContainer}
//                 onChangeText={(firstname) => { this.setState({ firstname }); }}
//                 value={this.state.firstname !== undefined ?
//                   this.state.firstname :
//                   recruiter.firstname}
//                 autoCorrect={false}
//                 underlineColorAndroid='#979797'
//               />
//             </View>
//             <View
//               style={styles.fieldView}
//             >
//               <FormLabel
//                 labelStyle={styles.formLabelContent}
//                 containerStyle={styles.formLabelContainer}
//               >
//                 {'E-mail de contact'.toUpperCase()}
//               </FormLabel>
//               <FormInput
//                 placeholder={'E-mail de contact'}
//                 inputStyle={themeElements.formInputText}
//                 containerStyle={styles.formInputContainer}
//                 onChangeText={(contactEmail) => { this.setState({ contactEmail }); }}
//                 value={this.state.contactEmail !== undefined ?
//                   this.state.contactEmail : recruiter.contact_email}
//                 autoCorrect={false}
//                 underlineColorAndroid='#979797'
//               />
//             </View>
//             <View
//               style={styles.fieldView}
//             >
//               <FormLabel
//                 labelStyle={styles.formLabelContent}
//                 containerStyle={styles.formLabelContainer}
//               >
//                 {'Adresse'.toUpperCase()}
//               </FormLabel>
//               <FormInput
//                 placeholder={'Adresse'}
//                 inputStyle={themeElements.formInputText}
//                 containerStyle={styles.formInputContainer}
//                 onChangeText={(address) => { this.setState({ address }); }}
//                 value={this.state.address !== undefined ?
//                   this.state.address :
//                   recruiter.address}
//                 autoCorrect={false}
//                 underlineColorAndroid='#979797'
//               />
//             </View>
//             <View
//               style={styles.fieldView}
//             >
//               <FormLabel
//                 labelStyle={styles.formLabelContent}
//                 containerStyle={styles.formLabelContainer}
//               >
//                 {'Téléphone'.toUpperCase()}
//               </FormLabel>
//               <FormInput
//                 placeholder={'Téléphone'}
//                 keyboardType="phone-pad"
//                 returnKeyType="done"
//                 inputStyle={themeElements.formInputText}
//                 containerStyle={styles.formInputContainer}
//                 onChangeText={(phone) => { this.setState({ phone }); }}
//                 value={this.state.phone !== undefined ?
//                   this.state.phone :
//                   recruiter.phone}
//                 autoCorrect={false}
//                 underlineColorAndroid='#979797'
//               />
//             </View>
//           </View>
//           <Button
//             containerViewStyle={styles.validateView}
//             title="Valider les modifications"
//             color={theme.secondaryColor}
//             backgroundColor={theme.primaryColor}
//             buttonStyle={themeElements.validateSmallButton}
//             onPress={this.handleValidateModifications}
//           />
//         </Card>
//       </KeyboardAwareScrollView>
//     );
//   }
// }

// export default compose(
//   graphql(VIEWER_RECRUITER),
//   graphql(EDIT_INFOS, { name: 'editInfos' }),
// )(connectAlert(AccountInfosRecruiter));

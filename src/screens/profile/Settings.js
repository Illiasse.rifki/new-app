// import React, { Component } from 'react';
// import { ScrollView, StyleSheet, Text, Platform, TouchableOpacity } from 'react-native';
// import gql from 'graphql-tag';
// import Expo from 'expo';
// import { graphql, compose } from 'react-apollo';
// import styled from 'styled-components';

// import Drawer from '../../components/Content/Drawer';
// import Card from '../../components/Card/CardEditable';
// import FormSwitch from '../../components/Elements/FormSwitch';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// // import CheckNotifications from '../../components/CheckPermissions/CheckNotifications';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//     paddingTop: 24,
//   },
//   switchView: {
//     paddingLeft: 8,
//     paddingRight: 8,
//     marginTop: 13,
//   },
//   section: {
//     paddingTop: 30,
//     paddingBottom: 45,
//   },
//   lastSection: {
//     paddingTop: 30,
//     paddingBottom: 45,
//   },
// });

// const ContainerNotifications = styled.View`
//   position: relative;
// `;

// const CheckNotificationsAbsolute = styled.TouchableOpacity`
//   position: absolute;
//   background-color: rgba(50, 50, 50, 0.7);
//   top: 0;
//   bottom: 0;
//   left: 0;
//   right: 0;
//   flex: 1;
//   align-items: center;
//   justify-content: center;
//   ${p => (p.hide ? `
//   display: none;
//   ` : '')}
// `;

// const VIEWER_CANDIDATE = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         enable_private_message
//         enable_mail_recrute
//         enable_push_offer
//         enable_push_event
//         enable_push_private_message
//       }
//     }
//   }
// `;

// class Settings extends Component {
//   static navigationOptions = ({ navigation }) => ({
//     title: 'Configuration du compte',
//     headerTruncatedBackTitle: '',
//     headerLeft: <Drawer.Icon
//       onPress={() => navigation.navigate('DrawerOpen')}
//     />,
//   });

//   props: {
//     data: {
//       viewer: {
//         id: String,
//         candidate: {
//           enable_private_message: boolean,
//           enable_mail_recrute: boolean,
//           enable_push_offer: boolean,
//           enable_push_event: boolean,
//           enable_push_private_message: boolean,
//         },
//       },
//       error: boolean,
//       loading: boolean,
//     },
//   };

//   constructor(props) {
//     super(props);
//     this.state = {
//       notificationStatus: null,
//     };
//   }

//   switchSetting = (setting) => {
//     const { candidate } = this.props.data.viewer;
//     const newState = {};
//     newState[setting] =
//       this.state[setting] === undefined ? !candidate[setting] : !this.state[setting];
//     const variables = {
//       user_id: this.props.data.viewer.id,
//     };
//     variables[setting] = newState[setting];
//     this.props[setting]({
//       refetchQueries: [{
//         query: VIEWER_CANDIDATE,
//       }],
//       variables,
//     });
//     this.setState(newState);
//     switch (setting) {
//       case 'enable_push_private_message':
//         // OneSignal.sendTag('isMessages', newState[setting].toString());
//         break;
//       case 'enable_push_event':
//         // OneSignal.sendTag('isEvents', newState[setting].toString());
//         break;
//       default:
//     }
//   };

//   handleOnGetNotificationStatus = (status) => {
//     this.setState({ notificationStatus: status });
//   };

//   checkNotification = async () => {
//     let response = await Expo.Permissions.getAsync(Expo.Permissions.NOTIFICATIONS);
//     if (response.status !== 'granted') {
//       response = await Expo.Permissions.askAsync(Expo.Permissions.NOTIFICATIONS);
//     }
//     this.handleOnGetNotificationStatus(response.status);
//     return response.status;
//   };

//   getSetting(setting) {
//     const { candidate } = this.props.data.viewer;
//     return this.state[setting] !== undefined ? this.state[setting] : candidate[setting] || false;
//   }

//   render() {
//     if (this.props.data.error) {
//       return <ErrorComponent message="Problème lors de la récupération du profil." />;
//     } else if (this.props.data.loading) {
//       return <LoadingComponent />;
//     }

//     return (
//       <ScrollView
//         style={styles.background}
//         showsVerticalScrollIndicator={false}
//         showsHorizontalScrollIndicator={false} >
//         <Card title="Recevoir par mail" containerStyle={styles.section}>
//           <FormSwitch
//             label="Mes messages privés"
//             onPress={() => {
//               this.switchSetting('enable_private_message');
//             }}
//             containerStyle={styles.switchView}
//             value={this.getSetting('enable_private_message')}
//           />
//           <FormSwitch
//             label="Mail des nouveaux recruteurs"
//             onPress={() => {
//               this.switchSetting('enable_mail_recrute');
//             }}
//             containerStyle={styles.switchView}
//             value={this.getSetting('enable_mail_recrute')}
//           />
//         </Card>
//         <ContainerNotifications>
//           <Card title="Notifications push" containerStyle={styles.lastSection}>
//             {/* <FormSwitch
//               label="Nouvelles offres"
//               onPress={() => {
//                 this.switchSetting('enable_push_offer');
//               }}
//               containerStyle={styles.switchView}
//               value={this.getSetting('enable_push_offer')}
//             /> */}
//             <FormSwitch
//               label="Nouveaux événements"
//               onPress={() => {
//                 this.switchSetting('enable_push_event');
//               }}
//               containerStyle={styles.switchView}
//               value={this.getSetting('enable_push_event')}
//             />
//             <FormSwitch
//               label="Nouveaux messages privés"
//               onPress={() => {
//                 this.switchSetting('enable_push_private_message');
//               }}
//               containerStyle={styles.switchView}
//               value={this.getSetting('enable_push_private_message')}
//             />
//           </Card>
//           <CheckNotificationsAbsolute hide={this.state.notificationStatus === 'granted'} onPress={async () => this.checkNotification()}/>
//           {/* <CheckNotifications
//               onGetNotificationStatus={this.handleOnGetNotificationStatus}
//             />
//           </CheckNotificationsAbsolute> */}
//         </ContainerNotifications>
//       </ScrollView>
//     );
//   }
// }

// const ENABLE_SETTING = setting => (gql`
//   mutation($user_id: String!, $${setting}: Boolean) {
//     editCandidate(
//       user_id: $user_id,
//       ${setting}: $${setting},
//     ) {
//       error {
//         message
//       }
//     }
//   }
// `);

// const ENABLE_PRIVATE_MESSAGE = ENABLE_SETTING('enable_private_message');
// const ENABLE_MAIL_RECRUTE = ENABLE_SETTING('enable_mail_recrute');
// const ENABLE_PUSH_OFFER = ENABLE_SETTING('enable_push_offer');
// const ENABLE_PUSH_EVENT = ENABLE_SETTING('enable_push_event');
// const ENABLE_PUSH_PRIVATE_MESSAGE = ENABLE_SETTING('enable_push_private_message');

// export default compose(
//   graphql(VIEWER_CANDIDATE),
//   graphql(ENABLE_PRIVATE_MESSAGE, { name: 'enable_private_message' }),
//   graphql(ENABLE_MAIL_RECRUTE, { name: 'enable_mail_recrute' }),
//   graphql(ENABLE_PUSH_OFFER, { name: 'enable_push_offer' }),
//   graphql(ENABLE_PUSH_EVENT, { name: 'enable_push_event' }),
//   graphql(ENABLE_PUSH_PRIVATE_MESSAGE, { name: 'enable_push_private_message' }),
// )(Settings);

// import React, { Component } from 'react';
// import { View, Text, StyleSheet, ScrollView } from 'react-native';
// import { Button } from 'react-native-elements';
// import gql from 'graphql-tag';
// import { graphql, compose } from 'react-apollo';
// import { themeElements } from '../../config/constants/index';
// import connectAlert from '../../components/Alert/connectAlert';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import QualitiesSelector from '../../components/Content/QualitiesSelector';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: 'white',
//     paddingLeft: 32,
//     paddingRight: 32,
//     paddingTop: 21.5,
//     paddingBottom: 20.5,
//   },
//   titleView: {
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   qualitiesSelector: {
//     marginBottom: 45,
//   },
// });

// const QUERY_COMPANY = gql`
//   query ($company_id: String!) {
//     singleCompany(company_id: $company_id) {
//       qualities
//     }
//   }
// `;

// class CompanyTags extends Component {
//   static navigationOptions = {
//     title: 'Sélectionner vos qualities',
//     headerTruncatedBackTitle: '',
//   };

//   componentWillMount() {
//     if (!this.props.data.loading) {
//       const { qualities } = this.props.data.singleCompany;
//       this.setState({ qualities: qualities || [] });
//     }
//   }

//   componentDidUpdate(prevProps) {
//     if (
//       prevProps.data.loading
//       && !this.props.data.loading
//       && this.props.data.singleCompany
//     ) {
//       const { qualities } = this.props.data.singleCompany;
//       this.setState({ qualities: qualities || [] });
//     }
//   }

//   constructor(props) {
//     super(props);
//     this.state = {
//       qualities: [],
//     };
//   }

//   props: {
//     navigation: {
//       navigate: {},
//       state: {
//         params: {
//           edit: boolean,
//           buttonLabel: string,
//         }
//       },
//     },
//     editInfos: Function,
//     data: {
//       error: boolean,
//       loading: boolean,
//       singleCompany: {
//         qualities: [string]
//       },
//     },
//     dispatch: Function,
//     tags: [],
//     alertWithType: Function,
//   };

//   getEditOptions() {
//     const { state } = this.props.navigation;
//     const options = {
//       refetchQueries: [],
//       variables: {
//         qualities: this.state.qualities,
//       },
//     };
//     options.refetchQueries.push({
//       query: QUERY_COMPANY,
//       variables: {
//         company_id: state.params.companyId,
//       },
//     });
//     options.variables.company_id = state.params.companyId;
//     return options;
//   }

//   handleSubscribeButton = async () => {
//     const { goBack } = this.props.navigation;

//     if (this.state.qualities.length > 4 || this.state.qualities.length < 2) {
//       this.props.alertWithType('error', 'Selection incomplète', 'Vous devez sélectionner entre 2 et 4 qualités.');
//     } else {
//       await this.props.editInfos(this.getEditOptions());
//       this.props.alertWithType('success', 'Modifications effectuées', 'Les modifications ont bien été prises en compte');
//       goBack(null);
//     }
//   };

//   selectQuality = (quality) => {
//     const selected = this.state.qualities.indexOf(quality);
//     const newQualities = this.state.qualities.slice();
//     if (selected !== -1) {
//       newQualities.splice(selected, 1);
//     } else {
//       newQualities.push(quality);
//     }
//     this.setState({ qualities: newQualities });
//   };

//   render() {
//     if (this.props.data.error) {
//       return (<ErrorComponent message='Problème lors de la récupération des tags.' />);
//     }

//     return (
//       <View style={{ flex: 1 }}>
//         <ScrollView style={styles.background}>
//           <View style={styles.titleView}>
//             <Text style={themeElements.title}>Pour pouvoir vous recommander
//               les annonces le plus pertinentes, merci de sélectionner
//               vos qualités les plus importantes (entre 2 et 4) :
//             </Text>
//           </View>
//           <QualitiesSelector
//             selectedQualities={this.state.qualities}
//             onSelect={this.selectQuality}
//             containerStyle={styles.qualitiesSelector}
//           />
//         </ScrollView>
//         <Button
//           title='VALIDER'
//           buttonStyle={themeElements.bottomPageButton}
//           containerViewStyle={themeElements.bottomPageButtonContainer}
//           textStyle={themeElements.bottomPageButtonText}
//           onPress={this.handleSubscribeButton}
//         />
//       </View>
//     );
//   }
// }

// const EDIT_INFOS = gql`
//   mutation (
//     $company_id: String!,
//     $qualities: [String!],
//   )  {
//     editCompany(
//       company_id: $company_id,
//       qualities: $qualities,
//     ) {
//       error {
//         message
//       }
//     }
//   }
// `;

// export default compose(
//   graphql(QUERY_COMPANY, {
//     options: ownProps => ({
//       variables: {
//         company_id: ownProps.navigation.state.params.companyId,
//       },
//     }),
//   }),
//   graphql(EDIT_INFOS, { name: 'editInfos' }),
// )(connectAlert(CompanyTags));

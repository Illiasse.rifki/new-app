// import React, { Component } from "react";
// import { ScrollView, StyleSheet } from "react-native";
// import gql from "graphql-tag";
// import { graphql, compose } from "react-apollo";

// import Card from "../../components/Card/CardEditable";
// import FormSwitch from "../../components/Elements/FormSwitch";
// import ErrorComponent from "../../components/Elements/ErrorComponent";
// import LoadingComponent from "../../components/Elements/LoadingComponent";

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: "#f5f5f5",
//     paddingTop: 24,
//   },
//   switchView: {
//     paddingLeft: 8,
//     paddingRight: 8,
//     marginTop: 13,
//   },
//   section: {
//     paddingTop: 30,
//     paddingBottom: 45,
//   },
//   lastSection: {
//     paddingTop: 30,
//     paddingBottom: 45,
//     marginBottom: 50,
//   },
// });

// const VIEWER_RECRUITER = gql`
//   query {
//     viewer {
//       id
//       recruiter {
//         enable_mail_from_canditate
//         enable_pushs
//       }
//     }
//   }
// `;

// class SettingsRecruiter extends Component {
//   static navigationOptions = ({ navigation }) => ({
//     title: "Alertes et notifications",
//     headerTruncatedBackTitle: "",
//   });

//   props: {
//     data: {
//       viewer: {
//         id: String,
//         candidate: {
//           enable_mail_from_canditate: boolean,
//           enable_pushs: boolean,
//         },
//       },
//       error: boolean,
//       loading: boolean,
//     },
//   };

//   constructor(props) {
//     super(props);
//     this.state = {};
//   }

//   switchSetting = (setting) => {
//     const { recruiter } = this.props.data.viewer;
//     const newState = {};
//     newState[setting] =
//       this.state[setting] === undefined
//         ? !recruiter[setting]
//         : !this.state[setting];
//     const variables = {
//       user_id: this.props.data.viewer.id,
//     };
//     variables[setting] = newState[setting];
//     this.props[setting]({
//       refetchQueries: [
//         {
//           query: VIEWER_RECRUITER,
//         },
//       ],
//       variables,
//     });
//     this.setState(newState);
//     switch (setting) {
//       case "enable_push_private_message":
//         // OneSignal.sendTag('isMessages', newState[setting].toString());
//         break;
//       case "enable_push_event":
//         // OneSignal.sendTag('isEvents', newState[setting].toString());
//         break;
//       default:
//     }
//   };

//   getSetting(setting) {
//     const { recruiter } = this.props.data.viewer;
//     return this.state[setting] !== undefined
//       ? this.state[setting]
//       : recruiter[setting] || false;
//   }

//   render() {
//     if (this.props.data.error) {
//       return (
//         <ErrorComponent message="Problème lors de la récupération du profil." />
//       );
//     } else if (this.props.data.loading) {
//       return <LoadingComponent />;
//     }

//     return (
//       <ScrollView
//         style={styles.background}
//         showsVerticalScrollIndicator={false}
//         showsHorizontalScrollIndicator={false}
//       >
//         <Card title="Recevoir par mail" containerStyle={styles.section}>
//           <FormSwitch
//             label="Mail des nouveaux candidats"
//             onPress={() => {
//               this.switchSetting("enable_mail_from_canditate");
//             }}
//             containerStyle={styles.switchView}
//             value={this.getSetting("enable_mail_from_canditate")}
//           />
//           <FormSwitch
//             label="Mes autres notifications"
//             onPress={() => {
//               this.switchSetting("enable_pushs");
//             }}
//             containerStyle={styles.switchView}
//             value={this.getSetting("enable_pushs")}
//           />
//         </Card>
//       </ScrollView>
//     );
//   }
// }

// const ENABLE_SETTING = (setting) => gql`
//   mutation($user_id: String!, $${setting}: Boolean) {
//     editRecruiter(
//       user_id: $user_id,
//       ${setting}: $${setting},
//     ) {
//       error {
//         message
//       }
//     }
//   }
// `;

// const ENABLE_MAIL_FROM_CANDIDATES = ENABLE_SETTING(
//   "enable_mail_from_canditate"
// );
// const ENABLE_PUSHS = ENABLE_SETTING("enable_pushs");

// export default compose(
//   graphql(VIEWER_RECRUITER),
//   graphql(ENABLE_MAIL_FROM_CANDIDATES, { name: "enable_mail_from_canditate" }),
//   graphql(ENABLE_PUSHS, { name: "enable_pushs" })
// )(SettingsRecruiter);

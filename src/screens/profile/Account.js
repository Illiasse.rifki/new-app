// import React, { Component } from 'react';
// import { ScrollView, StyleSheet } from 'react-native';
// import { NavigationActions } from 'react-navigation';
// import { Icon, List, ListItem } from 'react-native-elements';
// import { withApollo } from 'react-apollo';

// import { theme } from './../../config/constants';
// import { client } from '../../utils/Chat';
// import global from './../../config/global';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//     paddingBottom: 19,
//     paddingTop: 24,
//   },
//   list: {
//     marginTop: 0,
//     borderTopWidth: 0,
//     marginBottom: 40,
//   },
//   listItem: {
//     height: 72,
//     justifyContent: 'center',
//     backgroundColor: 'white',
//     borderBottomColor: '#f5f5f5',
//     borderBottomWidth: 4,
//   },
//   listItemTitle: {
//     fontSize: 18,
//     fontWeight: '500',
//     color: theme.fontBlack,
//   },
// });

// const rightIcon = {
//   type: 'simple-line-icon',
//   name: 'arrow-right',
//   style: { fontSize: 18, marginRight: 10 },
// };

// class Account extends Component {
//   static navigationOptions = {
//     title: 'Votre profil',
//     headerTruncatedBackTitle: 'Profil',
//     headerLeft: null,
//     tabBarLabel: 'Mon Compte',
//     tabBarIcon: ({ tintColor }) => (
//       <Icon size={20} name="user" type="font-awesome" color={tintColor || theme.gray} />
//     ),
//   };

//   static navigationList = [
//     { key: 'Settings', name: 'Configuration du compte' },
//     { key: 'QRCodeCandidate', name: 'Votre QRCode' },
//     // { key: 'Favorites', name: 'Modifier vos favoris' },
//     { key: 'LegalMentions', name: 'Mentions légales' },
//     { key: 'AccountInfos', name: 'Identifiants et coordonnées' },
//     // { key: 'FAQCandidate', name: 'Foire Aux Questions' },
//   ];

//   state = {
//     disconnect: false,
//   };

//   props: {
//     navigation: {
//       dispatch: Function,
//       NavigationActions: Function,
//       navigate: Function,
//     },
//     client: {
//       resetStore: Function,
//     },
//   };

//   componentWillUnmount() {
//     if (this.state.disconnect) {
//       // if (client && client.destroy) { client.destroy(); }
//       this.props.client.resetStore();
//     }
//   }

//   handleOnClickDisconnect = () => {
//     global.authStockage.setToken('access-token', null);
//     global.authStockage.setToken('recruiter-access-token', null);
//     const resetAction = NavigationActions.reset({
//       index: 0,
//       key: null,
//       actions: [NavigationActions.navigate({ routeName: 'OnBoarding' })],
//     });
//     this.props.navigation.dispatch(resetAction);
//     this.setState({ disconnect: true });
//   };

//   render() {
//     const { navigate } = this.props.navigation;
//     const hasRecruiterToken = global.authStockage.getToken('recruiter-access-token') || null;
//     return (
//       <ScrollView
//         style={styles.background}
//         showsVerticalScrollIndicator={false}
//         showsHorizontalScrollIndicator={false} >
//         <List containerStyle={styles.list}>
//           {Account.navigationList.map(item => (
//             <ListItem
//               key={item.key}
//               title={item.name}
//               containerStyle={styles.listItem}
//               titleStyle={styles.listItemTitle}
//               rightIcon={rightIcon}
//               onPress={() => {
//                 navigate(item.key);
//               }}
//             />
//           ))}
//           {
//             hasRecruiterToken ? (
//               <ListItem
//                 title="Passer en tant que recruteur"
//                 containerStyle={styles.listItem}
//                 titleStyle={styles.listItemTitle}
//                 rightIcon={rightIcon}
//                 onPress={() => {
//                   const resetAction = NavigationActions.reset({
//                     index: 0,
//                     key: null,
//                     actions: [
//                       NavigationActions.navigate({ routeName: 'RecruiterTabViews' }),
//                     ],
//                   });
//                   this.props.navigation.dispatch(resetAction);
//                 }}
//               />
//             ) : null
//           }
//           <ListItem
//             title="Déconnexion"
//             containerStyle={styles.listItem}
//             titleStyle={styles.listItemTitle}
//             rightIcon={rightIcon}
//             onPress={this.handleOnClickDisconnect}
//           />
//         </List>
//       </ScrollView>
//     );
//   }
// }

// export default withApollo(Account);

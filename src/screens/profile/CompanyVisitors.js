// import React, { Component } from 'react';
// import { ScrollView, Text, StyleSheet, RefreshControl } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';
// import { theme } from '../../config/constants/index';
// import CompanyVisitList from '../../components/Company/CompanyVisitList';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';

// const styles = StyleSheet.create({
//   main: { backgroundColor: '#f5f5f5' },
//   mainContent: { marginVertical: 33.5, marginHorizontal: 16 },
//   title: { color: theme.fontBlack, fontSize: 28, textAlign: 'left', fontWeight: 'bold', marginBottom: 0 },
//   visitsNb: {
//     color: theme.fontBlack,
//     fontSize: 14,
//     textAlign: 'left',
//     fontWeight: '100',
//     lineHeight: 24,
//     marginBottom: 17,
//     borderRadius: 4,
//     backgroundColor: 'white',
//     paddingVertical: 4,
//     paddingHorizontal: 8,
//     marginTop: 8,
//   },
// });

// class CompanyVisitors extends Component {
//   static navigationOptions = {
//     title: 'Les visites de l\'entreprise',
//   };

//   state = {
//     refreshing: false,
//     searchText: '',
//   };

//   shouldComponentUpdate(nextProps) {
//     const { route } = nextProps.screenProps;
//     const { routeName } = this.props.navigation.state;
//     return route === routeName || route === '';
//   }

//   onRefresh = () => {
//     this.setState({ refreshing: true });
//     this.props.data.refetch().then(() => {
//       this.setState({ refreshing: false });
//     }).catch(e => console.log('catch refetch Events.js', e));
//   };

//   props: {
//     navigation: {
//       navigate: Function,
//       state: { params: {
//           jobOfferId:string
//         }, routeName: string },
//     },
//     screenProps: { route: string },
//     data: {
//       error: {},
//       loading: boolean,
//       refetch: Function,
//       viewer: {
//         recruiter: {
//           company: {
//             view_counter: string,
//           },
//         },
//       },
//     },
//   };

//   purifyData = (): Array<Object> => {
//     const { jobOffer } = this.props.data;
//     if (!jobOffer || !jobOffer.registrations) {
//       return [];
//     }

//     return jobOffer.registrations.map(r => ({
//       id: r.job_offer_registration_id,
//       candidate: {
//         ...r.candidate,
//         status: r.status,
//         nbviews: r.nbviews,
//       },
//       years_experience: r.candidate.years_experience,
//       experiences: r.candidate.experience,
//     }));
//   };

//   render() {
//     if (this.props.data.error) {
//       return <ErrorComponent message="Problème lors de la récupération des visiteurs." />;
//     } else if (this.props.data.loading) {
//       return <LoadingComponent />;
//     }

//     const { company } = this.props.data.viewer.recruiter;
//     const uniqVisits = company.visitors.length;

//     return (
//       <ScrollView
//         style={styles.main}
//         contentContainerStyle={styles.mainContent}
//         refreshControl={<RefreshControl
//           refreshing={this.state.refreshing}
//           onRefresh={this.onRefresh}
//         />}
//       >
//         <Text style={styles.title}>Liste des visiteurs</Text>
//         <Text style={styles.visitsNb} >{`${company.view_counter} visite${company.view_counter > 1 ? 's' : ''} total dont ${uniqVisits} visiteur${uniqVisits > 1 ? 's' : ''} unique${uniqVisits > 1 ? 's' : ''}`}</Text>
//         <CompanyVisitList
//           navigate={this.props.navigation.navigate}
//           visitors={company.visitors}
//         />
//       </ScrollView>
//     );
//   }
// }

// const VIEWER_COMPANY_VISITOR = gql`
//   query {
//     viewer {
//       id
//       recruiter {
//         company {
//           name
//           visitors {
//             company_visitor_id
//             candidate {
//               user_id
//               fullname
//               firstname
//               lastname
//               headline
//               picture
//             }
//             nbviews
//           }
//           view_counter
//         }
//       }
//     }
//   }
// `;

// export default graphql(VIEWER_COMPANY_VISITOR)(CompanyVisitors);

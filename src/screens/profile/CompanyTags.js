// import React, { Component } from 'react';
// import { View, Text, StyleSheet, ScrollView } from 'react-native';
// import { Button } from 'react-native-elements';
// import gql from 'graphql-tag';
// import { graphql, compose } from 'react-apollo';
// import { themeElements } from '../../config/constants/index';
// import connectAlert from '../../components/Alert/connectAlert';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import TagsSelector from '../../components/Content/TagsSelector';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: 'white',
//     paddingLeft: 32,
//     paddingRight: 32,
//     paddingTop: 21.5,
//     paddingBottom: 20.5,
//   },
//   titleView: {
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   tagSelector: {
//     marginBottom: 45,
//   },
// });

// const QUERY_COMPANY = gql`
//   query ($company_id: String!) {
//     singleCompany(company_id: $company_id) {
//       tags
//     }
//   }
// `;

// class CompanyTags extends Component {
//   static navigationOptions = {
//     title: 'Sélectionner vos tags',
//     headerTruncatedBackTitle: '',
//   };

//   componentWillMount() {
//     if (!this.props.data.loading) {
//       const { tags } = this.props.data.singleCompany;
//       this.setState({ tags: tags || [] });
//     }
//   }

//   componentDidUpdate(prevProps) {
//     if (
//       prevProps.data.loading
//       && !this.props.data.loading
//       && this.props.data.singleCompany
//     ) {
//       const { tags } = this.props.data.singleCompany;
//       this.setState({ tags: tags || [] });
//     }
//   }

//   constructor(props) {
//     super(props);
//     this.state = {
//       tags: [],
//     };
//   }

//   props: {
//     navigation: {
//       navigate: {},
//       state: {
//         params: {
//           edit: boolean,
//           buttonLabel: string,
//         }
//       },
//     },
//     editInfos: Function,
//     data: {
//       error: boolean,
//       loading: boolean,
//       singleCompany: {
//         tags: [string]
//       },
//     },
//     dispatch: Function,
//     tags: [],
//     alertWithType: Function,
//   };

//   getEditOptions() {
//     const { state } = this.props.navigation;
//     const options = {
//       refetchQueries: [],
//       variables: {
//         tags: this.state.tags,
//       },
//     };
//     options.refetchQueries.push({
//       query: QUERY_COMPANY,
//       variables: {
//         company_id: state.params.companyId,
//       },
//     });
//     options.variables.company_id = state.params.companyId;
//     return options;
//   }

//   handleSubscribeButton = async () => {
//     const { goBack } = this.props.navigation;

//     if (this.state.tags.length > 4 || this.state.tags.length < 1) {
//       this.props.alertWithType('error', 'Selection incomplète', 'Vous devez sélectionner entre 1 et 4 tags.');
//     } else {
//       await this.props.editInfos(this.getEditOptions());
//       this.props.alertWithType('success', 'Modification effectuées', 'Les modifications ont bien été prises en compte');
//       goBack(null);
//     }
//   };

//   selectTag = (tag) => {
//     const selected = this.state.tags.indexOf(tag);
//     const newTags = this.state.tags.slice();
//     if (selected !== -1) {
//       newTags.splice(selected, 1);
//     } else {
//       newTags.push(tag);
//     }
//     this.setState({ tags: newTags });
//   };

//   render() {
//     if (this.props.data.error) {
//       return (<ErrorComponent message='Problème lors de la récupération des tags.' />);
//     }

//     const { state } = this.props.navigation;
//     return (
//       <View style={{ flex: 1 }}>
//         <ScrollView style={styles.background}>
//           <View style={styles.titleView}>
//             <Text style={themeElements.title}>Pour pouvoir vous recommander
//               les annonces le plus pertinentes,
//               merci de sélectionner les tags les plus importants (1 à 4) :
//             </Text>
//           </View>
//           <TagsSelector
//             selectedTags={this.state.tags}
//             onSelect={this.selectTag}
//             containerStyle={styles.tagSelector}
//           />
//         </ScrollView>
//         <Button
//           title='VALIDER'
//           buttonStyle={themeElements.bottomPageButton}
//           containerViewStyle={themeElements.bottomPageButtonContainer}
//           textStyle={themeElements.bottomPageButtonText}
//           onPress={this.handleSubscribeButton}
//         />
//       </View>
//     );
//   }
// }

// const EDIT_INFOS = gql`
//   mutation (
//     $company_id: String!,
//     $tags: [String!],
//   )  {
//     editCompany(
//       company_id: $company_id,
//       tags: $tags,
//     ) {
//       error {
//         message
//       }
//     }
//   }
// `;

// export default compose(
//   graphql(QUERY_COMPANY, {
//     options: ownProps => ({
//       variables: {
//         company_id: ownProps.navigation.state.params.companyId,
//       },
//     }),
//   }),
//   graphql(EDIT_INFOS, { name: 'editInfos' }),
// )(connectAlert(CompanyTags));

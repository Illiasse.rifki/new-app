import React, { Component } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import FAQ from '../../components/Content/FAQ';
import Drawer from '../../components/Content/Drawer';

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: '#f5f5f5',
    paddingTop: 24,
  },
  faqContainer: {
    paddingTop: 0,
    paddingBottom: 0,
    marginBottom: 25,
  },
  questionsContainer: {
    marginTop: 0,
    borderTopColor: '#f5f5f5',
    borderTopWidth: 4,
  },
  questionContainerStyle: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 25.5,
    paddingBottom: 25.5,
    borderBottomColor: '#f5f5f5',
    borderBottomWidth: 4,
  },
  questionTextStyle: {
    fontSize: 18,
    fontWeight: '500',
    lineHeight: 24,
  },
});

class FAQCandidate extends Component {
  // static navigationOptions = ({ navigation }) => ({
  //   title: 'Foire Aux Questions',
  //   headerTruncatedBackTitle: 'FAQ',
  //   headerLeft: <Drawer.Icon
  //     onPress={() => navigation.navigate('DrawerOpen')}
  //   />,
  // });

  static questions = [
    {
      question: 'J’ai vu une offre d’emploi intéressante, comment y postuler ?',
      answer: 'Il vous suffit de cliquer sur l’onglet « postuler » en bas de page sur l’offre en question. Les informations que vous avez renseigné sur votre profil seront utilisées pour vous permettre de postuler en quelques clics.',
    },
    {
      question: 'Je souhaite rentrer en contact avec un recruteur via l’application, comment faire ?',
      answer: 'Vous pouvez contacter directement l\'ensemble de nos partenaires, en cliquant sur le bouton « chat » sur leur fiche entreprise. Si un recruteur souhaite rentrer en contact avec vous, vous recevrez une notification qui sera affichée dans votre barre d’état en bas de l’écran dans l’onglet « chat ».',
    },
    {
      question: 'Comment savoir quelles entreprises proposent des offres sur l’application ?',
      answer: 'Il y a une liste de nos entreprises partenaires présente sur l’application que vous pourrez consulter dans l’onglet « entreprises partenaires ».',
    },
    {
      question: 'J’ai téléchargé le mauvais CV lors de mon inscription et je voudrais le retirer.',
      answer: 'Il vous est possible de modifier votre profil directement depuis l’application. Il vous suffira donc de supprimer le mauvais fichier téléchargé et de charger le nouveau.',
    },
    {
      question: 'Puis-je postuler à une offre ou un événement sans déposer de CV ?',
      answer: 'Vous avez la possibilité de postuler à l’un et l’autre si vous avez renseigné au moins une expérience professionnelle et une formation sur votre profil. Toutefois, il est conseillé de remplir avec attention votre profil pour offrir des informations complètes aux recruteurs.',
    },
    {
      question: 'Une entreprise m’intéresse particulièrement, comment avoir accès à toutes les offres proposées par cette dernière ?',
      answer: 'Vous avez la possibilité d’avoir accès à la « Fiche entreprise » de chacune des entreprises qui proposent des offres sur Bizzeo. En accédant à cette fiche entreprises, vous trouverez les offres proposées uniquement par cette entreprise.',
    },
  ];

  render() {
    return (
      <ScrollView
        style={styles.background}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false} >
        <FAQ
          faq={FAQCandidate.questions}
          containerStyle={styles.faqContainer}
          questionsContainerStyle={styles.questionsContainer}
          questionContainerStyle={styles.questionContainerStyle}
          questionTextStyle={styles.questionTextStyle}
        />
      </ScrollView>
    );
  }
}

export default FAQCandidate;

import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ExperienceEdit from '../../components/GQLLinked/ExperienceEdit';
import { CANDIDATE_EXPERIENCES } from './CandidateProfileExperiences';
import { QUERY_PROFILE_CANDIDATE } from '../../components/GQLLinked/Profile';
import Loader from '../../components/Loader/Loader';

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: 'white',
    paddingBottom: 16,
    paddingTop: 34,
    paddingLeft: 8.25,
    paddingRight: 8.25,
  },
});

class CandidateProfileAddExperience extends Component {
  static navigationOptions = {
    title: "Ajout d'une expérience",
    headerTruncatedBackTitle: '',
  };

  props: {
    navigation: {
      goBack: Function,
      state: {
        params: {
          userId: string,
          register: boolean
        },
      },
    },
  };

  handleAddExperienceButton = () => {
    const { state, goBack, navigate } = this.props.navigation;
    if (state.params.register) {
      navigate('CandidateFormLastEducation', { transition: 'noTransition', register: true, userId: state.params.userId });
    } else {
      goBack(null);
    }
  };

  render() {
    const { state } = this.props.navigation;

    return (
      <KeyboardAwareScrollView
        style={{ flex: 1, backgroundColor: 'white' }}
        keyboardShouldPersistTaps="always"
      >
        {
          this.props.register &&
          <Loader
            width={this.props.progress}
            start={this.props.progress}
          />
        }
        <View style={styles.background}>
          <ExperienceEdit
            userId={state.params.userId}
            refetchQueries={[{
              query: CANDIDATE_EXPERIENCES,
              variables: { user_id: state.params.userId },
            }, {
              query: QUERY_PROFILE_CANDIDATE,
              variables: { user_id: state.params.userId },
            }]}
            onAddExperience={this.handleAddExperienceButton}
          />
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

export default CandidateProfileAddExperience;

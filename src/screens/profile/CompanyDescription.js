// import React, { Component } from 'react';
// import {
//   View,
//   StyleSheet,
//   ScrollView,
// } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql, compose } from 'react-apollo';
// import { Button, FormInput } from 'react-native-elements';
// import { theme, themeElements } from '../../config/constants/index';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import connectAlert from '../../components/Alert/connectAlert';
// import CardEditable from '../../components/Card/CardEditable';
// import { QUERY_COMPANY } from '../../components/GQLLinked/ProfileCompany';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//     paddingBottom: 24,
//     paddingTop: 24,
//   },
//   idLabelContainer: {
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   idLabelContent: {
//     marginRight: 0,
//     marginLeft: 0,
//     fontSize: 18,
//     color: theme.fontBlack,
//     lineHeight: 24,
//     fontWeight: '500',
//   },
//   formInputContainer: {
//     paddingBottom: 8,
//     marginTop: -5,
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   validateView: {
//     marginTop: 20,
//   },
//   inputView: {
//     marginTop: 20,
//     marginLeft: 8,
//     marginRight: 8,
//     marginBottom: 20,
//   },
// });

// const VIEWER_RECRUITER = gql`
//   query {
//     viewer {
//       id
//       recruiter {
//         company {
//           company_id
//           description
//         }
//       }
//     }
//   }
// `;

// class CompanyDescription extends Component {
//   static navigationOptions = {
//     title: 'Modification de la descriptio',
//     headerTruncatedBackTitle: 'Modif',
//   };

//   constructor(props) {
//     super(props);
//     this.state = {
//     };
//   }

//   props: {
//     navigation: {
//       navigate: Function,
//       state: Function,
//       goBack: Function,
//     },
//     editInfos: Function,
//     alertWithType: Function,
//     data: {
//       error: string,
//       viewer: {
//         id: string,
//         recruiter: {
//           company: {
//             company_id: string,
//             description: string,
//           },
//         },
//       },
//       loading: boolean,
//     },
//   };

//   handleValidateModification = () => {
//     const variables = {
//       company_id: this.props.data.viewer.recruiter.company.company_id,
//       description: this.state.description,
//     };
//     this.props.editInfos({
//       refetchQueries: [{
//         query: VIEWER_RECRUITER,
//       }, {
//         query: QUERY_COMPANY,
//         variables: {
//           company_id: this.props.data.viewer.recruiter.company.company_id,
//         },
//       }],
//       variables,
//     }).then(() => {
//       this.props.alertWithType('success', 'Modifications effectuées', 'Les modifications ont bien été prises en compte');
//       this.props.navigation.goBack(null);
//     }).catch((e) => {
//       this.props.alertWithType('error', 'Erreur', e.message);
//     });
//   };

//   render() {
//     if (this.props.data.error) {
//       return (<ErrorComponent message="Problème lors de la récupération du champs." />);
//     } else if (this.props.data.loading) {
//       return (<LoadingComponent />);
//     }

//     const { company } = this.props.data.viewer.recruiter;
//     return (
//       <ScrollView style={styles.background} keyboardShouldPersistTaps="always">
//         <CardEditable
//           title="Qui sommes-nous ?"
//         >
//           <View style={styles.inputView}>
//             <FormInput
//               placeholder="Description de l'entreprise"
//               inputStyle={themeElements.formInputText}
//               containerStyle={styles.formInputContainer}
//               value={this.state.description === undefined ?
//                 company.description : this.state.description}
//               onChangeText={(description) => {
//                 this.setState({ description });
//               }}
//               multiline
//               autoCorrect
//               underlineColorAndroid='#979797'
//             />
//           </View>
//           <Button
//             containerViewStyle={styles.validateView}
//             title="Valider"
//             color={theme.secondaryColor}
//             backgroundColor={theme.primaryColor}
//             buttonStyle={themeElements.validateSmallButton}
//             onPress={this.handleValidateModification}
//           />
//         </CardEditable>
//       </ScrollView>
//     );
//   }
// }

// const EDIT_INFOS = gql`
//   mutation ($company_id: String!, $description: String) {
//     editCompany(
//       company_id: $company_id,
//       description: $description,
//     ) {
//       company {
//         company_id
//       }
//     }
//   }
// `;

// export default compose(
//   graphql(VIEWER_RECRUITER),
//   graphql(EDIT_INFOS, { name: 'editInfos' }),
// )(connectAlert(CompanyDescription));

import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import LanguageEdit from '../../components/GQLLinked/LanguageEdit';
import { CANDIDATE_LANGUAGES } from './CandidateProfileLanguages';

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: 'white',
    paddingBottom: 16,
    paddingTop: 20,
    paddingLeft: 8.25,
    paddingRight: 8.25,
  },
});

class CandidateProfileEditLangage extends Component {
  static navigationOptions = {
    title: "Edition d'une langue",
    headerTruncatedBackTitle: '',
  };

  props: {
    navigation: {
      goBack: Function,
      state: {
        params: {
          userId: string,
        },
      },
    },
  };

  handleButton = () => {
    const { goBack } = this.props.navigation;
    goBack(null);
  };

  render() {
    const { state } = this.props.navigation;

    return (
      <KeyboardAwareScrollView style={styles.background}>
        <LanguageEdit
          languageId={state.params.languageId}
          refetchQueries={[{
            query: CANDIDATE_LANGUAGES,
            variables: { user_id: state.params.userId },
          }]}
          onEditLanguage={this.handleButton}
          onDeleteLanguage={this.handleButton}
        />
      </KeyboardAwareScrollView>
    );
  }
}

export default CandidateProfileEditLangage;

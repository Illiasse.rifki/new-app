import React, { Component } from 'react';
import { ScrollView, Text, StyleSheet, View } from 'react-native';
import { theme, themeElements } from '../../config/constants/index';
import Url from '../../components/Content/Url';
import Drawer from '../../components/Content/Drawer';
import { version } from '../../../package.json';

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: '#f5f5f5',
    paddingTop: 24,
  },
  section: {
    backgroundColor: theme.secondaryColor,
    paddingTop: 37.5,
    paddingBottom: 20,
    marginBottom: 50,
  },
  noSepBloc: {
    paddingLeft: 32,
    paddingRight: 32,
  },
  bloc: {
    paddingLeft: 32,
    paddingRight: 32,
    borderTopColor: '#f5f5f5',
    borderTopWidth: 1,
    paddingBottom: 14.5,
    paddingTop: 13,
  },
  text: {
    fontSize: 14,
    lineHeight: 20,
    color: theme.fontBlack,
  },
  society: {
    marginTop: 20,
    marginBottom: 18.5,
  },
  url: {
    justifyContent: 'flex-start',
  },
});

class LegalMentions extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Mentions légales',
    headerTruncatedBackTitle: '',
    headerLeft: <Drawer.Icon
      onPress={() => navigation.navigate('DrawerOpen')}
    />,
  });

  render() {
    let env = ((process.env.NODE_ENV || '').toLowerCase() === 'production') ? '' : ' - dev mode';
    return (
      <ScrollView
        style={styles.background}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false} >
        <View style={styles.section}>
          <View style={styles.noSepBloc}>
            <Text style={themeElements.bigTitle}>Mentions légales</Text>
            <Text style={[styles.text, styles.society]}>
              Bizzeo Connect est une marque de la société CARREVOLUTIS, Société à Responsabilité
              Limitée au capital de 470 000 Euros. La société est enregistrée au registre du
              Commerce et des Sociétés de Paris sous le numéro B 513 473 835. TVA Intracommunautaire
              FR1951347383500033.
            </Text>
          </View>
          <View style={styles.bloc}>
            <Text style={themeElements.title}>Siège social</Text>
            <Text style={[styles.text]}>
              {"59 rue de l'ancienne mairie, 92100 Boulogne Billancourt - France\nMail : "}
            </Text>
            <Url containerStyle={styles.url} name="contact@bizzeo.co" url="mailto:contact@bizzeo.co" />
            <Text style={[styles.text]}>
              {'\nTéléphone : '}
            </Text>
            <Url containerStyle={styles.url} name="01 85 08 35 39" url="tel:0185083539" />
          </View>
          <View style={styles.bloc}>
            <Text style={themeElements.title}>Site web hébergé par</Text>
            <Text style={[styles.text]}>{'OVH\n2 rue Kellermann - 59100 Roubaix'}</Text>
          </View>
          <View style={styles.bloc}>
            <Text style={[styles.text]}>
              {"Les informations susceptibles d'être collectées sur le présent site sont exclusivement destinées " +
                'au traitement de votre demande par Bizzeo.co.\n' +
                '\n' +
                "Vous disposez d'un droit d'accès, de modification, de rectification et de suppression des données qui " +
                'vous concernent (art. 34 de la loi Informatique et Libertés).\n' +
                '\n' +
                "Plus d'informations sur "}
            </Text>
            <Url containerStyle={styles.url} name="www.cnil.fr" url="http://www.cnil.fr" />
            <Text style={[styles.text]}>
              {'\n' + 'Vous pouvez exercer ce droit en nous écrivant par mail à : '}
            </Text>
            <Url containerStyle={styles.url} name="contact@bizzeo.co" url="mailto:contact@bizzeo.co" />
            <Text containerStyle={styles.url} style={[styles.text]}>
              {" ou à l'adresse suivante :\n" +
                '\n' +
                'Carrévolutis\n' +
                "59 rue de l'ancienne Mairie - 92100 Boulogne\n" +
                '\n' +
                'Directeur de la publication : Alexandre Branche, Sébastien Guichard'}
            </Text>
          </View>
          <View style={styles.bloc}>
            <Text>
              {`Version ${version}${env}`}
            </Text>
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default LegalMentions;

// import React, { Component } from 'react';
// import {
//   StyleSheet,
//   ScrollView,
// } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql, compose } from 'react-apollo';
// import { Button } from 'react-native-elements';
// import { theme, themeElements } from '../../config/constants/index';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import connectAlert from '../../components/Alert/connectAlert';
// import Card from '../../components/Card/CardEditable';
// import Field from '../../components/Design/Field';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//     paddingBottom: 24,
//     paddingTop: 24,
//   },
//   idLabelContainer: {
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   idLabelContent: {
//     marginRight: 0,
//     marginLeft: 0,
//     fontSize: 18,
//     color: theme.fontBlack,
//     lineHeight: 24,
//     fontWeight: '500',
//   },
//   formInputContainer: {
//     marginTop: -5,
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   validateView: {
//     marginTop: 20,
//   },
//   inputView: {
//     marginBottom: 20,
//   },
// });

// const VIEWER_RECRUITER = gql`
//   query {
//     viewer {
//       id
//       recruiter {
//         email
//       }
//     }
//   }
// `;

// class ImportantFieldModificationRecruiter extends Component {
//   static navigationOptions = ({ navigation }) => ({
//     title: `Modification ${navigation.state.params.display.toLowerCase()}`,
//     headerTruncatedBackTitle: 'Modif',
//   });

//   state = {
//     confirmationField: '',
//     oldField: '',
//   };

//   props: {
//     navigation: {
//       navigate: Function,
//       state: Function,
//       goBack: Function,
//     },
//     editInfos: Function,
//     data: {
//       error: string,
//       viewer: {
//         id: string,
//         recruiter: {
//           email: string,
//           password: string,
//         },
//       },
//       loading: boolean,
//     },
//     alertWithType: Function,
//     editPassword: Function,
//   };

//   handleValidateModification = () => {
//     if (!(this.state.field === this.state.confirmationField)) {
//       this.props.alertWithType('error', 'Error', 'Le deux champs doivent être identiques');
//       return;
//     }
//     const { state } = this.props.navigation;
//     const variables = {};
//     variables[state.params.field] = this.state.confirmationField;
//     if (state.params.secure) {
//       variables[`old_${state.params.field}`] = this.state.oldField;
//     }
//     const mutation = state.params.field === 'password' ? 'editPassword' : 'editInfos';
//     this.props[mutation]({
//       refetchQueries: [{
//         query: VIEWER_RECRUITER,
//       }],
//       variables,
//     }).then((ret) => {
//       if (ret.data.edit.error) {
//         this.props.alertWithType('error', 'Erreur', ret.data.edit.error.message);
//       } else {
//         this.props.alertWithType('success', 'Info', 'Les modifications ont bien été prises en compte');
//         this.props.navigation.goBack(null);
//       }
//     }).catch((e) => {
//       this.props.alertWithType('error', 'Error', e.message);
//     });
//   };

//   render() {
//     const { state } = this.props.navigation;

//     if (this.props.data.error) {
//       return (<ErrorComponent message="Problème lors de la récupération du champs." />);
//     } else if (this.props.data.loading) {
//       return (<LoadingComponent />);
//     }

//     return (
//       <ScrollView
//         style={styles.background}
//         keyboardShouldPersistTaps="always"
//         showsVerticalScrollIndicator={false}
//         showsHorizontalScrollIndicator={false}
//         >
//         <Card
//           containerStyle={{
//             paddingLeft: 32,
//             paddingRight: 32,
//           }}
//         >
//           {
//             state.params.secure && <Field
//               placeholder={state.params.display}
//               label={`Ancien ${state.params.display.toLowerCase()}`}
//               labelStyle="dark"
//               containerStyle={styles.inputView}
//               inputContainer={styles.formInputContainer}
//               inputStyle={themeElements.formInputText}
//               value={this.state.oldField}
//               onChangeText={(oldField) => {
//                 this.setState({ oldField });
//               }}
//               textInputOptions={{
//                 secureTextEntry: state.params.secure,
//                 autoCorrect: false,
//                 autoCapitalize: 'none',
//               }}
//             />
//           }
//           <Field
//             placeholder={state.params.display}
//             label={state.params.display}
//             labelStyle="dark"
//             containerStyle={styles.inputView}
//             inputContainer={styles.formInputContainer}
//             inputStyle={themeElements.formInputText}
//             value={this.state.field !== undefined ?
//               this.state.field :
//               this.props.data.viewer.recruiter[state.params.field]}
//             onChangeText={(field) => {
//               this.setState({ field });
//             }}
//             textInputOptions={{
//               secureTextEntry: state.params.secure,
//               autoCorrect: false,
//               autoCapitalize: 'none',
//             }}
//           />
//           <Field
//             label={`Confirmation ${state.params.display.toLowerCase()}`}
//             labelStyle="dark"
//             placeholder={state.params.display}
//             containerStyle={styles.inputView}
//             inputContainer={styles.formInputContainer}
//             inputStyle={themeElements.formInputText}
//             value={this.state.confirmationField}
//             onChangeText={(confirmationField) => {
//               this.setState({ confirmationField });
//             }}
//             textInputOptions={{
//               secureTextEntry: state.params.secure,
//               autoCorrect: false,
//               autoCapitalize: 'none',
//             }}
//           />
//           <Button
//             containerViewStyle={styles.validateView}
//             title="Valider"
//             color={theme.secondaryColor}
//             backgroundColor={theme.primaryColor}
//             buttonStyle={themeElements.validateSmallButton}
//             onPress={this.handleValidateModification}
//           />
//         </Card>
//       </ScrollView>
//     );
//   }
// }

// const EDIT_INFOS = gql`
//   mutation ($email: String!)  {
//     edit: editEmail(
//       email: $email
//     ) {
//       updated
//       error {
//         message
//       }
//     }
//   }
// `;

// const EDIT_PASSWORD = gql`
//   mutation ($old_password: String, $password: String!)  {
//     edit: editPassword(
//       old_password: $old_password
//       password: $password
//     ) {
//       updated
//       error {
//         message
//       }
//     }
//   }
// `;

// export default compose(
//   graphql(VIEWER_RECRUITER),
//   graphql(EDIT_INFOS, { name: 'editInfos' }),
//   graphql(EDIT_PASSWORD, { name: 'editPassword' }),
// )(connectAlert(ImportantFieldModificationRecruiter));

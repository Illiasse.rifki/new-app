// import React, { Component } from 'react';
// import { View, Text, StyleSheet, ScrollView } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';
// import { Icon } from 'react-native-elements';
// import { themeElements } from '../../config/constants/index';
// import CardEditable from '../../components/Card/CardEditable';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import LanguageDisplay from '../../components/GQLLinked/LanguageDisplay';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//     paddingBottom: 16,
//     paddingTop: 16,
//   },
//   scrollViewFix: {
//     marginBottom: 32,
//   },
//   titleView: {
//     flexDirection: 'row',
//     paddingLeft: 32,
//     paddingRight: 19,
//     marginTop: 22,
//     marginBottom: 30,
//   },
//   mainTitle: {
//     flex: 16,
//   },
//   addIcon: {
//     alignItems: 'flex-end',
//     flex: 2,
//   },
//   section: {
//     flex: 1,
//     position: 'relative',
//     backgroundColor: 'white',
//     paddingLeft: 32,
//     paddingRight: 32,
//     paddingTop: 49,
//     paddingBottom: 51,
//     marginBottom: 16,
//   },
// });

// class CandidateProfileLanguages extends Component {
//   static navigationOptions = {
//     title: 'Vos langues',
//     headerTruncatedBackTitle: '',
//   };

//   props: {
//     navigation: {
//       navigate: Function,
//       state: {},
//     },
//     data: {
//       singleCandidate: {
//         experience: {},
//       },
//       error: String,
//       loading: boolean,
//     }
//   };

//   render() {
//     if (this.props.data.error) {
//       return (<ErrorComponent message='Problème lors de la récupération du profil.' />);
//     } else if (this.props.data.loading) {
//       return (<LoadingComponent />);
//     }

//     const { navigate, state } = this.props.navigation;
//     const candidate = this.props.data.singleCandidate;
//     const hasExperiences = candidate.language.length;
//     return (
//       <ScrollView
//         style={styles.background}
//         showsVerticalScrollIndicator={false}
//         showsHorizontalScrollIndicator={false} >
//         <View style={styles.titleView}>
//           <Text style={[themeElements.bigTitle, styles.mainTitle]}>Langues parlées</Text>
//           <Icon
//             name="plus"
//             type="entypo"
//             size={40}
//             containerStyle={styles.addIcon}
//             onPress={() => {
//               navigate('CandidateProfileAddLanguage', { userId: state.params.userId });
//             }}
//           />
//         </View>
//         <View>
//           {hasExperiences ? (
//             candidate.language.map((language) => {
//               return (
//                 <CardEditable
//                   key={language.language_id}
//                   containerStyle={styles.section}
//                   editable
//                   onPress={() => {
//                     navigate('CandidateProfileEditLanguage', { userId: state.params.userId, languageId: language.language_id });
//                   }}
//                 >
//                   <LanguageDisplay languageId={language.language_id} />
//                 </CardEditable>
//               );
//             })
//           ) : (
//             <CardEditable>
//               <Text>Aucune langue ajoutée</Text>
//             </CardEditable>
//           )}
//           <View style={styles.scrollViewFix} />
//         </View>
//       </ScrollView>
//     );
//   }
// }

// const CANDIDATE_LANGUAGES = gql`
//   query SingleCandidateQuery($user_id: String) {
//     singleCandidate(user_id: $user_id) {
//       language {
//         language_id
//       }
//     }
//   }
// `;

// export default graphql(CANDIDATE_LANGUAGES, {
//   options: ownProps => ({
//     variables: {
//       user_id: ownProps.navigation.state.params.userId,
//     },
//   }),
// })(CandidateProfileLanguages);
// export { CANDIDATE_LANGUAGES };

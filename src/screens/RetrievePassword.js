// import React, { Component } from 'react';
// import { View, StyleSheet, Keyboard, KeyboardAvoidingView, Platform, Animated, Text } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';
// import { FormLabel, FormInput, Button, Icon } from 'react-native-elements';
// import { theme } from '../config/constants/index';
// import connectAlert from '../components/Alert/connectAlert';

// const styles = StyleSheet.create({
//   screenBackground: {
//     flex: 1,
//     backgroundColor: theme.secondaryColor,
//     paddingLeft: 32,
//     paddingRight: 32,
//     paddingBottom: 32,
//   },
//   logoContainer: {
//     paddingTop: 36,
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   loginFormView: {
//     flex: 8,
//     justifyContent: 'center',
//   },
//   fieldContainer: {
//     flex: 1,
//     marginLeft: 0,
//     marginRight: 0,
//     marginTop: 0,
//   },
//   loginFormLabelContent: {
//     marginTop: 0,
//     marginRight: 0,
//     marginLeft: 0,
//     color: '#afafaf',
//     letterSpacing: 0.5,
//     fontSize: 14,
//     fontWeight: 'normal',
//   },
//   loginFormInputContainer: {
//     marginRight: 0,
//     marginLeft: 0,
//   },
//   loginFormInput: {
//     color: '#000000',
//     marginBottom: -4,
//   },
//   forgotPasswordButtonText: {
//     color: theme.primaryColor,
//   },
//   submitButtonsContainer: {
//     flex: 1,
//   },
//   connectButton: {
//     backgroundColor: theme.primaryColor,
//     borderRadius: 4,
//     shadowColor: 'rgba(0, 0, 0, 0.16)',
//     shadowOffset: { width: 0, height: 1.5 },
//     shadowOpacity: 0.8,
//     shadowRadius: 2,
//   },
//   connectButtonText: {
//     color: theme.secondaryColor,
//   },
//   connectButtonContainer: {
//     flex: 1,
//     marginLeft: 0,
//     marginRight: 0,
//     justifyContent: 'flex-start',
//   },
//   connectButtonView: {
//     justifyContent: 'flex-start',
//   },
//   title: {
//     fontSize: 28,
//     textAlign: 'center',
//   },
//   titleView: {
//     flex: 1,
//     justifyContent: 'center',
//   },
//   sentView: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
//   sentViewText: {
//     fontSize: 18,
//     lineHeight: 24,
//     textAlign: 'center',
//   },
//   backButton: {
//     marginTop: 60,
//   },
//   icon: {
//     marginBottom: 60,
//   },
// });

// class Login extends Component {
//   static navigationOptions = {
//     title: 'Mot de passe oublié',
//   };

//   componentWillMount() {
//     this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
//     this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
//   }

//   componentWillUnmount() {
//     this.keyboardWillShowSub.remove();
//     this.keyboardWillHideSub.remove();
//   }

//   constructor(props) {
//     super(props);
//     this.state = {
//       email: '',
//       sent: false,
//       loading: false,
//     };
//     this.getPasswordFlex = new Animated.Value(1);
//   }

//   handleOnClickGetPassword = () => {
//     if (!this.state.email) {
//       this.props.alertWithType('error', 'Erreur', 'Veuillez rentrer une adresse e-mail');
//       return;
//     }

//     this.props.recoverEmail({
//       variables: {
//         email: this.state.email,
//       },
//     }).then((res) => {
//       if (res.data.sendPasswordRecoverEmail && res.data.sendPasswordRecoverEmail.error) {
//         this.props.alertWithType('error', 'Erreur', res.data.sendPasswordRecoverEmail.error.message);
//       } else if (res.data.sendPasswordRecoverEmail && res.data.sendPasswordRecoverEmail.mail_sent) {
//         this.setState({ sent: true, loading: false });
//       }
//     }).catch(() => {
//       this.props.alertWithType('error', 'Erreur', 'Erreur serveur');
//       this.setState({ loading: false });
//     });
//     this.setState({ loading: true });
//   };

//   keyboardWillShow = (event) => {
//     Animated.timing(this.getPasswordFlex, {
//       duration: event.duration,
//       toValue: 4,
//     }).start();
//   };

//   keyboardWillHide = (event) => {
//     Animated.timing(this.getPasswordFlex, {
//       duration: event.duration,
//       toValue: 1,
//     }).start();
//   };

//   props: {
//     navigation: {
//       navigate: Function,
//     },
//     recoverEmail: Function,
//     alertWithType: Function,
//   };

//   render() {
//     return (
//       <View style={styles.screenBackground}>
//         {
//           this.state.sent ?
//             <View style={styles.sentView}>
//               <Icon name="check" type="font-awesome" size={90} containerStyle={styles.icon}/>
//               <Text style={styles.sentViewText}>{"E-mail de réinitialisation de mot de passe envoyé. \n" +
//               "Veuillez consulter votre boite mail."}</Text>
//               <Button
//                 title="Connexion"
//                 containerViewStyle={styles.backButton}
//                 buttonStyle={styles.connectButton}
//                 textStyle={styles.connectButtonText}
//                 onPress={() => this.props.navigation.goBack(null)}
//               />
//             </View>
//             :
//           <KeyboardAvoidingView
//             style={{ flex: 1 }}
//             behavior={Platform.OS === 'ios' ? 'padding' : null}
//           >
//             <View style={styles.loginFormView}>
//               <View style={styles.titleView}>
//                 <Text style={styles.title}>Mot de passe oublié ?</Text>
//               </View>
//               <View style={styles.fieldContainer}>
//                 <FormLabel labelStyle={styles.loginFormLabelContent}>SAISISSEZ VOTRE ADRESSE E-MAIL</FormLabel>
//                 <FormInput
//                   onChangeText={(email) => {
//                     this.setState({ email });
//                   }}
//                   placeholder="Votre adresse e-mail"
//                   keyboardType="email-address"
//                   containerStyle={styles.loginFormInputContainer}
//                   inputStyle={styles.loginFormInput}
//                   autoCapitalize="none"
//                   value={this.state.email}
//                   underlineColorAndroid='#979797'
//                 />
//               </View>
//             </View>
//             <Animated.View style={[styles.submitButtonsContainer, { flex: this.getPasswordFlex }]}>
//               <Button
//                 title="Récuperer mon mot de passe"
//                 buttonStyle={styles.connectButton}
//                 textStyle={styles.connectButtonText}
//                 containerViewStyle={styles.connectButtonContainer}
//                 onPress={this.handleOnClickGetPassword}
//                 disabled={this.state.loading}
//               />
//             </Animated.View>
//           </KeyboardAvoidingView>
//         }
//       </View>
//     );
//   }
// }

// const SEND_PASSWORD_RECOVERY_EMAIL = gql`
//   mutation sendPasswordRecoverEmail($email: String!) {
//   sendPasswordRecoverEmail(email: $email) {
//     mail_sent
//     error {
//       code
//       message
//     }
//   }
// }
// `;

// export default graphql(SEND_PASSWORD_RECOVERY_EMAIL, { name: 'recoverEmail' })(connectAlert(Login));

// import React, { Component } from "react";
// import { View, StyleSheet, ScrollView, StatusBar, RefreshControl, Text } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql, compose } from 'react-apollo';
// import Promise from 'bluebird';

// import { NextEvents } from '../components/GQLLinked/Events';
// import { NextEventsRegisteredRecruiter, CurrentEventsRegisteredRecruiter } from '../components/GQLLinked/EventRegistered';
// import { Icon } from '../components/Pure/react-native-elements';
// import { theme } from '../config/constants/index';
// import ErrorComponent from '../components/Elements/ErrorComponent';
// import LoadingComponent from '../components/Elements/LoadingComponent';
// import global from './../config/global';
// import SxCard from '../components/Card/SxCard';
// import JobOffersSmallCardList from '../components/JobOffer/JobOffersSmallCardList';
// import JobOfferRegistrationSmallCard from '../components/JobOffer/JobOfferRegistrationSmallCard';

// const styles = StyleSheet.create({
//   home: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//   },
//   homeContainer: {
//     paddingVertical: 24,
//     paddingHorizontal: 16,
//   },
//   homeHeader: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//   },
//   eventsContainer: {
//     marginTop: 34,
//     marginBottom: 20,
//     paddingLeft: 0,
//   },
//   title: {
//     fontSize: 28,
//     color: '#1c1c1c',
//     fontWeight: 'bold',
//     marginBottom: 17,
//     marginTop: 33.5,
//   },
//   more: {
//     color: '#33cbcc',
//     marginBottom: 28,
//     marginTop: 10,
//     fontSize: 16,
//     textAlign: 'center',
//     fontWeight: '500',
//   },
//   sxCard: { marginHorizontal: 15, marginBottom: 24 },
// });

// const VIEWER_RECRUITER = gql`
//   query {
//     viewer {
//       id
//       recruiter {
//         user_id
//         jobOffersRegistrations: job_offers_registrations {
//           job_offer_registration_id
//           candidate_id
//           job_offer_id
//         }
//         nextEventRegistrations: next_event_registrations(filter: "accepted") {
//           event_registration_id
//         }
//         current_event_registrations {
//           event_registration_id
//         }
//         jobOffers: job_offers {
//           job_offer_id
//           cover_picture
//           visibility_mode
//           title
//           nb_registrations(filter: "apply")
//           view_counter
//         }
//         preselectedCandidates: job_offers_registrations_preselected {
//           job_offer_registration_id
//           candidate_id
//           job_offer_id
//         }
//       }
//     }
//   }
// `;

// const EVENTS = gql`
//   query {
//     nextEvents: next_events {
//       event_id
//     }
//   }
// `;

// class HomeRecruiter extends Component {
//   static navigationOptions = {
//     title: 'Accueil Recruteur',
//     tabBarLabel: 'Accueil',
//     headerLeft: null,
//     tabBarIcon: ({ tintColor }) => (
//       <Icon size={20} name="home" type="entypo" color={tintColor || theme.gray} />
//     ),
//   };

//   state = {
//     refreshing: false,
//   };

//   componentWillMount = () => {
//     StatusBar.setBarStyle('light-content', true);
//     global.authStockage.setToken('signed-as', 'recruiter');
//   }

//   onRefresh = () => {
//     this.setState({ refreshing: true });
//     const promises = [
//       this.props.events.refetch(),
//       this.props.data.refetch(),
//     ];
//     Promise.all(promises).then(() => {
//       this.setState({ refreshing: false });
//     }).catch(e => console.log('catch refetch Events.js', e));
//   };

//   shouldComponentUpdate(nextProps) {
//     const { route } = nextProps.screenProps;
//     const { routeName } = this.props.navigation.state;
//     return route === routeName || route === '';
//   }

//   componentDidUpdate() {
//     const { data } = this.props;
//     if (data && !data.loading && !data.error && !data.viewer) {
//       this.props.data.refetch();
//     }
//   }

//   props: {
//     navigation: {
//       navigate: Function,
//       state: {
//         routeName: string,
//       },
//     },
//     events: {
//       refetch: Function,
//       loading: boolean,
//       error: {},
//       nextEvents: any,
//     },
//     data: {
//       refetch: Function,
//       loading: boolean,
//       error: {},
//       nextEventRegistrations: {
//         event_registration_id: string,
//       },
//       viewer: {
//         recruiter: {
//           jobOffers: {
//             job_offer_id: string,
//             cover_picture: any,
//             visibility_mode: string,
//             title: string,
//             nb_registrations: any,
//             view_counter: any,
//           },
//           jobOffersRegistrations: {
//             job_offer_registration_id: string,
//           },
//           preselectedCandidates: {
//             job_offer_registration_info_id: string,
//           },
//         },
//       },
//     },
//     screenProps: {
//       route: string,
//     },
//   };

//   render() {
//     const {
//       events,
//       data,
//     } = this.props;
//     const { route } = this.props.screenProps;
//     const { routeName } = this.props.navigation.state;
//     const { navigate } = this.props.navigation;

//     const error = events.error || data.error;
//     const loading = events.loading || data.loading || !data.viewer;

//     if (error) {
//       return (<ErrorComponent message="Problème lors de la récupération page d'accueil." />);
//     } if (loading) {
//       return <LoadingComponent />;
//     }
//     const { refreshing } = this.state;

//     const {
//       viewer: {
//         recruiter: {
//           // jobOffers,
//           // jobOffersRegistrations,
//           nextEventRegistrations,
//           // preselectedCandidates,
//         },
//       },
//     } = data;
//     const { nextEvents } = events;

//     const nbNextEvents = (nextEvents && nextEvents.length) || 0;
//     const nbNextEventRegistrations = (nextEventRegistrations &&
//       nextEventRegistrations.length) || 0;
//     // const nbJobOffers = (jobOffers && jobOffers.length) || 0;
//     // const nbJobOffersRegistrations = (jobOffersRegistrations &&
//     //   jobOffersRegistrations.length) || 0;
//     // const nbPreselectedCandidates = (preselectedCandidates &&
//     //   preselectedCandidates.length) || 0;

//     return (
//       <ScrollView
//         contentContainerStyle={styles.homeContainer}
//         style={styles.home}
//         refreshControl={<RefreshControl
//           refreshing={refreshing}
//           onRefresh={this.onRefresh}
//         />}
//       >
//         <View style={styles.homeHeader}>
//           {/* <SxCard
//             onPress={() => navigate('RecruiterJobOffers')}
//             number={nbJobOffers}
//             title="Offres"
//           /> */}
//           <SxCard
//             style={styles.sxCard}
//             onPress={() => navigate('RecruiterEventsStack')}
//             number={nbNextEvents}
//             title="Événements"
//           />
//           {/* <SxCard
//             onPress={() => navigate('RecruiterJobOffers')}
//             number={nbJobOffersRegistrations}
//             title="Candidatures"
//           /> */}
//         </View>
//         {/* {
//           nbJobOffersRegistrations > 0 &&
//           (<View>
//             <Text style={styles.title}>Candidatures à traiter</Text>
//             {
//               jobOffersRegistrations.slice(0, 3).map(jobOfferReg => (
//                 <JobOfferRegistrationSmallCard
//                   key={jobOfferReg.job_offer_registration_id}
//                   jobOfferRegistrationId={jobOfferReg.job_offer_registration_id}
//                   onPress={() => navigate('RecruiterJobOfferRegistration', {
//                     jobOfferRegistrationId: jobOfferReg.job_offer_registration_id,
//                     jobOfferId: jobOfferReg.job_offer_id,
//                     userId: jobOfferReg.candidate_id,
//                   })}
//                 />
//               ))
//             }
//             { nbJobOffersRegistrations > 3 &&
//               <Text
//                 onPress={() => navigate('RecruiterJobOffers')}
//                 style={styles.more}
//               >Voir le reste des candidatures à traiter</Text> }
//           </View>)
//         } */}

//         {/* {
//           nbPreselectedCandidates > 0 && (
//             <View>
//               <Text style={styles.title}>Candidatures préselectionnées</Text>
//               {preselectedCandidates.map(candidate => (
//                 <JobOfferRegistrationSmallCard
//                   key={`preselected_${candidate.job_offer_registration_id}`}
//                   jobOfferRegistrationId={candidate.job_offer_registration_id}
//                   onPress={() => navigate('RecruiterJobOfferRegistration', {
//                     jobOfferRegistrationId: candidate.job_offer_registration_id,
//                     jobOfferId: candidate.job_offer_id,
//                     userId: candidate.candidate_id,
//                   })}
//                 />
//               ))}
//               { nbPreselectedCandidates > 2 &&
//                 <Text
//                   onPress={() => navigate('RecruiterJobOffers')}
//                   style={styles.more}
//                 >Voir toutes mes offres</Text> }
//             </View>
//           )
//         } */}

//         {/* {
//           jobOffers && <JobOffersSmallCardList
//             containerStyle={{ marginTop: 33.5 }}
//             title='Offres en cours'
//             jobs={jobOffers}
//             onJobOfferPress={(jobOffer: any) => navigate('RecruiterJobOffer', { jobOfferId: jobOffer.job_offer_id })}
//           />
//         } */}

//         {
//           <CurrentEventsRegisteredRecruiter
//             navigate={navigate}
//             containerStyle={styles.eventsRegisterContainer}
//             title="Événements en cours"
//             more={this.state.moreInscriptions}
//             route={route}
//             routeName={routeName}
//             refresh={this.state.refreshing}
//           />
//         }
//         {
//           nbNextEventRegistrations === 0 ?
//             <NextEvents
//               containerStyle={styles.eventsContainer}
//               isRecruiter
//               navigate={navigate}
//               next
//               title="Prochains événements"
//               refresh={refreshing}
//             /> :
//             <NextEventsRegisteredRecruiter
//               isRecruiter
//               containerStyle={styles.eventsContainer}
//               next
//               navigate={navigate}
//               title={'Soirées auxquelles vous allez participez'}
//               refresh={refreshing}
//             />
//         }
//       </ScrollView>
//     );
//   }
// }

// export default compose(
//   graphql(VIEWER_RECRUITER),
//   graphql(EVENTS, { name: 'events' }),
// )(HomeRecruiter);

import React, { Component } from "react";
// import { StyleSheet, ScrollView } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import ProfileCompany from '../../components/GQLLinked/ProfileCompany';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//     paddingBottom: 24,
//     paddingTop: 24,
//   },
//   profileCompany: {
//     marginBottom: 40,
//   },
// });

// const VIEWER = gql`
//   query {
//     viewer {
//       id
//       recruiter {
//         company {
//           company_id
//         }
//       }
//     }
//   }
// `;

// class ProfileRecruiter extends Component {
//   static navigationOptions = {
//     title: 'Votre profil',
//     headerTruncatedBackTitle: 'Profil',
//     headerLeft: null,
//     tabBarLabel: 'Mes informations',
//   };

//   constructor(props) {
//     super(props);
//     this.state = {};
//   }

//   // Why ?
//   // shouldComponentUpdate(nextProps) {
//   //   const { route } = nextProps.screenProps;
//   //   const { routeName } = this.props.navigation.state;
//   //   return route === routeName;
//   // }

//   props: {
//     navigation: {
//       navigate: Function,
//     },
//     data: {
//       error: string,
//       viewer: {
//         recruiter: {
//           company: {
//             company_id: string,
//           },
//         },
//       },
//       loading: boolean,
//     },
//   };

//   render() {
//     const { navigate } = this.props.navigation;

//     if (this.props.data.error) {
//       return <ErrorComponent message="Problème lors de la récupération du profil." />;
//     } else if (this.props.data.loading) {
//       return <LoadingComponent />;
//     }
//     if (!this.props.data.viewer) { return null; }
//     const { company } = this.props.data.viewer.recruiter;
//     return (
//       <ScrollView style={styles.background} keyboardShouldPersistTaps="always">
//         <ProfileCompany
//           companyId={company.company_id}
//           navigate={navigate}
//           containerStyle={styles.profileCompany}
//         />
//       </ScrollView>
//     );
//   }
// }

// export default graphql(VIEWER)(ProfileRecruiter);

class HomeRecruiter extends Component {
  static navigationOptions = {
    title: "Votre profil",
    headerTruncatedBackTitle: "Profil",
    headerLeft: null,
    tabBarLabel: "Mes informations",
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  // Why ?
  // shouldComponentUpdate(nextProps) {
  //   const { route } = nextProps.screenProps;
  //   const { routeName } = this.props.navigation.state;
  //   return route === routeName;
  // }

  // props: {
  //   navigation: {
  //     navigate: Function,
  //   },
  //   data: {
  //     error: string,
  //     viewer: {
  //       recruiter: {
  //         company: {
  //           company_id: string,
  //         },
  //       },
  //     },
  //     loading: boolean,
  //   },
  // };

  render() {
    return <View>SLAUT</View>;
  }
}

export default HomeRecruiter;

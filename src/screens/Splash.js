// @flow
import React, { Component } from 'react';
import styled from 'styled-components/native';
import Spinner from 'react-native-spinkit';

import Logo from '../components/Logo/Logo';

const Background = styled.View`
  background-color: #33cbcc;
  width: 100%;
  height: 100%;
`;

const Wrapper = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

type State = {
  isLoading: boolean,
  type: string,
  size: number,
  color: string,
};

class Splash extends Component<Object, Object, State> {
  static defaultProps = {};

  state = {
    isLoading: true,
    type: 'Bounce',
    size: 60,
    color: '#FFFFFF',
  };

  _handleLoading = () => {
    this.setState({ isLoading: false });
  };

  render() {
    return (
      <Background>
        <Wrapper>
          <Logo width="245px" height="84px" marginBottom="24px" />
          <Spinner type={this.state.type} size={this.state.size} color={this.state.color} />
        </Wrapper>
      </Background>
    );
  }
}

export default Splash;

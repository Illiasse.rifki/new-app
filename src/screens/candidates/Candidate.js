// @flow
import React, { Component } from 'react';
import { StyleSheet, ScrollView } from 'react-native';

import Profile from '../../components/GQLLinked/Profile';
import EventRegistrationInfo from '../../components/GQLLinked/EventRegistrationInfo';
import Favorite from '../../components/GQLLinked/Favorite';
import { headerLeftNormal as HeaderLeftNormal } from '../../config/routes';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#f5f5f5',
    flex: 1,
    paddingTop: 8,
  },
  box: {
    backgroundColor: 'white',
    borderRadius: 4,
    marginVertical: 4,
    marginHorizontal: 16,
    paddingRight: 8,
    flexDirection: 'row',
    overflow: 'hidden',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  name: {
    color: '#1c1c1c',
    textAlign: 'left',
    fontWeight: '500',
    fontSize: 18,
  },
});

class Candidate extends Component {
  static navigationOptions = props => ({
    title: props.navigation.state.params.name || 'Profil Candidat',
    headerLeft: (
      <HeaderLeftNormal
        navigation={props.navigation}
        nbBack={props.navigation.state.params.fromScan ? 2 : null}
      />
    ),
    headerBackTitle: null,
    // headerRight: props.navigation.state.params.event_registration_info_id ? <Favorite
    //   event_registration_info_id={props.navigation.state.params.event_registration_info_id}
    //   editable
    // /> : null,
  });

  props: {
    navigation: {
      navigate: Function,
      setParams: Function,
      state: {
        params: {
          user_id: string,
          event_id: string,
          event_registration_id: string,
          event_registration_info_id: string,
        },
        routeName: string,
      },
    },
    screenProps: { route: string },
  };

  componentWillUnmount = () => {
    this.props.navigation.setParams({
      fromScan: undefined,
      event_registration_id: undefined,
      event_registration_info_id: undefined,
    });
  };

  render() {
    const { push: navigate, setParams } = this.props.navigation;
    const {
      user_id: userId,
      event_id: eventId,
      event_registration_id: eventRegistrationId,
      event_registration_info_id: eventRegistrationInfoId,
    } = this.props.navigation.state.params;
    return (
      <ScrollView style={styles.background}>
        <EventRegistrationInfo
          user_id={userId}
          setParams={setParams}
          refetch={this.props.navigation.state.params.refetch}
          event_id={eventId}
          event_registration_id={eventRegistrationId}
          event_registration_info_id={eventRegistrationInfoId}
          navigate={navigate}
        />
        <Profile editable={false} userId={userId} />
      </ScrollView>
    );
  }
}

export default Candidate;

import React, { Component } from 'react';
import { Text, View, ScrollView, Modal, StyleSheet } from 'react-native';
import { Button, FormLabel, FormInput as FI, FormValidationMessage, Header, Divider } from 'react-native-elements';
import { theme, tags, qualities } from '../../config/constants/index';
import { FiltersContext } from './Wrapper';
import MultiPicker from '../../components/Elements/MultiPicker';

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    // flex: 1,
  },
  modalView: {
    flex: 1,
  //  height: '100%',
  },
  modalContentView: {
    flex: 1,
    padding: 24,
  },
  button: {
    width: '40%',
    marginTop: 'auto',
  },
  input: {
    width: '50%',
    textAlign: 'right',
  },
});

const FormInput = props => <FI {...props} style={styles.input}/>;

export class FiltersButton extends React.PureComponent {
  openModal = filters => () => {
    filters.update('modal', true);
  }

  render() {
    return (
      <View style={styles.container}>
        <FiltersContext.Consumer>
          {
            filters => (
              <Button onPress={this.openModal(filters)} style={{ margin: 0, padding: 0 }}
                buttonStyle={{
                  backgroundColor: theme.primaryColor,
                  height: 40,
                  width: 40,
                  padding: 8,
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}
                containerViewStyle={{
                  marginRight: 15,
                  marginLeft: 0,
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  height: 40,
                  width: 40,
                  padding: 0,
                }}
                iconContainerStyle={{
                  justifyContent: 'center',
                  display: 'flex',
                  flexDirection: 'row',
                  height: 40,
                  width: 40,
                  padding: 0,
                }}
                raised
                icon={{
                  style: { marginRight: 0 },
                  name: 'filter',
                  type: 'font-awesome',
                  size: 20,
                  color: filters.isActive() ? '#333' : '#FFF',
                }}/>
            )
          }
        </FiltersContext.Consumer>
      </View>
    );
  }
}


export default class Filters extends React.PureComponent {
  closeModal = filters => () => {
    filters.update('modal', false);
  }

  changeFilter = (filters, name) => (text) => {
    filters.update(name, text);
  }

  resetFilters = filters => () => {
    filters.reset();
  }


  render() {
    const Spacer = () => <View style={{ marginBottom: 16 }}/>;
    return (
      <FiltersContext.Consumer>
        {
          filters => (
            <Modal
              animationType="slide"
              transparent={false}
              visible={filters.modal}
              onRequestClose={this.closeModal(filters)}>
              <View style={styles.modalView}>
                <Header
                  backgroundColor={theme.primaryColor}
                  centerComponent={{ text: 'Filtres', style: { color: '#fff' } }}
                />
                <View style={styles.modalContentView}>
                  <ScrollView>
                    <FormLabel>Experience minimum (en année)</FormLabel>
                    <FormInput onChangeText={this.changeFilter(filters, 'experience_min')}
                      keyboardType={'numeric'}
                      value={filters.experience_min}
                    />
                    <FormLabel>Experience maximum (en année)</FormLabel>
                    <FormInput onChangeText={this.changeFilter(filters, 'experience_max')}
                      keyboardType={'numeric'}
                      maxLength={2}
                      value={filters.experience_max}
                    />
                    <Divider/>
                    <FormLabel>Formation minimum (en année)</FormLabel>
                    <FormInput onChangeText={this.changeFilter(filters, 'formation_min')}
                      keyboardType={'numeric'}
                      value={filters.formation_min}
                    />
                    <FormLabel>Formation maximum (en année)</FormLabel>
                    <FormInput onChangeText={this.changeFilter(filters, 'formation_max')}
                      keyboardType={'numeric'}
                      value={filters.formation_max}
                    />
                    <Divider/>

                    <FormLabel>Tags</FormLabel>
                    <MultiPicker
                      viewStyle={{ minHeight: 160 }}
                      propositions={tags.map(({ value, key }) => ({ label: value, name: key }))}
                      onSelect={this.changeFilter(filters, 'tags')}
                      value={filters.tags}/>
                    <Divider/>
                    <FormLabel>Qualités</FormLabel>
                    <MultiPicker
                      viewStyle={{ minHeight: 160 }}
                      propositions={qualities.map(({ value, key }) => ({ label: value, name: key }))}
                      onSelect={this.changeFilter(filters, 'qualities')}
                      value={filters.qualities}/>
                  </ScrollView>

                  <Spacer/>
                  <View style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between' }}>
                    <Button
                      disabled={!filters.isActive()}
                      onPress={this.resetFilters(filters) }
                      containerViewStyle={styles.button}
                      title="Réinitialiser"
                      color="#fff"
                      backgroundColor="#345a5a" />
                    <Button onPress={this.closeModal(filters)}
                      containerViewStyle={styles.button}
                      title="Fermer"
                      color="#fff"
                      backgroundColor="#33cbcc" />
                  </View>
                </View>
              </View>
            </Modal>

          )
        }
      </FiltersContext.Consumer>
    );
  }
}

// import React from 'react';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';
// import Base from './Base';

// class Seen extends Base {
//   static navigationOptions = ({ navigation }) => ({
//     title: 'Absents',
//   });
// }

// export const QUERY_EVENT_CANDIDATES = gql`
//   query Seen(
//     $event_id: String!,
//     $tags: [String],
//     $qualities: [String],
//     $experience_max: Int,
//     $experience_min: Int,
//     $formation_max: Int,
//     $formation_min: Int,
//     ) {
//     viewer {
//       id
//       recruiter {
//         user_id
//         candidates: event_registrations(
//           event_id: $event_id,
//           seen: true,
//           qualities: $qualities,
//           tags: $tags,
//           experience_min: $experience_min,
//           experience_max: $experience_max,
//           formation_min: $formation_min,
//           formation_max: $formation_max,
//           ) {
//             event_registration_id
//             event_id
//             info: event_registration_info {
//               event_registration_info_id
//               comment
//             }
//             candidate {
//               user_id
//               firstname
//               lastname
//               fullname
//               picture
//               headline
//             }
//         }
//       }
//     }
//   }
// `;

// export default graphql(QUERY_EVENT_CANDIDATES, {
//   options: props => ({
//     fetchPolicy: 'network-only',
//     variables: {
//       event_id: props.navigation.state.params.event_id,
//       experience_min: parseInt(props.experience_min, 10) || 0,
//       experience_max: parseInt(props.experience_max, 10) || 10,
//       formation_min: parseInt(props.formation_min, 10) || 0,
//       formation_max: parseInt(props.formation_max, 10) || 10,
//       tags: props.tags || [],
//       qualitites: props.qualities || [],
//     } }),
// })(Seen);

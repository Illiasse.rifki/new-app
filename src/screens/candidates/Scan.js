// import React, { Component } from 'react';
// import { View, StyleSheet } from 'react-native';
// import { graphql, compose, withApollo } from 'react-apollo';
// import gql from 'graphql-tag';
// import { Button } from 'react-native-elements';

// import { BarCodeScanner, Camera, Permissions } from 'expo';

// import connectAlert from '../../components/Alert/connectAlert';

// const styles = StyleSheet.create({
//   left: { position: 'absolute', bottom: 10, left: 10 },
//   right: { position: 'absolute', bottom: 10, right: 10 },
//   flex: { flex: 1 },
// });

// const QUERY_PRELOAD_CANDIDATE = gql`
//   query preLoadCandidate($event_id: String!, $candidate_id: String!) {
//     preload: eventRegistrationInfoByCandidate(event_id: $event_id, candidate_id: $candidate_id) {
//       event_registration_info_id
//       event_registration_id
//     }
//   }
// `;

// class Scan extends Component {
//   state = {
//     fontLoaded: false,
//     hasCameraPermission: null,
//     type: Camera.Constants.Type.back,
//   };

//   static navigationOptions = ({ navigation }) => ({
//     title: 'Scan QRCode',
//   });

//   props: {
//     alertWithType: Function,
//     onScan: Function,
//     data: any,
//     client: { query: Function },
//     navigation: {
//       setParams: Function,
//       navigate: Function,
//       goBack: Function,
//       state: {
//         params: {
//           event_id: string,
//           user_id?: string,
//           candidate_id?: string,
//         }, routeName: string
//       },
//     },
//     screenProps: { route: string },
//   };

//   timeout = null;

//   isUID = s =>
//     s.match(/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i);

//   onRead = async (e) => {
//     try {
//       if (this.timeout) return;
//       this.timeout = setTimeout(() => clearTimeout(this.timeout), 500);
//       if (e && e.data && [BarCodeScanner.Constants.BarCodeType.qr].includes(e.type) && this.isUID(e.data)) {
//         const scanned = await this.props.onScan({
//           variables: {
//             candidate_id: e.data,
//             event_id: this.props.navigation.state.params.event_id,
//           },
//         });
//         if (scanned.data.onScanCandidate.ok) {
//           this.props.alertWithType('success', 'Succès', 'QRCode Scanné');
//           const res = await this.props.client.query({
//             query: QUERY_PRELOAD_CANDIDATE,
//             variables: {
//               event_id: this.props.navigation.state.params.event_id,
//               candidate_id: e.data,
//             },
//           });
//           try {
//             const { event_registration_id, event_registration_info_id } = res.data.preload;
//             clearTimeout(this.timeout);
//             this.props.navigation.push('EventCandidate', {
//               event_id: this.props.navigation.state.params.event_id,
//               user_id: e.data,
//               event_registration_id,
//               event_registration_info_id,
//               fromScan: true,
//             });
//           } catch (error) {
//             this.props.alertWithType('info', 'Info', 'Le candidat n\'a pas été trouvé dans l\'événement');
//             this.timeout = setTimeout(() => clearTimeout(this.timeout), 3000);
//           }
//         } else if (scanned.data.onScanCandidate.error && scanned.data.onScanCandidate.error.message === 'missing') {
//           this.props.alertWithType('error', 'Echec', 'Le candidat n\'est pas inscrit ou n\'a pas validé sa présence à l\'acceuil');
//           this.timeout = setTimeout(() => clearTimeout(this.timeout), 3000);
//         }
//       }
//     } catch (error) {
//       console.warn(error);
//     }
//   }

//   async componentWillMount() {
//     const { status } = await Permissions.askAsync(Permissions.CAMERA);
//     this.setState({ hasCameraPermission: status === 'granted' });
//   }

//   goProfil = () => {
//     // this.props.navigation.setParams({ candidate_id: this.state.id });
//   }

//   cancel = () => {
//     // this.props.navigation.setParams({ candidate_id: null });
//     this.props.navigation.goBack();
//   }

//   render() {
//     const { id } = this.state;
//     // reset Timeout in case shit go wrong
//     if (this.timeout) { setTimeout(() => clearTimeout(this.timeout), 3000); }
//     return (
//       <View style={styles.flex}>
//         <Camera
//           style={styles.flex}
//           permissionDialogTitle={'Permission d\'utiliser la camera'}
//           permissionDialogMessage={'Permission necessaire pour la lecture de QRCode'}
//           onBarCodeScanned={this.onRead}
//           type={this.state.type}
//           barCodeScannerSettings={{
//             barCodeTypes: [BarCodeScanner.Constants.BarCodeType.qr],
//           }} />
//         {/* {id !== '' && <Button
//           containerViewStyle={styles.left}
//           title='Chercher le Profil'
//           onPress={this.goProfil}
//         />}
//         {id !== '' && <Button
//           containerViewStyle={styles.right}
//           title='Annuler'
//           onPress={this.cancel}
//         />} */}
//       </View>
//     );
//   }
// }

// const ON_SCAN_CANDIDATE = gql`
// mutation onScanCandidate($candidate_id: String!, $event_id: String!) {
//   onScanCandidate(event_id: $event_id, candidate_id: $candidate_id) {
//     ok
//     error {
//       message
//     }
//   }
// }
// `;

// export default compose(
//   withApollo,
//   graphql(ON_SCAN_CANDIDATE, { name: 'onScan' }),
// )(connectAlert(Scan));

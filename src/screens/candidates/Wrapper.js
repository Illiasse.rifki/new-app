import React from 'react';
import { StyleSheet, View } from 'react-native';


const filters = {
  modal: false,
  experience_min: '0',
  experience_max: '50',
  formation_min: '0',
  formation_max: '10',
  tags: [],
  qualities: [],
  update: () => {},
  reset: () => {},
  isActive: () => {},
};

export const FiltersContext = React.createContext(filters);

const replaceEmptyBy0 = (value) => {
  if (!String(value).length) return '0';
  return String(value);
};

export default class Wrapper extends React.PureComponent {
  constructor(props) {
    super(props);
    this.reset = () => {
      this.setState({
        experience_min: '0',
        experience_max: '50',
        formation_min: '0',
        formation_max: '10',
        tags: [],
        qualities: [],
      });
    };
    this.isActive = () => {
      const {
        experience_min,
        experience_max,
        formation_min,
        formation_max,
        tags,
        qualities,
      } = this.state;

      if (experience_min !== '0') return true;
      if (experience_max !== '50') return true;
      if (formation_min !== '0') return true;
      if (formation_max !== '10') return true;
      if (tags.length) return true;
      if (qualities.length) return true;
      return false;
    };
    this.update = (name, value) => {
      const minimas = ['experience', 'formation'].map(e => `${e}_min`);
      const maximas = ['experience', 'formation'].map(e => `${e}_max`);
      if (minimas.indexOf(name) !== -1) {
        const b = name.slice(0, -4);
        const bval = this.state[`${b}_max`];
        this.setState({
          [name]: replaceEmptyBy0(value),
          [`${b}_max`]: parseInt(bval, 10) <= parseInt(value, 10) ? String(parseInt(value, 10) + 1) : String(bval),
        });
      } else if (maximas.indexOf(name) !== -1) {
        const b = name.slice(0, -4);
        const bval = this.state[`${b}_min`];
        this.setState({
          [name]: replaceEmptyBy0(value),
          [`${b}_min`]: parseInt(bval, 10) >= parseInt(value, 10) ? String(parseInt(value, 10) - 1) : String(bval),
        });
      } else {
        this.setState({
          [name]: value,
        });
      }
    };
    this.state = {
      modal: false,
      experience_min: '0',
      experience_max: '50',
      formation_min: '0',
      formation_max: '10',
      tags: [],
      qualities: [],
      isActive: this.isActive,
      update: this.update,
      reset: this.reset,
    };
  }


  props: {
    children: ?Object
  }

  render() {
    const { children } = this.props;
    return (
      <FiltersContext.Provider value={this.state}>
        {children || null}
      </FiltersContext.Provider>
    );
  }
}


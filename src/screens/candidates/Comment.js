// // @flow
// import React, { Component } from 'react';
// import { StyleSheet, View, Text, ScrollView, Dimensions, Keyboard, Platform } from 'react-native';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';
// import { Avatar, FormInput } from 'react-native-elements';

// import { QUERY_EVENT_CANDIDATES as QUERY_ANNOTATED } from './Annotated';
// import { QUERY_EVENT_CANDIDATES as QUERY_TREAT } from './Treat';

// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';

// import connectAlert from '../../components/Alert/connectAlert';

// import Button from '../../components/Design/Button';
// import { theme } from '../../config/constants/index';

// const styles = StyleSheet.create({
//   main: { flex: 1, backgroundColor: '#f5f5f5', height: 1300 },
//   header: {
//     backgroundColor: 'white',
//     paddingVertical: 16,
//     paddingHorizontal: 16,
//     shadowColor: 'rgba(0, 0, 0, 0.08)',
//     shadowOffset: { width: 0.5, height: 2 },
//     shadowOpacity: 0.8,
//     shadowRadius: 12,
//     height: 92,
//     zIndex: 1000,
//   },
//   headerContent: { flexDirection: 'row',
//     alignItems: 'center',
//     justifyContent: 'space-around',
//     height: 60,
//     overflow: 'hidden',
//   },
//   headerAvatar: {
//     borderRadius: 4,
//     width: 60,
//     height: 60,
//     marginHorizontal: 16,
//     marginRight: 16,
//     overflow: 'hidden',
//     borderColor: theme.primaryColor,
//     borderWidth: 1,
//   },
//   headerTextContainer: {
//     flex: 1,
//     flexDirection: 'column',
//     justifyContent: 'center',
//     height: 60,
//     overflow: 'hidden',
//   },
//   headerTitle: { color: '#1c1c1c', fontSize: 18 },
//   headerText: { color: '#afafaf', fontSize: 14, marginTop: 5 },
//   content: {
//     backgroundColor: '#f5f5f5',
//     paddingVertical: 32,
//     paddingHorizontal: 32,
//   },
//   contentText: {
//     color: theme.black,
//     fontSize: 28,
//     fontWeight: 'bold',
//   },
//   contentFormInput: {
//     borderColor: '#979797',
//     backgroundColor: 'white',
//     borderRadius: 4,
//     borderWidth: 1,
//     marginHorizontal: 10,
//     marginBottom: 32,
//     paddingHorizontal: 4,
//     marginTop: 9,
//     minHeight: 100,
//   },
//   formInput: {
//     height: 100,
//   },
//   contentInput: {
//     width: '100%',
//   },
//   button: {
//     width: 148,
//     height: 'auto',
//   },
// });

// const CANDIDATE = gql`
//   query singleCandidate($user_id: String!){
//     singleCandidate(user_id: $user_id) {
//       user_id
//       fullname
//       firstname
//       lastname
//       headline
//       picture
//     }
//   }
// `;

// const EVENT_REGISTRATION_INFO = gql`
//   query CandidateInfo($event_registration_info_id: String!){
//     eventRegistrationInfo(event_registration_info_id: $event_registration_info_id) {
//       event_registration_info_id
//       selected
//       comment
//     }
//   }
// `;

// class Comment extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       visibleHeight: null,
//       comment: this.props.data.eventRegistrationInfo.comment || '',
//     };
//   }
//   static navigationOptions = () => ({
//     title: 'Annotations',
//     headerBackTitle: null,
//   });
//   props: any;
//   editEventRegistrationInfo = async () => {
//     const { comment } = this.state;
//     const { event_registration_info_id, selected } = this.props.data.eventRegistrationInfo;
//     const v = { event_id: this.props.navigation.state.params.event_id };
//     try {
//       await this.props.edit(
//         {
//           refetchQueries: [
//             {
//               query: EVENT_REGISTRATION_INFO,
//               variables: { event_registration_info_id },
//             },
//             {
//               query: QUERY_ANNOTATED,
//               variables: v,
//             },
//             {
//               query: QUERY_TREAT,
//               variables: v,
//             },
//           ],
//           variables: { event_registration_info_id, selected, comment },
//         });
//     } catch (e) {
//       this.props.alertWithType('error', 'Erreur', 'L\'Annotation n\'à pas été enregistrée !');
//       return;
//     }
//     this.props.alertWithType('success', 'Succès', 'L\'Annotation bien enregistrée !');
//   };

//   shouldComponentUpdate(nextProps, nextState) {
//     const { route = this.props.navigation.state.routeName } = nextProps.screenProps;
//     const next = JSON.stringify({ props: nextProps, state: nextState });
//     const now = JSON.stringify({ props: this.props, state: this.state });
//     return next !== now;
//   }

//   componentWillUnmount() {
//     this.keyboardDidShowListener.remove();
//     this.keyboardDidHideListener.remove();
//     if (this.props.data.eventRegistrationInfo.comment !== this.state.comment) {
//       this.editEventRegistrationInfo();
//     }
//   }

//   componentWillMount() {
//     this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
//     this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this));
//   }

//   keyboardDidShow(e) {
//     const newSize = e.endCoordinates.height;
//     this.setState({
//       visibleHeight: newSize,
//       topLogo: { width: 100, height: 70 },
//     });
//   }

//   keyboardDidHide(e) {
//     this.setState({
//       visibleHeight: 0,
//       topLogo: { width: Dimensions.get('window').width },
//     });
//   }

//   _scrollToInput(reactNode: any) {
//     // Add a 'scroll' ref to your ScrollView
//     this.scroll.props.scrollToFocusedInput(reactNode);
//   }

//   _scrollToBottom() {
//     // Add a 'scroll' ref to your ScrollView
//     this.scroll.props.scrollToPosition(1000, 1000);
//   }

//   setScrollViewRef = (element) => {
//     this.scrollViewRef = element;
//   };

//   render() {
//     if ((this.props.data && this.props.data.loading) ||
//       (this.props.candidate && this.props.candidate.loading)) return <LoadingComponent />;
//     if (this.props.data && this.props.data.error) return <ErrorComponent />;
//     const action = this.editEventRegistrationInfo;

//     const { singleCandidate: candidate } = this.props.candidate;
//     return (
//       <ScrollView
//         style={[styles.main]}
//         ref={this.setScrollViewRef}>
//         <View style={styles.header}>
//           <View style={styles.headerContent}>
//             <Avatar large title={candidate.picture ? undefined :
//               `${candidate.firstname[0]}${candidate.lastname[0]}`}
//             source={candidate.picture ? {
//               uri: candidate.picture,
//             } : undefined }
//             style={styles.headerAvatar}
//             />
//             <View style={styles.headerTextContainer}>
//               <Text style={styles.headerTitle}>{candidate.fullname}</Text>
//               <Text style={styles.headerText}>{candidate.headline}</Text>
//             </View>
//           </View>
//         </View>
//         {/* NEVER USE THIS SHIT */}
//         {/* <KeyboardAwareScrollView
//           style={styles.content}
//           scrollEnabled={true}
//           enableOnAndroid
//           innerRef={(ref) => {
//             this.scroll = ref;
//           }}
//           keyboardShouldPersistTaps="always"
//         > */}
//         <View
//           style={[styles.content, { paddingBottom: this.state.visibleHeight || 0 }]}
//           behavior={Platform.OS === 'ios' ? 'padding' : null}
//         >
//           <Text style={styles.contentText}>Vos notes</Text>
//           <FormInput
//             style={styles.formInput}
//             placeholder="Écrivez vos notes ici"
//             inputStyle={styles.contentInput}
//             containerStyle={styles.contentFormInput}
//             onFocus={
//               () => {
//                 if (this.scroll) {
//                   this.scrollViewRef.scrollToEnd();
//                 }
//               }
//             }
//             onChangeText={(comment) => {
//               this.setState({ comment });
//             }}
//             value={this.state.comment === undefined ?
//               this.props.data.eventRegistrationInfo.comment : this.state.comment}
//             multiline
//             textAlignVertical="top"
//             onContentSizeChange={({ nativeEvent }) => {
//               this.scrollViewRef.scrollToEnd();
//             }}
//             numberOfLines={5}
//             returnKeyType={Platform.OS === 'ios' ? 'default' : 'next'}
//           />
//           <Button
//             buttonStyle={styles.button}
//             title='Enregistrer'
//             onPress={() => action()}
//           />

//         </View>
//         {/* </KeyboardAwareScrollView> */}
//       </ScrollView>
//     );
//   }
// }

// const CREATE_EVENT_REGISTRATION_INFO = gql`
//   mutation createEventRegistrationInfo($recruiter_id: String, $event_registration_id: String!, $selected: Boolean, $comment: String) {
//     createEventRegistrationInfo(recruiter_id: $recruiter_id, event_registration_id: $event_registration_id, selected: $selected, comment: $comment) {
//       event_registration_info {
//         event_registration_info_id
//         selected
//         comment
//       }
//       error {
//         code
//         message
//       }
//     }
//   }
// `;

// const EDIT_EVENT_REGISTRATION_INFO = gql`
//   mutation createEventRegistrationInfo($event_registration_info_id: String!, $selected: Boolean, $comment: String) {
//     editEventRegistrationInfo(event_registration_info_id: $event_registration_info_id, selected: $selected, comment: $comment) {
//       event_registration_info {
//         event_registration_info_id
//         selected
//         comment
//       }
//       error {
//         code
//         message
//       }
//     }
//   }
// `;

// export default compose(
//   graphql(EVENT_REGISTRATION_INFO, {
//     skip: props => !props.navigation.state.params.event_registration_info_id,
//     options: props => ({
//       variables: {
//         event_registration_info_id: props.navigation.state.params.event_registration_info_id,
//       },
//     }),
//   }),
//   graphql(CANDIDATE, {
//     name: 'candidate',
//     options: props => ({
//       variables: {
//         user_id: props.navigation.state.params.user_id,
//       },
//     }),
//   }),
//   graphql(CREATE_EVENT_REGISTRATION_INFO, { name: 'create' }),
//   graphql(EDIT_EVENT_REGISTRATION_INFO, { name: 'edit' }),
// )(connectAlert(Comment));

// // @flow
// import React, { Component } from 'react';
// import { StyleSheet, ScrollView, RefreshControl } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import CandidatesList from '../../components/Content/CandidatesList';
// import Wrapper from './Wrapper';

// const styles = StyleSheet.create({
//   background: {
//     backgroundColor: '#f5f5f5',
//     flex: 1,
//     paddingTop: 8,
//   },
//   contentContainer: {
//     paddingVertical: 8,
//     paddingHorizontal: 16,
//   },
//   box: {
//     backgroundColor: 'white',
//     borderRadius: 4,
//     marginVertical: 4,
//     flexDirection: 'row',
//     overflow: 'hidden',
//     justifyContent: 'space-between',
//     alignItems: 'center',
//   },
//   name: {
//     color: '#1c1c1c',
//     textAlign: 'left',
//     fontWeight: '500',
//     fontSize: 18,
//   },
// });

// class Base extends Component {
//   state = {
//     refreshing: false,
//   };
//   props: {
//     data: {
//       error: any,
//       loading: boolean,
//       candidates: any,
//       refetch: Function,
//     },
//     navigation: {
//       navigate: Function,
//       state: { params: {
//         event_id: string,
//       }, routeName: string },
//     },
//     screenProps: { route: string },
//   };

//   // componentDidUpdate() {
//   //   this.props.data.refetch().then((res) => {
//   //     console.info(res);
//   //     return res;
//   //   }).catch(e => console.log('catch refetch Selected', e));
//   // }

//   onRefresh = () => {
//     this.setState({ refreshing: true });
//     this.props.data.refetch().then(() => {
//       this.setState({ refreshing: false });
//     }).catch(e => console.log('catch refetch Events.js', e));
//   };

//   // shouldComponentUpdate(nextProps, nextState) {
//   //   const { route = this.props.navigation.state.routeName } = nextProps.screenProps;
//   //   if (route !== this.props.navigation.state.routeName) return false;
//   //   const next = JSON.stringify({ props: nextProps, state: nextState });
//   //   const now = JSON.stringify({ props: this.props, state: this.state });
//   //   return next !== now;
//   // }

//   render() {
//     const { data, navigation: { navigate } } = this.props;
//     if (data.loading) return <LoadingComponent />;
//     if (data.error) return <ErrorComponent />;
//     return (
//       <ScrollView
//         contentContainerStyle={styles.contentContainer}
//         style={styles.background}
//         refreshControl={<RefreshControl
//           refreshing={this.state.refreshing}
//           onRefresh={this.onRefresh}
//         />}
//       >
//         <CandidatesList
//           candidates={data.viewer.recruiter.candidates}
//           onPress={(cr) => {
//             navigate('EventCandidate', {
//               refetch: this.props.data.refetch,
//               user_id: cr.candidate.user_id,
//               event_id: this.props.navigation.state.params.event_id,
//               event_registration_id: cr.event_registration_id,
//               event_registration_info_id: cr.info ? cr.info.event_registration_info_id : null,
//             });
//           }}
//         />
//       </ScrollView>
//     );
//   }
// }

// export default Base;

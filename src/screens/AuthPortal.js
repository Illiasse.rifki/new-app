// @flow

import React, { Component } from 'react';
import { View, WebView } from 'react-native';

class AuthPortal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uri: '',
    };
  }

  _handleOnEventMove = () => {};
  _handleOnParamsUri = () => {};

  render() {
    return (
      <View>
        <WebView source={{ uri: this.state.uri }} onMessage={this._handleOnEventMove()} />;
      </View>
    );
  }
}

export default AuthPortal;

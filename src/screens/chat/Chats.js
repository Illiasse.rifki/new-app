// @flow
import React, { Component } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';

import { Icon } from '../../components/Pure/react-native-elements';
import { client } from '../../utils/Chat';
import { theme } from '../../config/constants';
import { connectAlert } from '../../components/Alert';
import LoadingComponent from '../../components/Elements/LoadingComponent';
import ErrorComponent from '../../components/Elements/ErrorComponent';

import Chat from '../../components/Chats/Chat';

const styles = StyleSheet.create({
  chatView: {
    marginVertical: 24,
    marginHorizontal: 16,
    backgroundColor: 'white',
    borderRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { width: 1, height: 4 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    paddingVertical: 2.5,
  },
  container: {
    backgroundColor: '#f5f5f5',
    flex: 1,
  },
});

class Chats extends Component {
  state = {
    rooms: [],
    loading: true,
    user: null,
    error: false,
    chatAlreadyClicked: false,
  };

  static navigationOptions = {
    title: 'Vos dernières discussions',
    headerTruncatedBackTitle: '',
    headerLeft: null,
    tabBarLabel: 'Chat',
    tabBarIcon: ({ tintColor }) => (
      <Icon
        size={20}
        name="chat-bubble-outline"
        type="material-icons"
        color={tintColor || theme.gray}
      />
    ),
  };

  async componentDidMount() {
    await client.set();
    client.on('user', this.onUser);
    client.on('rooms', this.onRooms);
    client.on('room', this.onRoom);
    client.on('typing', this.onTyping);
    client.on('connect', this.onConnect);
    client.on('connect_error', this.onConnectError);
  }

  componentWillUnmount() { // eslint-disable-line
    client.off('user');
    client.off('rooms');
    client.off('room');
    client.off('typing');
    client.off('connect');
    client.off('connect_error');
    client.disconnect();
  }

  props: {
    screenProps: { route: string },
    navigation: { navigate: Function, state: { params: any, routeName: string } },
    alertWithType: Function,
    data: {
      viewer: {
        id: string,
      }
    }
  };

  onRoom = (room) => {
    this.setState({
      rooms: this.state.rooms.map((e, i) => (
        e._id === room._id ? {
          ...room,
          users: this.state.rooms[i].users,
        } : e
      )),
    });
  }

  onRooms = (rooms) => {
    console.info('onRooms', rooms);
    this.setState({ rooms });
  }

  onTyping = (room) => {
    console.log('Someone is typing', room);
  }

  onUser = (user) => {
    this.setState({ user }, () => {
      client.emit('rooms');
    });
  }

  onConnect = () => {
    this.setState({ loading: false, error: false }, () => {
      client.emit('user');
    });
  };

  onConnectError = () => {
    setTimeout(() => this.setState({ loading: true, error: true }), 2000);
  };

  // Update from change of route
  // componentWillReceiveProps(nextProps) {
  //   const { route } = this.props.screenProps;
  //   const { routeName } = this.props.navigation.state;
  //   const { route: next } = nextProps.screenProps;
  //   if (next === routeName && route !== routeName) this.initChat();
  // }

  onPressChat = (content, target) => {
    this.setState({ chatAlreadyClicked: true });
    const { navigate } = this.props.navigation;
    const { user } = this.state;
    navigate('Chat', { target, user, room_id: content._id });
  };

  // shouldComponentUpdate(nextProps) {
  //   const { route } = nextProps.screenProps;
  //   const { routeName } = this.props.navigation.state;
  //   return route === routeName;
  // }

  MAINTENANCE = true;

  render() {
    if (this.MAINTENANCE) return <ErrorComponent message={'Le serveur de chat est actuellement en maintenance.  Veuillez ressayer ultérieurement.'} />;
    const { error, loading } = this.state;
    if (loading && !error) return <LoadingComponent />;
    if (loading && error) return <ErrorComponent message={'Le serveur de chat est actuellement en maintenance.  Veuillez ressayer ultérieurement.'} />;
    else if (error) return <ErrorComponent message={'Le serveur de chat est actuellement en maintenance.  Veuillez ressayer ultérieurement.'} />;
    const rooms = this.state.rooms || [];
    const end = rooms.length - 1;
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        style={styles.container}
      >
        <View style={styles.chatView}>
          {rooms.map((content, i) => {
            if (!content
              || !content.last_message
              || !content.last_message.content
            ) return null;
            const user = content.users;
            // user is target
            return (
              <Chat
                // disabled={this.state.chatAlreadyClicked}
                key={content.id}
                userId={user && user.id}
                online={user && user.online}
                content={content}
                isLast={end === i}
                onPress={(fullname, companyId) =>
                  this.onPressChat(content, {
                    ...user,
                    fullname,
                    companyId,
                  })}
              />
            );
          })
          }
        </View>
      </ScrollView>
    );
  }
}

export default connectAlert(Chats);

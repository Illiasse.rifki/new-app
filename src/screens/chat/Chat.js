// @flow
import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Text,
  TextInput,
  Animated,
  Keyboard,
} from 'react-native';
import { Icon } from 'react-native-elements';
import moment from 'moment';

import { theme } from '../../config/constants';

import LoadingComponent from '../../components/Elements/LoadingComponent';
import ErrorComponent from '../../components/Elements/ErrorComponent';

import Message from '../../components/Chats/Message';

import { client } from '../../utils/Chat';

moment.locale('fr');

const styles = StyleSheet.create({
  subTitleView: {
    backgroundColor: 'white',
    borderRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { width: 1, height: 4 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 15,
    paddingBottom: 15,
  },
  subTitleOnline: { fontSize: 17, color: '#33cc80' },
  subTitleOffline: { fontSize: 17, color: 'red' },
  subTitleDash: { fontSize: 17, color: 'black' },
  subTitleLink: { fontSize: 17, color: '#33cbcc' },
  input: { minHeight: 40, lineHeight: 19, flex: 1 },
  footerView: {
    padding: 5,
    alignContent: 'flex-end',
    borderTopWidth: 1,
    borderColor: 'lightgrey',
  },
  footerItem: { padding: 5, flexDirection: 'row', alignItems: 'center' },
  inputSend: { borderLeftWidth: 1, borderColor: 'lightgrey', paddingLeft: 5 },
  info: {
    marginHorizontal: 16,
    marginTop: 24,
    paddingHorizontal: 18,
    paddingVertical: 15,
    backgroundColor: '#f5f5f5',
  },
  infoText: {
    color: theme.fontBlack,
    fontSize: 14,
    fontStyle: 'italic',
    textAlign: 'center',
  },
});

class Chat extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.room && navigation.state.params.room.isAssistance
      ? 'Assistance Bizzeo' : navigation.state.params.target.fullname,
    headerTruncatedBackTitle: '',
  });

  props: {
    navigation: {
      setParams: any,
      navigate: {},
      state: {
        params: {
          room?: any,
          target: { online: boolean, id: string },
          user?: any,
        },
      },
    },
  };

  state = {
    connectionStatus: false,
    room: null,
    loading: true,
    error: false,
    keyboard: 0,
  };

  keyboardHeight = new Animated.Value(0);

  async componentDidMount() {
    this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
    this.keyboardDideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
    this.navigationFocusSubscription = this.props.navigation.addListener('didFocus', this.onRead);
    await client.set();
    client.on('room', this.onRoom);
    client.on('typing', this.onTyping);
    this.onConnect();
  }

  componentWillUnmount() {
    this.navigationFocusSubscription.remove();
    this.keyboardDidShowSub.remove();
    this.keyboardDideSub.remove();
    client.off('connect', this.onConnect);
    client.off('connect_error', this.onConnectError);
    client.off('room', this.onRoom);
    client.off('typing', this.onTyping);
  }


  onConnect = () => {
    const { room_id } = this.props.navigation.state.params;
    console.info('false room', room_id);
    this.setState({ loading: false, error: false }, () => {
      client.emit('messages', room_id);
    });
  };


  onRoom = (room) => {
    console.info('false room', room);
    this.setState({
      room,
    });
  }

  onRead = () => {
    const { room } = this.state;
    if (room) {
      client.sendRead(room._id);
    }
  }

  onConnectError = () => {
    setTimeout(() => this.setState({ loading: true, error: true }), 2000);
  };

  onReconnecting = () => {
    this.setState({ loading: true });
  };

  keyboardDidShow = (event) => {
    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        duration: 500,
        toValue: event.endCoordinates.height,
      }),
    ]).start();
    this.setState({ keyboard: event.endCoordinates.height });
  };

  keyboardDidHide = (event) => {
    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        duration: 500,
        toValue: 0,
      }),
    ]).start();
    this.setState({ keyboard: 0 });
  };

  onTyping = () => {
  }

  handleSend = () => {
    if (this.state.text === '') return false;
    const { text, room } = this.state;
    const { _id: id } = room;
    client.sendMessage(id, text);
    return this.setState({ text: '' });
  };

  onChangeText = (text) => {
    this.setState({ text });
  };

  onPressProfile = (target) => {
    const { navigate } = this.props.navigation;
    if (target.type === 'recruiter') {
      navigate('CompanyProfile', { companyId: target.companyId });
    } else {
      navigate('CandidateProfile', { userId: target.id });
    }
  };

  handleFocus = () => {
    setTimeout(() => this.scrollView.scrollToEnd(), 500);
    client.sendTyping(this.state.room._id);
  }

  MAINTENANCE = true;

  render = () => {
    if (this.MAINTENANCE) return <ErrorComponent message={'Le serveur de chat est actuellement en maintenance.  Veuillez ressayer ultérieurement.'} />;
    const { error, loading, room } = this.state;

    if ((loading || !room) && !error) return <LoadingComponent />;
    if (loading && error) return <ErrorComponent message={'Le serveur de chat est actuellement en maintenance.  Veuillez ressayer ultérieurement.'} />;
    else if (error) return <ErrorComponent message={'Le chat est actuellement en maintenance.  Veuillez ressayer ultérieurement.'} />;
    console.info(room);
    const { state } = this.props.navigation;
    const { target, user } = state.params;

    const link = !room.isAssistance ? (
      <TouchableOpacity transparent onPress={() => this.onPressProfile(target)}>
        <Text style={styles.subTitleDash}>
          {' - '}
          <Text style={styles.subTitleLink}>
            {
              target.type === 'recruiter' ?
                "Voir la fiche de l'entreprise" :
                'Voir le profil'
            }
          </Text>
        </Text>
      </TouchableOpacity>
    ) : null;

    const dayTime = ((new Date()).getHours() >= 9) &&
    ((new Date()).getHours() < 19);
    const checkOnline = (room.isAssistance && dayTime) || (!room.isAssistance && target.online);
    return (
      <Animated.View
        style={{
          backgroundColor: 'white',
          flex: 1,
          paddingBottom: this.keyboardHeight,
        }}
      >
        <View style={styles.subTitleView}>
          {
            checkOnline ?
              < Text style={styles.subTitleOnline}>
              En ligne
              </Text>
              :
              <Text style={styles.subTitleOffline}>
              Hors ligne
              </Text>
          }
          {link}
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          ref={(ref) => {
            this.scrollView = ref;
          }}
          onContentSizeChange={() => this.scrollView.scrollToEnd()}
        >
          {room.info && room.info !== 'Unknown' && (
            <View style={styles.info}>
              <Text style={styles.infoText}>
                {`Cette conversation a été démarrée pour ${room.info} le ${room.created_at}.`}
              </Text>
            </View>
          )}
          {room.messages.map((message, index, array) =>
            <Message
              key={message._id}
              message={message}
              index={index}
              array={array}
              target={target}
              user={user}
            />)}
        </ScrollView>
        <View style={styles.footerView}>
          <View style={styles.footerItem}>
            <TextInput
              multiline
              placeholder="Taper message"
              style={styles.input}
              onChangeText={this.onChangeText}
              value={this.state.text}
              onFocus={this.handleFocus}
            />
            <View style={styles.inputSend}>
              <Icon name="arrow-forward" onPress={this.handleSend} />
            </View>
          </View>
        </View>
      </Animated.View>
    );
  };
}

export default Chat;

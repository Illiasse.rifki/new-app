import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styled from 'styled-components';
import { theme } from '../../config/constants';

export default styled(KeyboardAwareScrollView)`
  flex: 1;
  background-color: ${theme.secondaryColor};
  padding: 32px;
`;
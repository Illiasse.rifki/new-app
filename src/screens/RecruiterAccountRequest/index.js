import React, { Component, Fragment } from 'react';
import KeyboardHelper from '../../components/Elements/KeyboardHelper';

// blocks
import Form from './Form';
import Header from './Header';
import Container from './Container';

class RecruiterAccountRequest extends Component {
  static navigationOptions = {
    title: 'Inscription recruteur',
  };

  props: {
    navigation: { navigate: {} },
    sendEmail: Function,
    alertWithType: Function,
  };

  render() {
    const { navigation } = this.props;
    return (
      <Fragment>
        <Container
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          extraScrollHeight={80}
        >
          <Header />
          <Form navigation={navigation} />
        </Container>
        <KeyboardHelper />
      </Fragment>
    );
  }
}

export default RecruiterAccountRequest;

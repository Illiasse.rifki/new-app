// import React, { Component } from "react";
// import gql from "graphql-tag";

// import FormGenerator from "../../../components/FormGenerator/index";
// import RaisedButton from "../../../components/Button/RaisedButton";
// import T1 from "../../../components/Dumb/T1";
// // import { graphql } from "react-apollo";

// // blocks
// import Container from "./Container";
// import connectAlert from "../../../components/Alert/connectAlert";

// class Form extends Component {
//   props: {
//     navigation: { navigate: {} },
//     sendEmail: Function,
//     alertWithType: Function,
//   };

//   constructor(props) {
//     super(props);
//     this.state = {
//       firstname: "",
//       lastname: "",
//       company: "",
//       phone: "",
//       email: "",
//       comment: "",
//     };
//     this.navigationFormButton = {
//       label: "Envoyer",
//       onPress: this.validateForm,
//     };
//   }

//   validateForm = () => {
//     const { navigate } = this.props.navigation;

//     if (
//       !this.state.firstname ||
//       !this.state.lastname ||
//       !this.state.company ||
//       !this.state.phone ||
//       !this.state.email ||
//       !this.state.comment
//     ) {
//       this.props.alertWithType(
//         "error",
//         "Error",
//         "Vous devez remplir tous les champs"
//       );
//       return;
//     }

//     this.props
//       .sendEmail({
//         variables: {
//           firstname: this.state.firstname,
//           lastname: this.state.lastname,
//           companyname: this.state.company,
//           phone: this.state.phone,
//           email: this.state.email,
//           message: this.state.comment,
//         },
//       })
//       .then((ret) => {
//         if (ret.data.recruiterAccountRequest.mail_sent) {
//           navigate("Validation", {
//             headerTitle: "Message d’inscription envoyé",
//             title: "Merci de l’intérêt porté à Bizzeo !",
//             subtitle:
//               "Notre équipe commerciale vous contactera dans les plus " +
//               "brefs délais pour répondre à vos questions. Merci beaucoup.",
//             buttonName: "C'est compris",
//             goTo: "OnBoarding",
//           });
//         } else if (ret.data.recruiterAccountRequest.error) {
//           this.props.alertWithType(
//             "error",
//             "Error",
//             ret.data.recruiterAccountRequest.error.message
//           );
//         }
//       })
//       .catch(() => {
//         this.props.alertWithType("error", "Error", "Erreur serveur");
//       });
//   };

//   getFormFields = () => {
//     return [
//       {
//         key: "firstname",
//         type: "text",
//         props: {
//           onChangeText: (firstname) => this.setState({ firstname }),
//           placeholder: "Prénom",
//         },
//       },
//       {
//         key: "lastname",
//         type: "text",
//         props: {
//           onChangeText: (lastname) => this.setState({ lastname }),
//           placeholder: "Nom",
//         },
//       },
//       {
//         key: "company-name",
//         type: "text",
//         props: {
//           onChangeText: (company) => this.setState({ company }),
//           placeholder: "Nom de l'entreprise",
//         },
//       },
//       {
//         key: "phone-number",
//         type: "number",
//         props: {
//           onChangeText: (phone) => this.setState({ phone }),
//           placeholder: "Numéro de téléphone",
//         },
//       },
//       {
//         key: "email",
//         type: "text",
//         props: {
//           onChangeText: (email) => this.setState({ email }),
//           placeholder: "Adresse e-mail",
//         },
//       },
//       {
//         key: "comment",
//         type: "text",
//         props: {
//           onChangeText: (comment) => this.setState({ comment }),
//           placeholder: "Écrivez votre message ici",
//           multiline: true,
//         },
//       },
//     ];
//   };

//   render() {
//     return (
//       <Container>
//         <T1>Formulaire</T1>
//         <FormGenerator
//           smartNavigation
//           navigationButton={this.navigationFormButton}
//           buildFields={this.getFormFields()}
//         />
//         <RaisedButton title="Envoyer" onPress={this.validateForm} />
//       </Container>
//     );
//   }
// }

// const SEND_EMAIL_REGISTER_RECRUITER = gql`
//   mutation recruiterAccountRequest(
//     $firstname: String!
//     $lastname: String!
//     $companyname: String!
//     $phone: String!
//     $email: String!
//     $message: String!
//   ) {
//     recruiterAccountRequest(
//       firstname: $firstname
//       lastname: $lastname
//       companyname: $companyname
//       phone: $phone
//       email: $email
//       message: $message
//     ) {
//       mail_sent
//       error {
//         message
//       }
//     }
//   }
// `;

// export default graphql(SEND_EMAIL_REGISTER_RECRUITER, { name: "sendEmail" })(
//   connectAlert(Form)
// );

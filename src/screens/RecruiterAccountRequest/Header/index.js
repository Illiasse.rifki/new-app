import React from 'react';
import Phone from '../../../components/Elements/Phone';

// blocks
import Container from './Container';
import Title from './Title';
import Subtitle from './Subtitle';
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  url: {
    marginTop: 10,
    alignSelf: 'center',
  },
  urlText: {
    fontSize: 18,
  },
});

export default () => (
  <Container>
    <Title>Vous souhaitez vous inscrire sur Bizzeo ?</Title>
    <Subtitle>Nous vous invitons à contacter notre équipe :</Subtitle>
    <Phone
      display="(+33)01.85.08.35.38"
      phoneNumber="0185083538"
      containerStyle={styles.url}
      textStyle={styles.urlText}
    />
  </Container>
);
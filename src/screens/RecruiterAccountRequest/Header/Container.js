import styled from 'styled-components';

export default styled.View`
  flex: 2;
  justify-content: center;
`;
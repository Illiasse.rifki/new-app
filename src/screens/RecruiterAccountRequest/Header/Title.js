import styled from 'styled-components';
import { theme } from '../../../config/constants';

export default styled.Text`
  font-size: 16;
  font-weight: 500;
  text-align: center;
  color: ${theme.fontBlack};
`;
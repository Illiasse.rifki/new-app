import styled from 'styled-components';
import { theme } from '../../../config/constants';

export default styled.Text`
  font-size: 14px;
  font-weight: 400;
  text-align: center;
  color: ${theme.fontBlack};
  margin-top: 10px;
  margin-bottom: 5px;
`;
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableHighlight } from 'react-native';

import candidatIcon from '../assets/sign-up/candidat.png';
import recruiterIcon from '../assets/sign-up/recruteur.png';

const styles = StyleSheet.create({
  screenBackground: {
    flex: 1,
    backgroundColor: '#f5f5f5',
    paddingRight: 16,
    paddingLeft: 16,
    paddingBottom: 19,
    paddingTop: 3,
  },
  imageStyle: {
    flex: 1,
    resizeMode: 'contain',
  },
  typeContainer: {
    flex: 1,
    marginTop: 13,
    backgroundColor: 'white',
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.9,
    shadowRadius: 4,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 16,
  },
  typeImageContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 10,
    paddingTop: 10,
  },
  typeTextContainer: {
    flex: 1,
    width: '80%',
  },
  typeText: {
    fontSize: 28,
    fontWeight: 'bold',
    color: '#1c1c1c',
    textAlign: 'center',
    letterSpacing: 1,
  },
});

class SignUpType extends Component {
  static navigationOptions = {
    title: 'Inscription',
    headerTruncatedBackTitle: '',
  };

  props: {
    navigation: { navigate: {} },
  };

  handleCandidateFormButton = async () => {
    const { navigate } = this.props.navigation;
    navigate('CandidateForm');
  };

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.screenBackground}>
        <TouchableHighlight
          onPress={this.handleCandidateFormButton}
          underlayColor='#ffffff'
          style={styles.typeContainer}
        >
          <View>
            <View style={styles.typeImageContainer}>
              <Image style={styles.imageStyle} source={candidatIcon} />
            </View>
            <View style={styles.typeTextContainer}>
              <Text style={styles.typeText}>Vous êtes un candidat</Text>
            </View>
          </View>
        </TouchableHighlight>
        <TouchableHighlight
          onPress={() => navigate('RecruiterAccountRequest')}
          underlayColor='#ffffff'
          style={styles.typeContainer}
        >
          <View>
            <View style={styles.typeImageContainer}>
              <Image style={styles.imageStyle} source={recruiterIcon} />
            </View>
            <View style={styles.typeTextContainer}>
              <Text style={styles.typeText}>Vous êtes un recruteur</Text>
            </View>
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

export default SignUpType;

// import React, { Component } from 'react';
// import { View, StyleSheet, ScrollView, StatusBar, RefreshControl } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql, compose } from 'react-apollo';
// import Promise from 'bluebird';

// import { NextEventsRegisteredCandidate } from '../components/GQLLinked/EventRegistered';
// import { NextEvents } from '../components/GQLLinked/Events';
// import { theme } from '../config/constants/index';
// import ErrorComponent from '../components/Elements/ErrorComponent';
// import LoadingComponent from '../components/Elements/LoadingComponent';
// import global from './../config/global';
// import SxCard from '../components/Card/SxCard';
// import Drawer from '../components/Content/Drawer';
// import JobsList from '../components/JobOffer/JobOffersList';
// import JobsRegistrationList from '../components/JobOffer/JobOfferRegistrationCardList';
// import { Icon } from '../components/Pure/react-native-elements';
// import { CurrentEventsRegistration } from '../components/GQLLinked/EventRegistration';

// const styles = StyleSheet.create({
//   home: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//   },
//   homeContainer: {
//     paddingVertical: 24,
//     paddingHorizontal: 16,
//   },
//   homeHeader: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//   },
//   eventsContainer: {
//     marginTop: 34,
//     marginBottom: 20,
//   },
//   bg: {
//     marginTop: 20,
//   },
//   sxCard: { marginHorizontal: 15 },
// });

// const VIEWER_CANDIDATE = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         finished_profile
//         next_event_registrations(filter: "accepted") {
//           event_registration_id
//         }
//         current_event_registrations {
//           event_registration_id
//         }
//         job_offers_registrations {
//           job_offer_registration_id
//           status
//           job_offer {
//             job_offer_id
//             title
//             date_publish(duration: true)
//             visibility_mode
//             cover_picture
//             location_name
//             location_departement
//             rating
//             company {
//               logo
//               name
//             }
//             tags
//           }
//         }
//       }
//     }
//   }
// `;

// const NB_EVENTS = gql`
//   query {
//     next_events {
//       event_id
//     }
//   }
// `;

// const JOB_OFFERS = gql`
//   query {
//     jobOffers {
//       job_offer_id
//     }
//   }
// `;

// class Home extends Component {
//   static navigationOptions = ({ navigation }) => ({
//     title: 'Accueil',
//     headerLeft: <Drawer.Icon
//       onPress={() => navigation.navigate('DrawerOpen')}
//     />,
//     tabBarIcon: ({ tintColor }) => (
//       <Icon size={20} name="home" type="entypo" color={tintColor || theme.gray} />
//     ),
//   });

//   constructor(props) {
//     super(props);
//     this.state = {
//       refreshing: false,
//     };
//     StatusBar.setBarStyle('light-content', true);
//     global.authStockage.setToken('signed-as', 'candidate');
//   }

//   shouldComponentUpdate(nextProps) {
//     const { route } = nextProps.screenProps;
//     const { routeName } = this.props.navigation.state;
//     if (route !== routeName && route !== '') {
//       return false;
//     } else if (this.props !== nextProps) {
//       if (this.props.data && !this.props.data.loading && !this.props.data.error && !this.props.data.viewer) {
//         this.props.data.refetch();
//       }
//       return true;
//     }
//     return false;
//   }

//   componentDidUpdate(prevProps) {
//     if (
//       prevProps.data.loading &&
//       !this.props.data.loading &&
//       (!this.props.data.viewer ||
//       !this.props.data.viewer.candidate)
//     ) {
//       this.props.navigation.navigate('Login');
//     }
//   }

//   onRefresh = () => {
//     this.setState({ refreshing: true });
//     const promises = [
//       this.props.events.refetch(),
//       this.props.data.refetch(),
//       this.props.jobs.refetch(),
//       this.props.jobsPertinence.refetch(),
//     ];
//     Promise.all(promises).then(() => {
//       this.setState({ refreshing: false });
//     }).catch(e => console.log('catch refetch Events.js', e));
//   };

//   props: {
//     navigation: {
//       navigate: Function,
//     },
//     jobsPertinence: {
//       error: {},
//       loading: boolean,
//       jobOffers: [],
//     },
//     events: {
//       loading: boolean,
//       error: {},
//       next_events: {},
//       refetch: Function,
//     },
//     jobs: {
//       loading: boolean,
//       error: {},
//       jobOffers: {},
//       refetch: Function,
//     },
//     data: {
//       loading: boolean,
//       error: {},
//       viewer: {
//         id: string,
//         candidate: {
//           finished_profile: boolean,
//         },
//       },
//     },
//   };

//   render() {
//     const { route } = this.props.screenProps;
//     const { routeName } = this.props.navigation.state;
//     const { navigate } = this.props.navigation;
//     if (this.props.data.error || this.props.events.error
//       || this.props.jobs.error || this.props.jobsPertinence.error) {
//       return <ErrorComponent message="Problème lors de la récupération la page d'accueil." />;
//     } else if (this.props.data.loading
//       || this.props.events.loading
//       || this.props.jobs.loading
//       || this.props.jobsPertinence.loading) {
//       return <LoadingComponent />;
//     }
//     const { next_events = [] } = this.props.events;
//     const { jobOffers = [] } = this.props.jobs;
//     const { jobOffers: jobOffersPertinence = [] } = this.props.jobsPertinence;

//     if (!this.props.data.viewer || !this.props.data.viewer.candidate) {
//       return <LoadingComponent />;
//     }
//     const { candidate: {
//       next_event_registrations: nextEventRegistrations = [],
//       job_offers_registrations: jobOffersRegistrations = [],
//     } } = this.props.data.viewer;
//     return (
//       <ScrollView
//         contentContainerStyle={styles.homeContainer}
//         style={styles.home}
//         showsVerticalScrollIndicator={false}
//         showsHorizontalScrollIndicator={false}
//         refreshControl={<RefreshControl
//           refreshing={this.state.refreshing}
//           onRefresh={this.onRefresh}
//         />}
//       >
//         <View style={styles.homeHeader}>
//           <SxCard
//             onPress={() => this.props.navigation.navigate('EventsList')}
//             number={next_events.length}
//             style={{ marginBottom: 24 }}
//             title="Événements"
//           />
//           {/* <SxCard
//             style={styles.sxCard}
//             onPress={() => this.props.navigation.navigate('JobOffers')}
//             number={jobOffers.length}
//             title="Offres"
//           /> */}
//           {/* <SxCard
//             onPress={() => this.props.navigation.navigate('JobOffersRegistrationsList')}
//             number={jobOffersRegistrations.length}
//             title="Candidatures"
//           /> */}
//         </View>
//         {/* { !!jobOffersPertinence.length &&
//           <JobsList
//             title={'Suggestions d\'offres'}
//             horizontal={true}
//             jobs={jobOffersPertinence}
//             listStyle={styles.bg}
//             onRefresh={this.onRefresh}
//             onJobOfferPress={(jobOffer) => {
//               this.props.navigation.navigate('JobOffer', { jobOfferId: jobOffer.job_offer_id });
//             }}
//           />
//         }
//         { !!jobOffersRegistrations.length &&
//           <JobsRegistrationList
//             title={'Offres auxquelles vous avez postulé'}
//             horizontal={true}
//             jobsRegistration={jobOffersRegistrations}
//             listStyle={styles.bg}
//             onRefresh={this.onRefresh}
//             onJobOfferRegistrationPress={(jobOffer) => {
//               this.props.navigation.navigate('JobOffer', { jobOfferId: jobOffer.job_offer_id });
//             }}
//           />
//         } */}
//         {
//           <CurrentEventsRegistration
//             navigate={navigate}
//             containerStyle={styles.eventsRegisterContainer}
//             title="Événement en cours"
//             more={this.state.moreInscriptions}
//             route={route}
//             routeName={routeName}
//             refresh={this.state.refreshing}
//           />
//         }
//         {
//           nextEventRegistrations.length === 0 ?
//             <NextEvents
//               containerStyle={styles.eventsContainer}
//               navigate={this.props.navigation.navigate}
//               next
//               title={'Prochains événements'}
//               refresh={this.state.refreshing}
//             /> :
//             <NextEventsRegisteredCandidate
//               containerStyle={styles.eventsContainer}
//               next
//               navigate={this.props.navigation.navigate}
//               title="Vos événements à venir"
//               route={this.props.screenProps.route}
//               routeName={this.props.navigation.state.routeName}
//               refresh={this.state.refreshing}
//             />
//         }
//       </ScrollView>
//     );
//   }
// }

// const JOB_OFFER_LIST_FILTER = filter => (gql`
//   query jobOffers($offset: Int, $limit: Int) {
//     jobOffers(offset: $offset, limit: $limit, filter: "${filter}") {
//       job_offer_id
//       title
//       date_publish(duration: true)
//       visibility_mode
//       cover_picture
//       location_name
//       location_departement
//       rating
//       company {
//         logo
//         header
//         name
//       }
//       tags
//     }
//   }
// `);

// export default compose(
//   graphql(VIEWER_CANDIDATE),
//   graphql(NB_EVENTS, { name: 'events' }),
//   graphql(JOB_OFFERS, { name: 'jobs' }),
//   graphql(JOB_OFFER_LIST_FILTER('pertinence'), {
//     name: 'jobsPertinence',
//     options: {
//       variables: {
//         offset: 0,
//         limit: 5,
//       },
//     },
//   }),
// )(Home);

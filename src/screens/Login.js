// import React, { Component } from 'react';
// import { View, ScrollView, StyleSheet, Keyboard, KeyboardAvoidingView, Platform, Image, Animated, WebView, TouchableHighlight, Text, Linking } from 'react-native';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';
// import { Button } from 'react-native-elements';
// // import { LoginManager, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
// import OneSignal from 'react-native-onesignal';
// import randomatic from 'randomatic';

// import global from './../config/global';
// import { theme, window, LINKEDIN } from '../config/constants/index';
// import Separator from '../components/Design/Separator';
// import connectAlert from '../components/Alert/connectAlert';
// import createChatClient from '../utils/Chat';
// import logo from '../assets/Logo-inverted.png';
// import KeyboardHelper from '../components/Elements/KeyboardHelper';
// import FormGenerator from '../components/FormGenerator';

// const styles = StyleSheet.create({
//   screenBackground: {
//     flex: 1,
//     backgroundColor: theme.secondaryColor,
//     paddingHorizontal: 32,
//     paddingVertical: 0,
//   },
//   logoContainer: {
//     alignItems: 'center',
//     justifyContent: 'flex-start',
//   },
//   loginFormView: {
//     flex: 2,
//   },
//   fieldContainer: {
//   },
//   loginFormLabelContent: {
//     marginTop: 0,
//     marginRight: 0,
//     marginLeft: 0,
//     color: '#afafaf',
//     letterSpacing: 0.5,
//     fontSize: 14,
//     fontWeight: 'normal',
//   },
//   loginFormInputContainer: {
//     marginRight: 0,
//     marginLeft: 0,
//   },
//   loginFormInput: {
//     color: '#000000',
//     marginBottom: -4,
//   },
//   forgotPasswordButton: {
//     backgroundColor: 'transparent',
//     justifyContent: 'flex-start',
//     paddingLeft: 0,
//     paddingRight: 0,
//   },
//   forgotPasswordButtonContainer: {
//     marginLeft: 0,
//   },
//   forgotPasswordButtonText: {
//     color: theme.primaryColor,
//     fontSize: 16,
//     fontWeight: '500',
//   },
//   submitButtonsContainer: {
//     flex: 2.5,
//     justifyContent: 'flex-start',
//   },
//   connectButton: {
//     backgroundColor: theme.primaryColor,
//     borderRadius: 4,
//     shadowColor: 'rgba(0, 0, 0, 0.16)',
//     shadowOffset: { width: 0, height: 1.5 },
//     shadowOpacity: 0.8,
//     shadowRadius: 2,
//   },
//   connectButtonText: {
//     color: theme.secondaryColor,
//   },
//   connectButtonContainer: {
//     marginTop: 20,
//   },
//   separatorView: {
//     marginTop: 5,
//     flex: 1,
//     flexDirection: 'row',
//   },
//   connectButtonView: {
//     justifyContent: 'center',
//   },
//   socialButtonsContainer: {
//     flex: 1,
//     flexDirection: 'row',
//     alignItems: 'center',
//     justifyContent: 'space-between',
//   },
//   socialButton: {
//     width: '100%',
//     borderRadius: 4,
//     paddingLeft: 0,
//     paddingTop: 4.5,
//     paddingBottom: 4.5,
//     height: 40,
//   },
//   socialButtonText: {
//     color: 'white',
//     fontSize: 16,
//   },
//   facebookButtonCont: {
//     width: window.x < 350 ? '48%' : '45%',
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   linkedButtonCont: {
//     width: window.x < 350 ? '48%' : '45%',
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   logo: {
//     flex: 1,
//     resizeMode: 'contain',
//     width: '70%',
//   },
//   cguButton: {
//     textAlign: 'center',
//     textDecorationLine: 'underline',
//   },
//   cguTextContainer: {
//     alignSelf: 'center',
//     flex: 1,
//     flexDirection: 'column',
//   },
// });

// class Login extends Component {
//   static navigationOptions = {
//     title: 'Connexion',
//   };

//   onIds = (ids) => {
//     this.mobileId = ids.userId;
//   };

//   componentWillMount() {
//     const willDid = Platform.OS === 'android' ? 'Did' : 'Will';
//     this.keyboardWillShowSub = Keyboard.addListener(`keyboard${willDid}Show`, this.keyboardWillShow);
//     this.keyboardWillHideSub = Keyboard.addListener(`keyboard${willDid}Hide`, this.keyboardWillHide);
//     OneSignal.addEventListener('ids', this.onIds);
//   }

//   componentWillUnmount() {
//     this.keyboardWillShowSub.remove();
//     this.keyboardWillHideSub.remove();
//     OneSignal.removeEventListener('ids', this.onIds);
//   }

//   constructor(props) {
//     super(props);
//     this.state = {
//       email: '',
//       password: '',
//       loading: false,
//       linkedin: false,
//       facebookId: '',
//       linkedinId: '',
//       keyboard: false,
//       firstname: '',
//       lastname: '',
//       currentRef: null,
//       headline: '',
//     };
//     this.imageHeight = new Animated.Value(83);
//     this.navigationFormButton = {
//       label: 'Se connecter',
//       onPress: this.handleOnClickConnect,
//     };
//   }

//   keyboardWillShow = () => {
//     if (this.state.keyboard) return;
//     Animated.timing(this.imageHeight, {
//       toValue: 40,
//     }).start();
//     this.setState({ keyboard: true });
//   };

//   keyboardWillHide = () => {
//     if (!this.state.keyboard) return;
//     Animated.timing(this.imageHeight, {
//       toValue: 83,
//     }).start();
//     this.setState({ keyboard: false });
//   };

//   props: {
//     navigation: {
//       navigate: Function,
//     },
//     addCandidate: any,
//     login: any,
//     alertWithType: Function,
//   };

//   handleOnClickConnect = async () => {
//     const { navigate } = this.props.navigation;
//     const {
//       linkedinId: linkedin,
//       facebook,
//       firstname,
//       lastname,
//       headline,
//       email,
//       password,
//     } = this.state;
//     if (!email || (!password && !linkedin && !facebook)) {
//       this.props.alertWithType('error', 'Erreur', 'Vous devez remplir le champ e-mail et mot de passe');
//       return;
//     }
//     this.setState({ loading: true });
//     const promises = [];
//     promises.push(this.props.login({
//       variables: {
//         email: email.trim(),
//         password: password.trim(),
//         facebook,
//         linkedin,
//         type: 'candidate',
//         player_id: this.mobileId,
//       },
//     }));
//     promises.push(this.props.login({
//       variables: {
//         email: email.trim(),
//         password: password.trim(),
//         facebook,
//         linkedin,
//         type: 'recruiter',
//         player_id: this.mobileId,
//       },
//     }));
//     let log;
//     try {
//       log = await Promise.all(promises);
//     } catch (e) {
//       this.props.alertWithType('error', 'Error', e.message);
//       return;
//     }
//     let candidate = log[0].data.login.token;
//     const recruiter = log[1].data.login.token;
//     if (!candidate && !recruiter && (linkedin || facebook)) {
//       try {
//         await this.props.addCandidate({
//           variables: {
//             email,
//             password: randomatic('Aa0', 6),
//             firstname,
//             lastname,
//             finished_profile: false,
//             headline,
//           },
//         });
//       } catch (e) {
//         this.props.alertWithType('error', 'Error', e.message);
//         return;
//       }
//       try {
//         candidate = await this.props.login({
//           variables: {
//             email,
//             password,
//             facebook,
//             linkedin,
//             type: 'candidate',
//             player_id: this.mobileId,
//           },
//         });
//       } catch (e) {
//         this.props.alertWithType('error', 'Error', e.message);
//         return;
//       }
//       candidate = candidate.data.login.token;
//     }
//     if (!candidate && !recruiter) {
//       this.props.alertWithType('error', 'Erreur', 'E-mail et/ou mot de passe incorrect');
//       this.setState({ loading: false });
//       return;
//     }
//     global.authStockage.setToken('access-token', candidate);
//     global.authStockage.setToken('recruiter-access-token', recruiter);
//     // createChatClient({ accessToken: candidate, recruiterAccessToken: recruiter });
//     if (candidate && recruiter) {
//       navigate('AccountType', {
//         candidateScreen: 'TabViews',
//         recruiterScreen: 'RecruiterTabViews',
//       });
//     } else if (candidate) {
//       navigate('TabViews');
//     } else if (recruiter) {
//       navigate('RecruiterTabViews');
//     }
//     this.setState({ loading: false });
//   };

//   facebookData = (res) => {
//     if (!res) return;
//     const { id: facebook, first_name: firstname, last_name: lastname, email } = res;
//     this.setState({ facebook, email, firstname, lastname }, () => this.handleOnClickConnect());
//   };

//   facebookSignIn = async () => {
//     const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync('2104766936516522', {
//       permissions: ['public_profile', 'email'],
//     });
//     if (type === 'success') {
//       // Get the user's name using Facebook's Graph API
//       const response = await fetch(
//         `https://graph.facebook.com/me?access_token=${token}&fields=id,first_name,last_name,email,birthday`);
//       const result = await response.json();
//       if (!result) {
//         this.props.alertWithType(
//           'info',
//           'Facebook',
//           'L\'authentification par Facebook à échoué, réssayer plus tard.\n',
//         );
//       }
//       this.facebookData(result);
//     } else {
//       this.props.alertWithType(
//         'info',
//         'Facebook',
//         'L\'authentification par Facebook à échoué, réssayer plus tard.\n',
//       );
//     }
//   }

//   linkedinSignIn = async ({ nativeEvent: { data: rawData } }) => {
//     let data;
//     try {
//       data = JSON.parse(rawData);
//     } catch (e) {
//       console.warn('catch', e); // gestion timeout to stop linkedinSingIn or LookAtUrl
//       return;
//     }
//     this.setState({ linkedin: false });
//     if (data.code !== 200) {
//       this.props.alertWithType('error', 'Erreur', data);
//       return;
//     }
//     const {
//       data: {
//         firstName: firstname,
//         lastName: lastname,
//         headline,
//         emailAddress: email,
//         id: linkedin,
//       },
//     } = data;
//     this.setState({ email, linkedinId: linkedin, firstname, lastname, headline }, () => this.handleOnClickConnect());
//   }

//   getFormFields = () => ([
//     {
//       key: 'login',
//       type: 'text',
//       props: {
//         label: 'ADRESSE E-MAIL',
//         placeholder: 'Votre adresse e-mail',
//         onChangeText: email => this.setState({ email }),
//         value: this.state.email,
//         autoCapitalize: 'none',
//       },
//     },
//     {
//       key: 'password',
//       type: 'text',
//       props: {
//         label: 'MOT DE PASSE',
//         placeholder: 'Votre mot de passe',
//         onChangeText: password => this.setState({ password }),
//         value: this.state.password,
//         secureTextEntry: true,
//         autoCapitalize: 'none',
//       },
//     },
//   ]);

//   renderExternalSignIn = () => (
//     <View style={{ flex: 1 }}>
//       <Separator color="#afafaf" text="OU AVEC" viewStyle={styles.separatorView} />
//       <View style={styles.socialButtonsContainer}>
//         <Button
//           title="Facebook"
//           onPress={this.facebookSignIn}
//           buttonStyle={styles.socialButton}
//           backgroundColor="#4970bb"
//           textStyle={styles.socialButtonText}
//           icon={{ name: 'facebook-official', type: 'font-awesome', size: 31 }}
//           containerViewStyle={styles.facebookButtonCont}
//         />
//         <Button
//           title="LinkedIn"
//           onPress={() => this.setState({ linkedin: true })}
//           buttonStyle={styles.socialButton}
//           textStyle={styles.socialButtonText}
//           backgroundColor="#225982"
//           icon={{ name: 'linkedin', type: 'font-awesome', size: 24 }}
//           containerViewStyle={styles.linkedButtonCont}
//         />
//       </View>
//       <View style={styles.cguTextContainer}>
//         <TouchableHighlight underlayColor="#ffffff" onPress={this.checkCGU}>
//           <Text>En vous connectant, vous acceptez nos</Text>
//         </TouchableHighlight>
//         <TouchableHighlight
//           underlayColor="#ffffff"
//           onPress={() => {
//             Linking.openURL('https://bizzeo.co/cgu').catch();
//           }}
//         >
//           <Text style={styles.cguButton}>conditions générales d’utilisation.</Text>
//         </TouchableHighlight>
//       </View>
//     </View>
//   );

//   render() {
//     if (this.state.linkedin) {
//       return (
//         <WebView
//           source={{ uri: LINKEDIN }}
//           injectedJavaScript={'setTimeout(function(){window.postMessage(document.body.innerHTML, \'*\');}, 10);'}
//           onMessage={this.linkedinSignIn}
//           javaScriptEnabled
//         />
//       );
//     }
//     return (
//       <ScrollView style={styles.screenBackground}>
//         <KeyboardAvoidingView
//           style={{ flex: 1 }}
//           behavior={Platform.OS === 'ios' ? 'padding' : null}
//         >
//           <Animated.View style={[styles.logoContainer, { height: this.imageHeight }]}>
//             <Image source={logo} resizeMode='contain' style={[styles.logo]} />
//           </Animated.View>
//           <View style={styles.loginFormView}>
//             <FormGenerator
//               smartNavigation
//               navigationButton={this.navigationFormButton}
//               buildFields={this.getFormFields()}
//             />
//             <Button
//               title="Mot de passe oublié ?"
//               onPress={() => this.props.navigation.navigate('RetrievePassword')}
//               containerViewStyle={styles.forgotPasswordButtonContainer}
//               buttonStyle={styles.forgotPasswordButton}
//               textStyle={styles.forgotPasswordButtonText}
//             />
//           </View>
//           <View style={styles.submitButtonsContainer}>
//             <View style={styles.connectButtonView}>
//               {!this.state.keyboard && <Button
//                 title="Se connecter"
//                 disabled={this.state.loading}
//                 buttonStyle={styles.connectButton}
//                 textStyle={styles.connectButtonText}
//                 containerViewStyle={styles.connectButtonContainer}
//                 onPress={this.handleOnClickConnect}
//               />}
//             </View>
//             {/* {!this.state.keyboard && this.renderExternalSignIn()} */}
//           </View>
//         </KeyboardAvoidingView>
//         <KeyboardHelper />
//       </ScrollView>
//     );
//   }
// }

// const LOGGED_IN_MUTATION = gql`
//   mutation login(
//     $email: String!,
//     $password: String!,
//     $type: String!,
//     $facebook: String,
//     $linkedin: String,
//     $player_id: String,
//   ) {
//     login(
//       email: $email,
//       password: $password,
//       type: $type,
//       facebook: $facebook,
//       linkedin: $linkedin,
//       player_id: $player_id
//     ) {
//       token
//     }
//   }
// `;

// const addCandidate = gql`
//   mutation(
//     $email: String!,
//     $password: String!,
//     $firstname: String!,
//     $lastname: String!,
//     $birthdate: String,
//     $phone: String,
//     $picture: String,
//     $headline: String,
//     $finished_profile: Boolean,
//   ) {
//     createCandidateWithoutUser(
//       email: $email,
//       password: $password,
//       firstname: $firstname,
//       lastname: $lastname,
//       birthdate: $birthdate
//       phone: $phone
//       finished_profile: $finished_profile
//       picture: $picture
//       headline: $headline
//     ) {
//       candidate {
//         user_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// export default compose(
//   graphql(addCandidate, { name: 'addCandidate' }),
//   graphql(LOGGED_IN_MUTATION, { name: 'login' }),
// )(connectAlert(Login));

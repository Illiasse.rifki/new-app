import { TabNavigator, TabBarTop } from 'react-navigation';
import React from 'react';

import { theme } from '../../config/constants/index';

import RecruiterJobOfferRegistrationProfile from './RecruiterJobOfferRegistrationProfile';
import RecruiterJobOfferRegistrationQuestions from './RecruiterJobOfferRegistrationQuestions';

import JobOfferCandidateFavorite from '../../components/JobOffer/JobOfferCandidateFavorite';

const favoriteIconStyle = { marginTop: 24, marginRight: 24 };

export default TabNavigator(
  {
    RecruiterJobOfferRegistrationProfile: {
      screen: RecruiterJobOfferRegistrationProfile,
    },
    RecruiterJobOfferRegistrationQuestions: {
      screen: RecruiterJobOfferRegistrationQuestions,
    },
  },
  {
    navigationOptions: ({ navigation: { state: { params } } }) => ({
      headerRight: <JobOfferCandidateFavorite
        jobOfferRegistrationId={params.jobOfferRegistrationId}
        style={favoriteIconStyle}
      />,
    }),
    lazy: true,
    tabBarComponent: TabBarTop,
    tabBarPosition: 'top',
    animationEnabled: false,
    swipeEnabled: false,
    tabBarOptions: {
      indicatorStyle: {
        backgroundColor: theme.primaryColor,
        height: 4,
      },
      showIcon: false,
      activeTintColor: theme.fontBlack,
      inactiveTintColor: theme.gray,
      upperCaseLabel: false,
      labelStyle: {
        fontSize: 12,
        marginTop: 0,
        marginLeft: 0,
        marginRight: 0,
      },
      iconStyle: {
        marginBottom: 0,
        color: theme.gray,
      },
      tabStyle: {
        justifyContent: 'flex-end',
        paddingTop: 15.5,
      },
      style: {
        justifyContent: 'center',
        borderTopColor: 'transparent',
        borderTopWidth: 0,
        backgroundColor: 'white',
        paddingLeft: 0,
        paddingRight: 0,
        height: 49,
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 0, height: 1.5 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
      },
    },
    initialRouteName: 'RecruiterJobOfferRegistrationProfile',
  },
);

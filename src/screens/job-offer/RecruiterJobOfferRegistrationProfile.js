// import React, { Component } from 'react';
// import { StyleSheet, View, ScrollView, Text } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql, compose } from 'react-apollo';

// import Profile from '../../components/GQLLinked/Profile';

// import { theme } from '../../config/constants/index';
// import { Button } from '../../components/Button';

// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';

// import connectAlert from '../../components/Alert/connectAlert';
// import {
//   CHECK_QUERY_JOB_OFFER_REGISTRATIONS,
//   QUERY_JOB_OFFER_REGISTRATIONS,
// } from './RecruiterJobOfferRegistrationListGeneric';

// const styles = StyleSheet.create({
//   main: {
//   },
//   header: {
//     backgroundColor: 'white',
//     paddingVertical: 12,
//     paddingHorizontal: 32,
//   },
//   headerText: {
//     color: theme.grey,
//     fontSize: 14,
//     lineHeight: 16,
//     textAlign: 'center',
//     fontWeight: '500',
//   },
//   JobOfferRegistration: {
//     backgroundColor: '#f5f5f5',
//     paddingVertical: 24,
//     paddingHorizontal: 32,
//   },
//   JobOfferRegistrationGroup: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     marginTop: 8,
//   },
//   JobOfferRegistrationPreselect: {
//     marginTop: 0,
//     marginBottom: 8,
//   },
//   JobOfferRegistrationChat: {
//     marginTop: 0,
//     marginBottom: 0,
//     width: 125,
//   },
//   JobOfferRegistrationRefuse: {
//     marginTop: 0,
//     marginBottom: 0,
//     width: 125,
//     backgroundColor: theme.red,
//   },
// });

// const MUTATION_JOB_OFFER_REGISTRATION = gql`
//   mutation EditJobOfferRegistration($job_offer_registration_id: String!, $status: String!) {
//     editJobOfferRegistration(job_offer_registration_id: $job_offer_registration_id, status: $status) {
//       jobOfferRegistration {
//         job_offer_registration_id
//       }
//       error {
//         code
//         message
//       }
//     }
//   }
// `;

// const QUERY_JOB_OFFER_REGISTRATION = gql`
//   query JobOfferRegistration($job_offer_registration_id: String!) {
//     jobOfferRegistration(job_offer_registration_id: $job_offer_registration_id) {
//       job_offer_registration_id
//       recruiter {
//         user_id
//       }
//       candidate {
//         user_id
//         fullname
//         wish_raw_salary
//         wish_variable_salary
//       },
//       jobOffer: job_offer {
//         job_offer_id
//         title
//         raw_salary_min
//         raw_salary_max
//         var_salary_min
//         var_salary_max
//       }
//       status
//     }
//   }
// `;

// class RecruiterJobOfferRegistrationProfile extends Component {
//   static navigationOptions = {
//     title: 'Profil',
//   };

//   props: {
//     alertWithType: Function,
//     mutation: Function,
//     data: {
//       refetch: Function,
//       jobOfferRegistration: {
//         job_offer_registration_id: string,
//         status: string,
//         jobOffer: {
//           job_offer_id: string,
//           title: string,
//         },
//         recruiter: {
//           user_id: string,
//         },
//         candidate: {
//           user_id: string,
//           fullname: string,
//         },
//       },
//     },
//     navigation: {
//       navigate: Function,
//       state: {
//         params: {
//           userId: string,
//           jobOfferId: string,
//           jobOfferRegistrationId: string,
//         },
//       },
//     },
//   };

//   componentDidUpdate() {
//     const { data } = this.props;
//     if (data && !data.loading && !data.error && !data.jobOfferRegistration) {
//       this.props.data.refetch();
//     }
//   }

//   handleMutation = (status, info) => {
//     const { jobOffer } = this.props.data.jobOfferRegistration;
//     this.props.mutation({
//       variables: {
//         job_offer_registration_id: this.props.navigation.state.params.jobOfferRegistrationId,
//         status,
//       },
//       refetchQueries: [{
//         query: QUERY_JOB_OFFER_REGISTRATION,
//         variables: {
//           job_offer_registration_id: this.props.navigation.state.params.jobOfferRegistrationId,
//         },
//       }, {
//         query: CHECK_QUERY_JOB_OFFER_REGISTRATIONS,
//         variables: {
//           job_offer_id: jobOffer.job_offer_id,
//           status: 'pending',
//         },
//       }, {
//         query: QUERY_JOB_OFFER_REGISTRATIONS,
//         variables: {
//           job_offer_id: jobOffer.job_offer_id,
//           status: 'preselected',
//         },
//       }, {
//         query: QUERY_JOB_OFFER_REGISTRATIONS,
//         variables: {
//           job_offer_id: jobOffer.job_offer_id,
//           status: 'apply',
//         },
//       }],
//     }).then(() => (status !== 'pending' ? this.handleConfirm(
//       `Le candidat est ${status === 'refused' ? 'refusé' : 'préselecionné'}`,
//       `${status === 'refused' ? 'Refus' : 'Préselection'} effectué`,
//       `${this.props.data.jobOfferRegistration.candidate.fullname} à bien été ${status === 'refused' ? 'refusé' : 'préselecionné'} pour l'ofrre ${this.props.data.jobOfferRegistration.jobOffer.title}`,
//       'Retourner aux candidatures',
//     ) : this.props.alertWithType('info', 'Info', info)))
//       .catch(e => this.props.alertWithType('error', 'Erreur', e.toString()));
//   }

//   handleConfirm = (headerTitle, title, subtitle, buttonName) => this.props.navigation.navigate('Validation', {
//     headerTitle,
//     title,
//     subtitle,
//     buttonName,
//     goBack: true,
//   });

//   handlePreselect = () => this.handleMutation('preselected', 'Status de la candidate mis à jour: préselectionné', this.handleConfirm);

//   handleCancel = () => this.handleMutation('pending', 'Status de la candidate mis à jour: en attente');

//   handleRefuse = () => this.handleMutation('refused', 'Status de la candidate mis à jour: refusé', this.handleConfirm);

//   handleChat = () => this.props.navigation.navigate('Chat', {
//     target: {
//       id: this.props.data.jobOfferRegistration.candidate.user_id,
//       fullname: this.props.data.jobOfferRegistration.candidate.fullname,
//     },
//     user: {
//       user_id: this.props.data.jobOfferRegistration.recruiter.user_id,
//     },
//     from: `la candidature sur l'offre ${this.props.data.jobOfferRegistration.jobOffer.title}`,
//   });

//   render() {
//     const { userId } = this.props.navigation.state.params;
//     const { data } = this.props;

//     if (!data || data.error) return <ErrorComponent message={data.error} />;
//     if (data.loading || !data.jobOfferRegistration) return <LoadingComponent />;

//     const { status, jobOffer, candidate } = data.jobOfferRegistration;

//     let compatibility;
//     const { raw_salary_min: rawMin, raw_salary_min: rawMax, var_salary_min: varMin, var_salary_max: varMax } = jobOffer;
//     const { wish_raw_salary: wishRaw, wish_variable_salary: wishVar } = candidate;

//     if (wishRaw <= rawMax && wishRaw >= rawMin && wishVar <= varMax && wishVar >= varMin) {
//       compatibility = 'Compatible';
//     } else if (wishRaw <= rawMax * 1.25 && wishRaw >= rawMin / 1.25 && wishVar <= varMax * 1.25 && wishVar >= varMin / 1.25) {
//       compatibility = 'Proche';
//     } else if ((wishRaw > rawMax || wishRaw <= rawMin) || (wishVar >= varMax && wishVar <= varMin)) {
//       compatibility = 'Incompatible';
//     }

//     const jobOfferRegistration = (
//       <View style={styles.JobOfferRegistration}>
//         {status === 'pending' && <Button onPress={this.handlePreselect} buttonStyle={styles.JobOfferRegistrationPreselect} title='Préselectionner' />}
//         <View style={styles.JobOfferRegistrationGroup}>
//           {status === 'pending' && <Button onPress={this.handleRefuse} buttonStyle={styles.JobOfferRegistrationRefuse} title='Refuser' />}
//           {status !== 'pending' && <Button onPress={this.handleCancel} buttonStyle={styles.JobOfferRegistrationRefuse} title='Annuler' />}
//         </View>
//       </View>
//     );
//     return (
//       <ScrollView style={styles.main} contentContainerStyle={styles.mainContent}>
//         <View style={styles.header}>
//           <Text style={styles.headerText}>Ce profil a postulé pour votre offre {jobOffer.title}</Text>
//         </View>
//         {jobOfferRegistration}
//         <Profile editable={false} userId={userId} compatibility={compatibility} />
//         {jobOfferRegistration}
//       </ScrollView>
//     );
//   }
// }

// export default compose(
//   graphql(QUERY_JOB_OFFER_REGISTRATION, {
//     options: ({ navigation }) => ({
//       variables: {
//         job_offer_registration_id: navigation.state.params.jobOfferRegistrationId,
//       },
//     }),
//   }),
//   graphql(MUTATION_JOB_OFFER_REGISTRATION, { name: 'mutation' }),
// )(connectAlert(RecruiterJobOfferRegistrationProfile));

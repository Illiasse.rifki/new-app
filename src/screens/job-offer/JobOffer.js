// import React, { Component } from 'react';
// import { StyleSheet, ScrollView } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';
// import Promise from 'bluebird';

// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import jobOfferBuilder from '../../components/JobOffer/JobOffer';
// import JobOfferFavorite from '../../components/JobOffer/JobOfferFavorite';

// const styles = StyleSheet.create({
//   main: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//   },
//   bg: {
//     flex: 1,
//     paddingTop: 90,
//     paddingRight: 16,
//     paddingLeft: 16,
//   },
//   searchContainer: {
//     position: 'absolute',
//     zIndex: 100,
//     top: 24,
//     left: 0,
//     right: 0,
//   },
//   card: {
//     marginBottom: 16,
//   },
//   favoriteIconStyle: { marginRight: 24 },
// });

// const VISIT_JOB_OFFER = gql`
//   query visitJobOffer($job_offer_id: String!) {
//     visitJobOffer(job_offer_id: $job_offer_id) {
//       job_offer_id
//     }
//   }
// `;

// class JobOffer extends Component {
//   static navigationOptions = ({ navigation: { state: { params } } }) => ({
//     title: "Offre d'emploi",
//     headerRight: <JobOfferFavorite
//       jobOfferId={params.jobOfferId}
//       style={styles.favoriteIconStyle}
//     />,
//   });

//   constructor(props) {
//     super(props);
//     this.state = {
//       refreshing: false,
//       loading: false,
//     };
//     this.JobOfferComponent = jobOfferBuilder(false);
//   }

//   shouldComponentUpdate(nextProps) {
//     const { route } = nextProps.screenProps;
//     const { routeName } = this.props.navigation.state;
//     return route === routeName || route === '';
//   }

//   onRefresh = () => {
//     const promises = [
//       this.props.data.refetch(),
//     ];
//     Promise.all(promises).then(() => {
//       this.setState({ refreshing: false });
//     }).catch(e => console.log('catch refetch Events.js', e));
//   };

//   props: {
//     navigation: {
//       navigate: Function,
//       state: {
//         routeName: string,
//       },
//     },
//     data: {
//       loading: boolean,
//       error: {},
//       refetch: Function,
//       fetchMore: Function,
//       visitJobOffer: {
//         job_offer_id: string,
//       },
//     },
//   };

//   render() {
//     if (this.props.data.error) {
//       return <ErrorComponent message="Problème lors de la récupération la page de l'offre" />;
//     } else if (this.props.data.loading) {
//       return <LoadingComponent />;
//     }
//     const { JobOfferComponent } = this;
//     const { visitJobOffer: jobOffer } = this.props.data;
//     const { navigate } = this.props.navigation;
//     return (
//       <ScrollView
//         style={styles.main}
//         showsVerticalScrollIndicator={false}
//       >
//         <JobOfferComponent
//           jobOfferId={jobOffer.job_offer_id}
//           navigate={navigate}
//         />
//       </ScrollView>
//     );
//   }
// }

// export default graphql(VISIT_JOB_OFFER, {
//   options: ({ navigation }) => ({
//     variables: {
//       job_offer_id: navigation.state.params.jobOfferId,
//     },
//   }),
// })(JobOffer);

// // @flow
// import React, { Component } from 'react';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';

// import JobOfferRegistrationList from '../../components/JobOffer/JobOfferRegistrationList';

// const QUERY_JOB_OFFER_REGISTRATIONS = gql`
//   query JobOfferRegistrations($job_offer_id: String!, $status: String) {
//     jobOfferRegistrations(job_offer_id: $job_offer_id, status: $status) {
//       job_offer_registration_id
//       job_offer {
//         job_offer_id
//       }
//       candidate {
//         user_id
//         fullname
//         firstname
//         lastname
//       }
//     }
//   }
// `;

// const CHECK_QUERY_JOB_OFFER_REGISTRATIONS = gql`
//   query JobOfferRegistrations($job_offer_id: String!, $status: String) {
//     jobOfferRegistrations(job_offer_id: $job_offer_id, status: $status, updateLastView: true) {
//       job_offer_registration_id
//       job_offer {
//         job_offer_id
//       }
//       candidate {
//         user_id
//         fullname
//         firstname
//         lastname
//       }
//     }
//   }
// `;

// class RecruiterJobOfferRegistrationListGeneric extends Component {
//   state = {
//     refreshing: false,
//   };

//   props: {
//     screenProps: {
//       route: string,
//     },
//     data: {
//       refetch: Function,
//       jobOfferRegistrations: {
//         job_offer_registration_id: string,
//         status: string,
//       },
//     },
//     emptyListMessage: string,
//     navigation: {
//       navigate: Function,
//       state: {
//         routeName: string,
//         params: {
//           jobOfferId: string,
//         },
//       },
//     },
//   };

//   shouldComponentUpdate(nextProps) {
//     const { route } = nextProps.screenProps;
//     const { routeName } = this.props.navigation.state;
//     return route === routeName || route === '';
//   }

//   componentDidUpdate() {
//     const { data } = this.props;
//     if (data && !data.loading && !data.error && !data.jobOfferRegistrations) {
//       this.props.data.refetch();
//     }
//   }

//   onRefreshList = () => {
//     this.props.data.refetch().then(() => {
//       this.setState({ refreshing: false });
//     }).catch(e => console.log('catch refetch Events.js', e));
// };

//   render() {
//     const { data } = this.props;

//     if (!data) return <ErrorComponent />;
//     if (data.error) return <ErrorComponent message={data.error} />;
//     if (data.loading || !data.jobOfferRegistrations) return <LoadingComponent />;

//     const { navigate } = this.props.navigation;

//     return (
//       <JobOfferRegistrationList
//         jobOfferRegistrations={data.jobOfferRegistrations}
//         jobOfferId={this.props.navigation.state.params.jobOfferId}
//         navigate={navigate}
//         refreshing={this.state.refreshing}
//         onRefresh={this.onRefreshList}
//         emptyText={this.props.emptyListMessage}
//       />
//     );
//   }
// }

// const RecruiterJobOfferRegistrationListTreat = graphql(CHECK_QUERY_JOB_OFFER_REGISTRATIONS, {
//   options: ({ navigation }) => ({
//     variables: {
//       job_offer_id: navigation.state.params.jobOfferId,
//       status: 'pending',
//     },
//     fetchPolicy: 'network-only',
//   }),
// })(props => <RecruiterJobOfferRegistrationListGeneric {...props} emptyListMessage="Aucune candidature à traiter" />);

// const RecruiterJobOfferRegistrationListPreselected = graphql(QUERY_JOB_OFFER_REGISTRATIONS, {
//   options: ({ navigation }) => ({
//     variables: {
//       job_offer_id: navigation.state.params.jobOfferId,
//       status: 'preselected',
//     },
//     fetchPolicy: 'network-only',
//   }),
// })(props => <RecruiterJobOfferRegistrationListGeneric {...props} emptyListMessage="Aucune candidature préselectionnée" />);

// const RecruiterJobOfferRegistrationList = graphql(QUERY_JOB_OFFER_REGISTRATIONS, {
//   options: ({ navigation }) => ({
//     variables: {
//       job_offer_id: navigation.state.params.jobOfferId,
//       status: 'apply',
//     },
//     fetchPolicy: 'network-only',
//   }),
// })(props => <RecruiterJobOfferRegistrationListGeneric {...props} emptyListMessage="Aucune candidature" />);

// export {
//   RecruiterJobOfferRegistrationList,
//   RecruiterJobOfferRegistrationListTreat,
//   RecruiterJobOfferRegistrationListPreselected,
//   QUERY_JOB_OFFER_REGISTRATIONS,
//   CHECK_QUERY_JOB_OFFER_REGISTRATIONS,
// };

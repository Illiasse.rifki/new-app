// import React, { Component } from 'react';
// import { StyleSheet, View, Platform } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';
// import { Icon } from 'react-native-elements';
// import Promise from 'bluebird';

// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import SearchField from '../../components/Design/SearchField';
// import JobsList from '../../components/JobOffer/JobOffersList';

// const styles = StyleSheet.create({
//   main: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//   },
//   bg: {
//     flex: 1,
//     marginTop: 0,
//     marginRight: 16,
//     marginLeft: 16,
//   },
//   searchContainer: {
//     position: 'absolute',
//     zIndex: 100,
//     top: 24,
//     left: 0,
//     right: 0,
//   },
//   card: {
//     marginBottom: 16,
//   },
//   headerRightStyle: { paddingRight: Platform.OS === 'ios' ? 35 : 10 },
// });

// const JOB_OFFER_LIST = gql`
//   query jobOffers($offset: Int, $limit: Int) {
//     jobOffers(offset: $offset, limit: $limit) {
//       job_offer_id
//       title
//       date_publish(duration: true)
//       visibility_mode
//       cover_picture
//       location_name
//       location_departement
//       rating
//       company {
//         logo
//         header
//         name
//       }
//       tags
//     }
//   }
// `;

// const JOB_OFFER_LIST_FILTER = filter => (gql`
//   query jobOffers($offset: Int, $limit: Int) {
//     jobOffers(offset: $offset, limit: $limit, filter: "${filter}") {
//       job_offer_id
//       title
//       date_publish(duration: true)
//       visibility_mode
//       cover_picture
//       location_name
//       location_departement
//       rating
//       company {
//         logo
//         header
//         name
//       }
//       tags
//     }
//   }
// `);

// const JOB_OFFER_LIST_FAV = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         favorite_job_offers {
//           job_offer_id
//           title
//           date_publish(duration: true)
//           visibility_mode
//           cover_picture
//           location_name
//           location_departement
//           rating
//           company {
//             logo
//             name
//             header
//           }
//           tags
//         }
//       }
//     }
//   }
// `;

// class JobOffersList extends Component {
//   static navigationOptions = props => ({
//     headerLeft: null,
//     title: 'Liste des offres',
//     headerRight: <Icon
//       iconStyle={styles.headerRightStyle}
//       name='filter-list'
//       color='white'
//       onPress={() => props.navigation.state.params &&
//         props.navigation.navigate('JobOfferListFilter', {
//           updateState: props.navigation.state.params.updateState,
//           state: props.navigation.state.params.state,
//         })}
//     />,
//   });

//   state = {
//     refreshing: false,
//     valueToSearch: '',
//     searchText: '',
//     loading: false,
//     timeout: null,
//     tags: [],
//     contracts: [],
//     yearsStudy: null,
//   };

//   componentWillMount() {
//     this.props.navigation.setParams({ updateState: this.updateState, state: this.state });
//   }

//   shouldComponentUpdate(nextProps) {
//     const { route } = nextProps.screenProps;
//     const { routeName } = this.props.navigation.state;
//     return route === routeName || route === '';
//   }

//   updateState = vState => this.setState({
//     ...vState,
//     //    loading: !!vState.searchText,
//     //    valueToSearch: vState.searchText,
//   }, () => this.props.navigation.setParams({ state: this.state }));

//   onRefresh = () => {
//     const promises = [
//       this.props.data.refetch(),
//     ];
//     Promise.all(promises).then(() => {
//       this.setState({ refreshing: false });
//     }).catch(e => console.log('catch refetch Events.js', e));
//   };

//   searchJobOffers = (searchText) => {
//     if (this.state.timeout) {
//       clearTimeout(this.state.timeout);
//     }
//     this.setState({
//       searchText,
//       loading: true,
//       timeout: setTimeout(() => {
//         this.setState({ valueToSearch: searchText, loading: false });
//       }, 300),
//     }, () => this.props.navigation.setParams({ state: this.state }));
//   };

//   onLoadMoreJobs = () => {
//     const { jobOffers } = this.props.data;

//     if (this.props.data && jobOffers && jobOffers.length > 0) {
//       this.props.data.fetchMore({
//         variables: {
//           offset: jobOffers.length,
//           limit: 10,
//         },
//         updateQuery: (prev, { fetchMoreResult }) => {
//           if (!fetchMoreResult) return prev;
//           return Object.assign({}, prev, {
//             jobOffers: [...prev.jobOffers, ...fetchMoreResult.jobOffers],
//           });
//         },
//       });
//     }
//   };

//   props: {
//     screenProps: {
//       route: string,
//     },
//     navigation: {
//       setParams: Function,
//       navigate: Function,
//       state: {
//         routeName: string,
//       },
//     },
//     loadMore: boolean,
//     data: {
//       loading: boolean,
//       error: {},
//       refetch: Function,
//       fetchMore: Function,
//       jobOffers: [],
//     },
//   };

//   render() {
//     if (this.props.data.error) {
//       return <ErrorComponent message="Problème lors de la récupération la page d'accueil." />;
//     } else if (this.props.data.loading) {
//       return <LoadingComponent />;
//     }
//     const { jobOffers } = this.props.data;
//     const { navigate } = this.props.navigation;
//     return (
//       <View style={styles.main}>
//         <SearchField
//           loading={this.state.loading}
//           containerStyle={styles.searchContainer}
//           placeholder="Rechercher une offre par nom..."
//           value={this.state.searchText}
//           onChangeText={this.searchJobOffers}
//         />
//         <JobsList
//           insetFirst
//           searchText={this.state.valueToSearch}
//           filters={{
//             tags: this.state.tags,
//             contracts: this.state.contracts,
//             yearsStudy: this.state.yearsStudy && parseInt(this.state.yearsStudy, 10),
//           }}
//           jobs={jobOffers}
//           listStyle={styles.bg}
//           onRefresh={this.onRefresh}
//           onLoadMore={this.onLoadMoreJobs}
//           onJobOfferPress={(jobOffer) => {
//             navigate('JobOffer', { jobOfferId: jobOffer.job_offer_id });
//           }}
//         />
//       </View>
//     );
//   }
// }

// export default graphql(JOB_OFFER_LIST, {
//   options: {
//     variables: {
//       offset: 0,
//       limit: 5,
//     },
//   },
// })(JobOffersList);

// const FavoriteJobComponent = props => (<JobOffersList {...props} loadMore={false} />);
// const FavoriteJobOffersList = graphql(JOB_OFFER_LIST_FAV, {
//   props: ({ data }) => ({
//     data: {
//       ...data,
//       jobOffers: (data.viewer &&
//       data.viewer.candidate && data.viewer.candidate.favorite_job_offers
//         ? data.viewer.candidate.favorite_job_offers : []),
//     },
//   }),
// })(FavoriteJobComponent);
// const PertinenceJobOffersList = graphql(JOB_OFFER_LIST_FILTER('pertinence'), {
//   options: {
//     variables: {
//       offset: 0,
//       limit: 5,
//     },
//   },
// })(JobOffersList);
// export { FavoriteJobOffersList, PertinenceJobOffersList, JOB_OFFER_LIST_FAV };

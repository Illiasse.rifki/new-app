// import React, { Component } from 'react';
// import {
//   View,
//   Text,
//   StyleSheet,
//   ScrollView,
// } from 'react-native';
// import { Button } from 'react-native-elements';
// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';

// import { theme } from '../../config/constants/index';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import Profile from '../../components/GQLLinked/Profile';
// import AskPhone from '../../components/Candidate/AskPhone';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//   },
//   validateButton: {
//     marginBottom: 24,
//     borderRadius: 4,
//     shadowColor: 'rgba(0, 0, 0, 0.08)',
//     shadowOffset: { width: 0.5, height: 4 },
//     shadowOpacity: 0.8,
//     shadowRadius: 2,
//     width: '90%',
//     height: 40,
//     marginTop: 24,
//     alignSelf: 'center',
//   },
//   neededContentContainer: {
//     alignItems: 'center',
//     marginTop: 16,
//   },
//   neededExperienceText: {
//     color: '#ffa064',
//     lineHeight: 24,
//   },
// });

// class UpdateProfileJobOffer extends Component {
//   static navigationOptions = {
//     title: 'Aperçu de votre profil',
//     headerTruncatedBackTitle: '',
//   };

//   props: {
//     navigation: {
//       navigate: Function,
//     },
//     data: {
//       loading: boolean,
//       error: boolean,
//       refetch: Function,
//       viewer: {
//         id: string,
//         candidate: {
//         }
//       }
//     },
//   };

//   shouldComponentUpdate(nextProps) {
//     if (nextProps.screenProps.route !== 'UpdateProfileJobOffer') {
//       if (nextProps.data) {
//         nextProps.data.refetch();
//       }
//     }
//     return true;
//   }

//   handleValidateButton = () => {
//     const { navigation } = this.props;
//     const { navigate } = navigation;
//     const { status, jobOfferId, jobOfferRegistrationId } = navigation.state.params;
//     navigate('PostulateJobOffer', { status, jobOfferId, jobOfferRegistrationId, name: 'Formulaire' });
//   };

//   refetchUpdateJob = () => {
//     this.props.data.refetch();
//   };

//   render() {
//     if (this.props.data.error) {
//       return (<ErrorComponent message='Problème lors de la récupération du profil.' />);
//     } else if (this.props.data.loading) {
//       return (<LoadingComponent />);
//     }

//     const { candidate } = this.props.data.viewer;
//     const isIncompleteProfile = !(candidate && candidate.phone && candidate.last_experience && candidate.last_education);
//     return (
//       <View style={styles.background}>
//         <ScrollView
//           showsVerticalScrollIndicator={false}
//           showsHorizontalScrollIndicator={false} >
//           <Profile userId={this.props.data.viewer.id} navigate={this.props.navigation.navigate} />
//         </ScrollView>
//         <View>
//           <View style={styles.neededContentContainer}>
//             {
//               !(candidate && candidate.last_experience) &&
//                 <Text style={styles.neededExperienceText}>Une expérience professionnelle minimum est requise</Text>
//             }
//             {
//               !(candidate && candidate.last_education) &&
//               <Text style={styles.neededExperienceText}>Une formation minimale est requise</Text>
//             }
//             {
//               !(candidate && candidate.phone) &&
//               <AskPhone
//                 refetch={this.refetchUpdateJob}
//               />
//             }
//           </View>
//           <Button
//             title='Postuler'
//             color={theme.secondaryColor}
//             backgroundColor={theme.primaryColor}
//             disabled={isIncompleteProfile}
//             buttonStyle={styles.validateButton}
//             onPress={this.handleValidateButton}
//           />
//         </View>
//       </View>
//     );
//   }
// }

// const QUERY_VIEWER_UPDATE_JOB = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         phone
//         last_experience {
//           role
//         }
//         last_education {
//           diploma
//         }
//       }
//     }
//   }
// `;

// export default graphql(QUERY_VIEWER_UPDATE_JOB, {
//   options: {
//     fetchPolicy: 'network-only',
//   },
// })(UpdateProfileJobOffer);

// export { QUERY_VIEWER_UPDATE_JOB };

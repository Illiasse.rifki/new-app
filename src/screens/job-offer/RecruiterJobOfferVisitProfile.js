import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text } from 'react-native';

import Profile from '../../components/GQLLinked/Profile';

import { theme } from '../../config/constants/index';
import { Button } from '../../components/Button';

const styles = StyleSheet.create({
  main: {
  },
  header: {
    backgroundColor: 'white',
    paddingVertical: 12,
    paddingHorizontal: 32,
  },
  headerText: {
    color: theme.grey,
    fontSize: 14,
    lineHeight: 16,
    textAlign: 'center',
    fontWeight: '500',
  },
  JobOfferRegistration: {
    backgroundColor: '#f5f5f5',
    paddingVertical: 24,
    paddingHorizontal: 32,
  },
  JobOfferRegistrationPreselect: {
    marginTop: 0,
    marginBottom: 8,
  },
  JobOfferRegistrationChat: {
    marginTop: 0,
    marginBottom: 0,
    marginLeft: 'auto',
    marginRight: 'auto',
    width: 125,
  },
  JobOfferRegistrationRefuse: {
    marginTop: 0,
    marginBottom: 0,
    width: 125,
    backgroundColor: theme.red,
  },
});

class RecruiterJobOfferVisitProfile extends Component {
    static navigationOptions = {
      title: 'Profil',
    };

    props: {
        alertWithType: Function,
        mutation: Function,
        data: {
            jobOfferRegistration: {
                job_offer_registration_id: string,
                status: string,
                jobOffer: {
                    job_offer_id: string,
                    title: string,
                },
                recruiter: {
                    user_id: string,
                },
                candidate: {
                    user_id: string,
                    fullname: string,
                },
            },
        },
        navigation: {
            navigate: Function,
            state: {
                params: {
                    userId: string,
                    jobOfferId: string,
                    jobOfferRegistrationId: string,
                    jobOfferRegistration: any,
                    jobOfferTitle: string,
                },
            },
        },
    };


handleChat = () => this.props.navigation.navigate('Chat', {
  target: {
    id: this.props.navigation.state.params.jobOfferRegistration.candidate.user_id,
    fullname: this.props.navigation.state.params.jobOfferRegistration.candidate.fullname,
  },
  user: {
    user_id: this.props.navigation.state.params.jobOfferRegistration.recruiter.user_id,
  },
  from: `la visite sur l'offre ${this.props.navigation.state.params.jobOfferTitle}`,
});

render() {
  const { userId, jobOfferTitle } = this.props.navigation.state.params;

  let compatibility;

  return (
    <ScrollView style={styles.main} contentContainerStyle={styles.mainContent}>
      <View style={styles.header}>
        <Text style={styles.headerText}>Ce profil a visité votre offre {jobOfferTitle}</Text>
      </View>
    
      <Profile editable={false} userId={userId} compatibility={compatibility} />
    </ScrollView>
  );
}
}

export default RecruiterJobOfferVisitProfile;

import React, { Component } from 'react';
import { StyleSheet, ScrollView, StatusBar, RefreshControl, FlatList, View, Text } from 'react-native';
import jobOfferBuilder from '../../components/JobOffer/JobOffer';

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  bg: {
    flex: 1,
    paddingTop: 90,
    paddingRight: 16,
    paddingLeft: 16,
  },
  searchContainer: {
    position: 'absolute',
    zIndex: 100,
    top: 24,
    left: 0,
    right: 0,
  },
  card: {
    marginBottom: 16,
  },
});

class RecruiterJobOffer extends Component {
  static navigationOptions = {
    title: "Offre d'emploi",
  };

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: false,
    };
    this.JobOfferComponent = jobOfferBuilder(true);
  }

  props: {
    navigation: {
      navigate: Function,
      state: {
        routeName: string,
        params: {
          jobOfferId: string,
        },
      },
    },
    data: {
      loading: boolean,
      error: {},
      refetch: Function,
      fetchMore: Function,
      visitJobOffer: {
        job_offer_id: string,
      },
    },
  };

  shouldComponentUpdate(nextProps) {
    const { route } = nextProps.screenProps;
    const { routeName } = this.props.navigation.state;
    return route === routeName || route === '';
  }

  render() {
    const { JobOfferComponent } = this;
    const { navigation } = this.props;
    const { navigate } = navigation;
    return (
      <ScrollView
        style={styles.main}
        showsVerticalScrollIndicator={false}
      >
        <JobOfferComponent
          isRecruiter={true}
          jobOfferId={navigation.state.params.jobOfferId}
          navigate={navigate}
        />
      </ScrollView>
    );
  }
}

export default RecruiterJobOffer;

// import React, { Component } from 'react';
// import { graphql, compose } from 'react-apollo';

// import Questions from '../../components/JobOffer/JobOfferQuestions';
// import JobOfferRegistrationEnd from '../../components/JobOffer/JobOfferRegistrationEnd';

// import connectAlert from '../../components/Alert/connectAlert';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';

// import { JOB_OFFER_REGISTRATION_QUERY } from '../../queries/JobOfferRegistration';
// import { JOB_OFFER_REGISTRATION_MUTATION } from '../../mutations/JobOfferRegistration';

// class PostulateJobOffer extends Component {
//   static navigationOptions = props => ({
//     title: props.navigation.state.params.name || 'Formulaire',
//   });

//   state = { mutate: false };

//   props: {
//     navigation: {
//       setParams: Function,
//       navigate: Function,
//       goBack: Function,
//       state: {
//         routeName: string,
//         params: { name: string, jobOfferRegistrationId: string, jobOfferId: string },
//       },
//     },
//     data: {
//       loading: boolean,
//       error: {},
//       jobOfferRegistration: {
//         jobOfferRegistration_id: string,
//         status: string,
//         jobOffer: {
//           questions: Array<string>,
//           answers: Array<string>,
//         },
//       },
//     },
//     screenProps: {
//       route: String,
//     },
//     mutation: Function,
//     alertWithType: Function,
//   };

//   shouldComponentUpdate(nextProps) {
//     const { route } = nextProps.screenProps;
//     const { routeName } = this.props.navigation.state;
//     return route === routeName || route === '';
//   }

//   componentWillUpdate(nextProps) {
//     if (nextProps.data && nextProps.data.jobOfferRegistration && nextProps.data.jobOfferRegistration.status === 'incomplete' && nextProps.navigation.state.params.name === 'Formulaire 1/2') {
//       this.props.navigation.setParams({ name: 'Formulaire 2/2' });
//     }
//     if (!this.props.data ||
//       this.props.data.loading ||
//       this.props.data.error ||
//       !nextProps.data ||
//       nextProps.data.loading ||
//       nextProps.data.error || !nextProps.data.jobOfferRegistration) {
//       return;
//     }
//     const {
//       jobOfferRegistration: {
//         status,
//         jobOffer: {
//           questions,
//         },
//       },
//     } = nextProps.data;
//     const { mutate } = this.state;
//     const { name } = nextProps.navigation.state.params;
//     if (questions && status === 'no_apply' && name !== 'Formulaire 1/2' && !mutate) {
//       this.setState({ mutate: true });
//       this.props.navigation.setParams({ name: 'Formulaire 1/2' });
//       return;
//     }
//     if (questions && status === 'incomplete' && name !== 'Formulaire 2/2' && !mutate) {
//       this.setState({ mutate: true });
//       this.props.navigation.setParams({ name: 'Formulaire 2/2' });
//     }
//   }

//   finishQuestions = (applyAnswers) => {
//     const { mutation, alertWithType, navigation } = this.props;
//     const { jobOfferRegistrationId } = navigation.state.params;
//     const { jobOfferRegistration } = this.props.data;
//     const { jobOffer: { questions } } = jobOfferRegistration;

//     if (applyAnswers.length !== questions.length) {
//       alertWithType('info', 'Info', 'Vous devez répondre à toutes les questions.');
//       return;
//     }
//     mutation({
//       variables: {
//         jobOfferRegistrationId,
//         applyAnswers,
//         apply_questions: questions,
//         status: 'incomplete',
//       },
//       refetchQueries: [{
//         query: JOB_OFFER_REGISTRATION_QUERY,
//         variables: {
//           jobOfferRegistrationId,
//         },
//       }],
//     }).catch(e => alertWithType('error', 'Erreur', `Impossible de passer à l'étape suivante: ${e.toString()}.`));
//   };

//   render() {
//     const { alertWithType } = this.props;
//     const { data, mutation } = this.props;
//     if (!data || data.error) {
//       return <ErrorComponent message="Problème lors de la récupération du formulaire de postulation" />;
//     } else if (data.loading) {
//       return <LoadingComponent />;
//     }
//     const { jobOfferId } = this.props.navigation.state.params;
//     if (!data.jobOfferRegistration) data.refetch();

//     const {
//       jobOfferRegistration: {
//         status,
//         jobOfferRegistrationId,
//         jobOffer: { questions },
//       },
//     } = data;
//     const { navigate, goBack } = this.props.navigation;
//     if (questions && (status === 'no_apply')) {
//       return (<Questions
//         questions={questions}
//         onPress={answers => this.finishQuestions(answers)}
//       />);
//     }
//     return (<JobOfferRegistrationEnd
//       jobOfferRegistrationId={jobOfferRegistrationId}
//       jobOfferId={jobOfferId}
//       alertWithType={alertWithType}
//       mutation={mutation}
//       navigate={navigate}
//       goBack={goBack}
//       refetch={data.refetch}
//     />);
//   }
// }

// export default compose(
//   graphql(JOB_OFFER_REGISTRATION_QUERY, {
//     options: ({ navigation: { state: { params: { jobOfferRegistrationId } } } }) => ({
//       variables: {
//         jobOfferRegistrationId,
//       },
//     }),
//   }),
//   graphql(JOB_OFFER_REGISTRATION_MUTATION, { name: 'mutation' }),
// )(connectAlert(PostulateJobOffer));

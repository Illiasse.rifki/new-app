// import React, { Component } from 'react';
// import { StyleSheet, View } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';
// import Promise from 'bluebird';

// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import JobsList from '../../components/JobOffer/JobOffersRegistrationsList';

// const styles = StyleSheet.create({
//   main: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//   },
//   bg: {
//     flex: 1,
//     marginTop: 24,
//     marginRight: 16,
//     marginLeft: 16,
//   },
//   searchContainer: {
//     position: 'absolute',
//     zIndex: 100,
//     top: 24,
//     left: 0,
//     right: 0,
//   },
//   card: {
//     marginBottom: 16,
//   },
// });

// const JOB_OFFER_REGISTRATIONS_LIST = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         job_offers_registrations {
//           job_offer_registration_id
//           status
//           job_offer {
//             job_offer_id
//             company {
//               name
//             }
//             title
//             cover_picture
//           }
//         }
//       }
//     }
//   }
// `;

// class JobOffersRegistrationsList extends Component {
//   static navigationOptions = {
//     headerLeft: null,
//     title: 'Vos candidatures',
//   };

//   state = {
//     refreshing: false,
//     searchText: '',
//     loading: false,
//     timeout: null,
//   };

//   shouldComponentUpdate(nextProps) {
//     const { route } = nextProps.screenProps;
//     const { routeName } = this.props.navigation.state;
//     return route === routeName || route === '';
//   }

//   onRefresh = () => {
//     const promises = [
//       this.props.data.refetch(),
//     ];
//     Promise.all(promises).then(() => {
//       this.setState({ refreshing: false });
//     }).catch(e => console.log('catch refetch Events.js', e));
//   };

//   props: {
//     navigation: {
//       navigate: Function,
//       state: {
//         routeName: string,
//       },
//     },
//     loadMore: boolean,
//     data: {
//       loading: boolean,
//       error: {},
//       refetch: Function,
//       fetchMore: Function,
//       jobOffers: [],
//     },
//   };

//   render() {
//     if (this.props.data.error) {
//       return <ErrorComponent message="Problème lors de la récupération la page d'accueil." />;
//     } else if (this.props.data.loading) {
//       return <LoadingComponent />;
//     }
//     const { candidate } = this.props.data.viewer;
//     const { navigate } = this.props.navigation;
//     return (
//       <View style={styles.main}>
//         <JobsList
//           jobs={candidate.job_offers_registrations}
//           listStyle={styles.bg}
//           onRefresh={this.onRefresh}
//           onJobOfferPress={(jobOfferRegistration) => {
//             navigate('JobOffer', { jobOfferId: jobOfferRegistration.job_offer.job_offer_id });
//           }}
//         />
//       </View>
//     );
//   }
// }

// export default graphql(JOB_OFFER_REGISTRATIONS_LIST)(JobOffersRegistrationsList);

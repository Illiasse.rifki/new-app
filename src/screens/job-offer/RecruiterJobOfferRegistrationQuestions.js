// import React, { Component } from 'react';
// import { StyleSheet, View, ScrollView, Text } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import { theme } from '../../config/constants/index';
// import { Button } from '../../components/Button';

// import Line from '../../components/Design/Line';

// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';

// const styles = StyleSheet.create({
//   main: { paddingTop: 33, backgroundColor: 'white' },
//   questionsText: { fontSize: 28, color: theme.fontBlack, textAlign: 'left', marginLeft: 32, fontWeight: 'bold', marginRight: 32, },
//   question: { marginTop: 30, marginBottom: 35, marginHorizontal: 32 },
//   questionText: { color: theme.fontBlack, fontSize: 18, textAlign: 'left' },
//   questionAnswer: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center' },
//   button: { width: 140, height: 40, marginTop: 14, marginBottom: 0 },
//   noQuestionsText: { color: '#afafaf', fontWeight: '300', fontSize: 15, lineHeight: 24, fontStyle: 'italic', marginHorizontal: 32, },
// });

// const QUERY_JOB_OFFER_REGISTRATION = gql`
//   query JobOfferRegistration($job_offer_registration_id: String!) {
//     jobOfferRegistration(job_offer_registration_id: $job_offer_registration_id) {
//       job_offer_registration_id
//       apply_answers
//       apply_questions
//       status
//     }
//   }
// `;

// const Question = (props: any) => (
//   <View style={styles.question}>
//     <Text style={styles.questionText}>{props.question}</Text>
//     <View style={styles.questionAnswer}>
//       <Button
//         alt={props.answer !== 'Non'}
//         title='Non'
//         buttonStyle={styles.button}
//       />
//       <Button
//         alt={props.answer !== 'Oui'}
//         title='Oui'
//         buttonStyle={styles.button}
//       />
//     </View>
//   </View>
// );

// class JobOfferQuestionsCandidate extends Component {
//   static navigationOptions = {
//     title: 'Questions',
//   };

//   props: {
//     data: {
//       refetch: Function,
//       JobOfferRegistration: {
//         job_offer_registration_id: string,
//         status: string,
//       },
//     },
//     navigation: {
//       state: {
//         params: {
//           userId: string,
//           jobOfferId: string,
//           jobOfferRegistrationId: string,
//         },
//       },
//     },
//   };

//   componentDidUpdate() {
//     const { data } = this.props;
//     if (data && !data.loading && !data.error && !data.jobOfferRegistration) {
//       this.props.data.refetch();
//     }
//   }

//   render() {
//     const { data } = this.props;

//     if (!data || data.error) return <ErrorComponent message={data.error} />;
//     if (data.loading || !data.jobOfferRegistration) return <LoadingComponent />;

//     const { apply_answers: answers } = data.jobOfferRegistration;
//     const answersLength = answers ? answers.length : 0;

//     return (
//       <ScrollView style={styles.main}>
//         {
//           answers ? <Text style={styles.questionsText}>Questions</Text>
//             : <Text style={styles.noQuestionsText}>Aucun questionnaire rempli pour cette offre</Text>
//         }
//         {
//           answers && answers.map((v, i) => {
//             const answer = v;
//             const question = data.jobOfferRegistration.apply_questions[i];
//             return (
//               <View key={question}>
//                 <Question
//                   answersLength={answersLength}
//                   answer={answer}
//                   question={question}
//                 />
//                 {answersLength !== i + 1 && <Line color='#f5f5f5' />}
//               </View>
//             );
//           })
//         }
//       </ScrollView>
//     );
//   }
// }

// export default graphql(QUERY_JOB_OFFER_REGISTRATION, {
//   options: ({ navigation }) => ({
//     variables: {
//       job_offer_registration_id: navigation.state.params.jobOfferRegistrationId,
//     },
//   }),
// })(JobOfferQuestionsCandidate);

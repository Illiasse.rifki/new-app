// import React, { Component } from 'react';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import JobOfferVisitsList from '../../components/JobOffer/JobOfferVisitsList';

// const JOB_OFFER_VISITORS = gql`
//   query jobOffer($job_offer_id: String!) {
//     jobOffer(job_offer_id: $job_offer_id) {
//       view_counter
//       title
//       nb_registrations
//       registrations {
//         job_offer_registration_id
//         status
//         nbviews
//         candidate {
//           user_id
//           fullname
//           picture
//           headline
//           years_experience
//           experience {
//             experience_id
//             role
//           }
//         }
//         recruiter{
//           user_id
//         }
//         apply_date
//       }
//     }
//   }
// `;

// class JobOfferVisits extends Component {
//     static navigationOptions = {
//       title: 'Les visites sur l\'offre',
//     };

//     state = {
//       refreshing: false,
//       searchText: '',
//     };

//     shouldComponentUpdate(nextProps) {
//       const { route } = nextProps.screenProps;
//       const { routeName } = this.props.navigation.state;
//       return route === routeName || route === '';
//     }

//     onRefresh = () => {
//       this.setState({ refreshing: true });
//       this.props.data.refetch().then(() => {
//         this.setState({ refreshing: false });
//       }).catch(e => console.log('catch refetch Events.js', e));
//     };

//     props: {
//         navigation: {
//             navigate: Function,
//             state: { params: {
//                 jobOfferId:string
//             }, routeName: string },
//         },
//         screenProps: { route: string },
//         data: {
//             error: {},
//             loading: boolean,
//             refetch: Function,
//             jobOffer: {
//                 contract_type: string,
//                     years_xp: number,
//                     visibility_mode: string,
//             }
//         },
//     };

//     purifyData = (): Array<Object> => {
//       const { jobOffer } = this.props.data;
//       if (!jobOffer || !jobOffer.registrations) {
//         return [];
//       }

//       return jobOffer.registrations.map(r => ({
//         id: r.job_offer_registration_id,
//         candidate: {
//           ...r.candidate,
//           status: r.status,
//           nbviews: r.nbviews,
//         },
//         years_experience: r.candidate.years_experience,
//         experiences: r.candidate.experience,
//       }));
//     };

//     render() {
//       if (this.props.data.error) {
//         return <ErrorComponent message="Problème lors de la récupération des visiteurs." />;
//       } else if (this.props.data.loading) {
//         return <LoadingComponent />;
//       }

//       const {
//         jobOfferId,
//       } = this.props.navigation.state.params;

//       const { jobOffer } = this.props.data;

//       const { navigate } = this.props.navigation;

//       return (
//         <JobOfferVisitsList
//           jobOfferRegistrations={jobOffer.registrations}
//           jobOfferViews={jobOffer.view_counter}
//           jobOfferTitle={jobOffer.title}
//           navigate={navigate}
//           onRefresh={this.onRefreshList}
//           refreshing={this.state.refreshing}
//         />
//       );
//     }
// }

// export default graphql(JOB_OFFER_VISITORS, {
//   options: ownProps => ({
//     variables: {
//       job_offer_id: ownProps.navigation.state.params.jobOfferId,
//     },
//   }),
// })(JobOfferVisits);

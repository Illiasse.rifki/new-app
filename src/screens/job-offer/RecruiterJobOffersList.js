// import React, { Component } from 'react';
// import { StyleSheet, ScrollView, RefreshControl } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import JobOffersSmallCardList from '../../components/JobOffer/JobOffersSmallCardList';

// const styles = StyleSheet.create({
//   main: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//   },
//   containerMain: {
//     marginRight: 16,
//     marginLeft: 16,
//     marginTop: 33.5,
//   },
//   list: {
//     flex: 1,
//   },
// });

// const VIEWER_JOB_OFFERS = gql`
//   query {
//     viewer {
//       id
//       recruiter {
//         company {
//           job_offers(filter: "published") {
//             job_offer_id
//             title
//             cover_picture
//             view_counter
//             nb_registrations(filter: "apply")
//             visibility_mode
//             new_registrations: nb_registrations(filter: "new")
//           }
//         }
//       }
//     }
//   }
// `;

// class RecruiterJobOffersList extends Component {
//   static navigationOptions = {
//     headerLeft: null,
//     title: 'Liste des offres en cours',
//   };

//   state = {
//     refreshing: false,
//   };

//   shouldComponentUpdate(nextProps) {
//     const { route } = nextProps.screenProps;
//     const { routeName } = this.props.navigation.state;
//     return route === routeName || route === '';
//   }

//   onRefresh = () => {
//     this.props.data.refetch().then(() => {
//       this.setState({ refreshing: false });
//     }).catch(e => console.log('catch refetch Events.js', e));
//   };

//   props: {
//     navigation: {
//       navigate: Function,
//       state: {
//         routeName: string,
//       },
//     },
//     data: {
//       loading: boolean,
//       error: {},
//       refetch: Function,
//       fetchMore: Function,
//       viewer: {
//         recruiter: {
//           company: {
//             job_offers: {},
//           },
//         },
//       },
//     },
//   };

//   render() {
//     if (this.props.data.error) {
//       return <ErrorComponent message="Problème lors de la récupération des offres" />;
//     } else if (this.props.data.loading) {
//       return <LoadingComponent />;
//     }
//     const { job_offers: jobOffers } = this.props.data.viewer.recruiter.company;
//     const { navigate } = this.props.navigation;
//     return (
//       <ScrollView
//         contentContainerStyle={styles.containerMain}
//         style={styles.main}
//         refreshControl={<RefreshControl
//           refreshing={this.state.refreshing}
//           onRefresh={this.onRefresh}
//         />}
//         contentInset={{
//           top: 0,
//           left: 0,
//           right: 0,
//           bottom: 40,
//         }}
//         scrollIndicatorInsets={{
//           top: 0,
//           left: 0,
//           right: 0,
//           bottom: 0,
//         }}
//       >
//         <JobOffersSmallCardList
//           title="Offres en cours"
//           containerStyle={styles.list}
//           jobs={jobOffers}
//           onJobOfferPress={jobOffer => navigate('RecruiterJobOffer', { jobOfferId: jobOffer.job_offer_id })}
//         />
//       </ScrollView>
//     );
//   }
// }

// export default graphql(VIEWER_JOB_OFFERS)(RecruiterJobOffersList);

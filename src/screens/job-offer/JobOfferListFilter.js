import React, { Component } from 'react';
import { StyleSheet, ScrollView, View, Text } from 'react-native';
import { FormInput, FormLabel, Badge } from 'react-native-elements';

import SearchField from '../../components/Design/SearchField';
import TagsSelector from '../../components/Content/TagsSelector';
//import QualitiesSelector from '../../components/Content/QualitiesSelector';
import CardEditable from '../../components/Card/CardEditable';
import { BottomButton } from '../../components/Button';
import ContratsSelector from '../../components/Content/ContratsSelector';

import { theme, themeElements } from '../../config/constants/index';

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  mainContent: {
    marginTop: 20,
    marginBottom: 20,
    paddingBottom: 20,
  },
  searchContainer: {
    justifyContent: 'flex-start',
    marginBottom: 20,
  },
  formInputContainer: {
    marginTop: 5,
    marginLeft: 0,
    marginRight: 0,
  },
  formLabelContent: {
    fontSize: 18,
    color: theme.fontBlack,
    fontWeight: '500',
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 0,
    marginTop: 30,
  },
  formLabelContainer: {
    marginBottom: 0,
    marginTop: 0,
  },
  cardTitle: {
    fontWeight: 'bold',
    fontSize: 28,
    lineHeight: 40,
    marginTop: 8,
    marginLeft: 8,
    backgroundColor: 'transparent',
  },
});

class JobOfferListFilter extends Component {
  static navigationOptions = {
    title: 'Choix des filtres',
  };

  props: {
    navigation: {
      setParams: Function,
      navigate: Function,
      goBack: Function,
      state: {
        routeName: string,
        params: {
          state: any,
          updateState: Function,
        },
      },
    },
  };

  state = {
    searchText: '',
    tags: [],
    contracts: [],
    yearsStudy: '',
  };

  componentWillMount() {
    const { state } = this.props.navigation.state.params;
    this.setState({ ...state });
  }

  selectTag = (tag) => {
    const selected = this.state.tags.indexOf(tag);
    const newTags = this.state.tags.slice();
    if (selected !== -1) {
      newTags.splice(selected, 1);
    } else {
      newTags.push(tag);
    }
    this.setState({ tags: newTags });
  };

  selectContrat = (contrat) => {
    const selected = this.state.contracts.indexOf(contrat);
    const newContrats = this.state.contracts.slice();
    if (selected !== -1) {
      newContrats.splice(selected, 1);
    } else {
      newContrats.push(contrat);
    }
    this.setState({ contracts: newContrats });
  };

  // selectQuality = (quality) => {
  //   const selected = this.state.qualities.indexOf(quality);
  //   const newQualities = this.state.qualities.slice();
  //   if (selected !== -1) {
  //     newQualities.splice(selected, 1);
  //   } else {
  //     newQualities.push(quality);
  //   }
  //   this.setState({ qualities: newQualities });
  // };

  handleDone = () => {
    const { updateState } = this.props.navigation.state.params;
    this.props.navigation.goBack();
    updateState(this.state);
  };

  render() {
    return (
      <ScrollView
        style={styles.main}
        contentContainerStyle={styles.mainContent}
      >
        <SearchField
          containerStyle={styles.searchContainer}
          placeholder="Mots clés..."
          value={this.state.searchText}
          onChangeText={searchText => this.setState({ searchText })}
        />
        <CardEditable title='Tags'>
          <TagsSelector
            selectedTags={this.state.tags}
            onSelect={this.selectTag}
            containerStyle={styles.tagSelector}
          />
          <View style={{ paddingVertical: 16 }}>
            <Text style={styles.cardTitle}>Type de contrat</Text>
            <ContratsSelector
              selectedContrats={this.state.contracts}
              onSelect={this.selectContrat}
              containerStyle={styles.contratsSelector}
            />
          </View>
          <FormLabel
            labelStyle={styles.formLabelContent}
            containerStyle={styles.formLabelContainer}
          >
            Nombre d’années d’expérience
          </FormLabel>
          <FormInput
            placeholder='Chiffre'
            autoCapitalize='none'
            keyboardType='number-pad'
            inputStyle={themeElements.formInputText}
            containerStyle={styles.formInputContainer}
            onChangeText={yearsStudy => this.setState({ yearsStudy })}
            value={this.state.yearsStudy}
            returnKeyType='done'
            underlineColorAndroid='#979797'
          />
        </CardEditable>
        <BottomButton title='Appliquer' onPress={this.handleDone} />
      </ScrollView>
    );
  }
}

export default JobOfferListFilter;

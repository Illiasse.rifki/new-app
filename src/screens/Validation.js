import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, Image } from 'react-native';
import { Button } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';
import { theme } from '../config/constants/index';

import validate from './../assets/sign-up/validate.png';

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: theme.secondaryColor,
    paddingLeft: 32,
    paddingRight: 32,
    paddingBottom: 10,
    paddingTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerTitle: {
    fontSize: 18,
    color: theme.fontBlack,
    fontWeight: '500',
  },
  imageView: {
    flex: 6,
    alignItems: 'center',
    justifyContent: 'center',
    width: '80%',
    paddingTop: 10,
    paddingBottom: 10,
  },
  image: {
    flex: 1,
    resizeMode: 'contain',
    width: '100%',
  },
  infos: {
    flex: 6,
    alignItems: 'center',
    justifyContent: 'center',
  },
  infosTitle: {
    fontSize: 28,
    fontWeight: 'bold',
    textAlign: 'center',
    lineHeight: 40.04,
  },
  infosSubTitle: {
    fontSize: 14,
    lineHeight: 23.94,
    textAlign: 'center',
    marginTop: 9,
  },
  submit: {
    width: 147,
    height: 40,
    borderRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.16)',
    shadowOffset: { width: 0, height: 1.5 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  submitContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // paddingTop: 37,
  },
  titleView: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 3,
  },
});

class Validation extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    StatusBar.setBarStyle('dark-content', true);
  }

  props: {
    navigation: {
      navigate: Function,
      dispatch: Function,
      pop: Function,
      state: {
        params: {
          buttonName: ?string,
          title: ?string,
          subtitle: ?string,
          headerTitle: ?string,
          goTo: ?string,
          goBack: ?Boolean,
          reset: ?Boolean,
        },
      },
    }
  };

  handleOnPress = () => {
    const { navigate, state } = this.props.navigation;
    StatusBar.setBarStyle('light-content', true);
    if (state.params.reset) {
      const whereNavigate = { routeName: state.params.goTo };
      if (state.params.goToParams) {
        whereNavigate.params = state.params.goToParams;
      }
      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate(whereNavigate),
        ],
      });
      this.props.navigation.dispatch(resetAction);
    } else if (state.params.goBack) {
      this.props.navigation.pop(2);
    } else {
      navigate(state.params.goTo);
    }
  };

  render() {
    const { state } = this.props.navigation;

    return (
      <View style={styles.background}>
        <View style={styles.header}>
          <Text style={styles.headerTitle}>{state.params.headerTitle}</Text>
        </View>
        <View style={styles.imageView}>
          <Image style={styles.image} source={validate}></Image>
        </View>
        <View style={styles.infos}>
          <View style={styles.titleView}>
            <Text style={styles.infosTitle}>{state.params.title}</Text>
            <Text style={styles.infosSubTitle}>{state.params.subtitle}</Text>
          </View>
          <Button
            title={state.params.buttonName || "C'est parti"}
            backgroundColor={theme.primaryColor}
            color={theme.secondaryColor}
            buttonStyle={styles.submit}
            containerViewStyle={styles.submitContainer}
            onPress={this.handleOnPress}
          />
        </View>
      </View>
    );
  }
}

export default Validation;

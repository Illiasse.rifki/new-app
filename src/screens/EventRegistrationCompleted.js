import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Button } from 'react-native-elements';
import { theme } from '../config/constants/index';

const validatePng = require('../assets/validate/validate.png');

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 35,
    paddingHorizontal: 32,
    paddingBottom: 28,
    alignContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 18,
    fontWeight: '500',
    color: theme.fontBlack,
    marginBottom: 49,
  },
  bigText: {
    textAlign: 'center',
    fontSize: 28,
    marginBottom: 9,
    fontWeight: 'bold',
  },
  text: {
    textAlign: 'center',
    fontSize: 14,
    marginBottom: 13,
    color: theme.fontBlack,
  },
  img: {
    width: 310,
    height: 272,
    marginBottom: 18,
    resizeMode: 'contain',
  },
  btn: {
    borderRadius: 4,
    shadowOpacity: 0.8,
    shadowOffset: { height: 0.5, width: 0 },
    shadowColor: 'rgba(0, 0, 0, 0.16)',
  },
});

class EventRegistrationCompleted extends Component {
  static navigationOptions = {
    header: null,
  };
  props: {
    navigation: {
      goBack: Function,
    },
  };
  render() {
    return (
      <View style={styles.main}>
        <Text style={styles.title}>Candidature à la soirée envoyée</Text>
        <Image style={styles.img} source={validatePng} />
        <Text style={styles.bigText}>Votre candidature a bien été envoyée !</Text>
        <Text style={styles.text}>
          Vous pourrez suivre l’évolution de votre statut à tout moment à partir de votre tableau de
          bord.. Notre équipe va prendre contact avec vous si vous êtes sélectionné.
        </Text>
        <Button
          buttonStyle={styles.btn}
          backgroundColor={theme.primaryColor}
          title={"C'est compris"}
          onPress={() => this.props.navigation.goBack()}
        />
      </View>
    );
  }
}

export default EventRegistrationCompleted;

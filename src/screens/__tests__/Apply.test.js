import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import Apply from '../Apply';

describe('Apply Screen', () => {
  const tree = renderer.create(<Apply />);
  const { state } = tree.getInstance();

  test('renders correctly', () => {
    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('verify state', () => {
    expect(state).toMatchSnapshot();
  });

  test('Should update the state with anwsers', () => {
    const { _handleOnPressAnwser } = tree.getInstance();
    _handleOnPressAnwser([1, 0, 1]);
    expect(state.anwsers).toBe([1, 0, 1]);
  });

  test('Should send state and go Confirm Screen', () => {
    const { _handleOnPressNext } = tree.getInstance();
    expect(_handleOnPressNext.toString()).toMatchSnapshot();
  });

  test('Should see user profile', () => {
    const { _handleOnPressCheckUser } = tree.getInstance();
    expect(_handleOnPressCheckUser.toString()).toMatchSnapshot();
  });

  test('Should cancel and go to previous screen', () => {
    const { _handleOnPressCancel } = tree.getInstance();
    expect(_handleOnPressCancel.toString()).toMatchSnapshot();
  });

  test('Should apply', () => {
    const { _handleOnPressApply } = tree.getInstance();
    expect(_handleOnPressApply.toString()).toMatchSnapshot();
  });
});

import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import CandidateQuestions from '../CandidateQuestions';

describe('CandidateQuestions Screen', () => {
  const tree = renderer.create(<CandidateQuestions />);
  const {
    state,
    _handleOnChangeIndex,
    _handleOnChangeTags,
    _handleOnChangeQualities,
    _handleOnChangeArea,
    _handleOnChangeEdu,
    _handleOnChangeExp,
    _handleOnChangeDomain,
    _handleOnChangeSearchArea,
    _handleOnChangeSize,
    _handleOnChangeSearchDomain,
    _handleOnChangeSalary,
    _handleMaximumChoice,
    _handleOnPressNext,
    _handleOnPressPrev,
    _handleOnPressCV,
    _handleOnPressSkip,
  } = tree.getInstance();

  test('render correctly', () => {
    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('check state', () => {
    expect(state).toMatchSnapshot();
  });

  test('Change index state', () => {
    _handleOnChangeIndex(1);
    expect(state.index).toBe(1);
  });

  test('Change tags state', () => {
    _handleOnChangeTags(['B to B', 'Conseil']);
    expect(state.tags).toBe(['B to B', 'Conseil']);
  });

  test('Change qualities state', () => {
    _handleOnChangeQualities(['Creativité', 'Curiosité']);
    expect(state.qualities).toBe(['Creativité', 'Curiosité']);
  });

  test('Change area state', () => {
    _handleOnChangeArea('France');
    expect(state.area).toBe('France');
  });

  test('Change edu state', () => {
    _handleOnChangeEdu('Bac');
    expect(state.edu).toBe('Bac');
  });

  test('Change exp state', () => {
    _handleOnChangeExp('1 year');
    expect(state.exp).toBe('1 year');
  });

  test('Change domain state', () => {
    _handleOnChangeDomain('Sport');
    expect(state.domain).toBe('Sport');
  });

  test('Change searchArea state', () => {
    _handleOnChangeSearchArea('Paris');
    expect(state.searchArea).toBe('Paris');
  });

  test('Change size state', () => {
    _handleOnChangeSize('ETI');
    expect(state.size).toBe('EFI');
  });

  test('Change searchDomain state', () => {
    _handleOnChangeSearchDomain('IT');
    expect(state.searchArea).toBe('IT');
  });

  test('Change salary state', () => {
    _handleOnChangeSalary('1223');
    expect(state.salary).toBe('1223');
  });

  test('Limit to 4 choices for tags and qualities', () => {
    expect(_handleMaximumChoice.toString()).toMatchSnapshot();
  });

  test('Avanced in the form', () => {
    _handleOnPressNext();
    expect(state.index).toBe(1);
  });

  test('Navigate back to CandidateForm screen', () => {
    expect(_handleOnPressPrev.toString()).toMatchSnapshot();
  });

  test('Upload CV and navigate to Candidate Preview', () => {
    expect(_handleOnPressCV.toString()).toMatchSnapshot();
  });

  test('Navigate to Candidate Preview', () => {
    expect(_handleOnPressSkip.toString()).toMatchSnapshot();
  });
});

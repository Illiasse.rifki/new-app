import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import CandidatePreview from '../CandidatePreview';

describe('Candidate Preview Screen', () => {
  const tree = renderer.create(<CandidatePreview />);
  const { _handleOnPressConfirm, _handleOnPressPrev, _handleOnPressEdit } = tree.getInstance();

  test('Create Profile and navigate to Home screen', () => {
    expect(_handleOnPressConfirm.toString()).toMatchSnapshot();
  });

  test('Navigate back', () => {
    expect(_handleOnPressPrev.toString()).toMatchSnapshot();
  });

  test('Edit info of Candidate', () => {
    expect(_handleOnPressEdit.toString()).toMatchSnapshot();
  });
});

import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import SignupType from '../SignUpType';

describe('SignupType Screen', () => {
  const tree = renderer.create(<SignupType />);
  const {
    _handleOnPressCandidate,
    _handleOnPressRecruiter,
    _handleOnPressPrev,
  } = tree.getInstance();

  test('renders correctly', () => {
    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('Navigate the user to Candidate form', () => {
    expect(_handleOnPressCandidate.toString()).toMatchSnapshot();
  });

  test('Navigate the user to Recruiter form', () => {
    expect(_handleOnPressRecruiter.toString()).toMatchSnapshot();
  });

  test('Navigate back', () => {
    expect(_handleOnPressPrev.toString()).toMatchSnapshot();
  });
});

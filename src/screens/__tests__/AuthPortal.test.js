import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import AuthPortal from '../AuthPortal';

jest.unmock('ScrollView');

describe('AuthPortal Screen', () => {
  const tree = renderer.create(<AuthPortal />);
  const { state, _handleOnEventMove, _handleOnParamsUri } = tree.getInstance();

  test('renders correctly', () => {
    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('check state', () => {
    expect(state).toMatchSnapshot();
  });

  test('Either go back to Signup or to Form Screen', () => {
    expect(_handleOnEventMove).toMatchSnapshot();
  });

  test('Change uri state', () => {
    _handleOnParamsUri('facebook');
    expect(state.uri).toBe('facebook');
  });
});

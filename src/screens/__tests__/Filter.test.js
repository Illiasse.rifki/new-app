import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import Filter from '../Filter';

describe('Filter Screen', () => {
  const tree = renderer.create(<Filter />);
  const { state } = tree.getInstance();

  test('renders correctly', () => {
    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('verify state', () => {
    expect(state).toMatchSnapshot();
  });

  test('Should change state while typing', () => {
    const { _handleOnChangeSearch } = tree.getInstance();
    _handleOnChangeSearch('test');
    expect(state.search).toBe('test');
  });

  test('Should change the state with the selected tags', () => {
    const { _handleOnPressTags } = tree.getInstance();
    _handleOnPressTags('test');
    expect(state.tags).toBe(['test']);
  });

  test('Should change state type with the selected button', () => {
    const { _handleOnPressType } = tree.getInstance();
    _handleOnPressType('test');
    expect(state.type).toBe('test');
  });

  test('Should change state experience with the selected button', () => {
    const { _handleOnPressExp } = tree.getInstance();
    _handleOnPressExp('test');
    expect(state.exp).toBe('test');
  });

  test('Should send the state to server and go to Offers Screen', () => {
    const { _handleOnPressConfirm } = tree.getInstance();
    expect(_handleOnPressConfirm.toString()).toMatchSnapshot();
  });

  test('Should go back to the Offer Screen', () => {
    const { _handleOnPressCancel } = tree.getInstance();
    expect(_handleOnPressCancel.toString()).toMatchSnapshot();
  });
});

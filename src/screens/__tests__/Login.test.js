import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import Login from '../Login';

describe('Login Screen', () => {
  const tree = renderer.create(<Login />);
  const {
    state,
    _handleOnChangeEmail,
    _handleOnChangePassword,
    _handleOnPressLogin,
    _handleOnChangeWebView,
    _handleOnPressForgot,
    _handleOnPressPrev,
  } = tree.getInstance();

  test('render correctly', () => {
    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('check state', () => {
    expect(state).toMatchSnapshot();
  });

  test('Change the email state', () => {
    _handleOnChangeEmail('test@test.com');
    expect(state.email).toBe('test@test.com');
  });

  test('Change the password state', () => {
    _handleOnChangePassword('test');
    expect(state.test).toBe('test');
  });

  test('Send the form and connect the user', () => {
    expect(_handleOnPressLogin.toString()).toMatchSnapshot();
  });

  test('Manage WebView State', () => {
    expect(_handleOnChangeWebView.toString()).toMatchSnapshot();
  });

  test('Send the user to the Forgot screen', () => {
    expect(_handleOnPressForgot.toString()).toMatchSnapshot();
  });

  test('Navigate back', () => {
    expect(_handleOnPressPrev.toString()).toMatchSnapshot();
  });
});

import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import Confirmation from '../Confirmation';

describe('Confirmation Screen', () => {
  const tree = renderer.create(<Confirmation />);

  test('renders correctly', () => {
    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('Should remove this screen', () => {
    const { _handleOnPressDone } = tree.getInstance();
    expect(_handleOnPressDone.toString()).toMatchSnapshot();
  });
});

import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import OnBoarding from '../OnBoarding/index';

describe('Onboarding Screen', () => {
  const tree = renderer.create(<OnBoarding />);
  const {
    state,
    _handleOnSlideChange,
    _handleOnPressSignin,
    _handleOnPressSignup,
  } = tree.getInstance();

  test('renders correctly', () => {
    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('check state', () => {
    expect(state).toMatchSnapshot();
  });

  test('Fill the bubble with the right index of the content', () => {
    _handleOnSlideChange(1);
    expect(state.index).toBe(1);
  });

  test('Navigate to Login screen', () => {
    expect(_handleOnPressSignin.toString()).toMatchSnapshot();
  });

  test('Navigate to Signup screen', () => {
    expect(_handleOnPressSignup.toString()).toMatchSnapshot();
  });
});

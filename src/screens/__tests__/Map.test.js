import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import Map from '../Map';

describe('Map FullScreen', () => {
  const tree = renderer.create(<Map />);

  test('renders correctly', () => {
    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('Go back to Company Screen', () => {
    const { _handleOnPressExit } = tree.getInstance();
    expect(_handleOnPressExit.toString()).toMatchSnapshot();
  });
});

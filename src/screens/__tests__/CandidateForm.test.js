import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import CandidateForm from '../candidate-form/CandidateForm';

describe('CandidateForm Screen', () => {
  const tree = renderer.create(<CandidateForm />);
  const {
    state,
    _handleOnChangeFirstname,
    _handleOnChangeLastname,
    _handleOnChangeEmail,
    _handleOnChangeNumber,
    _handleOnChangeBirthdate,
    _handleOnChangePassword,
    _handleOnPressCGU,
    _handleOnPressPrev,
  } = tree.getInstance();

  test('renders correctly', () => {
    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('check state', () => {
    expect(state).toMatchSnapshot();
  });

  test('Change firstname state', () => {
    _handleOnChangeFirstname('underdog');
    expect(state.firstname).toBe('underdog');
  });

  test('Change lastname state', () => {
    _handleOnChangeLastname('power');
    expect(state.lastname).toBe('power');
  });

  test('Change email state', () => {
    _handleOnChangeEmail('underdog@power.com');
    expect(state.email).toBe('underdog@power.com');
  });

  test('Change number state', () => {
    _handleOnChangeNumber('123');
    expect(state.number).toBe('123');
  });

  test('Change birthdate state', () => {
    _handleOnChangeBirthdate('10/01/2017');
    expect(state.birthdate).toBe('10/01/2017');
  });

  test('Change password state', () => {
    _handleOnChangePassword('lol');
    expect(state.password).toBe('lol');
  });

  test('Open CGU Modal', () => {
    _handleOnPressCGU(true);
    expect(state.modalVisible).toBe(true);
  });

  test('Close CGU Modal', () => {
    _handleOnPressCGU(!state.modalVisible);
    expect(state.modalVisible).toBe(false);
  });

  test('Navigate back', () => {
    expect(_handleOnPressPrev.toString()).toMatchSnapshot();
  });
});

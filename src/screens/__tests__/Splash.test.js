import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import Splash from '../Splash';

describe('Splash Screen', () => {
  const tree = renderer.create(<Splash />);
  const { state } = tree.getInstance();
  test('renders correctly', () => {
    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('check state', () => {
    expect(state).toMatchSnapshot();
  });

  test('Should stop loading animation', () => {
    const { _handleLoading } = tree.getInstance();
    _handleLoading();
    const result = tree.getInstance().state.isLoading;
    expect(result).toBe(false);
  });
});

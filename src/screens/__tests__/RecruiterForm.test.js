import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import RecruiterForm from '../RecruiterAccountRequest/index';

describe('RecruiterForm Screen', () => {
  const tree = renderer.create(<RecruiterForm />);
  const {
    state,
    _handleOnChangeFirstname,
    _handleOnChangeLastname,
    _handleOnChangeCompany,
    _handleOnChangeNumber,
    _handleOnChangeEmail,
    _handleOnChangeComment,
    _handleOnPressSend,
    _handleOnPressPrev,
  } = tree.getInstance();

  test('renders correctly', () => {
    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('check state', () => {
    expect(state).toMatchSnapshot();
  });

  test('Change firstname state', () => {
    _handleOnChangeFirstname('tom');
    expect(state.firstname).toBe('tom');
  });

  test('Change lastname state', () => {
    _handleOnChangeLastname('bob');
    expect(state.lastname).toBe('bob');
  });

  test('Change company state', () => {
    _handleOnChangeCompany('facebook');
    expect(state.company).toBe('facebook');
  });

  test('Change number state', () => {
    _handleOnChangeNumber('012');
    expect(state.number).toBe('012');
  });

  test('Change email state', () => {
    _handleOnChangeEmail('tom.bob@facebook.com');
    expect(state.email).toBe('tom.bob@facebook.com');
  });

  test('Change comment state', () => {
    _handleOnChangeComment('hello');
    expect(state.comment).toBe('hello');
  });

  test('Send the state and navigat to Confirm screen', () => {
    expect(_handleOnPressSend.toString()).toMatchSnapshot();
  });

  test('Navigate back', () => {
    expect(_handleOnPressPrev.toString()).toMatchSnapshot();
  });
});

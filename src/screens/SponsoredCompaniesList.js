// import React, { Component } from 'react';
// import { StyleSheet, ScrollView, RefreshControl, FlatList, View } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';
// import Promise from 'bluebird';
// import { Icon } from '../components/Pure/react-native-elements';
// import { theme } from '../config/constants/index';
// import ErrorComponent from '../components/Elements/ErrorComponent';
// import LoadingComponent from '../components/Elements/LoadingComponent';
// import CompanyCard from '../components/Company/CompanyCard';
// import SearchField from '../components/Design/SearchField';
// import Drawer from '../components/Content/Drawer';
// import EmptyCard from '../components/Card/EmptyCard';

// const styles = StyleSheet.create({
//   main: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//   },
//   bg: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//     paddingBottom: 24,
//     paddingRight: 16,
//     marginLeft: 16,
//   },
//   container: {
//     marginBottom: 96,
//   },
//   searchContainer: {
//     position: 'absolute',
//     zIndex: 100,
//     top: 24,
//     left: 0,
//     right: 0,
//   },
//   card: {
//     marginBottom: 16,
//   },
//   cardFirst: {
//     marginBottom: 16,
//     marginTop: 90,
//   },
//   companyEmpty: {
//     marginTop: 90,
//   },
// });

// const SPONSORED_COMPANIES = gql`
//   query {
//     sponsoredCompanies {
//       company_id
//       logo
//       header
//       name
//       nb_job_offers
//     }
//   }
// `;

// class SponsoredCompaniesList extends Component {
//   static navigationOptions = ({ navigation }) => ({
//     title: 'Nos entreprises partenaires',
//     headerLeft: <Drawer.Icon
//       onPress={() => navigation.navigate('DrawerOpen')}
//     />,
//     tabBarLabel: 'Entreprises',
//     tabBarIcon: ({ tintColor }) => (
//       <Icon size={20} name="business" type="material-icons" color={tintColor || theme.gray} />
//     ),
//   });

//   state = {
//     refreshing: false,
//     searchText: '',
//   };

//   shouldComponentUpdate(nextProps) {
//     const { route } = nextProps.screenProps;
//     const { routeName } = this.props.navigation.state;
//     return route === routeName || route === '';
//   }

//   onRefresh = () => {
//     this.setState({ refreshing: true });
//     const promises = [
//       this.props.data.refetch(),
//     ];
//     Promise.all(promises).then(() => {
//       this.setState({ refreshing: false });
//     }).catch(e => console.log('catch refetch Events.js', e));
//   };

//   onChangeTextSearchField = searchText => this.setState({ searchText });

//   searchCompanies = (companies) => {
//     if (!this.state.searchText) return companies;
//     const regexText = `([^"]*${this.state.searchText.toLowerCase()}[^"]*)`;
//     return companies.slice().filter(company => company.name.toLowerCase().match(regexText));
//   };

//   props: {
//     navigation: {
//       navigate: Function,
//       state: {
//         routeName: string,
//       },
//     },
//     data: {
//       loading: boolean,
//       error: {},
//       refetch: Function,
//       sponsoredCompanies: [{
//         name: string,
//         nb_job_offers: number,
//         logo: string,
//         header: string,
//       }],
//     },
//   };

//   keyExtractorCompaniesList = company => company.company_id;

//   renderCompanyCard = ({ item: company, index }) => {
//     const { navigate } = this.props.navigation;
//     return (
//       <CompanyCard
//         onPress={() => navigate('CompanyProfile', { companyId: company.company_id })}
//         containerStyle={(index === 0) ? styles.cardFirst : styles.card}
//         name={company.name}
//         coverPicture={company.header}
//         logo={company.logo}
//         nbJobOffers={company.nb_job_offers}
//       />
//     );
//   };

//   render() {
//     if (this.props.data.error) {
//       return <ErrorComponent message="Problème lors de la récupération la page d'accueil." />;
//     } else if (this.props.data.loading) {
//       return <LoadingComponent />;
//     }
//     const { sponsoredCompanies } = this.props.data;
//     return (
//       <View style={styles.main}>
//         <SearchField
//           containerStyle={styles.searchContainer}
//           placeholder="Rechercher une entreprise par nom..."
//           value={this.state.searchText}
//           onChangeText={this.onChangeTextSearchField}
//         />
//         <ScrollView
//           style={styles.bg}
//           showsVerticalScrollIndicator={false}
//           showsHorizontalScrollIndicator={false}
//           refreshControl={<RefreshControl
//             refreshing={this.state.refreshing}
//             onRefresh={this.onRefresh}
//           />}
//         >
//           <FlatList
//             style={styles.container}
//             ListEmptyComponent={<EmptyCard containerStyle={styles.companyEmpty} title="Aucune entreprise partenaire" />}
//             data={this.searchCompanies(sponsoredCompanies)}
//             keyExtractor={this.keyExtractorCompaniesList}
//             keyboardShouldPersistTaps={'always'}
//             renderItem={this.renderCompanyCard}
//           />
//         </ScrollView>
//       </View>
//     );
//   }
// }

// export default graphql(SPONSORED_COMPANIES)(SponsoredCompaniesList);

// import React, { Component } from 'react';
// import { connect } from 'react-redux';
// import {
//   Platform,
//   View,
//   Text,
//   StyleSheet,
//   TouchableHighlight,
//   Linking,
//   WebView,
//   Keyboard,
// } from 'react-native';

// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';
// import DatePicker from 'react-native-datepicker';
// import {
//   FormInput,
//   FormLabel as Label,
//   CheckBox,
//   FormValidationMessage,
//   Button,
// } from 'react-native-elements';
// import moment from 'moment';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import validator from 'validator';
// // import { LoginManager, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
// import randomatic from 'randomatic';
// import OneSignal from 'react-native-onesignal';

// import { themeElements, theme, LINKEDIN } from '../../config/constants/index';
// import Loader from '../../components/Loader/Loader';
// import global from '../../config/global';
// import { setField, flushForm } from '../../actions/candidateForm';
// import { BottomButton } from '../../components/Button';
// import ProfilePicture from '../../components/GQLLinked/ProfilePicture';
// import Separator from '../../components/Design/Separator';
// import connectAlert from '../../components/Alert/connectAlert';
// import defaultProfilePictureAdd from '../../assets/sign-up/photo.png';
// import createChatClient, { client } from '../../utils/Chat';
// import Firstname from '../../components/Candidate/Register/Firstname';
// import Lastname from '../../components/Candidate/Register/Lastname';
// import Email from '../../components/Candidate/Register/Email';
// import Phone from '../../components/Candidate/Register/Phone';
// import Password from '../../components/Candidate/Register/Password';
// import Birthdate from '../../components/Candidate/Register/Birthdate';
// import Confirmation from '../../components/Candidate/Register/Confirmation';
// import Headline from '../../components/Candidate/Register/Headline';
// import JobSituation from '../../components/Candidate/Register/JobSituation';
// import ExperimentCheckbox from '../../components/Candidate/Register/ExperimentCheckbox';

// const styles = StyleSheet.create({
//   background: {
//     backgroundColor: 'white',
//     paddingLeft: 32,
//     paddingRight: 32,
//     paddingTop: 10.5,
//     paddingBottom: 20.5,
//   },
//   formLabelContainer: {
//     marginLeft: 0,
//     marginRight: 0,
//     marginTop: 17,
//   },
//   formLabelContent: {
//     marginTop: 0,
//     marginRight: 0,
//     marginLeft: 0,
//     color: '#afafaf',
//     letterSpacing: 0.5,
//     fontSize: 14,
//     fontWeight: '500',
//   },
//   separatorView: {
//     marginTop: 25.5,
//     flexDirection: 'row',
//     justifyContent: 'center',
//   },
//   profilePictureView: {
//     alignSelf: 'center',
//     justifyContent: 'center',
//     marginBottom: 14,
//     borderWidth: 0,
//     width: 110,
//     height: 110,
//     backgroundColor: 'white',
//     borderRadius: 0,
//   },
//   profilePicture: {
//     backgroundColor: 'white',
//     borderRadius: 0,
//   },
//   socialButtonsContainer: {
//     marginTop: 18.5,
//     flexDirection: 'row',
//     alignItems: 'center',
//     justifyContent: 'space-between',
//   },
//   socialButton: {
//     width: '100%',
//     borderRadius: 4,
//     paddingLeft: 0,
//     paddingTop: 4.5,
//     paddingBottom: 4.5,
//     height: 40,
//   },
//   socialButtonText: {
//     color: 'white',
//     fontSize: 16,
//   },
//   facebookButtonCont: {
//     width: window.x < 350 ? '48%' : '45%',
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   linkedButtonCont: {
//     width: window.x < 350 ? '48%' : '45%',
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   form: {
//     marginTop: 30.5,
//   },
//   formImageView: {
//     alignItems: 'center',
//     marginBottom: 17,
//   },
//   cguButton: {
//     textDecorationLine: 'underline',
//   },
//   cguTextContainer: {
//     flex: 1,
//     flexDirection: 'column',
//   },
//   cguCheckboxView: {
//     flexDirection: 'row',
//     alignItems: 'center',
//     marginTop: 25,
//   },
//   cguCheckboxContainer: {
//     backgroundColor: 'transparent',
//     borderColor: 'transparent',
//     paddingLeft: 0,
//     paddingRight: 0,
//     marginRight: 0,
//     marginLeft: 0,
//   },
//   birthContainer: {
//     width: '100%',
//     marginTop: 8,
//     marginLeft: 0,
//     marginRight: 0,
//   },
// });

// function FormLabel({ children }) {
//   return (
//     <Label
//       labelStyle={styles.formLabelContent}
//       containerStyle={styles.formLabelContainer}
//     >
//       {children}
//     </Label>
//   );
// }

// class CandidateForm extends Component {
//   static navigationOptions = {
//     title: 'Inscription candidat',
//     headerTruncatedBackTitle: '',
//   };

//   static progressionIndicator = 10;

//   componentWillMount() {
//     OneSignal.addEventListener('ids', this.onIds);
//     // We should really flush redux store at this point
//     this.props.dispatch(flushForm());
//   }

//   componentWillUnmount() {
//     OneSignal.removeEventListener('ids', this.onIds);
//     this.props.dispatch(setField('progress', CandidateForm.progressionIndicator));
//   }

//   componentDidMount() {
//     const { state = {} } = this.props.navigation;
//     if (!state.params) return;
//     const {
//       params: { email, firstname, lastname, headline, linkedin, facebook },
//     } = state;
//     if (email && (linkedin || facebook)) {
//       this.changeField('email', email);
//       this.changeField('firstname', firstname);
//       this.changeField('lastname', lastname);
//       this.changeField('headline', headline);
//       this.setState({ linkedin });
//       this.setState({ facebook });
//     }
//   }

//   onIds = (ids) => {
//     this.mobileId = ids.userId;
//   };

//   props: {
//     addCandidate: Function,
//     login: Function,
//     picture: String,
//     firstname: String,
//     lastname: String,
//     email: String,
//     phone: String,
//     headline: String,
//     password: String,
//     confirmation: String,
//     jobSituation: String,
//     birthdate: String,
//     cguAccepted: boolean,
//     headline: String,
//     experimented: boolean,
//     navigation: {
//       navigate: {},
//       state: {
//         params: {
//           email: string,
//           linkedin: string,
//           facebook: string,
//         },
//       },
//     },
//     progress: number,
//     dispatch: Function,
//     alertWithType: Function,
//   };

//   state = {
//     picture: '',
//     loading: false,
//     emailError: false,
//     phoneError: false,
//     passwordError: false,
//     linkedin: false,
//     linkedinId: '',
//     facebook: '',
//   };

//   checkCGU = () => {
//     this.props.dispatch(setField('cguAccepted', !this.props.cguAccepted));
//   };

//   isRequiredFieldsFilled = () =>
//     !(
//       this.props.firstname === '' ||
//       this.props.lastname === '' ||
//       this.props.email === '' ||
//       this.props.phone === '' ||
//       this.props.birthdate === '' ||
//       this.props.password === '' ||
//       this.state.emailError ||
//       this.state.phoneError ||
//       this.state.passwordError
//     );

//   handleSubscribeButton = async () => {
//     const { navigate } = this.props.navigation;
//     if (!this.props.cguAccepted) {
//       this.props.alertWithType(
//         'info',
//         "Conditions générales d'utilisations",
//         "Vous devez accepter les conditions générales d'utilisation avant de continuer.",
//       );
//       return;
//     } else if (
//       this.state.emailError ||
//       this.state.phoneError ||
//       this.state.passwordError ||
//       this.props.password !== this.props.confirmation
//     ) {
//       this.props.alertWithType(
//         'error',
//         'Champs de formulaires incorrect',
//         'Veuillez vérifier les données que vous avez entré',
//       );
//       return;
//     } else if (
//       this.props.firstname === '' ||
//       this.props.lastname === '' ||
//       this.props.email === '' ||
//       this.props.phone === '' ||
//       this.props.headline === '' ||
//       this.props.jobSituation === '' ||
//       this.props.birthdate === '' ||
//       this.props.password === '' ||
//       this.props.confirmation === ''
//     ) {
//       this.props.alertWithType(
//         'error',
//         'Champs de formulaires incomplets',
//         'Vous devez compléter tous les champs du formulaire avant de continuer.',
//       );
//       return;
//     }
//     this.setState({ loading: true });
//     try {
//       const createCandidate = await this.props.addCandidate({
//         variables: {
//           email: this.props.email,
//           password: this.props.password,
//           firstname: this.props.firstname,
//           lastname: this.props.lastname,
//           jobSituation: this.props.jobSituation,
//           years_experience: this.props.experimented ? '3' : '0',
//           birthdate: moment(this.props.birthdate, 'DD-MM-YYYY').toDate(),
//           finished_profile: false,
//           phone: this.props.phone,
//           headline: this.props.headline,
//           picture: this.props.picture,
//           registered_from_phone: true,
//         },
//       });
//       const { candidate, error: errorCandidate } = createCandidate.data.createCandidateWithoutUser;
//       if (errorCandidate || !candidate) {
//         this.setState({ loading: false });
//         this.props.alertWithType(
//           'error',
//           'Erreur serveur',
//           `Désolé... une erreur est survenu lors de l'inscription : ${errorCandidate.message}`,
//         );
//         return;
//       }
//       const log = await this.props.login({
//         variables: {
//           email: this.props.email,
//           password: this.props.password,
//           linkedin: this.state.linkedinId,
//           facebook: this.state.facebook,
//           type: 'candidate',
//           player_id: this.mobileId,
//         },
//       });
//       this.setState({ loading: false });
//       global.authStockage.setToken('access-token', log.data.login.token);
//       global.authStockage.setToken('signed-as', 'candidate');
//       this.props.dispatch(setField('progress', CandidateForm.progressionIndicator));
//       navigate('CandidateFormLastExperience', {
//         transition: 'noTransition',
//         register: true,
//         userId: candidate.user_id,
//       });
//     } catch (e) {
//       this.setState({ loading: false });
//       this.props.alertWithType(
//         'error',
//         'Erreur serveur',
//         `Désolé... une erreur est survenu lors de l'inscription : ${e.toString()}`,
//       );
//     }
//   };

//   changeField = (key, value) => {
//     this.props.dispatch(setField(key, value));
//   };

//   externalSignIn = async (user) => {
//     const { navigate } = this.props.navigation;
//     let login;
//     try {
//       login = await this.props.login({
//         variables: {
//           email: user.email,
//           password: user.password,
//           linkedin: this.state.linkedinId,
//           facebook: this.state.facebook,
//           type: 'candidate',
//         },
//       });
//     } catch (e) {
//       console.log('externalSignIn login catch', e);
//       return false;
//     }
//     const { token, error } = login.data.login;
//     if (error) {
//       this.setState({ loading: false });
//       this.props.alertWithType(
//         'error',
//         'Erreur serveur',
//         `Désolé... une erreur est survenu lors de la connexion : ${error.message}`,
//       );
//       return false;
//     }
//     if (!token) return false;
//     this.setState({ loading: false });
//     this.props.dispatch(setField('progress', CandidateForm.progressionIndicator));
//     global.authStockage.setToken('access-token', token);
//     global.authStockage.setToken('signed-as', 'candidate');
//     // createChatClient({ accessToken: token });
//     navigate('TabViews');
//     return true;
//   };

//   externalSign = async (user) => {
//     this.setState({ loading: true });
//     let logged;
//     try {
//       logged = await this.externalSignIn(user);
//     } catch (e) {
//       console.log('catch externSignIn ', e);
//     }
//     if (logged) return;
//     let createCandidate;
//     try {
//       createCandidate = await this.props.addCandidate({
//         variables: {
//           email: user.email,
//           password: user.password,
//           firstname: user.firstname,
//           lastname: user.lastname,
//           finished_profile: false,
//           headline: user.headline,
//           registered_from_phone: true,
//         },
//       });
//     } catch (e) {
//       console.log('catch', e);
//     }
//     const { candidate, error: errorCandidate } = createCandidate.data.createCandidateWithoutUser;
//     if (errorCandidate || !candidate) {
//       this.setState({ loading: false });
//       this.props.alertWithType(
//         'error',
//         'Erreur serveur',
//         `Désolé... une erreur est survenu lors de l'inscription : ${errorCandidate.message}`,
//       );
//       return;
//     }
//     await this.externalSignIn(user);
//   };

//   // facebookData = (err, res) => {
//   //   if (err || !res) return;
//   //   const { id: facebook, first_name: firstname, last_name: lastname, email } = res;
//   //   const password = randomatic('Aa0', 6);
//   //   this.setState({ facebook }, () => this.externalSign({ email, firstname, lastname, password }));
//   // }

//   // facebookSignUp = async () => {
//   //   let result;
//   //   try {
//   //     result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
//   //   } catch (e) {
//   //     this.props.alertWithType(
//   //       'info',
//   //       'Facebook',
//   //       `L'authentification par Facebook à échoué, réssayer plus tard.\n${result.error}`,
//   //     );
//   //     return;
//   //   }
//   //   if (result.isCancelled) {
//   //     this.props.alertWithType(
//   //       'info',
//   //       'Facebook',
//   //       'Authentification par Facebook annulé.',
//   //     );
//   //     return;
//   //   }
//   //   const infoRequest = new GraphRequest(
//   //     '/me?fields=id,first_name,last_name,email,birthday',
//   //     null,
//   //     this.facebookData,
//   //   );
//   //   new GraphRequestManager().addRequest(infoRequest).start();
//   // }

//   linkedinSignUp = async ({ nativeEvent: { data: rawData } }) => {
//     let data;
//     try {
//       data = JSON.parse(rawData);
//     } catch (e) {
//       console.warn('catch', e, data);
//       return;
//     }

//     this.setState({ linkedin: false });
//     if (data.code !== 200) {
//       this.props.alertWithType('error', 'Erreur', data);
//       return;
//     }
//     const password = randomatic('Aa0', 6);
//     const {
//       data: {
//         firstName: firstname,
//         lastName: lastname,
//         emailAddress: email,
//         headline,
//         id: linkedinId,
//       },
//     } = data;
//     this.changeField('email', email);
//     this.changeField('firstname', firstname);
//     this.changeField('lastname', lastname);
//     this.changeField('headline', headline);
//     this.changeField('password', password);

//     this.setState({ linkedinId }, () =>
//       this.externalSign({ email, firstname, lastname, headline, password }),
//     );
//   };

//   render() {
//     const legalBirth = moment().subtract(18, 'years');
//     const {
//       lastname,
//       firstname,
//       email,
//       birthdate,
//       phone,
//       headline,
//       experimented,
//       confirmation,
//       jobSituation,
//       password,
//     } = this.props;
//     if (this.state.linkedin) {
//       return (
//         <WebView
//           source={{ uri: LINKEDIN }}
//           injectedJavaScript={
//             "setTimeout(function(){window.postMessage(document.body.innerHTML, '*');}, 10);"
//           }
//           onMessage={this.linkedinSignUp}
//           javaScriptEnabled
//         />
//       );
//     }
//     return (
//       <KeyboardAwareScrollView
//         showsVerticalScrollIndicator={false}
//         style={{ backgroundColor: 'white' }}
//         keyboardShouldPersistTaps="always"
//         showsHorizontalScrollIndicator={false}
//       >
//         {<Loader width={this.props.progress} start={this.props.progress} />}
//         <View style={styles.background}>
//           {/* <Text style={themeElements.title}>Remplissez votre profil facilement :</Text> */}
//           {/* <View style={styles.socialButtonsContainer}>
//             { <Button
//               title="Facebook"
//               buttonStyle={styles.socialButton}
//               backgroundColor="#4970bb"
//              textStyle={styles.socialButtonText}
//               icon={{ name: 'facebook-official', type: 'font-awesome', size: 31 }}
//               containerViewStyle={styles.facebookButtonCont}
//               onPress={this.facebookSignUp}
//             /> }
//             { <Button
//               title="LinkedIn"
//               onPress={() => this.setState({ linkedin: true })}
//               buttonStyle={styles.socialButton}
//               textStyle={styles.socialButtonText}
//               backgroundColor="#225982"
//               icon={{ name: 'linkedin', type: 'font-awesome', size: 24 }}
//               containerViewStyle={styles.linkedButtonCont}
//             />}
//           </View> */}
//           {/* <Separator
//             viewStyle={styles.separatorView}
//             color="#afafaf"
//             text="OU"
//             lineLength={2}
//           /> */}
//           <View style={styles.form}>
//             <ProfilePicture
//               edit
//               alertWithType={this.props.alertWithType}
//               width={110}
//               defaultPicture={defaultProfilePictureAdd}
//               containerStyle={styles.profilePictureView}
//               avatarStyle={styles.profilePicture}
//               onChange={picture => this.changeField('picture', picture)}
//             />
//             <FormLabel>Prénom</FormLabel>
//             <Firstname value={firstname} onChangeText={this.changeField} />
//             <FormLabel>Nom</FormLabel>
//             <Lastname value={lastname} onChangeText={this.changeField} />
//             <FormLabel>Email</FormLabel>
//             <Email
//               onChangeText={this.changeField}
//               value={email}
//               onBlur={() => {
//                 this.setState({
//                   emailError: !validator.isEmail(this.props.email),
//                 });
//               }}
//             />
//             {this.state.emailError && (
//               <FormValidationMessage>{'E-mail incorrect'}</FormValidationMessage>
//             )}
//             <FormLabel>Téléphone portable</FormLabel>
//             <Phone
//               onChangeText={this.changeField}
//               onBlur={() => {
//                 this.setState({
//                   phoneError: !validator.isMobilePhone(phone, ['fr-FR']),
//                 });
//               }}
//               value={phone}
//             />
//             {this.state.phoneError && (
//               <FormValidationMessage>{'Numéro de téléphone incorrect'}</FormValidationMessage>
//             )}
//             <FormLabel>Date de naissance</FormLabel>
//             <Birthdate
//               value={birthdate}
//               onDateChange={this.changeField}
//               styles={styles}
//               maxDate={legalBirth.format('DD-MM-YYYY')}
//             />
//             <FormLabel>Mot de passe</FormLabel>
//             <Password
//               onChangeText={this.changeField}
//               onBlur={() => {
//                 this.setState({
//                   passwordError: !validator.isLength(this.props.password, {
//                     min: 6,
//                   }),
//                 });
//               }}
//               value={password}
//             />
//             <FormLabel>Confirmation du mot de passe</FormLabel>
//             <Confirmation
//               onChangeText={this.changeField}
//               onBlur={() => {
//                 this.setState({
//                   confirmationError: password !== confirmation,
//                 });
//               }}
//               value={confirmation}
//             />
//             {this.state.passwordError && (
//               <FormValidationMessage>
//                 {'Le mot de passe doit être composé de 6 caractères minimum'}
//               </FormValidationMessage>
//             )}
//             <FormLabel>Fonction actuelle ou recherchée</FormLabel>
//             <Headline onChangeText={this.changeField} value={headline} />
//             <FormLabel>{"Niveau d'expérience"}</FormLabel>
//             <ExperimentCheckbox
//               checked={experimented}
//               onPress={() => this.changeField('experimented', !experimented)}
//             />
//             <FormLabel>{'Situation actuelle'}</FormLabel>
//             <JobSituation value={jobSituation} onValueChange={this.changeField} />
//             <View style={styles.cguCheckboxView}>
//               <CheckBox
//                 checked={this.props.cguAccepted}
//                 checkedIcon="check-square"
//                 checkedColor={theme.primaryColor}
//                 containerStyle={styles.cguCheckboxContainer}
//                 onPress={this.checkCGU}
//                 fontSize={24}
//               />
//               <View style={styles.cguTextContainer}>
//                 <TouchableHighlight underlayColor="#ffffff" onPress={this.checkCGU}>
//                   <Text>En vous inscrivant, vous acceptez nos</Text>
//                 </TouchableHighlight>
//                 <TouchableHighlight
//                   underlayColor="#ffffff"
//                   onPress={() => {
//                     Linking.openURL('https://bizzeo.co/cgu').catch();
//                   }}
//                 >
//                   <Text style={styles.cguButton}>conditions générales d’utilisation.</Text>
//                 </TouchableHighlight>
//               </View>
//             </View>
//           </View>
//         </View>
//         <BottomButton
//           title="JE M'INSCRIS"
//           loading={this.state.loading}
//           onPress={this.handleSubscribeButton}
//           disabled={!this.isRequiredFieldsFilled()}
//         />
//       </KeyboardAwareScrollView>
//     );
//   }
// }

// const LOGGED_IN_MUTATION = gql`
//   mutation login(
//     $email: String!
//     $password: String!
//     $type: String!
//     $facebook: String
//     $linkedin: String
//   ) {
//     login(
//       email: $email
//       password: $password
//       type: $type
//       facebook: $facebook
//       linkedin: $linkedin
//     ) {
//       token
//     }
//   }
// `;

// const addCandidate = gql`
//   mutation(
//     $email: String!
//     $password: String!
//     $firstname: String!
//     $lastname: String!
//     $birthdate: String
//     $phone: String
//     $picture: String
//     $headline: String
//     $years_experience: String!
//     $jobSituation: String!
//     $finished_profile: Boolean
//   ) {
//     createCandidateWithoutUser(
//       email: $email
//       password: $password
//       firstname: $firstname
//       lastname: $lastname
//       birthdate: $birthdate
//       phone: $phone
//       jobSituation: $jobSituation
//       finished_profile: $finished_profile
//       picture: $picture
//       headline: $headline
//       years_experience: $years_experience
//       registered_from_phone: true
//     ) {
//       candidate {
//         user_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const mapStateToProps = state => ({
//   firstname: state.candidateForm.firstname,
//   lastname: state.candidateForm.lastname,
//   email: state.candidateForm.email,
//   phone: state.candidateForm.phone,
//   headline: state.candidateForm.headline,
//   birthdate: state.candidateForm.birthdate,
//   password: state.candidateForm.password,
//   jobSituation: state.candidateForm.jobSituation,
//   experimented: state.candidateForm.experimented,
//   confirmation: state.candidateForm.confirmation,
//   progress: state.candidateForm.progress,
//   picture: state.candidateForm.picture,
//   cguAccepted: state.candidateForm.cguAccepted,
//   linkedin: state.candidateForm.linkedin,
// });

// const ContainerWithData = compose(
//   graphql(addCandidate, { name: 'addCandidate' }),
//   graphql(LOGGED_IN_MUTATION, { name: 'login' }),
// )(CandidateForm);

// export default connect(mapStateToProps)(connectAlert(ContainerWithData));

// import React, { Component } from 'react';
// import { View, Text, StyleSheet, ScrollView } from 'react-native';
// import { HeaderBackButton } from 'react-navigation';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';
// import { connect } from 'react-redux';

// import { themeElements } from '../../config/constants/index';
// import { setField } from '../../actions/candidateForm';
// import { BottomButton } from '../../components/Button';
// import Loader from '../../components/Loader/Loader';
// import connectAlert from '../../components/Alert/connectAlert';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import TagsSelector from '../../components/Content/TagsSelector';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: 'white',
//     paddingLeft: 32,
//     paddingRight: 32,
//     paddingTop: 21.5,
//     paddingBottom: 20.5,
//   },
//   titleView: {
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   tagSelector: {
//     marginBottom: 45,
//   },
// });

// const VIEWER_CANDIDATE = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         tags
//       }
//     }
//   }
// `;

// const QUERY_CANDIDATE = gql`
//   query ($user_id: String) {
//     singleCandidate(user_id: $user_id) {
//       tags
//     }
//   }
// `;

// class CandidateFormTags extends Component {
//   static navigationOptions = ({ navigation }) => ({
//     title: 'Sélectionner vos tags',
//     headerTruncatedBackTitle: '',
//     headerLeft: navigation.state.params && navigation.state.params.edit
//       ? (<HeaderBackButton tintColor='white'
//         onPress={() => navigation.goBack(null)}
//       />) : null,
//   });

//   static progressionIndicator = 40;

//   componentWillMount() {
//     if (!this.props.data.loading) {
//       const newTags = this.props.data.singleCandidate ?
//         this.props.data.singleCandidate.tags : (this.props.data.viewer ? this.props.data.viewer.candidate.tags : []);
//       this.setState({ tags: newTags || [] });
//     }
//   }

//   componentDidUpdate(prevProps) {
//     if (
//       prevProps.data &&
//       prevProps.data.loading
//       && !this.props.data.loading
//       && ((this.props.data.viewer && this.props.data.viewer.candidate)
//         || this.props.data.singleCandidate)
//     ) {
//       const newTags = this.props.data.singleCandidate ?
//         this.props.data.singleCandidate.tags : (this.props.data.viewer ? this.props.data.viewer.candidate.tags : []);
//       this.setState({ tags: newTags || [] });
//     }
//   }

//   componentWillUnmount() {
//     this.props.dispatch(setField('progress', CandidateFormTags.progressionIndicator));
//   }

//   state = {
//     tags: [],
//     loading: false,
//   };

//   props: {
//     navigation: {
//       navigate: {},
//       state: {
//         params: {
//           edit: boolean,
//           buttonLabel: string,
//         }
//       },
//     },
//     editInfos: Function,
//     data: {
//       error: boolean,
//       loading: boolean,
//       singleCandidate: {
//         tags: [string]
//       },
//       viewer: {
//         id: string,
//         candidate: {
//           tags: [string]
//         },
//       },
//     },
//     dispatch: Function,
//     tags: [],
//     progress: number,
//     alertWithType: Function,
//   };

//   isRequiredFieldsFilled = () => !(this.state.tags.length > 4 || this.state.tags.length < 1);

//   getEditOptions() {
//     const { state } = this.props.navigation;
//     const options = {
//       refetchQueries: [],
//       variables: {
//         tags: this.state.tags,
//       },
//     };
//     if (state.params && state.params.userId) {
//       options.refetchQueries.push({
//         query: QUERY_CANDIDATE,
//         variables: {
//           user_id: state.params.userId,
//         },
//       });
//       options.variables.user_id = state.params.userId;
//     } else {
//       options.refetchQueries.push({
//         query: VIEWER_CANDIDATE,
//       });
//       options.variables.user_id = this.props.data.viewer.id;
//     }
//     return options;
//   }

//   handleSubscribeButton = async () => {
//     const { navigate, state, goBack } = this.props.navigation;

//     if (this.state.tags.length > 4 || this.state.tags.length < 1) {
//       this.props.alertWithType('error', 'Selection incomplète', 'Vous devez sélectionner entre 1 et 4 tags.');
//     } else {
//       this.setState({ loading: true });
//       await this.props.editInfos(this.getEditOptions());
//       if (state.params && state.params.edit) {
//         this.props.alertWithType('success', 'Modification effectuées', 'Les modifications ont bien été prises en compte');
//         goBack(null);
//       } else {
//         this.props.alertWithType('success', 'Modification effectuées', 'Les modifications ont bien été prises en compte');
//         this.props.dispatch(setField('progression', CandidateFormTags.progressionIndicator));
//         navigate('CandidateFormQualities', { transition: 'noTransition', userId: state.params.userId });
//       }
//       this.setState({ loading: false });
//     }
//   };

//   selectTag = (tag) => {
//     const selected = this.state.tags.indexOf(tag);
//     const newTags = this.state.tags.slice();
//     if (selected !== -1) {
//       newTags.splice(selected, 1);
//     } else if (selected === -1 && this.state.tags.length < 4) {
//       newTags.push(tag);
//     }
//     this.setState({ tags: newTags });
//   };

//   render() {
//     if (this.props.data.error) {
//       return (<ErrorComponent message='Problème lors de la récupération du profil.' />);
//     }

//     const { state } = this.props.navigation;
//     return (
//       <View style={{ flex: 1 }}>
//         {
//           state.params && state.params.edit ? null : (
//             <Loader
//               width={this.props.progress}
//               start={this.props.progress}
//             />
//           )
//         }
//         <ScrollView
//           style={styles.background}
//           showsVerticalScrollIndicator={false}
//           showsHorizontalScrollIndicator={false} >
//           <View style={styles.titleView}>
//             <Text style={themeElements.title}>Pour pouvoir vous recommander
//               les annonces les plus pertinentes,
//               merci de sélectionner les tags les plus importants (1 à 4) :
//             </Text>
//           </View>
//           <TagsSelector
//             selectedTags={this.state.tags}
//             onSelect={this.selectTag}
//             containerStyle={styles.tagSelector}
//           />
//         </ScrollView>
//         <BottomButton
//           title={state.params && state.params.buttonLabel ? state.params.buttonLabel : 'SUITE'}
//           loading={this.state.loading}
//           onPress={this.handleSubscribeButton}
//           disabled={!this.isRequiredFieldsFilled()}
//         />
//       </View>
//     );
//   }
// }

// const EDIT_INFOS = gql`
//   mutation (
//     $user_id: String!,
//     $tags: [String!],
//   )  {
//     editCandidate(
//       user_id: $user_id,
//       tags: $tags,
//     ) {
//       candidate {
//         user_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const mapStateToProps = state => ({
//   progress: state.candidateForm.progress,
// });

// const checkIfUserId = (ownProps) => {
//   if (ownProps.navigation.state.params
//     && ownProps.navigation.state.params.userId) {
//     return true;
//   }
//   return false;
// };

// const ContainerWithData = compose(
//   graphql(QUERY_CANDIDATE, {
//     skip: ownProps => (!checkIfUserId(ownProps)),
//     options: ownProps => ({
//       variables: {
//         user_id: checkIfUserId(ownProps) ? ownProps.navigation.state.params.userId : null,
//       },
//     }),
//   }),
//   graphql(VIEWER_CANDIDATE, {
//     skip: ownProps => (!!checkIfUserId(ownProps)),
//   }),
//   graphql(EDIT_INFOS, { name: 'editInfos' }),
// )(CandidateFormTags);

// export default connect(mapStateToProps)(connectAlert(ContainerWithData));

// import React, { Component } from 'react';
// import {
//   View,
//   StyleSheet,
//   ScrollView,
// } from 'react-native';
// import { Button } from 'react-native-elements';
// import { connect } from 'react-redux';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';

// import { theme } from '../../config/constants/index';
// import Loader from '../../components/Loader/Loader';
// import connectAlert from '../../components/Alert/connectAlert';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import Profile from '../../components/GQLLinked/Profile';
// import { setField } from '../../actions/candidateForm';
// import HeaderBackButton from 'react-navigation/src/views/Header/HeaderBackButton';

// const QUERY_PROFILE_CANDIDATE = gql`
//   query SingleCandidateQuery($user_id: String) {
//     singleCandidate(user_id: $user_id) {
//       editable
//       firstname
//       lastname
//       tags
//       qualities
//       headline
//       work_city {
//         name
//       }
//       experience(order_by_date: true) {
//         experience_id
//         role
//       }
//       education(order_by_date: true) {
//         education_id
//       }
//       language {
//         language_id
//       }
//     }
//   }
// `;

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//   },
//   validateButton: {
//     marginBottom: 24,
//     borderRadius: 4,
//     shadowColor: 'rgba(0, 0, 0, 0.08)',
//     shadowOffset: { width: 0.5, height: 4 },
//     shadowOpacity: 0.8,
//     shadowRadius: 2,
//     width: '90%',
//     height: 40,
//     marginTop: 24,
//     alignSelf: 'center',
//   },
// });

// const VIEWER_CANDIDATE = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         education {
//           education_id
//         }
//         experience {
//           experience_id
//         }
//       }
//     }
//   }
// `;

// class CandidateFormPreview extends Component {
//   static navigationOptions = ({ navigation }) => ({
//     title: 'Aperçu de votre profil',
//     headerTruncatedBackTitle: '',
//     headerLeft: navigation.state.params && navigation.state.params.register
//       ? (<HeaderBackButton tintColor='white'
//         onPress={() => navigation.goBack(null)}
//       />) : null,
//   });

//   props: {
//     navigation: {
//       navigate: Function,
//     },
//     data: {
//       loading: boolean,
//       error: boolean,
//       viewer: {
//         id: string,
//         candidate: {
//           firstname: string,
//           lastname: string,
//           work_city: [{
//             name: string,
//           }],
//           tags: [string],
//           qualities: [string],
//         }
//       }
//     },
//     validateProfile: Function,
//     mutate: Function,
//     dispatch: Function,
//     progression: number,
//     experiences: [],
//     languages: [],
//     training: [],
//     alertWithType: Function,
//   };

//   handleValidateButton = async () => {
//     const { navigate } = this.props.navigation;

//     const ret = await this.props.validateProfile({
//       refetchQueries: [
//         {
//           query: VIEWER_CANDIDATE,
//         },
//         {
//           query: QUERY_PROFILE_CANDIDATE,
//           variables: {
//             user_id: this.props.data.viewer.id,
//           },
//         },
//       ],
//       variables: {
//         user_id: this.props.data.viewer.id,
//       },
//     });
//     if (ret.data.editCandidate.error) {
//       this.props.alertWithType('error', 'Erreur', ret.data.editCandidate.error.message);
//       return;
//     }
//     // TODO verifier le profil, si profil complet ... si incomplet
//     const { viewer: { candidate } } = this.props.data;
//     const complete = candidate.education.length > 0 && candidate.experience.length > 0;
//     if (complete) {
//       this.props.dispatch(setField('progress', 0));
//       navigate('Validation', {
//         headerTitle: 'Inscription terminée',
//         title: 'Votre profil est maintenant complété !',
//         subtitle: 'Merci d’avoir pris le temps de remplir votre profil et bienvenue sur Bizzeo.' +
//         ' Nous vous souhaitons une bonne recherche !',
//         goTo: 'TabViews',
//         reset: true,
//         buttonName: 'C\'est parti',
//       });
//     } else {
//       navigate('ValidationChoice', {
//         headerTitle: 'Inscription terminée',
//         title: 'Votre profil n\'est pas complet !',
//         subtitle: "Vous n'avez pas ajouté de formation ou d'expérience à votre profil, les recruteurs ne pourront donc pas vous contacter.",
//         type: 'alert',
//         goTo: 'TabViews',
//         reset: true,
//       });
//     }
//   };

//   render() {
//     if (this.props.data.error) {
//       return (<ErrorComponent message='Problème lors de la récupération du profil.' />);
//     } else if (this.props.data.loading) {
//       return (<LoadingComponent />);
//     }

//     return (
//       <View style={styles.background}>
//         <Loader
//           width={100}
//           start={this.props.progression}
//         />
//         <ScrollView
//           showsVerticalScrollIndicator={false}
//           showsHorizontalScrollIndicator={false} >
//           <Profile preview userId={this.props.data.viewer.id} navigate={this.props.navigation.navigate} />
//         </ScrollView>
//         <Button
//           title='Valider votre profil'
//           color={theme.secondaryColor}
//           backgroundColor={theme.primaryColor}
//           buttonStyle={styles.validateButton}
//           onPress={this.handleValidateButton}
//         />
//       </View>
//     );
//   }
// }

// const mapStateToProps = state => ({
//   progress: state.candidateForm.progress,
//   experiences: state.candidateForm.experiences,
//   languages: state.candidateForm.languages,
//   training: state.candidateForm.training,
// });

// const VALIDATE_PROFILE = gql`
//   mutation (
//     $user_id: String!,
//   )  {
//     editCandidate(
//       user_id: $user_id,
//       finished_profile: true,
//     ) {
//       candidate {
//         user_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const ContainerWithData = compose(
//   graphql(VIEWER_CANDIDATE),
//   graphql(VALIDATE_PROFILE, { name: 'validateProfile' }),
// )(CandidateFormPreview);

// export default connect(mapStateToProps)(connectAlert(ContainerWithData));

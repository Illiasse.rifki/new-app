// import React, { Component } from 'react';
// import { View, Text, StyleSheet } from 'react-native';
// import { ButtonGroup } from 'react-native-elements';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';
// import { connect } from 'react-redux';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// import { setField } from '../../actions/candidateForm';
// import { theme, themeElements, graduationLevels } from '../../config/constants';
// import { BottomButton } from '../../components/Button';
// import UserCurrentCity from '../../components/GQLLinked/UserCurrentCity';
// import Loader from '../../components/Loader/Loader';
// import connectAlert from '../../components/Alert/connectAlert';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import Field from '../../components/Design/Field';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: 'white',
//     paddingLeft: 32,
//     paddingRight: 32,
//     paddingTop: 21.5,
//     paddingBottom: 20.5,
//   },
//   titleView: {
//     flex: 3,
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   formInputContainer: {
//     marginTop: 5,
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   formLabelContent: {
//     fontSize: 18,
//     color: theme.fontBlack,
//     fontWeight: '500',
//     marginLeft: 0,
//     marginRight: 0,
//     marginBottom: 0,
//     marginTop: 0,
//   },
//   formLabelContainer: {
//     marginBottom: 0,
//     marginTop: 0,
//   },
//   graduationLevelButtonsStyle: {
//     marginTop: 10,
//     marginLeft: 0,
//     marginRight: 0,
//     backgroundColor: theme.secondaryColor,
//     borderColor: theme.gray,
//   },
//   yearsExpContainer: {
//     marginTop: 16,
//     marginBottom: 50,
//     zIndex: -1,
//   },
//   headlineContainer: {
//     marginTop: 8,
//     marginBottom: 8,
//   },
// });

// class CandidateFormExperience extends Component {
//   static navigationOptions = {
//     title: 'A propos de vous',
//     headerTruncatedBackTitle: '',
//   };

//   static progressIndicator = 60;

//   componentWillUnmount() {
//     this.props.dispatch(setField('progress', CandidateFormExperience.progressIndicator));
//   }

//   componentWillMount() {
//     if (!this.props.data.loading) {
//       const { candidate } = this.props.data.viewer;
//       this.setState({
//         graduationLevel: graduationLevels.find(e => e.value === candidate.graduation_level) || null,
//         yearsStudy: candidate.years_experience,
//         currentCityId: candidate.current_city ? candidate.current_city.current_city_id : null,
//         userId: this.props.data.viewer.id,
//         headline: candidate.headline !== 'Utilisateur de Bizzeo' ? candidate.headline : '',
//       });
//     }
//   }

//   componentDidUpdate(prevProps) {
//     if (prevProps.data.loading && !this.props.data.loading && this.props.data.viewer.candidate) {
//       const { candidate } = this.props.data.viewer;
//       this.setState({
//         graduationLevel: graduationLevels.find(e => e.value === candidate.graduation_level) || null,
//         yearsStudy: candidate.years_experience,
//         currentCityId: candidate.current_city ? candidate.current_city.current_city_id : null,
//         userId: this.props.data.viewer.id,
//         headline: candidate.headline !== 'Utilisateur de Bizzeo' ? candidate.headline : '',
//       });
//     }
//   }

//   props: {
//     navigation: {
//       navigate: Function,
//     },
//     editInfos: Function,
//     data: {
//       error: boolean,
//       loading: boolean,
//       viewer: {
//         id: string,
//         candidate: {
//           graduation_level: string,
//           years_experience: string,
//           current_city: {
//             current_city_id: string,
//           },
//         },
//       },
//     },
//     progress: number,
//     city: string,
//     yearsStudy: number,
//     graduationLevel: string,
//     dispatch: Function,
//     alertWithType: Function,
//   };

//   state = {
//     city: {
//       description: '',
//       lat: null,
//     },
//     loading: false,
//   };

//   isRequiredFieldsFilled = () =>
//     !(
//       this.state.currentCityId === null ||
//       this.state.graduationLevel === null ||
//       this.state.yearsStudy === null
//     );

//   handleSubscribeButton = async () => {
//     const { navigate } = this.props.navigation;

//     if (
//       this.state.currentCityId === null ||
//       this.state.graduationLevel === null ||
//       this.state.yearsStudy === null
//     ) {
//       this.props.alertWithType(
//         'error',
//         'Champs de formulaires incomplets',
//         'Vous devez compléter tous les champs du formulaire avant de continuer.',
//       );
//       return;
//     }
//     this.setState({ loading: true });
//     await this.props.editInfos({
//       variables: {
//         user_id: this.props.data.viewer.id,
//         graduation_level: this.state.graduationLevel.value,
//         headline: this.state.headline,
//       },
//     });
//     this.props.dispatch(setField('progress', CandidateFormExperience.progressIndicator));
//     this.setState({ loading: false });
//     navigate('CandidateFormLookingFor', { transition: 'noTransition' });
//   };

//   selectGraduationLevel = (selectedIndex) => {
//     const { graduationLevel } = this.state;
//     const newIndex =
//       graduationLevel && selectedIndex === graduationLevel.value - 1 ? null : selectedIndex + 1;
//     const newGrad = newIndex && graduationLevels.find(g => g.value === newIndex.toString());
//     this.setState({ graduationLevel: newGrad });
//   };

//   render() {
//     if (this.props.data.error) {
//       return <ErrorComponent message="Problème lors de la récupération du profil." />;
//     }
//     return (
//       <View style={{ flex: 1 }}>
//         <Loader width={this.props.progress} start={this.props.progress} />
//         <KeyboardAwareScrollView
//           behavior="padding"
//           style={styles.background}
//           keyboardShouldPersistTaps="always"
//           showsVerticalScrollIndicator={false}
//           showsHorizontalScrollIndicator={false}
//         >
//           <View style={styles.titleView}>
//             <Text style={themeElements.title}>
//               Pour permettre de mieux vous vendre aux recruteurs, nous avons besoin d’en savoir plus
//               sur vous :
//             </Text>
//           </View>
//           {/* <Field
//             label="Quel est votre poste actuel ou souhaité?"
//             labelStyle="dark"
//             placeholder="Ex: Business développeur chez ..."
//             containerStyle={styles.headlineContainer}
//             value={this.state.headline}
//             onChangeText={headline => this.setState({ headline })}
//           /> */}
//           <Field label="Où habitez-vous actuellement ?" labelStyle="dark">
//             <UserCurrentCity
//               createWorkCityOnAdd
//               currentCityId={this.state.currentCityId}
//               userId={this.state.userId}
//               onChangeCity={(currentCityId) => {
//                 this.setState({ currentCityId });
//               }}
//             />
//           </Field>
//           <Field label="Votre niveau d'études" labelStyle="dark">
//             <ButtonGroup
//               buttons={graduationLevels.map(g => g.text)}
//               containerStyle={styles.graduationLevelButtonsStyle}
//               onPress={this.selectGraduationLevel}
//               selectedIndex={this.state.graduationLevel && this.state.graduationLevel.value - 1}
//               selectedBackgroundColor={theme.primaryColor}
//               textStyle={{ color: theme.fontBlack, fontWeight: '400' }}
//               innerBorderStyle={{ color: 'transparent' }}
//               selectedTextStyle={{ color: theme.secondaryColor }}
//             />
//           </Field>
//           {/* <Field
//             label="Nombre d’années d’expérience"
//             labelStyle="dark"
//             placeholder="Chiffre (0, 1, 3, 5, ...)"
//             containerStyle={styles.yearsExpContainer}
//             value={this.state.yearsStudy}
//             onChangeText={yearsStudy => this.setState({ yearsStudy })}
//             textInputOptions={{
//               keyboardType: 'number-pad',
//               returnKeyType: 'done',
//             }}
//           /> */}
//         </KeyboardAwareScrollView>
//         <BottomButton
//           loading={this.state.loading}
//           onPress={this.handleSubscribeButton}
//           disabled={!this.isRequiredFieldsFilled()}
//         />
//       </View>
//     );
//   }
// }

// const mapStateToProps = state => ({
//   progress: state.candidateForm.progress,
// });

// const VIEWER_CANDIDATE = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         current_city {
//           current_city_id
//         }
//         graduation_level
//         headline
//       }
//     }
//   }
// `;

// const EDIT_INFOS = gql`
//   mutation($user_id: String!, $graduation_level: String, $headline: String) {
//     editCandidate(user_id: $user_id, graduation_level: $graduation_level, headline: $headline) {
//       candidate {
//         user_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const ContainerWithData = compose(
//   graphql(VIEWER_CANDIDATE),
//   graphql(EDIT_INFOS, { name: 'editInfos' }),
// )(CandidateFormExperience);

// export default connect(mapStateToProps)(connectAlert(ContainerWithData));

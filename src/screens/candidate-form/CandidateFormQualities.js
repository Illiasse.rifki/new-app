// import React, { Component } from 'react';
// import {
//   ScrollView,
//   View,
//   Text,
//   StyleSheet,
// } from 'react-native';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';
// import { connect } from 'react-redux';

// import { setField } from '../../actions/candidateForm';
// import { themeElements } from '../../config/constants/index';
// import { BottomButton } from '../../components/Button';
// import Loader from '../../components/Loader/Loader';
// import connectAlert from '../../components/Alert/connectAlert';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import QualitiesSelector from '../../components/Content/QualitiesSelector';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: 'white',
//     paddingLeft: 32,
//     paddingRight: 32,
//     paddingTop: 21.5,
//     paddingBottom: 20.5,
//   },
//   titleView: {
//     marginLeft: 0,
//     marginRight: 0,
//   },
// });

// const VIEWER_CANDIDATE = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         qualities
//       }
//     }
//   }
// `;

// const QUERY_CANDIDATE = gql`
//   query ($user_id: String) {
//     singleCandidate(user_id: $user_id) {
//       qualities
//     }
//   }
// `;

// class CandidateFormQualities extends Component {
//   static navigationOptions = {
//     title: 'Sélectionner vos qualités',
//     headerTruncatedBackTitle: '',
//   };

//   static progressionIndicator = 50;

//   componentWillUnmount() {
//     this.props.dispatch(setField('progress', CandidateFormQualities.progressionIndicator));
//   }

//   componentWillMount() {
//     if (!this.props.data.loading) {
//       const qualities = this.props.data.singleCandidate ?
//         this.props.data.singleCandidate.qualities : this.props.data.viewer.candidate.qualities;
//       this.setState({ qualities: qualities || [] });
//     }
//   }

//   componentDidUpdate(prevProps) {
//     if (
//       prevProps.data.loading
//       && !this.props.data.loading
//       && (this.props.data.viewer.candidate || this.props.data.singleCandidate)
//     ) {
//       const qualities = this.props.data.singleCandidate ?
//         this.props.data.singleCandidate.qualities : this.props.data.viewer.candidate.qualities;
//       this.setState({ qualities: qualities || [] });
//     }
//   }

//   state = {
//     qualities: [],
//     loading: false,
//   };

//   props: {
//     navigation: {
//       navigate: {},
//       state: {
//         params: {
//           edit: boolean,
//           buttonLabel: string,
//         }
//       },
//       goBack: Function,
//     },
//     editInfos: Function,
//     data: {
//       singleCandidate: {
//         qualities: [string],
//       },
//       viewer: {
//         id: string,
//         candidate: {
//           qualities: [string]
//         },
//       },
//       loading: boolean,
//       error: boolean,
//     },
//     qualities: [],
//     progress: number,
//     dispatch: Function,
//     alertWithType: Function,
//   };

//   isRequiredFieldsFilled() {
//     if (this.state.qualities.length > 4 || this.state.qualities.length < 2) {
//       return false;
//     }
//     return true;
//   }

//   getEditOptions() {
//     const { state } = this.props.navigation;
//     const options = {
//       refetchQueries: [],
//       variables: {
//         qualities: this.state.qualities,
//       },
//     };
//     if (state.params && state.params.userId) {
//       options.refetchQueries.push({
//         query: QUERY_CANDIDATE,
//         variables: {
//           user_id: state.params.userId,
//         },
//       });
//       options.variables.user_id = state.params.userId;
//     } else {
//       options.refetchQueries.push({
//         query: VIEWER_CANDIDATE,
//       });
//       options.variables.user_id = this.props.data.viewer.id;
//     }
//     return options;
//   }

//   handleSubscribeButton = async () => {
//     const { navigate, state, goBack } = this.props.navigation;

//     if (this.state.qualities.length > 4 || this.state.qualities.length < 2) {
//       this.props.alertWithType('error', 'Selection incomplète', 'Vous devez sélectionner entre 2 et 4 qualités.');
//     } else {
//       this.setState({ loading: true });
//       await this.props.editInfos(this.getEditOptions());
//       if (state.params && state.params.edit) {
//         this.props.alertWithType('success', 'Modifications effectuées', 'Les modifications ont bien été prises en compte');
//         goBack(null);
//       } else {
//         this.props.alertWithType('success', 'Modifications effectuées', 'Les modifications ont bien été prises en compte');
//         this.props.dispatch(setField('progress', CandidateFormQualities.progressionIndicator));
//         navigate('CandidateFormExperience', { transition: 'noTransition', userId: state.params.userId });
//       }
//       this.setState({ loading: false });
//     }
//   };

//   selectQuality = (quality) => {
//     const selected = this.state.qualities.indexOf(quality);
//     const newQualities = this.state.qualities.slice();
//     if (selected !== -1) {
//       newQualities.splice(selected, 1);
//     } else if (selected === -1 && this.state.qualities.length < 4) {
//       newQualities.push(quality);
//     }
//     this.setState({ qualities: newQualities });
//   };

//   render() {
//     if (this.props.data.error) {
//       return (<ErrorComponent message='Problème lors de la récupération du profil.' />);
//     }

//     const { state } = this.props.navigation;
//     return (
//       <View style={{ flex: 1 }}>
//         {
//           state.params && state.params.edit ? null : (
//             <Loader
//               width={this.props.progress}
//               start={this.props.progress}
//             />
//           )
//         }
//         <ScrollView
//           style={styles.background}
//           showsVerticalScrollIndicator={false}
//           showsHorizontalScrollIndicator={false} >
//           <View style={styles.titleView}>
//             <Text style={themeElements.title}>Pour pouvoir vous recommander
//               les annonces les plus pertinentes, merci de sélectionner
//               vos qualités les plus importantes (entre 2 et 4) :
//             </Text>
//           </View>
//           <View style={styles.qualitiesContainer}>
//             <QualitiesSelector
//               selectedQualities={this.state.qualities}
//               onSelect={this.selectQuality}
//             />
//           </View>
//         </ScrollView>
//         <BottomButton
//           title={state.params && state.params.buttonLabel ? state.params.buttonLabel : 'SUITE'}
//           loading={this.state.loading}
//           onPress={this.handleSubscribeButton}
//           disabled={!this.isRequiredFieldsFilled()}
//         />
//       </View>
//     );
//   }
// }

// const mapStateToProps = state => ({
//   progress: state.candidateForm.progress,
// });

// const EDIT_INFOS = gql`
//   mutation (
//     $user_id: String!,
//     $qualities: [String!],
//   )  {
//     editCandidate(
//       user_id: $user_id,
//       qualities: $qualities,
//     ) {
//       candidate {
//         user_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const checkIfUserId = (ownProps) => {
//   if (ownProps.navigation.state.params
//     && ownProps.navigation.state.params.userId) {
//     return true;
//   }
//   return false;
// };

// const ContainerWithData = compose(
//   graphql(QUERY_CANDIDATE, {
//     skip: ownProps => (!checkIfUserId(ownProps)),
//     options: ownProps => ({
//       variables: {
//         user_id: checkIfUserId(ownProps) ? ownProps.navigation.state.params.userId : null,
//       },
//     }),
//   }),
//   graphql(VIEWER_CANDIDATE, {
//     skip: ownProps => (!!checkIfUserId(ownProps)),
//   }),
//   graphql(EDIT_INFOS, { name: 'editInfos' }),
// )(CandidateFormQualities);

// export default connect(mapStateToProps)(connectAlert(ContainerWithData));

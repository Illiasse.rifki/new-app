// // @flow
// import React, { Component } from "react";
// import { View, Text, StyleSheet } from "react-native";
// import { Button } from "react-native-elements";
// import { connect } from "react-redux";
// import { graphql } from "react-apollo";
// import gql from "graphql-tag";
// // import { ReactNativeFile } from 'apollo-upload-client';

// import Expo from "expo";

// import { theme, themeElements } from "../../config/constants/index";

// import Loader from "../../components/Loader/Loader";
// import Separator from "../../components/Design/Separator";
// import connectAlert from "../../components/Alert/connectAlert";
// import { setField } from "../../actions/candidateForm";

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: "white",
//     paddingLeft: 32,
//     paddingRight: 32,
//     paddingTop: 29.5,
//     paddingBottom: 20.5,
//   },
//   button: {
//     borderRadius: 4,
//     shadowColor: "rgba(0, 0, 0, 0.16)",
//     shadowOffset: { width: 0, height: 1.5 },
//     shadowOpacity: 0.8,
//     shadowRadius: 2,
//   },
//   buttonText: {
//     color: theme.secondaryColor,
//   },
//   buttonContainer: {
//     width: "80%",
//     marginTop: 26,
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   downloadCVView: {
//     flex: 10,
//     alignItems: "center",
//     justifyContent: "center",
//   },
//   linkedView: {
//     flex: 12,
//     alignItems: "center",
//     justifyContent: "center",
//   },
//   separatorView: {
//     flex: 1,
//     alignItems: "center",
//     justifyContent: "center",
//   },
//   skipView: {
//     flex: 2,
//     alignItems: "center",
//     justifyContent: "center",
//   },
//   title: {
//     textAlign: "center",
//   },
// });

// class CandidateFormCV extends Component {
//   static navigationOptions = {
//     title: "Inscription candidat",
//     headerTruncatedBackTitle: "",
//   };

//   props: {
//     navigation: {
//       navigate: {},
//       state: {},
//     },
//     alertWithType: Function,
//     progression: number,
//     mutate: Function,
//     dispatch: Function,
//   };

//   handleUpload = () => {
//     const { navigate } = this.props.navigation;
//     Expo.DocumentPicker.getDocumentAsync({ type: "*/*" })
//       .then((res) => {
//         // const file = new ReactNativeFile({
//         //   uri: res.uri,
//         //   type: "application/pdf",
//         //   name: res.name,
//         // });
//         const file = null;
//         this.props
//           .mutate({
//             variables: {
//               file,
//             },
//           })
//           .then(() =>
//             navigate("CandidateFormPreview", {
//               transition: "noTransition",
//             })
//           )
//           .catch((e) => {
//             this.props.alertWithType(
//               "info",
//               "Information",
//               "Réessayer d'envoyer votre pdf plus tard."
//             );
//           });
//       })
//       .catch((err) => {
//         if (err) {
//           this.props.alertWithType(
//             "info",
//             "Information",
//             "Fichier incompatible."
//           );
//         }
//       });
//   };

//   componentDidMount() {
//     this.handlePassButton();
//   }

//   handlePassButton = () => {
//     const { navigate } = this.props.navigation;
//     this.props.dispatch(setField("progress", 0));
//     navigate("CandidateFormPreview", { transition: "noTransition" });
//   };

//   render() {
//     return null;
//     // <View style={{ flex: 1 }}>
//     //   <Loader width={100} start={this.props.progression} />
//     //   <View style={styles.background}>
//     //     <View style={styles.downloadCVView}>
//     //       <Text style={[themeElements.title, styles.title]}>
//     //         Remplissez votre profil avec votre CV
//     //       </Text>
//     //       <Button
//     //         title="Télécharger mon CV"
//     //         backgroundColor={theme.primaryColor}
//     //         buttonStyle={styles.button}
//     //         textStyle={styles.buttonText}
//     //         containerViewStyle={styles.buttonContainer}
//     //         onPress={this.handleUpload}
//     //       />
//     //     </View>
//     //     <View style={styles.separatorView}>
//     //       <Separator text="OU" lineLength={2} color={theme.gray} />
//     //     </View>
//     //     {/* <View style={styles.linkedView}>
//     //       <Text style={[themeElements.title, styles.title]}>
//     //         Récupérez votre compte avec LinkedIn
//     //       </Text>
//     //       <Button
//     //         title="Connexion à LinkedIn"
//     //         backgroundColor="#0076b7"
//     //         buttonStyle={styles.button}
//     //         textStyle={styles.buttonText}
//     //         containerViewStyle={styles.buttonContainer}
//     //       />
//     //     </View> */}
//     //     <View style={styles.skipView}>
//     //       <Button
//     //         title="Passer cette étape"
//     //         color={theme.primaryColor}
//     //         backgroundColor={theme.secondaryColor}
//     //         textStyle={{ fontWeight: '500' }}
//     //         onPress={this.handlePassButton}
//     //       />
//     //     </View>
//     //   </View>
//     // </View>
//   }
// }

// const MUTATION = gql`
//   mutation ($file: Upload!) {
//     upload(file: $file) {
//       uploaded
//     }
//   }
// `;

// const mapStateToProps = (state) => ({
//   progression: state.candidateForm.progression,
// });

// const CandidateFormCVgraphQL = graphql(MUTATION)(CandidateFormCV);

// export default connect(mapStateToProps)(connectAlert(CandidateFormCVgraphQL));

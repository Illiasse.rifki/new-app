import { connect } from 'react-redux';
import React from 'react';
import { HeaderBackButton } from 'react-navigation';
import CandidateProfileAddEducation from '../profile/CandidateProfileAddEducation';
import { setField } from '../../actions/candidateForm';
// This is a genuine way to not duplicate code


const progressionIndicator = 30;

class CandidateFormLastEducation extends CandidateProfileAddEducation {
  static defaultProps = {
    register: true,
    progress: progressionIndicator,
  }

  static navigationOptions = ({ navigation }) => ({
    title: 'Votre dernière formation',
    headerTruncatedBackTitle: '',
    headerLeft: navigation.state.params && navigation.state.params.register
      ? (<HeaderBackButton tintColor='white'
        onPress={() => navigation.goBack(null)}
      />) : null,
  });

  handleAddEducationButton = () => {
    const { goBack, navigate, state } = this.props.navigation;
    if (state.params.register) {
      navigate('CandidateFormTags', { transition: 'noTransition', userId: state.params.userId });
    } else { goBack(null); }
    this.props.dispatch(setField('progress', progressionIndicator));
  }
}

export default connect(({ candidateForm }) =>
  ({ progress: candidateForm.progress }))(CandidateFormLastEducation);

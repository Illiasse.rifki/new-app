// import React, { Component } from 'react';
// import {
//   View,
//   ScrollView,
//   Text,
//   StyleSheet,
// } from 'react-native';
// import { connect } from 'react-redux';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';

// import { setField } from '../../actions/candidateForm';
// import { theme, sectors } from '../../config/constants/index';
// import Loader from '../../components/Loader/Loader';
// import connectAlert from '../../components/Alert/connectAlert';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import BottomButton from '../../components/Button/BottomButton';
// import ActivitySectorPicker from '../../components/Content/ActivitySectorPicker';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: theme.secondaryColor,
//     paddingTop: 29.5,
//   },
//   title: {
//     paddingLeft: 32,
//     paddingRight: 32,
//     lineHeight: 24,
//     fontSize: 18,
//     fontWeight: '500',
//     color: theme.fontBlack,
//   },
//   titleView: {
//     marginTop: 20,
//   },
//   list: {
//     marginBottom: 32,
//   },
// });

// const VIEWER_CANDIDATE = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         wish_activity
//       }
//     }
//   }
// `;

// class CandidateFormActivitySectorWanted extends Component {
//   static navigationOptions = {
//     title: 'Secteur d\'activité souhaité',
//     headerTruncatedBackTitle: '',
//   };

//   state = {
//     wantSectors: [],
//     search: '',
//     loading: false,
//   };

//   componentWillUnmount() {
//     this.props.dispatch(setField('progression', 75));
//   }

//   componentWillMount() {
//     if (!this.props.data.loading) {
//       this.setState({ wantSectors: this.props.data.viewer.candidate.wish_activity || [] });
//     }
//   }

//   componentDidUpdate(prevProps) {
//     if (
//       prevProps.data.loading
//       && !this.props.data.loading
//       && this.props.data.viewer.candidate
//     ) {
//       this.setState({ wantSectors: this.props.data.viewer.candidate.wish_activity || [] });
//     }
//   }

//   props: {
//     navigation: {
//       navigate: Function,
//       goBack: Function,
//       state: {
//         params: {
//           edit: boolean,
//         },
//       },
//     },
//     dispatch: Function,
//     progression: number,
//     wantSectors: [string],
//     editInfos: Function,
//     alertWithType: Function,
//     data: {
//       loading: boolean,
//       error: boolean,
//       viewer: {
//         id: string,
//         candidate: {
//           wish_activity: [string],
//         },
//       },
//     },
//   };

//   selectActivitySector = (wantSectors) => {
//     this.setState({ wantSectors });
//   };

//   handleSubscribeButton = async () => {
//     const { navigate, state, goBack } = this.props.navigation;

//     this.setState({ loading: true });
//     const ret = await this.props.editInfos({
//       refetchQueries: [{
//         query: VIEWER_CANDIDATE,
//       }],
//       variables: {
//         user_id: this.props.data.viewer.id,
//         wish_activity: this.state.wantSectors,
//       },
//     });
//     if (ret.data.editCandidate.error) {
//       this.props.alertWithType(
//         'error',
//         'Erreur',
//         ret.data.editCandidate.error.message,
//       );
//       return;
//     }
//     if (state.params.edit) {
//       this.props.alertWithType(
//         'success',
//         'Modification effectuée',
//         'Les modifications ont bien été prises en compte',
//       );
//       goBack(null);
//     } else {
//       this.props.dispatch(setField('progression', 75));
//       navigate('CandidateFormSalary', { transition: 'noTransition' });
//     }
//     this.setState({ loading: false });
//   };

//   selectAllSectors = () => {
//     let wantSectors = [];
//     if (this.state.wantSectors.length < sectors.length) {
//       wantSectors = sectors.map(sector => sector.key).slice();
//     }
//     this.setState({ wantSectors });
//   };

//   render() {
//     if (this.props.data.error) {
//       return (<ErrorComponent message='Problème lors de la récupération du profil.' />);
//     }

//     const { state } = this.props.navigation;
//     return (
//       <View style={{ flex: 1 }}>
//         {
//           state.params.edit ? null : (
//             <Loader
//               width={75}
//               start={this.props.progression}
//             />
//           )
//         }
//         <ScrollView
//           style={styles.background}
//           showsVerticalScrollIndicator={false}
//           showsHorizontalScrollIndicator={false} >
//           <Text style={styles.title}>Sélectionnez les secteurs où vous souhaitez travaillez</Text>
//           <ActivitySectorPicker
//             multi
//             enableSearch
//             enableSelectAll
//             showTrendySectors
//             containerStyle={styles.list}
//             sectors={sectors}
//             onSelectActivity={this.selectActivitySector}
//             value={this.state.wantSectors}
//           />
//         </ScrollView>
//         <BottomButton
//           title="CHOISIR VOS SECTEURS"
//           loading={this.state.loading}
//           onPress={this.handleSubscribeButton}
//           disabled={this.state.wantSectors.length === 0}
//         />
//       </View>
//     );
//   }
// }

// const mapStateToProps = state => ({
//   progression: state.candidateForm.progression,
// });

// const EDIT_INFOS = gql`
//   mutation (
//     $user_id: String!,
//     $wish_activity: [String],
//   )  {
//     editCandidate(
//       user_id: $user_id,
//       wish_activity: $wish_activity,
//     ) {
//       candidate {
//         user_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const ContainerWithData = compose(
//   graphql(VIEWER_CANDIDATE),
//   graphql(EDIT_INFOS, { name: 'editInfos' }),
// )(CandidateFormActivitySectorWanted);

// export default connect(mapStateToProps)(connectAlert(ContainerWithData));

// import React, { Component } from 'react';
// import {
//   View,
//   Text,
//   ScrollView,
//   StyleSheet,
// } from 'react-native';
// import { FormInput, FormLabel, ListItem, List } from 'react-native-elements';
// import { connect } from 'react-redux';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';
// import { setField } from '../../actions/candidateForm';

// import { theme, themeElements } from '../../config/constants/index';
// import Loader from '../../components/Loader/Loader';
// import connectAlert from '../../components/Alert/connectAlert';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import BottomButton from '../../components/Button/BottomButton';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: 'white',
//     paddingLeft: 32,
//     paddingRight: 32,
//     paddingTop: 29.5,
//     paddingBottom: 20.5,
//   },
//   formInputContainer: {
//     marginTop: 5,
//     marginLeft: 0,
//     marginRight: 0,
//     flex: 1,
//   },
//   formInputView: {
//     flexDirection: 'row',
//   },
//   euro: {
//     alignSelf: 'center',
//     fontSize: 18,
//     color: theme.fontBlack,
//     fontWeight: '500',
//     marginBottom: -7,
//     marginLeft: 10,
//   },
//   formLabelContent: {
//     fontSize: 18,
//     color: theme.fontBlack,
//     fontWeight: '500',
//     marginLeft: 0,
//     marginRight: 0,
//     marginBottom: 0,
//     marginTop: 32,
//   },
//   topFormLabelContent: {
//     fontSize: 18,
//     color: theme.fontBlack,
//     fontWeight: '500',
//     marginLeft: 0,
//     marginRight: 0,
//     marginBottom: 0,
//     marginTop: 0,
//   },
//   formLabelContainer: {
//     marginBottom: 0,
//   },
//   benefitsListContainer: {
//     borderWidth: 1,
//     borderColor: theme.gray,
//     borderRadius: 2,
//     marginBottom: 55,
//     borderBottomColor: 'transparent',
//   },
//   benefitTitle: {
//     fontSize: 16,
//     color: theme.fontBlack,
//   },
//   benefitTitleSelected: {
//     fontSize: 16,
//     color: theme.primaryColor,
//   },
//   benefitContainer: {
//     paddingTop: 6,
//     paddingBottom: 6,
//   },
//   benefitIcon: {
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   benefitWrapper: {
//     marginLeft: 0,
//     marginRight: 0,
//   },
// });

// class CandidateFormSalary extends Component {
//   static navigationOptions = {
//     title: 'Salaires & Avantages',
//     headerTruncatedBackTitle: '',
//   };

//   static companyBenefits = [
//     { value: 'Voiture de fonction', key: 'car' },
//     { value: 'Téléphone de fonction', key: 'phone' },
//     { value: 'Ordinateur de fonction', key: 'computer' },
//     { value: 'Tickets restaurants', key: 'food' },
//     { value: 'Carte bleu de fonction', key: 'credit-card' },
//   ];

//   static salaries = [
//     { label: '- 18.000', value: '0-18000' },
//     { label: '18.000 - 25.000', value: '18000-25000' },
//     { label: '25.000 - 32.000', value: '25000-32000' },
//     { label: '32.000 - 39.000', value: '32000-39000' },
//     { label: '39.000 - 46.000', value: '39000-46000' },
//     { label: '46.000 - 53.000', value: '46000-53000' },
//     { label: '+ 53.000', value: '53000-9999999' },
//   ];

//   componentWillUnmount() {
//     this.props.dispatch(setField('progression', 87.5));
//   }

//   componentWillMount() {
//     if (!this.props.data.loading) {
//       this.setState({
//         annualFixedSalary: this.props.data.viewer.candidate.raw_salary || '',
//         annualVariableSalary: this.props.data.viewer.candidate.variable_salary || '',
//         annualFixedSalaryWanted: this.props.data.viewer.candidate.wish_raw_salary || '',
//         annualVariableSalaryWanted: this.props.data.viewer.candidate.wish_variable_salary || '',
//         companyBenefits: this.props.data.viewer.candidate.advantages || [],
//       });
//     }
//   }

//   componentDidUpdate(prevProps) {
//     if (
//       prevProps.data.loading
//       && !this.props.data.loading
//       && this.props.data.viewer
//       && this.props.data.viewer.candidate
//     ) {
//       this.setState({
//         annualFixedSalary: this.props.data.viewer.candidate.raw_salary || '',
//         annualVariableSalary: this.props.data.viewer.candidate.variable_salary || '',
//         annualFixedSalaryWanted: this.props.data.viewer.candidate.wish_raw_salary || '',
//         annualVariableSalaryWanted: this.props.data.viewer.candidate.wish_variable_salary || '',
//         companyBenefits: this.props.data.viewer.candidate.advantages || [],
//       });
//     }
//   }

//   props: {
//     navigation: {
//       navigate: Function,
//     },
//     editInfos: Function,
//     data: {
//       loading: boolean,
//       error: boolean,
//       viewer: {
//         id: string,
//         candidate: {
//           raw_salary: string,
//           wish_raw_salary: string,
//           variable_salary: string,
//           wish_variable_salary: string,
//           advantages: [string],
//         },
//       },
//     },
//     dispatch: Function,
//     progression: number,
//     companyBenefits: [string],
//     annualFixedSalary: number,
//     annualVariableSalary: number,
//     annualFixedSalaryWanted: number,
//     annualVariableSalaryWanted: number,
//     alertWithType: Function,
//   };

//   state = {
//     companyBenefits: [],
//     loading: false,
//   };

//   isRequiredFieldsFilled = () => {
//     return true;
//   };

//   handleSubscribeButton = async () => {
//     const { navigate } = this.props.navigation;

//     this.setState({ loading: true });
//     const ret = await this.props.editInfos({
//       variables: {
//         user_id: this.props.data.viewer.id,
//         raw_salary: this.state.annualFixedSalary,
//         variable_salary: this.state.annualVariableSalary,
//         wish_raw_salary: this.state.annualFixedSalaryWanted,
//         wish_variable_salary: this.state.annualVariableSalaryWanted,
//         advantages: this.state.companyBenefits,
//       },
//     });
//     if (ret.data.editCandidate.error) {
//       this.props.alertWithType(
//         'error',
//         'Erreur',
//         ret.data.editCandidate.error.message,
//       );
//       return;
//     }
//     this.props.dispatch(setField('progression', 87.5));
//     this.setState({ loading: false });
//     navigate('CandidateFormCV', { transition: 'noTransition' });
//   };

//   selectBenefit(benefit) {
//     const idx = this.isSelectedBenefit(benefit);
//     const companyBenefits = this.state.companyBenefits.slice();
//     if (idx === false) {
//       companyBenefits.push(benefit);
//     } else {
//       companyBenefits.splice(idx, 1);
//     }
//     this.setState({ companyBenefits });
//   }

//   isSelectedBenefit(benefit) {
//     const idx = this.state.companyBenefits.indexOf(benefit);
//     return idx === -1 ? false : idx;
//   }

//   render() {
//     if (this.props.data.error) {
//       return (<ErrorComponent message='Problème lors de la récupération du profil.' />);
//     }

//     return (
//       <View style={{ flex: 1 }}>
//         <Loader
//           width={87.5}
//           start={this.props.progression}
//         />
//         <ScrollView
//           style={styles.background}
//           showsVerticalScrollIndicator={false}
//           showsHorizontalScrollIndicator={false} >
//           <FormLabel
//             labelStyle={styles.topFormLabelContent}
//             containerStyle={styles.formLabelContainer}
//           >
//             &quot;Brisons les tabous&quot;, si vous travaillez actuellement, quel est le montant
//             de votre salaire annuel brut en fixe et variable ?
//           </FormLabel>
//           <View style={styles.formInputView}>
//             <FormInput
//               placeholder='Votre salaire annuel brut fixe'
//               autoCapitalize='none'
//               keyboardType='number-pad'
//               inputStyle={themeElements.formInputText}
//               containerStyle={styles.formInputContainer}
//               onChangeText={annualFixedSalary => this.setState({ annualFixedSalary })}
//               value={this.state.annualFixedSalary}
//               returnKeyType='done'
//               underlineColorAndroid='#979797'
//             />
//             <Text style={styles.euro}>€</Text>
//           </View>
//           <View style={styles.formInputView}>
//             <FormInput
//               placeholder='Votre salaire annuel brut variable'
//               autoCapitalize='none'
//               keyboardType='number-pad'
//               inputStyle={themeElements.formInputText}
//               containerStyle={styles.formInputContainer}
//               onChangeText={annualVariableSalary => this.setState({ annualVariableSalary })}
//               value={this.state.annualVariableSalary}
//               returnKeyType='done'
//               underlineColorAndroid='#979797'
//             />
//             <Text style={styles.euro}>€</Text>
//           </View>
//           <FormLabel
//             labelStyle={styles.formLabelContent}
//             containerStyle={styles.formLabelContainer}
//           >
//             Quel est le salaire annuel brut en fixe et variable que vous recherchez ?
//           </FormLabel>
//           <View style={styles.formInputView}>
//             <FormInput
//               placeholder='Salaire annuel brut fixe recherché'
//               autoCapitalize='none'
//               keyboardType='number-pad'
//               returnKeyType='done'
//               inputStyle={themeElements.formInputText}
//               containerStyle={styles.formInputContainer}
//               onChangeText={annualFixedSalaryWanted => this.setState({ annualFixedSalaryWanted })}
//               value={this.state.annualFixedSalaryWanted}
//               underlineColorAndroid='#979797'
//             />
//             <Text style={styles.euro}>€</Text>
//           </View>
//           <View style={styles.formInputView}>
//             <FormInput
//               placeholder='Salaire annuel brut variable recherché'
//               autoCapitalize='none'
//               keyboardType='number-pad'
//               returnKeyType='done'
//               inputStyle={themeElements.formInputText}
//               containerStyle={styles.formInputContainer}
//               onChangeText={annualVariableSalaryWanted =>
//                 this.setState({ annualVariableSalaryWanted })}
//               value={this.state.annualVariableSalaryWanted}
//               underlineColorAndroid='#979797'
//             />
//             <Text style={styles.euro}>€</Text>
//           </View>
//           <FormLabel
//             labelStyle={styles.formLabelContent}
//             containerStyle={styles.formLabelContainer}
//           >
//             Quels sont les avantages les plus intéressants pour vous ?
//           </FormLabel>
//           <List
//             containerStyle={styles.benefitsListContainer}
//           >
//             {
//               CandidateFormSalary.companyBenefits.map((benefit) => {
//                 const isSelectedBenefit = this.isSelectedBenefit(benefit.key);
//                 return (<ListItem
//                   key={benefit.key}
//                   title={benefit.value}
//                   rightIcon={{ color: 'transparent' }}
//                   leftIcon={{
//                     name: isSelectedBenefit === false ? 'square-o' : 'check-square',
//                     type: 'font-awesome',
//                     size: 15,
//                     color: isSelectedBenefit === false ? '#979797' : theme.primaryColor,
//                     style: styles.benefitIcon,
//                   }}
//                   titleStyle={isSelectedBenefit === false ?
//                     styles.benefitTitle : styles.benefitTitleSelected}
//                   containerStyle={styles.benefitContainer}
//                   wrapperStyle={styles.benefitWrapper}
//                   onPress={() => { this.selectBenefit(benefit.key); }}
//                 />);
//               })
//             }
//           </List>
//         </ScrollView>
//         <BottomButton
//           loading={this.state.loading}
//           onPress={this.handleSubscribeButton}
//           disabled={!this.isRequiredFieldsFilled()}
//         />
//       </View>
//     );
//   }
// }

// const mapStateToProps = state => ({
//   progression: state.candidateForm.progression,
// });

// const VIEWER_CANDIDATE = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         raw_salary
//         variable_salary
//         wish_raw_salary
//         wish_variable_salary
//         advantages
//       }
//     }
//   }
// `;

// const EDIT_INFOS = gql`
//   mutation (
//     $user_id: String!,
//     $raw_salary: String,
//     $variable_salary: String,
//     $wish_raw_salary: String,
//     $wish_variable_salary: String,
//     $advantages: [String],
//   )  {
//     editCandidate(
//       user_id: $user_id,
//       raw_salary: $raw_salary,
//       variable_salary: $variable_salary,
//       wish_raw_salary: $wish_raw_salary,
//       wish_variable_salary: $wish_variable_salary,
//       advantages: $advantages,
//     ) {
//       candidate {
//         user_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const ContainerWithData = compose(
//   graphql(VIEWER_CANDIDATE),
//   graphql(EDIT_INFOS, { name: 'editInfos' }),
// )(CandidateFormSalary);

// export default connect(mapStateToProps)(connectAlert(ContainerWithData));

import { connect } from 'react-redux';
import React from 'react';
import { HeaderBackButton } from 'react-navigation';
import CandidateProfileAddExperience from '../profile/CandidateProfileAddExperience';
import { setField } from '../../actions/candidateForm';


// This is a genuine way to not duplicate code

const progressionIndicator = 20;

class CandidateFormLastExperience extends CandidateProfileAddExperience {
  static defaultProps = {
    register: true,
    progress: progressionIndicator,
  }

  static navigationOptions = ({ navigation }) => ({
    title: 'Votre dernière expérience',
    headerTruncatedBackTitle: '',
    headerLeft: navigation.state.params && navigation.state.params.register
      ? (<HeaderBackButton tintColor='white'
        onPress={() => navigation.goBack(null)}
      />) : null,
  });

  handleAddExperienceButton = () => {
    const { state, goBack, navigate } = this.props.navigation;
    if (state.params.register) {
      navigate('CandidateFormLastEducation', { transition: 'noTransition', register: true, userId: state.params.userId });
    } else {
      goBack(null);
    }
    this.props.dispatch(setField('progress', progressionIndicator));
  }
}

export default connect(({ candidateForm }) =>
  ({ progress: candidateForm.progress }))(CandidateFormLastExperience);

// import React, { Component } from 'react';
// import {
//   View,
//   Text,
//   StyleSheet,
//   ScrollView,
// } from 'react-native';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';
// import { FormLabel } from 'react-native-elements';
// import { connect } from 'react-redux';

// import { setField } from '../../actions/candidateForm';
// import { theme, themeElements, companySize } from '../../config/constants/index';
// import Loader from '../../components/Loader/Loader';
// import connectAlert from '../../components/Alert/connectAlert';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import UserWorkCity from '../../components/GQLLinked/UserWorkCity';
// import { BottomButton } from '../../components/Button';
// import CompanySizesPicker from '../../components/Content/CompanySizesPicker';
// import LoadingComponent from '../../components/Elements/LoadingComponent';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: 'white',
//     paddingLeft: 32,
//     paddingRight: 32,
//     paddingTop: 29.5,
//     paddingBottom: 20.5,
//   },
//   titleView: {
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   formInputContainer: {
//     marginTop: 5,
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   formLabelContent: {
//     fontSize: 18,
//     color: theme.fontBlack,
//     fontWeight: '500',
//     marginLeft: 0,
//     marginRight: 0,
//     lineHeight: 24,
//     marginBottom: 0,
//     marginTop: 32,
//   },
//   formLabelContainer: {
//     marginBottom: 0,
//   },
//   companySizeButtonsStyle: {
//     marginTop: 10,
//     marginLeft: 0,
//     marginRight: 0,
//     marginBottom: 0,
//     backgroundColor: theme.secondaryColor,
//     borderColor: '#afafaf',
//   },
//   cityInputContainer: {
//     position: 'relative',
//     backgroundColor: 'transparent',
//     zIndex: 10,
//     marginTop: 8,
//     marginBottom: 0,
//     borderTopWidth: 0,
//     borderBottomWidth: 0,
//   },
//   cityInput: {
//     zIndex: -1,
//     backgroundColor: 'transparent',
//     marginLeft: 0,
//     marginRight: 0,
//     marginTop: 0,
//     marginBottom: 0,
//     paddingBottom: 0,
//     fontSize: 16,
//     color: theme.fontBlack,
//     paddingLeft: 0,
//     paddingRight: 0,
//     borderRadius: 0,
//     borderTopWidth: 0,
//     borderBottomWidth: 1,
//     borderBottomColor: '#c9c9c9',
//   },
//   cityContainer: {
//     flex: 1,
//   },
//   citiesList: {
//     marginTop: -18,
//     backgroundColor: 'white',
//     borderLeftWidth: 1,
//     borderRightWidth: 1,
//     borderBottomWidth: 1,
//     borderColor: '#c9c9c9',
//   },
//   smallHelp: {
//     fontStyle: 'italic',
//     fontSize: 15,
//   },
// });

// class CandidateFormLookingFor extends Component {
//   static navigationOptions = {
//     title: 'Que recherchez vous ?',
//     headerTruncatedBackTitle: '',
//   };

//   static progressIndicator = 70;

//   componentWillUnmount() {
//     this.props.dispatch(setField('progress', CandidateFormLookingFor.progressIndicator));
//   }

//   // componentWillMount() {
//   //   if (!this.props.data.loading) {
//   //     const { candidate } = this.props.data.viewer;
//   //     this.setState({
//   //       companySize: this.getCompanySizesFromValue(candidate.company_size),
//   //       searchCities: candidate.work_city,
//   //       userId: this.props.data.viewer.id,
//   //     });
//   //   }
//   // }

//   // componentDidUpdate(prevProps) {
//   //   if (
//   //     prevProps.data.loading
//   //     && !this.props.data.loading
//   //     && this.props.data.viewer.candidate
//   //   ) {
//   //     const { candidate } = this.props.data.viewer;
//   //     this.setState({
//   //       companySize: this.getCompanySizesFromValue(candidate.company_size),
//   //       searchCities: candidate.work_city,
//   //       userId: this.props.data.viewer.id,
//   //     });
//   //   }
//   // }

//   getCompanySizesFromValue = values => companySize.slice().filter(s => (values.indexOf(s.value) !== -1));

//   props: {
//     navigation: {
//       navigate: Function,
//     },
//     dispatch: Function,
//     editInfos: Function,
//     progress: number,
//     companySize: [string],
//     searchCities: [],
//     alertWithType: Function,
//     data: {
//       loading: boolean,
//       error: boolean,
//       viewer: {
//         id: string,
//         candidate: {
//           company_size: [String],
//           work_city: [{
//             work_city_id: string,
//           }],
//         },
//       },
//     },
//   };

//   state = {
//     companySize: [],
//     searchCities: [],
//     loading: false,
//   };

//   isRequiredFieldsFilled = () => !(this.state.companySize.length === 0
//       || this.state.searchCities.length === 0);

//   handleSubscribeButton = async () => {
//     const { navigate } = this.props.navigation;
//     if (this.state.companySize.length === 0
//     || this.state.searchCities.length === 0) {
//       this.props.alertWithType(
//         'error',
//         'Champs de formulaires incomplets',
//         'Vous devez selectionner au moins une ville et une taille d\'entreprise.',
//       );
//       return;
//     }
//     const companySizeData = this.state.companySize;
//     this.setState({ loading: true });
//     try {
//       const ret = await this.props.editInfos({
//         variables: {
//           user_id: this.props.data.viewer.id,
//           company_size: companySizeData && companySizeData.map(s => s.value),
//         },
//       });
//       if (ret.data.editCandidate.error) {
//         this.props.alertWithType(
//           'error',
//           'Erreur',
//           ret.data.editCandidate.error.message,
//         );
//         return;
//       }
//       this.props.dispatch(setField('progress', CandidateFormLookingFor.progressIndicator));
//       navigate('CandidateFormActivitySectorWanted', { transition: 'noTransition' });
//     } catch (e) {
//       this.props.alertWithType(
//         'error',
//         'Erreur',
//         'Erreur serveur',
//       );
//     }
//     this.setState({ loading: false });
//   };

//   selectCompanySize = (newCompanySize) => {
//     this.setState({ companySize: newCompanySize });
//   };

//   renderSearchCities() {
//     const cities = this.state.searchCities.slice();
//     return (
//       <View>
//         {
//           this.state.searchCities.map((city, i) => (
//             <UserWorkCity
//               key={city.work_city_id}
//               userId={this.props.data.viewer.id}
//               workCityId={city.work_city_id}
//               onDeleteCity={() => {
//                 cities.splice(i, 1);
//                 this.setState({ searchCities: cities });
//               }}
//             />))
//         }
//         <UserWorkCity
//           userId={this.props.data.viewer.id}
//           searchRef={(instance) => { this.googlePlacesRef = instance; }}
//           onAddCity={(workCityId) => {
//             cities.push({ work_city_id: workCityId });
//             this.googlePlacesRef.setAddressText('');
//             this.setState({ searchCities: cities });
//           }}
//         />
//       </View>
//     );
//   }

//   render() {
//     if (this.props.data.loading) return <LoadingComponent/>;
//     if (this.props.data.error) {
//       return (<ErrorComponent message='Problème lors de la récupération du profil.' />);
//     }

//     return (
//       <View style={{ flex: 1 }}>
//         <Loader
//           width={this.props.progress}
//           start={this.props.progress}
//         />
//         <ScrollView
//           keyboardShouldPersistTaps='always'
//           style={styles.background}
//         >
//           <View style={styles.titleView}>
//             <Text style={themeElements.title}>C’est bientôt fini,
//               nous avons besoin de savoir ce que vous recherchez
//             </Text>
//           </View>
//           <FormLabel
//             labelStyle={styles.formLabelContent}
//             containerStyle={styles.formLabelContainer}
//           >
//             Où recherchez-vous ?
//           </FormLabel>
//           <View>
//             {
//               this.renderSearchCities()
//             }
//           </View>
//           <View style={{ zIndex: -1, marginBottom: 50 }}>
//             <FormLabel
//               labelStyle={styles.formLabelContent}
//               containerStyle={styles.formLabelContainer}
//             >
//               Dans quelle taille d'entreprise souhaitez-vous travailler ?
//               <Text style={styles.smallHelp}>{' (en nombre d\'employés, plusieurs choix possibles)'}</Text>
//             </FormLabel>
//             <CompanySizesPicker
//               selectedSizes={this.state.companySize}
//               onCompanySizesChange={this.selectCompanySize}
//             />
//           </View>
//         </ScrollView>
//         <BottomButton
//           loading={this.state.loading}
//           onPress={this.handleSubscribeButton}
//           disabled={!this.isRequiredFieldsFilled()}
//         />
//       </View>
//     );
//   }
// }

// const mapStateToProps = state => ({
//   progress: state.candidateForm.progress,
// });

// const VIEWER_CANDIDATE = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         company_size
//         work_city {
//           work_city_id
//         }
//       }
//     }
//   }
// `;

// const EDIT_INFOS = gql`
//   mutation (
//     $user_id: String!,
//     $company_size: [String],
//   )  {
//     editCandidate(
//       user_id: $user_id,
//       company_size: $company_size,
//     ) {
//       candidate {
//         user_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const ContainerWithData = compose(
//   graphql(VIEWER_CANDIDATE),
//   graphql(EDIT_INFOS, { name: 'editInfos' }),
// )(CandidateFormLookingFor);

// export default connect(mapStateToProps)(connectAlert(ContainerWithData));

// import React, { Component } from 'react';
// import {
//   View,
//   ScrollView,
//   Text,
//   StyleSheet,
// } from 'react-native';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';
// import { connect } from 'react-redux';

// import { setField } from '../../actions/candidateForm';
// import { theme } from '../../config/constants';
// import { BottomButton } from '../../components/Button';
// import Loader from '../../components/Loader/Loader';
// import connectAlert from '../../components/Alert/connectAlert';
// import ActivitySectorPicker from '../../components/Content/ActivitySectorPicker';

// const styles = StyleSheet.create({
//   background: {
//     flex: 1,
//     backgroundColor: theme.secondaryColor,
//     paddingTop: 29.5,
//   },
//   title: {
//     paddingLeft: 32,
//     paddingRight: 32,
//     lineHeight: 24,
//     fontSize: 18,
//     fontWeight: '500',
//     color: theme.fontBlack,
//   },
//   list: {
//     marginBottom: 62,
//   },
// });

// class CandidateFormActivitySector extends Component {
//   static navigationOptions = {
//     title: 'Secteur d\'activité travaillé',
//     headerTruncatedBackTitle: '',
//   };

//   state = {
//     loading: false,
//   };

//   componentWillUnmount() {
//     this.props.dispatch(setField('progression', 50));
//   }

//   componentWillMount() {
//     if (!this.props.data.loading) {
//       this.setState({ activitySector: this.props.data.viewer.candidate.activity });
//     }
//   }

//   componentDidUpdate(prevProps) {
//     if (
//       prevProps.data.loading
//       && !this.props.data.loading
//       && this.props.data.viewer.candidate
//     ) {
//       this.setState({ activitySector: this.props.data.viewer.candidate.activity });
//     }
//   }

//   props: {
//     navigation: {
//       navigate: Function,
//     },
//     dispatch: Function,
//     activitySector: string,
//     progression: number,
//     editInfos: Function,
//     data: {
//       loading: boolean,
//       error: boolean,
//       viewer: {
//         id: string,
//         candidate: {
//           activity: string,
//         },
//       },
//     },
//     alertWithType: Function,
//   };

//   handleSubscribeButton = async () => {
//     const { navigate } = this.props.navigation;

//     this.setState({ loading: true }, async () => {
//       const ret = await this.props.editInfos({
//         variables: {
//           user_id: this.props.data.viewer.id,
//           activity: this.state.activitySector,
//         },
//       });
//       if (ret.data.editCandidate.error) {
//         this.props.alertWithType(
//           'error',
//           'Erreur',
//           ret.data.editCandidate.error.message,
//         );
//       } else {
//         this.props.dispatch(setField('progression', 50));
//         navigate('CandidateFormLookingFor', { transition: 'noTransition' });
//       }
//       this.setState({ loading: false });
//     });
//   };

//   render() {
//     return (
//       <View style={{ flex: 1 }}>
//         <Loader
//           width={50}
//           start={this.props.progression}
//         />
//         <ScrollView
//           style={styles.background}
//           showsVerticalScrollIndicator={false}
//           showsHorizontalScrollIndicator={false} >
//           <Text style={styles.title}>Sélectionnez le secteur où vous travaillez</Text>
//           <ActivitySectorPicker
//             enableSearch
//             containerStyle={styles.list}
//             onSelectActivity={activitySector => this.setState({ activitySector })}
//             value={this.state.activitySector}
//           />
//         </ScrollView>
//         <BottomButton
//           title="CHOISIR VOTRE SECTEUR"
//           loading={this.state.loading}
//           onPress={this.handleSubscribeButton}
//           disabled={this.state.activitySector === null}
//         />
//       </View>
//     );
//   }
// }

// const mapStateToProps = state => ({
//   progression: state.candidateForm.progression,
// });

// const VIEWER_CANDIDATE = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         activity
//       }
//     }
//   }
// `;

// const EDIT_INFOS = gql`
//   mutation (
//     $user_id: String!,
//     $activity: String,
//   )  {
//     editCandidate(
//       user_id: $user_id,
//       activity: $activity,
//     ) {
//       candidate {
//         user_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const ContainerWithData = compose(
//   graphql(VIEWER_CANDIDATE),
//   graphql(EDIT_INFOS, { name: 'editInfos' }),
// )(CandidateFormActivitySector);

// export default connect(mapStateToProps)(connectAlert(ContainerWithData));

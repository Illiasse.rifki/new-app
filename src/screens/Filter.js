import React, { Component } from 'react';
import { View, Text } from 'react-native';

class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      tags: [],
      type: '',
      exp: '',
    };
  }

  _handleOnChangeSearch = search => this.setState({ search });
  _handleOnPressTags = () => {};
  _handleOnPressType = () => {};
  _handleOnPressExp = () => {};
  _handleOnPressConfirm = () => {};
  _handleOnPressCancel = () => {};

  render() {
    return (
      <View>
        <Text>Filter search of jobs</Text>
      </View>
    );
  }
}

export default Filter;

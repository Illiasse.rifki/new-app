import styled from 'styled-components';
import { theme } from '../../config/constants';

const Container = styled.View`
  backgroundColor: ${theme.primaryColor};
  flex: 1;
`;

export default Container;
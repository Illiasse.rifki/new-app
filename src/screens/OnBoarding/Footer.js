import React from 'react';
import { StyleSheet, Platform } from 'react-native';
import styled from 'styled-components';
import RaisedButton from '../../components/Button/RaisedButton';
import { theme } from '../../config/constants';

const FooterContainer = styled.View`
  flex: 2;
  padding: 0 2px;
  align-items: center;
  background-color: ${theme.secondaryColor};
  flex-direction: row;
  justify-content: space-around;
`;

const styles = StyleSheet.create({
  buttonView: {
    flex: 1,
  },
});

type Props = {
  navigation: Function,
};

const Footer = (props: Props) => (
  <FooterContainer>
    <RaisedButton
      title="Connexion"
      containerStyle={styles.buttonView}
      onPress={() => props.navigate('Login')}
      alt
    />
    <RaisedButton
      title="S'inscrire"
      containerStyle={styles.buttonView}
      onPress={() => props.navigate(Platform.OS === 'ios' ? 'CandidateForm' : 'SignUpType')}
    />
  </FooterContainer>
);

export default Footer;

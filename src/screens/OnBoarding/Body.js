import React from 'react';
import styled from 'styled-components';

import OnBoarding from './index';
import TextSwiper from '../../components/Swiper/TextSwiper';
import { theme } from '../../config/constants';

const BodyContainer = styled.View`
  backgroundColor: ${theme.primaryColor};
  flex: 2.75;
`;

const Body = () => (
  <BodyContainer>
    <TextSwiper
      texts={OnBoarding.texts}
    />
  </BodyContainer>
);

export default Body;
import React from 'react';
import { Image } from 'react-native';
import styled from 'styled-components';

import iphoneImg from '../../assets/iphone/i-phone-clay.png';

const MarketingImageContainer = styled.View`
  flex: 5.25;
  align-items: center;
  justify-content: flex-start;
`;

const Iphone = styled(Image)`
  flex: 1;
  width: 80%;
`;

const MarketingImage = () => (
  <MarketingImageContainer>
    <Iphone source={iphoneImg} />
  </MarketingImageContainer>
);

export default MarketingImage;
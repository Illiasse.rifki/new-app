import React, { Component } from 'react';
import { StatusBar } from 'react-native';

// blocks
import Container from './Container';
import Footer from './Footer';
import Header from './Header';
import Body from './Body';
import MarketingImage from './MarketingImage';

class OnBoarding extends Component {
  static navigationOptions = {
    header: null,
  };

  static texts = [
    "Laissez tomber les CV et lettres de motivation, les recruteurs ont accès à votre profil directement depuis leur smartphone.",
    "En prenant simplement quelques minutes pour remplir votre profil, vous aurez accès à la liste des offres correspondantes!"
  ];

  constructor(props) {
    super(props);
    StatusBar.setBarStyle('light-content', true);
  }

  props: {
    navigation: { navigate: {} },
  };

  render() {
    const { navigate } = this.props.navigation;

    return (
      <Container>
        <Header />
        <Body />
        <MarketingImage />
        <Footer navigate={navigate} />
      </Container>
    );
  }
}

export default OnBoarding;

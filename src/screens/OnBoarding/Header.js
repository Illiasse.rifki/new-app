import React from 'react';
import { StyleSheet } from 'react-native';
import styled from 'styled-components';
import Logo from '../../components/Logo/Logo';
import { theme } from '../../config/constants';

const HeaderContainer = styled.View`
  align-items: center;
  background-color: ${theme.primaryColor};
  justify-content: center;
  flex: 2.75;
`;

const styles = StyleSheet.create({
  logoWidth: {
    width: '55%',
  },
});

const Header = () => (
  <HeaderContainer>
    <Logo
      style={styles.logoWidth}
      resizeMode='contain'
    />
  </HeaderContainer>
);


export default Header;
// import React, { Component } from 'react';
// import { View, Text, StyleSheet, WebView } from 'react-native';
// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';

// import SubscriptionButton from '../../components/Events/RegistrationButton/index';
// import CompaniesLogos from '../../components/Company/CompaniesLogos';
// import PartnersLogos from '../../components/Partner/PartnersLogos';
// import CardEditable from '../../components/Card/CardEditable';
// import { theme } from '../../config/constants/index';

// /**
//  * @see https://medium.com/@fxwio/set-webview-height-to-the-height-of-html-content-react-native-2d696128c181
//  */
// const webViewAutoHeight = {
//   rawScript: `<script>
//   window.location.hash = 1;
//   var calculator = document.createElement("div");
//   calculator.id = "height-calculator";
//   while (document.body.firstChild){
//     calculator.appendChild(document.body.firstChild);
//   }
//   document.body.appendChild(calculator);
//   document.title = calculator.clientHeight;
// </script>`,
//   rawStyle: `<style>
//   body,html,#height-calculator {
//     margin: 0;
//     padding: 0
//   }
//   #height-calculator {
//     position: absolute;
//     top: 0;
//     left: 0;
//     right: 0;
//   }
// </style>`,
// };

// const styles = StyleSheet.create({
//   screenBackground: {
//     backgroundColor: '#f5f5f5',
//   },
//   typeText: {
//     marginTop: 8,
//     fontSize: 14,
//     color: theme.fontBlack,
//     textAlign: 'left',
//     lineHeight: 24,
//     alignSelf: 'flex-start',
//   },
//   title: {
//     marginLeft: 0,
//     marginBottom: 10,
//     paddingTop: 0,
//     marginTop: 0,
//   },
//   eventContainer: {
//     paddingLeft: 32,
//     paddingRight: 32,
//     paddingBottom: 10,
//     paddingTop: 10,
//   },
//   lastEventContainer: {
//     paddingLeft: 32,
//     paddingRight: 32,
//     paddingBottom: 21,
//     marginBottom: 0,
//     backgroundColor: 'white',
//   },
//   typeTextDedicated: {
//     fontWeight: '600',
//   },
// });

// function legacy__IsPlainTextDescription(element: string): boolean {
//   const regexHTMLLike = new RegExp(/^(<.*?>).*(<\/.*>)$/gm);
//   return !regexHTMLLike.test(element);
// }

// class EventAbout extends React.Component {
//   static navigationOptions = ({ screenProps }) => ({
//     tabBarOnPress: () => {
//       screenProps.updateIndex('About');
//     },
//   });

//   state : {
//     webViewHeight: Number
//   } = {
//     webViewHeight: 0,
//   }

//   componentWillReceiveProps(nextProps) {
//     if (nextProps.screenProps.refresh && this.props.data) {
//       this.props.data.refetch().catch(e => console.log('catch refetch Events.js', e));
//     }
//   }

//   handleScroll = (e) => {
//     const { y } = e.nativeEvent.contentOffset;
//     if (y < 10) this.props.screenProps.updateMinified(false);
//   };

//   props: {
//     screenProps: {
//       updateMinified: Function,
//       minified: boolean,
//       navigation: {
//         navigate: Function,
//       },
//       isRecruiter: Boolean,
//       refresh: boolean,
//       past: boolean,
//     },
//     data: {
//       refetch: Function,
//       event: {
//         description: string,
//       },
//     },
//   };

//   button = (hide) => {
//     const { event } = this.props.data;
//     const {
//       isRecruiter,
//       past,
//     } = this.props.screenProps;
//     return (
//       <SubscriptionButton
//         navigate={this.props.screenProps.navigation.navigate}
//         event_id={event.event_id}
//         isRecruiter={isRecruiter}
//         hideIfStatus={hide}
//         refresh={this.props.screenProps.refresh}
//         isPast={past}
//       />
//     );
//   }

//   onNavigationStateChange = (event) => {
//     if (event && event.title) {
//       const htmlHeight = Number(event.title);
//       this.setState({ webViewHeight: htmlHeight });
//     }
//   }

//   webViewHandler = (rawHtml: String): React.Node => {
//     const { rawScript, rawStyle } = webViewAutoHeight;
//     return <WebView style={{ height: this.state.webViewHeight }}
//       javaScriptEnabled ={true}
//       scrollEnabled={false}
//       onNavigationStateChange={this.onNavigationStateChange}
//       originWhitelist={['*']}
//       source={{ html: rawStyle + rawHtml + rawScript }}
//     />;
//   }

//   renderDescription = () => {
//     const { event } = this.props.data;
//     if (!event) {
//       return null;
//     } else if (legacy__IsPlainTextDescription(event.description)) {
//       return <Text style={styles.typeText}>{event.description}</Text>;
//     }
//     return this.webViewHandler(event.description);
//   }

//   render = () => {
//     const { event } = this.props.data;
//     if (!event) {
//       return null;
//     }
//     const {
//       minified,
//       navigation: { navigate },
//     } = this.props.screenProps;
//     const v = (
//       <View>
//         {this.button()}
//         {
//           event.type === 'dedicated' &&
//           <CardEditable
//             containerStyle={styles.eventContainer}
//             titleStyle={styles.title}
//           >
//             <Text style={styles.typeTextDedicated}>{event.detail}</Text>
//           </CardEditable>
//         }
//         <CardEditable
//           containerStyle={styles.eventContainer}
//           titleStyle={styles.title}
//           title="A propos"
//         >
//           {this.renderDescription()}
//         </CardEditable>
//         {
//           event.type === 'mutual' ? (
//             <CardEditable
//               containerStyle={styles.eventContainer}
//               titleStyle={styles.title}
//               title="Les entreprises"
//             >
//               <CompaniesLogos
//                 eventType={event.type}
//                 screenProps={this.props.screenProps}
//                 companiesIds={event.companies.map(c => c.company_id)}
//                 onPress={(company) => {
//                   navigate('CompanyProfile', { companyId: company.company_id });
//                 }}
//               />
//             </CardEditable>
//           ) : null
//         }
//         {
//           event.partners.length > 0 ? (
//             <CardEditable
//               containerStyle={styles.lastEventContainer}
//               titleStyle={styles.title}
//               title="Les partenaires"
//             >
//               <PartnersLogos partnersIds={event.partners.map(c => c.partner_id)} />
//             </CardEditable>
//           ) : null
//         }
//         {this.button(true)}
//       </View >
//     );

//     if (minified) {
//       return (
//         <View style={styles.screenBackground}>
//           <View onMomentumScrollEnd={this.handleScroll} onScrollEndDrag={this.handleScroll}>
//             {v}
//           </View>
//         </View>
//       );
//     }
//     return <View style={styles.screenBackground}>{v}</View>;
//   };
// }

// const EVENT_BY_ID_QUERY = gql`
//   query event_by_id($event_id: String!) {
//     event: event_by_id(event_id: $event_id) {
//       type
//       event_id
//       detail
//       description
//       companies {
//         company_id
//       }
//       partners {
//         partner_id
//       }
//     }
//   }
// `;

// export default graphql(EVENT_BY_ID_QUERY, {
//   options: props => ({
//     fetchPolicy: 'network-only',
//     variables: {
//       event_id: props.screenProps.event_id,
//     },
//   }),
// })(EventAbout);

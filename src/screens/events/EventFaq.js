import React, { Component } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import SubscriptionButton from '../../components/Events/RegistrationButton/index';
import FAQ from '../../components/Content/FAQ';

const styles = StyleSheet.create({
  screenBackground: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  title: {
    fontSize: 28,
    fontWeight: '500',
    color: 'black',
    textAlign: 'left',
    alignSelf: 'flex-start',
  },
});

class EventFaq extends Component {
  static navigationOptions = ({ screenProps }) => ({
    tabBarOnPress: () => {
      screenProps.updateIndex('Faq');
    },
  });

  handleScroll = (e) => {
    const { y } = e.nativeEvent.contentOffset;
    if (y < 10) this.props.screenProps.updateMinified(false);
  };

  props: {
    screenProps: {
      updateMinified: Function,
      minified: boolean,
      event_id: string,
      navigation: {
        navigate: Function,
      },
      isRecruiter: Boolean,
      refresh: boolean,
    },
  };

  static FAQ_candidate = [
    {
      question: 'Quelle est la différence par rapport à un salon classique ?',
      answer: 'Les Connect permettent d’organiser des mises en relation entre candidats et recruteurs dans un cadre détendu et prestigieux. Il n’y a pas de « stands » comme dans un salon classique, les recruteurs se baladent sur le lieu de l’événement librement. Cela permet d’instaurer un climat serein lors des échanges et donc d’obtenir des opportunités d’une manière un peu plus informelle, mais bien plus efficace.',
    },
    {
      question: 'Est-il possible de contacter les entreprises si je suis sélectionné ?',
      answer: 'Si vous êtes sélectionné et que votre profil intéresse l’entreprise, c’est cette dernière qui vous contactera directement dans un délai très court. Elle aura la possibilité de vous contacter via le Chat de notre application avant ou après l’événement.',
    },
    {
      question: 'Quels sont les postes de ceux que vous appelez les « recruteurs » ?',
      answer: 'Les personnes représentant les entreprises avec qui nous travaillons vont varier selon les événements. De ce fait, nous avons l’occasion d’accueillir sur nos événements des Directeurs Régionaux, des Directeurs Commerciaux, des Responsables des Ventes, ou encore des Responsables Ressources Humaines.',
    },
    {
      question: 'Nous devons déposer nos CV sur votre Plateforme pour pouvoir postuler aux événements, quelle en est l’utilité ?',
      answer: 'Une fois vos CV déposés sur notre plateforme, notre équipe de Sourcing va s’occuper de traiter les différentes candidatures en se penchant sur les CV de chacun. De ce fait, nous assurons à la fois une présélection de qualité aux entreprises, et une invitation pertinente pour les candidats. Nous nous assurons que les candidats sélectionnés soient intéressants pour les entreprises.',
    },
    {
      question: 'Les postes proposés par les entreprises sont-ils disponibles sur toute la France ?',
      answer: 'Nous organisons des événements dans différentes régions dans le but de pouvoir satisfaire nos clients (entreprises ou candidats) partout en France. De ce fait, les postes qui pourront éventuellement être proposés par les entreprises lors de nos événements vont principalement se situer dans la région dans laquelle l’événement est organisé.',
    },
    {
      question: 'Comment repérer les recruteurs sur l’événement ?',
      answer: 'Chaque recruteur aura un badge sur lequel sera stipulé son entreprise dessus. De plus, chaque entreprise a un espace dédié sur lequel ils étalent un Kakémono (une pancarte) ainsi que des petits manuels décrivant leur entreprise. Cela ne les empêche cependant pas de se déplacer partout sur le lieu de l’événement qui est suffisamment grand pour une circulation libre.',
    },
    {
      question: 'Je n’ai toujours pas reçu de réponse alors que l’événement approche. Est-ce normal ?',
      answer: 'Il arrive que notre équipe de Sourcing ait beaucoup de candidatures à traiter en peu de temps. Vous recevrez une réponse, qu’elle soit positive ou négative, dernier délai la veille de l’événement.',
    },
    {
      question: 'Je me suis inscris et je n’ai pas l’adresse de l’événement. Est-ce normal ?',
      answer: 'Nous communiquons l’adresse de l’événement aux candidats ayant été sélectionnés via un mail informatif d’invitation. Ce mail est généralement envoyé dans les jours qui suivent votre inscription, mais peut prendre plus de temps à être transmis en cas de forte afluence des candidatures. N’hésitez pas à nous contacter par téléphone ou par mail si vous ne l’avez pas reçu la veille de l’événement.',
    },
    {
      question: 'J’ai téléchargé le mauvais CV lors de mon inscription et je voudrais le retirer.',
      answer: 'Il vous est possible de modifier votre profil directement depuis l’application. Il vous suffira donc de supprimer le mauvais fichier téléchargé et de charger le nouveau.',
    },
    {
      question: 'D’autres questions ?',
      answer: 'N’hésitez pas à nous contacter directement par mail ou par téléphone !',
    }
  ];

  static FAQ_recruiter = [
    {
      question: 'Comment se distinguent les candidats durant les événements ?',
      answer: 'Les candidats sont différenciés par des badges de couleurs : Bleu pour les candidats ayant un bac 2 ou 3, rouge pour les candidats ayant un bac + 4 ou 5. Cela permet de savoir vers quels types de postes vont s’orienter ces candidats en fonction de leur niveau d’études et de leurs aptitudes.',
    },
    {
      question: 'Comment prendre contact avec un candidat rencontré lors de l’événement ?',
      answer: 'Notre application a un onglet « chat » vous permettant d’échanger avec les candidats ayant participé à l’évènement. Il vous suffira donc de cliquer sur le nom du candidat et d’engager facilement la conversation avec ce dernier.',
    },
    {
      question: 'Comment se déroule l’événement ?',
      answer: 'Dans un premier temps, les recruteurs des entreprises sont conviés 1h avant les candidats pour leur laisser le temps de s’installer et d’exposer leurs kakémonos. Ensuite, les candidats ayant une session VIP arriveront 15 à 30 min avant les autres candidats, dans le but d’avoir un instant privilégié avec les recruteurs. Le reste des candidats arriveront aux alentours de 18h30-45 pout échanger avec les recruteurs.',
    },
    {
      question: 'Si un candidat ne s’est pas rendu à l’événement, est-il possible de le contacter ?',
      answer: 'Notre onglet de chat permet de communiquer avec l’intégralité des candidats ayant été sélectionnés pour cet événement. De ce fait, si un candidat ne s’est pas rendu sur l’événement mais y était convié, il vous sera possible de le contacter directement par chat.',
    },
    {
      question: 'Un candidat avec qui j’ai échangé avait un profil intéressant mais je ne connais pas ses expériences passées et il est maintenant introuvable, comment faire ?',
      answer: 'Vous pouvez avoir accès aux profils des candidats depuis votre smartphone ou tablette directement depuis l’application Bizzeo pendant la soirée où il vous sera possible de consulter des CV ou des profils. Notre application vous sert en quelque sorte de vivier de candidats sur lequel vous pouvez voguer avant, pendant, ou après la soirée, sans aucune limite.',
    },
    {
      question: 'Je souhaite garder contact avec plusieurs profils rencontrés ce soir, comment faire ?',
      answer: 'Il vous est possible de sélectionner les profils intéressants que vous avez rencontré durant la soirée en quelques clics et d’ensuite retourner sur ces profils ultérieurement pour les recontacter.',
    },
    {
      question: 'Qui contacter en cas de problème sur l’application ?',
      answer: 'Notre service client est disponible toute la semaine pour répondre à vos questions et régler vos problèmes. N’hésitez pas à nous contacter via l’Assistance dans l’onglet « Chat » de l’application.',
    }
  ];

  render = () => {
    const { minified, isRecruiter, past } = this.props.screenProps;

    const button = hide => (
      <SubscriptionButton
        navigate={this.props.screenProps.navigation.navigate}
        event_id={this.props.screenProps.event_id}
        isRecruiter={isRecruiter}
        hideIfStatus={hide}
        refresh={this.props.screenProps.refresh}
        isPast={past}
      />
    );
    const v = (
      <View>
        {button()}
        <FAQ
          title="FAQ"
          faq={(isRecruiter) ? EventFaq.FAQ_recruiter : EventFaq.FAQ_candidate}
        />
        {button(true)}
      </View>
    );

    if (minified) {
      return (
        <View style={styles.screenBackground}>
          <ScrollView onMomentumScrollEnd={this.handleScroll} onScrollEndDrag={this.handleScroll}>
            {v}
          </ScrollView>
        </View>
      );
    }
    return <ScrollView style={styles.screenBackground}>{v}</ScrollView>;
  };
}

export default EventFaq;

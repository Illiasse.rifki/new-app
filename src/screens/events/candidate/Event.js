// import React, { Component } from 'react';
// import { View, StyleSheet, ScrollView, RefreshControl } from 'react-native';
// import gql from 'graphql-tag';
// import moment from 'moment';
// import { graphql } from 'react-apollo';
// import EventHeader from '../../../components/Events/EventHeader';
// import ErrorComponent from '../../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../../components/Elements/LoadingComponent';
// import { createEventTabs } from '../../../config/NavigatorStacks/Candidate/EventsStack';

// const styles = StyleSheet.create({
//   screenBackground: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//   },
//   typeText: {
//     fontSize: 14,
//     color: '#1c1c1c',
//     textAlign: 'left',
//     marginBottom: 15,
//     marginRight: 16,
//     marginLeft: 16,
//   },
//   title: {
//     fontSize: 18,
//     color: '#1c1c1c',
//     fontWeight: '500',
//     marginBottom: 7,
//     marginTop: 25,
//     textAlign: 'center',
//   },
//   contentContainer: {
//     paddingBottom: 0,
//   },
// });

// class Event extends Component {
//   static navigationOptions = ({ navigation }) => ({
//     title: navigation.state.params.name,
//     headerTruncatedBackTitle: 'Events',
//   });

//   state = {
//     minified: false,
//     index: '',
//     refreshing: false,
//   };

//   updateMinified = (minified) => {
//     this.setState({ minified });
//   };

//   updateIndex = (index) => {
//     this.setState({ index });
//   };

//   handleScrollEnd = (e) => {
//     const { y } = e.nativeEvent.contentOffset;
//     // if (y > 200) this.setState({ minified: true });
//   };

//   componentDidMount() {
//     const { eventById } = this.props;
//     if (eventById) eventById.refetch().catch(e => console.warn('Catch refetch Candidate Event', e));
//   }

//   onRefresh = () => {
//     this.setState({ refreshing: true });
//     this.props.eventById.refetch().then(() => {
//       this.setState({ refreshing: false });
//     }).catch(e => console.log('catch refetch Events.js', e));
//   };

//   shouldComponentUpdate(nextProps, nextState) {
//     const { route = this.props.navigation.state.routeName } = nextProps.screenProps;
//     const next = JSON.stringify({ props: nextProps, state: nextState });
//     const now = JSON.stringify({ props: this.props, state: this.state });
//     return next !== now;
//   }

//   props: {
//     navigation: { navigate: {}, state: { routeName: string, params: { event: {} } } },
//     eventById: {
//       error: boolean,
//       loading: boolean,
//       refetch: Function,
//       event_by_id: {
//         event_id: string,
//         name: string,
//         picture: string,
//         description: string,
//         passed: Boolean
//       },
//     },
//   };

//   render() {
//     const { error, loading } = this.props.eventById;
//     if (error) {
//       return <ErrorComponent message="Problème lors de la récupération la page des events." />;
//     } else if (loading) {
//       return <LoadingComponent />;
//     }

//     const event = this.props.eventById.event_by_id;
//     if (!event) {
//       return null;
//     }
//     const EventTabs = createEventTabs(this.state.index);

//     const tabs = (
//       <EventTabs
//         screenProps={{
//           ...this.props,
//           isRecruiter: false,
//           event_id: event.event_id,
//           past: moment().endOf('date').isAfter(event.date, 'date'),
//           minified: this.state.minified,
//           updateMinified: this.updateMinified,
//           updateIndex: this.updateIndex,
//           refresh: this.state.refreshing,
//         }}
//       />
//     );

//     if (this.state.minified) {
//       return <View style={styles.screenBackground}>{tabs}</View>;
//     }
//     return (
//       <ScrollView
//         style={{ backgroundColor: '#f5f5f5' }}
//         showsVerticalScrollIndicator={false}
//         showsHorizontalScrollIndicator={false}
//         keyboardShouldPersistTaps="always"
//         refreshControl={<RefreshControl
//           refreshing={this.state.refreshing}
//           onRefresh={this.onRefresh}
//         />}
//       >
//         <EventHeader
//           navigate={this.props.navigation.navigate}
//           event={event} />
//         {tabs}
//       </ScrollView>
//     );
//   }
// }

// const EVENT_BY_ID_QUERY = gql`
//   query event_by_id($event_id: String!) {
//     event_by_id(event_id: $event_id) {
//       type
//       event_id
//       name
//       detail
//       date
//       date_format
//       description
//       passed
//       picture
//       companies {
//         company_id
//         logo
//         __typename
//     }    }
//   }
// `;

// export default graphql(EVENT_BY_ID_QUERY, {
//   name: 'eventById',
//   options:
//     ownProps => ({
//       variables: {
//         event_id: ownProps.navigation.state.params.event_id,
//       },
//     }),
// })(Event);

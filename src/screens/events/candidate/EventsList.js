import React, { Component } from 'react';
import { StyleSheet, ScrollView, RefreshControl } from 'react-native';
import { NextEventsRegistration, PastEventsRegistration, CurrentEventsRegistration } from '../../../components/GQLLinked/EventRegistration';
import { NextEvents, PastEvents } from '../../../components/GQLLinked/Events';

const styles = StyleSheet.create({
  screenBackground: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  screenContainer: {
    paddingVertical: 24,
    paddingHorizontal: 16,
  },
  eventsRegisterContainer: {
    marginBottom: 50,
  },
  eventsContainer: {
    marginBottom: 30,
  },
});

class EventsList extends Component {
  state = {
    moreInscriptions: false,
    refreshing: false,
  };

  onRefresh = () => {
    this.setState({ refreshing: true });
    setTimeout(() => this.setState({ refreshing: false }), 500);
  };

  props: {
    navigation: { navigate: {}, state: { routeName: string } },
    screenProps: { route: string },
  };

  shouldComponentUpdate(nextProps, nextState) {
    const { route = this.props.navigation.state.routeName } = nextProps.screenProps;
    if (route !== this.props.navigation.state.routeName) return false;
    const next = JSON.stringify({ props: nextProps, state: nextState });
    const now = JSON.stringify({ props: this.props, state: this.state });
    return next !== now;
  }

  render() {
    const { route } = this.props.screenProps;
    const { routeName } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    const next = routeName === 'EventsFutur';
    return (
      <ScrollView
        style={styles.screenBackground}
        contentContainerStyle={styles.screenContainer}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        refreshControl={<RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this.onRefresh}
        />}
      >

        {
          <CurrentEventsRegistration
            navigate={navigate}
            containerStyle={styles.eventsRegisterContainer}
            title="Événement en cours"
            more={this.state.moreInscriptions}
            route={route}
            routeName={routeName}
            refresh={this.state.refreshing}
          />
        }
        {next ? <NextEventsRegistration
          navigate={navigate}
          containerStyle={styles.eventsRegisterContainer}
          title="Vos inscriptions"
          more={this.state.moreInscriptions}
          route={route}
          routeName={routeName}
          refresh={this.state.refreshing}
        /> : <PastEventsRegistration
          navigate={navigate}
          containerStyle={styles.eventsRegisterContainer}
          title="Candidatures passées"
          more={this.state.moreInscriptions}
          route={route}
          routeName={routeName}
          refresh={this.state.refreshing}
        />}
        {/* {next ? <NextEvents
          containerStyle={styles.eventsContainer}
          navigate={navigate}
          next
          title={'Événements à venir'}
          route={route}
          routeName={routeName}
          refresh={this.state.refreshing}
        /> : <PastEvents
          containerStyle={styles.eventsContainer}
          navigate={navigate}
          title={'Événements passés'}
          route={route}
          routeName={routeName}
          refresh={this.state.refreshing}
        />} */}
        <NextEvents
          containerStyle={styles.eventsContainer}
          navigate={navigate}
          next
          title={'Événements à venir'}
          route={route}
          routeName={routeName}
          refresh={this.state.refreshing}
        />
        <PastEvents
          containerStyle={styles.eventsContainer}
          navigate={navigate}
          title={'Événements passés'}
          route={route}
          routeName={routeName}
          refresh={this.state.refreshing}
        />
      </ScrollView>
    );
  }
}

export default EventsList;

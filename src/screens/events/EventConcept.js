import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { Icon } from 'react-native-elements';

import SubscriptionButton from '../../components/Events/RegistrationButton/index';
import CardEditable from '../../components/Card/CardEditable';
import { theme } from '../../config/constants/index';

const bizzeoConnect =
  "Bizzeo Connect est un concept innovant de recrutement sur les fonctions commerciales partout en France. Candidats et recruteurs sélectionnés se retrouvent dans un lieu prestigieux, autour d'un cocktail & champagne, afin de networker. Pas de CV, pas de lettre de motivation, une application smartphone sera à votre disposition pour faciliter les échanges !";

const bizzeoVideoId = 'LIOiYAIplmQ';

const styles = StyleSheet.create({
  screenBackground: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  typeText: {
    marginTop: 8,
    fontSize: 14,
    color: '#1c1c1c',
    textAlign: 'left',
    lineHeight: 24,
    alignSelf: 'flex-start',
  },
  title: {
    marginLeft: 0,
    marginRight: 0,
  },
  eventContainer: {
    paddingLeft: 32,
    paddingRight: 32,
  },
  lastEventContainer: {
    paddingLeft: 32,
    paddingRight: 32,
    paddingBottom: 21,
    marginBottom: 0,
    backgroundColor: 'white',
  },
  conceptsContainer: {
    marginTop: 8,
  },
  conceptContainer: {
    flexDirection: 'row',
    marginBottom: 16,
    marginRight: 16,
    alignItems: 'flex-start',
  },
  conceptText: {
    color: theme.fontBlack,
    fontSize: 14,
    lineHeight: 24,
  },
  conceptIcon: { marginRight: 8, marginTop: 4 },
  video: { marginTop: 16, height: 200, alignSelf: 'stretch' },
});

export default class EventConcept extends Component {
  static navigationOptions = ({ screenProps }) => ({
    tabBarOnPress: ({ jumpToIndex }) => {
      screenProps.updateIndex('Concept');
      //      console.warn(Object.entries(scene));
      //      jumpToIndex(1);
    },
  });

  static concepts = [
    'Nous sélectionnons les profils correspondant aux besoins des recruteurs et en rapport avec le thème de la soirée.',
    "Les candidats sélectionnés et les recruteurs se rencontrent autour d'un cocktail.",
    'Pas de CV, les recruteurs ont accès aux profils des candidats directement depuis leur smartphone.',
    'Les entreprises recrutent leur futurs talents suite à la soirée !',
  ];

  constructor(props) {
    super(props);
    this.player = null;
    this.state = {};
  }

  onVideoError = (err) => {
    console.error(err);
  }

  handleScroll = (e) => {
    const { y } = e.nativeEvent.contentOffset;
    if (y < 10) this.props.screenProps.updateMinified(false);
  };

  props: {
    screenProps: {
      updateMinified: Function,
      minified: boolean,
      event_id: string,
      navigation: {
        navigate: Function,
      },
      isRecruiter: Boolean,
      refresh: boolean,
    },
  };

  render = () => {
    const { minified, isRecruiter, past } = this.props.screenProps;
    const button = hide => (
      <SubscriptionButton
        navigate={this.props.screenProps.navigation.navigate}
        event_id={this.props.screenProps.event_id}
        isRecruiter={isRecruiter}
        hideIfStatus={hide}
        refresh={this.props.screenProps.refresh}
        isPast={past}
      />
    );
    const v = (
      <View>
        {button()}
        <CardEditable
          title="Bizzeo Connect"
          titleStyle={styles.title}
          containerStyle={styles.eventContainer}
        >
          <Text style={styles.typeText}>{bizzeoConnect}</Text>
          {/* <YouTube
            videoId={bizzeoVideoId}
            style={styles.video}/> */}
        </CardEditable>
        <CardEditable
          title="Concept"
          titleStyle={styles.title}
          containerStyle={styles.lastEventContainer}
        >
          <View style={styles.conceptsContainer}>
            {
              EventConcept.concepts.map(concept => (
                <View
                  key={concept}
                  style={styles.conceptContainer}
                >
                  <Icon
                    type="simple-line-icon"
                    color="#33cbcc"
                    size={16}
                    name="check"
                    iconStyle={styles.conceptIcon}
                  />
                  <Text style={styles.conceptText}>
                    {concept}
                  </Text>
                </View>
              ))
            }
          </View>
        </CardEditable>
        {button(true)}
      </View>
    );

    if (minified) {
      return (
        <View style={styles.screenBackground}>
          <ScrollView onMomentumScrollEnd={this.handleScroll} onScrollEndDrag={this.handleScroll}>
            {v}
          </ScrollView>
        </View>
      );
    }
    return <ScrollView style={styles.screenBackground}>{v}</ScrollView>;
  };
}

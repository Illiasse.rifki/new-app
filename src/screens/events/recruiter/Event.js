// import React, { Component } from 'react';
// import { View, StyleSheet, ScrollView, RefreshControl } from 'react-native';
// import gql from 'graphql-tag';
// import moment from 'moment';
// import { graphql } from 'react-apollo';

// import { createEventTabs } from '../../../config/NavigatorStacks/Recruiter/EventsStack';
// import EventHeader from '../../../components/Events/EventHeader';

// const styles = StyleSheet.create({
//   screenBackground: {
//     backgroundColor: '#f5f5f5',
//   },
// });

// class Event extends Component {
//   static navigationOptions = ({ navigation }) => ({
//     title: navigation.state.params.name,
//     headerTruncatedBackTitle: 'Events',
//   });

//   state = {
//     minified: false,
//     index: 'About',
//     refreshing: false,
//   };

//   updateMinified = (minified) => {
//     this.setState({ minified });
//   };

//   updateIndex = (index) => {
//     this.setState({ index });
//   };

//   handleScrollEnd = (e) => {
//     const { y } = e.nativeEvent.contentOffset;
//     // if (y > 200) this.setState({ minified: true });
//   };

//   // componentDidMount() {
//   //   const { eventById } = this.props;
//   //   if (eventById) eventById.refetch().catch(e => console.warn('Catch refetch Recruiter Event', e));
//   // }

//   props: {
//     navigation: { navigate: {}, state: { routeName: string, params: { event: {} } } },
//     event: {
//       refetch: Function,
//       event: {
//         event_id: string,
//         name: string,
//         picture: string,
//         description: string,
//       },
//     },
//   };

//   onRefresh = () => {
//     this.setState({ refreshing: true });
//     this.props.event.refetch().then(() => {
//       this.setState({ refreshing: false });
//     }).catch(e => console.log('catch refetch Events.js', e));
//   };

//   shouldComponentUpdate(nextProps, nextState) {
//     const { route = this.props.navigation.state.routeName } = nextProps.screenProps;
//     const next = JSON.stringify({ props: nextProps, state: nextState });
//     const now = JSON.stringify({ props: this.props, state: this.state });
//     return next !== now;
//   }

//   render() {
//     const { event } = this.props.event;
//     if (!event) {
//       return null;
//     }
//     const EventTabs = createEventTabs(this.state.index);
//     const tabs = (
//       <EventTabs
//         screenProps={{
//           ...this.props,
//           isRecruiter: true,
//           event_id: event.event_id,
//           past: moment().endOf('date').isAfter(event.date, 'date'),
//           minified: this.state.minified,
//           updateMinified: this.updateMinified,
//           updateIndex: this.updateIndex,
//           refresh: this.state.refreshing,
//         }}
//       />
//     );

//     if (this.state.minified) {
//       return <View style={styles.screenBackground}>{tabs}</View>;
//     }
//     return (
//       <ScrollView
//         style={styles.screenBackground}
//         showsVerticalScrollIndicator={false}
//         showsHorizontalScrollIndicator={false}
//         refreshControl={<RefreshControl
//           refreshing={this.state.refreshing}
//           onRefresh={this.onRefresh}
//         />}
//       >
//         <EventHeader
//           navigate={this.props.navigation.navigate}
//           event={event}
//           isRecruiter={true}
//         />
//         {tabs}
//       </ScrollView>
//     );
//   }
// }

// const EVENT_BY_ID_QUERY = gql`
//   query event_by_id($event_id: String!) {
//     event: event_by_id(event_id: $event_id) {
//       event_id
//       name
//       detail
//       date_format
//       description
//       passed
//       picture
//     }
//   }
// `;

// export default graphql(EVENT_BY_ID_QUERY, {
//   name: 'event',
//   options: props => ({
//     fetchPolicy: 'network-only',
//     variables: {
//       event_id: props.navigation.state.params.event_id,
//     },
//   }),
// })(Event);

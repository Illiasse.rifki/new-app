// import React, { Component } from 'react';
// import { View, Text, StyleSheet } from 'react-native';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import { FormInput } from 'react-native-elements';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';

// import Url from '../../../components/Content/Url';
// import { theme } from '../../../config/constants/index';
// import Button from '../../../components/Design/Button';
// import { EVENT_REGISTRATION_BY_EVENT_ID } from '../../../components/Events/RegistrationButton/Recruiter/';

// const EVENT_BY_ID_QUERY = gql`
//   query event_by_id($event_id: String!) {
//     event_by_id(event_id: $event_id) {
//       event_id
//       location
//       date_format
//     }
//   }
// `;

// const styles = StyleSheet.create({
//   screenBackground: {
//     flex: 1,
//     backgroundColor: '#ffffff',
//     paddingLeft: 34,
//     paddingRight: 34,
//     paddingTop: 30.5,
//     paddingBottom: 32,
//   },
//   title: {
//     alignSelf: 'center',
//     fontWeight: '500',
//     fontSize: 16,
//     lineHeight: 24,
//     color: theme.fontBlack,
//     textAlign: 'center',
//   },
//   subtitle: {
//     lineHeight: 24,
//     fontSize: 14,
//     fontWeight: '400',
//     color: theme.fontBlack,
//     alignSelf: 'center',
//     textAlign: 'center',
//   },
//   bigTitle: {
//     fontSize: 28,
//     fontWeight: 'bold',
//     color: theme.fontBlack,
//   },
//   header: {
//     flex: 1,
//   },
//   body: {
//     marginTop: 37,
//   },
//   input: {
//     width: '100%',
//     minHeight: 215,
//   },
//   inputContainer: {
//     borderColor: '#979797',
//     borderRadius: 4,
//     borderWidth: 1,
//     marginRight: 0,
//     marginLeft: 0,
//     marginBottom: 32,
//     paddingVertical: 5,
//     paddingHorizontal: 10,
//     marginTop: 9,
//     minHeight: 215,
//   },
//   button: {
//     width: 147,
//     marginBottom: 55,
//   },
//   url: {
//     marginTop: 10,
//     alignSelf: 'center',
//   },
//   urlText: {
//     fontSize: 18,
//   },
// });

// class EventRegistrationRecruiter extends Component {
//   static navigationOptions = {
//     title: 'Participation à l\'évènement',
//     headerTruncatedBackTitle: 'Events',
//   };

//   state = {
//     minified: false,
//     index: '',
//     message: '',
//   };

//   props: {
//     navigation: { navigate: {}, state: { routeName: string, params: { event: {} } } },
//     register: Function,
//     eventById: {
//       event_by_id: {
//         event_id: string,
//         name: string,
//         picture: string,
//         description: string,
//       },
//     },
//   };

//   handleButton = async () => {
//     const { goBack } = this.props.navigation;
//     const event = this.props.eventById.event_by_id;
//     try {
//       const res = await this.props.register({
//         refetchQueries: [
//           {
//             query: EVENT_REGISTRATION_BY_EVENT_ID,
//             variables: {
//               event_id: event.event_id,
//               is_recruiter: true,
//             },
//           },
//         ],
//         variables: {
//           event_id: event.event_id,
//           is_recruiter: true,
//         },
//       });
//       const { createEventRegistration } = res.data;
//       const { error } = createEventRegistration;
//       if (error) {
//         throw res.data.createEventRegistration.error.message;
//       } else {
//         goBack();
//       }
//     } catch (e) {
//       console.log(e);
//     }
//   };

//   render() {
//     const event = this.props.eventById.event_by_id;
//     if (!event) {
//       return null;
//     }

//     return (
//       <KeyboardAwareScrollView
//         style={styles.screenBackground}
//         showsVerticalScrollIndicator={false}
//         scrollEnabled={true}
//         ref={(ref) => { this.scrollView = ref; }}
//       >
//         <View style={styles.header}>
//           <Text style={styles.title}>
//             {`Vous souhaitez participer à notre événement${event.location ? ` à ${event.location}` : ''} du ${event.date_format} ?`}
//           </Text>
//           <Text style={styles.subtitle}>Nous vous invitons à contacter notre équipe :</Text>
//           <Url
//             icon={{
//               name: 'phone',
//               type: 'font-awesome',
//               size: 20,
//               color: theme.primaryColor,
//             }}
//             containerStyle={styles.url}
//             textStyle={styles.urlText}
//             name="(+33)01.85.08.35.38"
//             url="tel:0185083538"
//           />
//         </View>
//         <View style={styles.body}>
//           <Text style={styles.bigTitle}>Formulaire</Text>
//           <FormInput
//             value={this.state.message}
//             ref={(ref) => { this.messageInput = ref; }}
//             containerStyle={styles.inputContainer}
//             inputStyle={styles.input}
//             placeholder="Écrivez votre message ici"
//             returnKeyType={'done'}
//             onChange={(e, message) => this.setState({ message })}
//             multiline
//             blurOnSubmit
//             onContentSizeChange={(e) => {
//               if (this.messageInput.input.isFocused()) {
//                 const { height } = e.nativeEvent.contentSize;
//                 this.scrollView.scrollToPosition(0, height);
//               }
//             }}
//             autoGrow
//           />
//           <Button
//             buttonStyle={styles.button}
//             title="Envoyer"
//             onPress={() => this.handleButton()}
//           />
//         </View>
//       </KeyboardAwareScrollView>
//     );
//   }
// }

// const EVENT_REGISTER_MUTATION = gql`
//   mutation createEventRegistration($event_id: String!, $is_recruiter: Boolean) {
//     createEventRegistration(event_id: $event_id, is_recruiter: $is_recruiter) {
//       event_registration {
//         event_registration_id
//       }
//       error {
//         code
//         message
//       }
//     }
//   }
// `;

// export default compose(
//   graphql(EVENT_BY_ID_QUERY, {
//     name: 'eventById',
//     options:
//       props => ({
//         variables: {
//           event_id: props.navigation.state.params.event_id,
//         },
//       }),
//   }),
//   graphql(EVENT_REGISTER_MUTATION, { name: 'register' }),
// )(EventRegistrationRecruiter);

import React, { Component } from 'react';
import { StyleSheet, ScrollView, RefreshControl } from 'react-native';

import { NextEventsRegistrationRecruiter } from '../../../components/GQLLinked/EventRegistration';
import { NextEvents } from '../../../components/GQLLinked/Events';
import { PastEventsRegisteredRecruiter, CurrentEventsRegisteredRecruiter } from '../../../components/GQLLinked/EventRegistered';

const styles = StyleSheet.create({
  screenBackground: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  screenContainer: {
    paddingVertical: 24,
    paddingHorizontal: 16,
  },
  eventsRegisterContainer: {
    marginBottom: 27,
  },
  eventsContainer: {
    marginBottom: 6,
  },
});

class EventsList extends Component {
  state = {
    moreInscriptions: false,
    refreshing: false,
  };

  shouldComponentUpdate(nextProps) {
    const { route } = nextProps.screenProps;
    const { routeName } = this.props.navigation.state;
    return route === routeName;
  }

  onRefresh = () => {
    this.setState({ refreshing: true });
    setTimeout(() => this.setState({ refreshing: false }), 500);
  };

  props: {
    navigation: { navigate: {}, state: { routeName: string } },
    screenProps: { route: string },
  };

  render() {
    const { route } = this.props.screenProps;
    const { navigate } = this.props.navigation;
    const { routeName } = this.props.navigation.state;
    if (route && route !== routeName) return null;
    return (
      <ScrollView
        contentContainerStyle={styles.screenContainer}
        style={styles.screenBackground}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        refreshControl={<RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this.onRefresh}
        />}
      >
        {
          <CurrentEventsRegisteredRecruiter
            navigate={navigate}
            containerStyle={styles.eventsRegisterContainer}
            title="Événement en cours"
            isRecruiter
            more={this.state.moreInscriptions}
            route={route}
            routeName={routeName}
            refresh={this.state.refreshing}
          />
        }
        <NextEventsRegistrationRecruiter
          navigate={navigate}
          next
          isRecruiter
          title="Événements auxquels vous allez participez"
          containerStyle={styles.eventsRegisterContainer}
          more={this.state.moreInscriptions}
          refresh={this.state.refreshing}
        />
        <NextEvents
          containerStyle={styles.eventsContainer}
          isRecruiter
          navigate={navigate}
          next
          title={'Événements à venir'}
          refresh={this.state.refreshing}
        />
        <PastEventsRegisteredRecruiter
          navigate={navigate}
          isRecruiter
          title="Événements passés auxquels vous avez participé"
          containerStyle={styles.eventsRegisterContainer}
          more={this.state.moreInscriptions}
          refresh={this.state.refreshing}
        />
      </ScrollView>
    );
  }
}

export default EventsList;

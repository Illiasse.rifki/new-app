import React from 'react';
import styled from 'styled-components';
import { theme } from '../../config/constants';

const T1 = styled.Text`
  font-size: 28px;
  font-weight: bold;
  color: ${theme.fontBlack};
`;

export default T1;
// @flow
import React from 'react';
import { pure } from 'recompose';
import { StyleSheet, View } from 'react-native';
import { FormInput, Icon } from 'react-native-elements';
import { theme } from '../../config/constants/index';
import LoadingComponent from '../Elements/LoadingComponent';

type Props = {
  containerStyle: {},
  value: string,
  onChangeText: Function,
  placeholder: string,
  loading: boolean,
};

const styles = StyleSheet.create({
  searchInput: {
    fontSize: 14,
    color: theme.gray,
    fontWeight: '500',
    width: 'auto',
  },
  searchInputContainer: {
    flex: 1,
    marginRight: 10,
    marginLeft: 10,
    borderBottomColor: 'transparent',
  },
  searchInputView: {
    borderRadius: 2,
    display: 'flex',
    backgroundColor: 'white',
    borderBottomColor: 'transparent',
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { width: 1, height: 0.5 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    height: 40,
    flex: 1,
    width: '100%',
    paddingLeft: 8,
    flexDirection: 'row',
  },
  iconLoading: {
    flex: 0,
    minHeight: 0,
    paddingLeft: 8,
    paddingRight: 16,
  },
});

const SearchField = (props: Props) => (
  <View style={[styles.searchInputView, props.containerStyle]}>
    <Icon name='search' color={theme.gray}/>
    <FormInput
      placeholder={props.placeholder}
      inputStyle={styles.searchInput}
      containerStyle={styles.searchInputContainer}
      onChangeText={props.onChangeText}
      value={props.value}
      underlineColorAndroid='#979797'
    />
    {
      props.loading &&
      <LoadingComponent
        containerStyle={styles.iconLoading}
        transparent
        type="Pulse"
        size={18}
        color={theme.primaryColor}
      />
    }
  </View>
);

export default pure(SearchField);

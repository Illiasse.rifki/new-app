// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Button, FormLabel } from 'react-native-elements';
import { theme } from '../../config/constants/index';

type Props = {
  containerStyle: {},
  label: string,
  labelStyle: string,
  subtitle: any,
  title: string,
  icon: {},
  onPress: Function,
};

const styles = StyleSheet.create({
  text: {
    flex: 1,
    fontSize: 16,
  },
  labelContainer: {
    marginLeft: 0,
    marginRight: 0,
  },
  labelContent: {
    marginRight: 0,
    marginLeft: 0,
    fontSize: 18,
    color: theme.fontBlack,
    lineHeight: 24,
    fontWeight: '500',
  },
  lightLabelContent: {
    marginTop: 0,
    marginRight: 0,
    marginLeft: 0,
    color: '#afafaf',
    letterSpacing: 0.5,
    fontSize: 14,
    fontWeight: '500',
  },
  button: {
    width: '100%',
    borderWidth: 1,
    borderColor: theme.gray,
    borderRadius: 2,
    marginTop: 10.5,
    marginLeft: 0,
    marginRight: 0,
    paddingLeft: 12,
    justifyContent: 'flex-start',
  },
  buttonView: {
    marginLeft: 0,
    marginRight: 0,
    flex: 1,
  },
});

const SquareButtonField = (props: Props) => (
  <View style={[styles.fieldView, props.containerStyle]}>
    {
      props.label ? (
        <FormLabel
          labelStyle={props.labelStyle === 'light' ? styles.lightLabelContent : styles.labelContent}
          containerStyle={styles.labelContainer}
        >
          {props.label}
        </FormLabel>) : null
    }
    {
      props.subtitle ? (props.subtitle) : null
    }
    <Button
      iconRight={props.icon || {
        type: 'simple-line-icon',
        name: 'arrow-right',
        color: '#afafaf',
        size: 15,
      }}
      title={props.title}
      backgroundColor={theme.secondaryColor}
      color={theme.fontBlack}
      textStyle={styles.text}
      buttonStyle={styles.button}
      containerViewStyle={styles.buttonView}
      onPress={props.onPress}
    />
  </View>
);

export default SquareButtonField;

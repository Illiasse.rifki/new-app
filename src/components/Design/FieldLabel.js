// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Button, FormLabel, FormInput } from 'react-native-elements';
import { theme } from '../../config/constants/index';

type Props = {
  containerStyle: {},
  label: string,
  labelStyle: string,
};

const styles = StyleSheet.create({
  labelContainer: {
    marginLeft: 0,
    marginRight: 0,
  },
  labelContent: {
    marginRight: 0,
    marginLeft: 0,
    fontSize: 18,
    color: theme.fontBlack,
    lineHeight: 24,
    fontWeight: '500',
  },
  lightLabelContent: {
    marginTop: 0,
    marginRight: 0,
    marginLeft: 0,
    color: theme.gray,
    letterSpacing: 0.5,
    fontSize: 14,
    fontWeight: '500',
  },
});

const FieldLabel = (props: Props) => (
  <View style={props.containerStyle}>
    {
      props.label ? (
        <FormLabel
          labelStyle={props.labelStyle === 'dark' ? styles.labelContent : styles.lightLabelContent}
          containerStyle={styles.labelContainer}
        >
          {props.labelStyle === 'dark' ? props.label : props.label.toUpperCase()}
        </FormLabel>
      ) : null
    }
  </View>
);

export default FieldLabel;

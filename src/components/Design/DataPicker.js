// @flow
import React, { Component } from 'react';
import { View, StyleSheet, Picker } from 'react-native';
import { FormLabel } from 'react-native-elements';
import SquareButtonField from './SquareButtonField';
import { theme } from '../../config/constants/index';


const styles = StyleSheet.create({
  labelContainer: {
    marginLeft: 0,
    marginRight: 0,
  },
  labelContent: {
    marginRight: 0,
    marginLeft: 0,
    fontSize: 18,
    color: theme.fontBlack,
    lineHeight: 24,
    fontWeight: '500',
  },
  lightLabelContent: {
    marginTop: 0,
    marginRight: 0,
    marginLeft: 0,
    color: theme.gray,
    letterSpacing: 0.5,
    fontSize: 14,
    fontWeight: '500',
  },
  inputContainer: {
    marginTop: -5,
    marginRight: 0,
    marginLeft: 0,
  },
  inputText: {
    color: theme.fontBlack,
    width: '100%',
    fontSize: 16,
    marginBottom: -5,
  },
});

class DataPicker extends Component {
  props: {
    containerStyle: {},
    label: string,
    labelStyle: string,
    placeholder: string,
    subtitle: any,
    data: [],
    value: string,
    onConfirm: Function,
  };

  state = {
    value: null,
  }

  constructor(props) {
    super(props);
    this.state = {
      init: false,
      value: props.value || (props.data && props.data[0]),
    };
    this.pickerProps = {
      pickerData: props.data,
      selectedValue: [props.value],
      onPickerConfirm: props.onConfirm,
      pickerTextEllipsisLen: 35,
      pickerBg: [255, 255, 255, 1],
      pickerToolBarBg: [245, 245, 245, 1],
      pickerConfirmBtnColor: [51, 203, 204, 1],
      pickerConfirmBtnText: 'Confirmer',
      pickerCancelBtnColor: [51, 203, 204, 1],
      pickerCancelBtnText: 'Annuler',
      pickerTitleText: '',
    };
  }

  onSelectValue = (value) => {
    this.setState({ value });
    this.props.onConfirm(value);
  }

  render() {
    return (
      <View style={[styles.fieldView, this.props.containerStyle]}>
        {
          this.props.label ? (
            <FormLabel
              labelStyle={this.props.labelStyle === 'dark' ? styles.labelContent : styles.lightLabelContent}
              containerStyle={styles.labelContainer}
            >
              {this.props.label.toUpperCase()}
            </FormLabel>
          ) : null
        }
        {
          this.props.subtitle ? (this.props.subtitle) : null
        }
        {/* <SquareButtonField
          title={this.props.value || this.props.placeholder}
          onPress={() => {
            this.pickerProps.selectedValue = [this.props.value];
            Picker.init(this.pickerProps);
            Picker.toggle();
          }}
          icon={{
            type: 'simple-line-icon',
            name: 'arrow-down',
            color: '#afafaf',
            size: 15,
          }}
        /> */}
        <Picker
          selectedValue={this.state.value || this.props.value}
          onValueChange={this.onSelectValue}>
          {
            this.props.data.map(elem => <Picker.Item key={elem} label={elem} value={elem} />)
          }
        </Picker>
      </View>
    );
  }
}
export default DataPicker;

// @flow
import React from 'react';
import { View } from 'react-native';

type Props = {
  color?: string,
  length?: number,
};

const lineStyle = (color) => {
  return {
    backgroundColor: color,
    height: 1,
  };
};

const lineLength = (length) => {
  return {
    flex: !length ? 1 : length,
    justifyContent: 'center',
  };
};

const Line = (props: Props) => (
  <View style={lineLength(props.length)}>
    <View style={lineStyle(props.color)} />
  </View>
);

export default Line;

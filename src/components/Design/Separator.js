// @flow
import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import Line from './Line';

type Props = {
  text?: string,
  textStyle?: Object,
  color?: string,
  viewStyle?: Object,
  lineLength?: number,
};

const styles = StyleSheet.create({
  separatorTextContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  separatorText: {
    color: '#1c1c1c',
    fontWeight: '500',
    fontSize: 18,
    textAlign: 'center',
  },
  separator: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

const Separator = (props: Props) => (
  <View style={props.viewStyle ? props.viewStyle : styles.separator}>
    <Line color={props.color ? props.color : '#000000'} length={props.lineLength}/>
    <View style={styles.separatorTextContainer}>
      <Text style={props.textStyle ? props.textStyle : styles.separatorText}>{props.text}</Text>
    </View>
    <Line color={props.color ? props.color : '#000000'} length={props.lineLength}/>
  </View>
);

export default Separator;

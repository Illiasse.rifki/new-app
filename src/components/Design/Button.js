// @flow
import React from 'react';
import { StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import { theme } from '../../config/constants/index';

type Props = {
  containerStyle: {},
  title: string,
  onPress: Function,
  buttonStyle: {},
};

const styles = StyleSheet.create({
  validateButton: {
    borderRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { width: 0.5, height: 4 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    width: 310,
    height: 40,
    alignSelf: 'center',
  },
});

const Field = (props: Props) => (
  <Button
    title={props.title}
    containerViewStyle={props.containerStyle}
    color={theme.secondaryColor}
    backgroundColor={theme.primaryColor}
    buttonStyle={[styles.validateButton, props.buttonStyle]}
    onPress={props.onPress}
  />
);

export default Field;

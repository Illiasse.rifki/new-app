// @flow
import React from 'react';
import { View, StyleSheet, Platform } from 'react-native';
import DatePicker from 'react-native-datepicker';
import { FormLabel } from 'react-native-elements';
import { theme } from '../../config/constants/index';

type Props = {
  containerStyle: {},
  label: string,
  disabled: boolean,
  labelStyle: string,
  placeholder: string,
  subtitle: any,
  onDateChange: Function,
  date: string,
  dateOptions: {
    format: string,
    minDate: string,
    maxDate: string,
  },
};

const styles = StyleSheet.create({
  dateInput: {
    borderTopColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderBottomColor: Platform.OS === 'ios' ? '#c9c9c9' : '#979797',
  },
  dateText: {
    fontSize: 16,
    width: '100%',
    color: theme.fontBlack,
    marginBottom: -8,
    textAlign: 'left',
  },
  placeholderText: {
    fontSize: 16,
    width: '100%',
    marginBottom: -8,
    textAlign: 'left',
  },
  labelContainer: {
    marginLeft: 0,
    marginRight: 0,
  },
  labelContent: {
    marginRight: 0,
    marginLeft: 0,
    fontSize: 18,
    color: theme.fontBlack,
    lineHeight: 24,
    fontWeight: '500',
  },
  lightLabelContent: {
    marginTop: 0,
    marginRight: 0,
    marginLeft: 0,
    color: theme.gray,
    letterSpacing: 0.5,
    fontSize: 14,
    fontWeight: '500',
  },
  datePickerContainer: {
    width: '100%',
    marginTop: -10,
    marginLeft: 0,
    marginRight: 0,
  },
  disabled: {
    height: 25,
    marginTop: 15,
    paddingBottom: 15,
    backgroundColor: '#f5f5f5',
  },
});

const DateField = (props: Props) => (
  <View style={[styles.fieldView, props.containerStyle]}>
    {
      props.label ? (
        <FormLabel
          labelStyle={props.labelStyle === 'dark' ? styles.labelContent : styles.lightLabelContent}
          containerStyle={styles.labelContainer}
        >
          {props.label.toUpperCase()}
        </FormLabel>
      ) : null
    }
    {
      props.subtitle ? (props.subtitle) : null
    }
    <DatePicker

      {...props.dateOptions}
      placeholder={props.placeholder || props.label}
      style={styles.datePickerContainer}
      onDateChange={props.onDateChange}
      date={props.date}
      disabled={props.disabled}
      confirmBtnText='Confirmer'
      cancelBtnText='Annuler'
      customStyles={{
        dateInput: styles.dateInput,
        dateText: styles.dateText,
        placeholderText: styles.placeholderText,
        disabled: styles.disabled,
      }}
      showIcon={false}
    />
  </View>
);

export default DateField;

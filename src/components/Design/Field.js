// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { FormInput } from 'react-native-elements';
import FieldLabel from './FieldLabel';
import { theme } from '../../config/constants/index';

type Props = {
  containerStyle: {},
  label: string,
  labelStyle: string,
  placeholder: string,
  subtitle: any,
  multiline: boolean,
  onChangeText: Function,
  textInputOptions: {},
  inputStyle: {},
  value: string,
  secureTextEntry: boolean,
};

const styles = StyleSheet.create({
  inputContainer: {
    marginTop: 0,
    marginRight: 0,
    marginLeft: 0,
    marginBottom: 0,
  },
  inputText: {
    color: theme.fontBlack,
    width: '100%',
    fontSize: 16,
    marginBottom: -5,
  },
  multiline: {
    paddingBottom: 10,
  },
});

const Field = (props: Props) => (
  <View style={[styles.fieldView, props.containerStyle]}>
    <FieldLabel
      label={props.label}
      labelStyle={props.labelStyle}
    />
    {
      props.subtitle ? (props.subtitle) : null
    }
    {
      props.children ||
      <FormInput
        underlineColorAndroid='#979797'
        {...props.textInputOptions}
        textInputRef={props.textInputRef}
        multiline={props.multiline}
        value={props.value}
        onFocus={props.onFocus}
        autoCapitalize={props.autoCapitalize}
        onChangeText={props.onChangeText}
        placeholder={props.placeholder || props.label}
        containerStyle={styles.inputContainer}
        secureTextEntry={props.secureTextEntry}
        inputStyle={[styles.inputText, props.multiline ? styles.multiline : {}, props.inputStyle]}
      />
    }
  </View>
);

export default Field;

// // @flow
// import React, { Component } from 'react';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import ErrorComponent from '../Elements/ErrorComponent';
// import LoadingComponent from '../Elements/LoadingComponent';
// import UserSmallCard from '../Card/UserSmallCard';

// const JOB_OFFER_REGISTRATION_QUERY = gql`
//   query($job_offer_registration_id: String){
//     jobOfferRegistration(job_offer_registration_id: $job_offer_registration_id) {
//       job_offer_registration_id
//       nbviews
//       is_favorite_candidate
//       jobOffer: job_offer {
//         title
//       }
//       candidate {
//         picture
//         fullname
//         firstname
//         lastname
//         headline
//       }
//     }
//   }
// `;

// class JobOfferVisitSmallCard extends Component {
//   props: {
//     onPress: Function,
//     navigate: Function,
//     jobOfferRegistrationId: string,
//     data: {
//       error: any,
//       loading: Boolean,
//       refetch: Function,
//       jobOfferRegistration: {
//         is_favorite_candidate: string,
//         nbviews: number,
//         jobOffer: {
//           title: string,
//         },
//         candidate: {
//           picture: string,
//           fullname: string,
//           firstname: string,
//           lastname: string,
//           headline: string,
//         }
//       },
//     },
//   };

//   render() {
//     const { data, onPress } = this.props;

//     if (!data || data.error) return <ErrorComponent message={data.error} />;
//     if (data.loading || !data.jobOfferRegistration) return <LoadingComponent />;
//     const { candidate } = data.jobOfferRegistration;
//     if (!candidate) return null;
//     return (
//       <UserSmallCard
//         offerTitle={`${data.jobOfferRegistration.nbviews} visite${data.jobOfferRegistration.nbviews > 1 ? 's' : ''}`}
//         favorite={data.jobOfferRegistration.is_favorite_candidate}
//         picture={candidate.picture}
//         title={candidate.fullname}
//         pictureTitle={`${candidate.firstname[0]}${candidate.lastname[0]}`}
//         subtitle={candidate.headline}
//         onPress={onPress}
//       />
//     );
//   }
// }

// export default graphql(JOB_OFFER_REGISTRATION_QUERY, {
//   skip: props => !props.jobOfferRegistrationId,
//   options: props => ({
//     variables: {
//       job_offer_registration_id: props.jobOfferRegistrationId,
//     },
//   }),
// })(JobOfferVisitSmallCard);

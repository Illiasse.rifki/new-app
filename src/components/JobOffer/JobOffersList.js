// import React, { Component } from 'react';
// import { StyleSheet, FlatList, View, Text } from 'react-native';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';

// import JobOfferCard from './JobOfferCard';
// import ErrorComponent from '../Elements/ErrorComponent';
// import LoadingComponent from '../Elements/LoadingComponent';
// import { theme } from '../../config/constants/index';
// import EmptyCard from '../Card/EmptyCard';

// const styles = StyleSheet.create({
//   main: {
//     flex: 1,
//     // position: 'relative',
//   },
//   container: {
//     flex: 1,
//   },
//   title: {
//     fontSize: 28,
//     lineHeight: 32,
//     textAlign: 'left',
//     fontWeight: 'bold',
//     color: theme.fontBlack,
//     marginTop: 34,
//   },
//   listContentStyle: {
//     marginBottom: 96,
//   },
//   listContentHorizontal: {
//     marginBottom: 16,
//   },
//   card: {
//     marginBottom: 16,
//   },
//   cardFirst: {
//     marginTop: 90,
//     marginBottom: 16,
//   },
//   cardHorizontal: {
//     marginRight: 16,
//     width: 310,
//   },
//   cardHorizontalFirst: {
//     marginRight: 16,
//     width: 310,
//     marginTop: 90,
//   },
//   emptyInsetCardStyle: {
//     marginTop: 90,
//   },
// });

// const isObjectInvalid = obj =>
//   !obj || (!obj.tags.length && !obj.contracts.length && !obj.yearsStudy);

// class JobOffersList extends Component {
//   state = {
//     refreshing: false,
//   };

//   props: {
//     navigation: {
//       navigate: Function,
//     },
//     insetFirst: boolean,
//     listStyle: {},
//     light: boolean,
//     searchText: string,
//     filters: any,
//     searchJobs: any,
//     jobs: [],
//     title: string,
//     onJobOfferPress: Function,
//     onLoadMore: Function,
//     onRefresh: Function,
//     horizontal: boolean,
//     data: {
//       loading: boolean,
//       error: {},
//       viewer: {
//         candidate: {
//           tags: [string],
//         },
//       },
//     },
//   };

//   render() {
//     if (this.props.data.error) {
//       return <ErrorComponent message="Erreur lors de la récupération des jobs" />;
//     } else if (this.props.data.loading) {
//       return <LoadingComponent />;
//     }
//     const jobs = (this.props.searchText || !isObjectInvalid(this.props.filters)) ?
//       this.props.searchJobs.jobs : this.props.jobs;
//     const { title } = this.props;
//     return (
//       <View style={styles.main}>
//         {title && <Text style={styles.title}>{title}</Text>}
//         {
//           <FlatList
//             style={[styles.container, this.props.listStyle]}
//             data={jobs}
//             ListEmptyComponent={
//               <EmptyCard
//                 containerStyle={this.props.insetFirst ? styles.emptyInsetCardStyle : {}}
//                 title="Aucune offre pour ces critères"
//               />
//             }
//             contentContainerStyle={this.props.horizontal ? styles.listContentHorizontal : styles.listContentStyle}
//             keyExtractor={jobOffer => jobOffer.job_offer_id}
//             keyboardShouldPersistTaps={'always'}
//             showsHorizontalScrollIndicator={false}
//             onRefresh={this.props.onRefresh}
//             refreshing={this.state.refreshing}
//             onEndReached={this.props.onLoadMore}
//             horizontal={this.props.horizontal}
//             renderItem={({ item: jobOffer, index }) => (
//               <JobOfferCard
//                 light={this.props.light}
//                 onPress={() => this.props.onJobOfferPress(jobOffer)}
//                 onPressFavorite={() => this.props.onJobOfferPress(jobOffer)}
//                 containerStyle={this.props.horizontal ? styles.cardHorizontal :
//                   (index == 0 && this.props.insetFirst ? styles.cardFirst : styles.card)}
//                 candidateTags={this.props.data.viewer.candidate.tags}
//                 jobOffer={jobOffer}
//               />
//             )}
//           />
//         }
//       </View>
//     );
//   }
// }

// const VIEWER_QUERY = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         tags
//       }
//     }
//   }
// `;

// const SEARCH_JOB_OFFERS = gql`
//   query searchJobOffers($value: String, $filters: SearchJobOffersFiltersInput, $offset: Int, $limit: Int) {
//     jobs: searchJobOffers(
//         value: $value
//         filters: $filters
//         offset: $offset
//         limit: $limit
//       ) {
//       job_offer_id
//       title
//       date_publish(duration: true)
//       visibility_mode
//       cover_picture
//       location_name
//       location_departement
//       rating
//       company {
//         logo
//         header
//         name
//       }
//       tags
//       is_favorite: is_favorite_offer
//     }
//   }
// `;

// export default compose(
//   graphql(VIEWER_QUERY),
//   graphql(SEARCH_JOB_OFFERS, {
//     name: 'searchJobs',
//     skip: ownProps => !ownProps.searchText && isObjectInvalid(ownProps.filters),
//     options: ownProps => ({
//       variables: {
//         value: ownProps.searchText,
//         filters: ownProps.filters,
//       },
//     }),
//   }),
// )(JobOffersList);

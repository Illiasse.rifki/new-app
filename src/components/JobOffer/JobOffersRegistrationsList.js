import React, { Component } from 'react';
import { StyleSheet, FlatList, View, Text, RefreshControl } from 'react-native';
import JobOfferRegistrationCandidateSmallCard from './JobOfferRegistrationCandidateSmallCard';

const styles = StyleSheet.create({
  main: {
    flex: 1,
    // position: 'relative',
  },
  container: {
    flex: 1,
  },
  listContentStyle: {
    paddingBottom: 96,
  },
  card: {
    marginBottom: 16,
  },
  noJobCard: {
    backgroundColor: 'white',
    borderRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.8,
    padding: 32,
  },
  noJobCardText: {
    fontSize: 16,
    textAlign: 'center',
    color: '#afafaf',
    fontStyle: 'italic',
  },
});

class JobOffersRegistrationsList extends Component {
  state = {
    refreshing: false,
  };

  props: {
    navigation: {
      navigate: Function,
    },
    listStyle: {},
    light: boolean,
    searchText: string,
    searchJobs: {},
    jobs: [],
    onJobOfferPress: Function,
    onLoadMore: Function,
    onRefresh: Function,
    data: {
      loading: boolean,
      error: {},
      viewer: {
        candidate: {
          tags: [string],
        },
      },
    },
  };

  render() {
    const { jobs } = this.props;
    return (
      <View style={styles.main}>
        {
          <FlatList
            style={[styles.container, this.props.listStyle]}
            data={jobs}
            ListEmptyComponent={
              <View style={styles.noJobCard}>
                <Text style={styles.noJobCardText}>
                Aucune candidature
                </Text>
              </View>}
            contentContainerStyle={styles.listContentStyle}
            keyExtractor={jobOffer => jobOffer.job_offer_registration_id}
            keyboardShouldPersistTaps={'always'}
            showsHorizontalScrollIndicator={false}
            onRefresh={this.props.onRefresh}
            refreshing={this.state.refreshing}
            onEndReached={this.props.onLoadMore}
            renderItem={({ item: jobOfferRegistration }) => (
              <JobOfferRegistrationCandidateSmallCard
                light={this.props.light}
                onPress={() => this.props.onJobOfferPress(jobOfferRegistration)}
                containerStyle={styles.card}
                jobOfferRegistration={jobOfferRegistration}
              />
            )}
          />
        }
      </View>
    );
  }
}

export default JobOffersRegistrationsList;

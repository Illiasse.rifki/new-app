// // @flow
// import React, { Component } from 'react';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import ErrorComponent from '../Elements/ErrorComponent';
// import LoadingComponent from '../Elements/LoadingComponent';
// import UserSmallCard from '../Card/UserSmallCard';

// const JOB_OFFER_REGISTRATION_QUERY = gql`
//   query($job_offer_registration_id: String){
//     jobOfferRegistration(job_offer_registration_id: $job_offer_registration_id) {
//       job_offer_registration_id
//       is_favorite_candidate
//       jobOffer: job_offer {
//         title
//       }
//       candidate {
//         picture
//         fullname
//         firstname
//         lastname
//         headline
//       }
//     }
//   }
// `;

// class JobOfferRegistrationSmallCard extends Component {
//   props: {
//     navigate: Function,
//     jobOfferRegistrationId: string,
//     light: boolean,
//     data: {
//       error: any,
//       loading: Boolean,
//       refetch: Function,
//       jobOfferRegistration: {
//         is_favorite_candidate: string,
//         jobOffer: {
//           title: string,
//         },
//         candidate: {
//           picture: string,
//           fullname: string,
//           firstname: string,
//           lastname: string,
//           headline: string,
//         }
//       },
//     },
//   };

//   render() {
//     const { data, onPress } = this.props;

//     if (!data || data.error) return <ErrorComponent message={data.error} />;
//     if (data.loading || !data.jobOfferRegistration) return <LoadingComponent />;
//     const { jobOffer } = data.jobOfferRegistration;
//     const { candidate } = data.jobOfferRegistration;
//     return (
//       <UserSmallCard
//         offerTitle={!this.props.light && jobOffer.title}
//         favorite={data.jobOfferRegistration.is_favorite_candidate}
//         picture={candidate.picture}
//         title={`${candidate.lastname.toUpperCase()} ${candidate.firstname
//           .toLowerCase().replace(/(?:^|\s)\S/g, a => a.toUpperCase())}`}
//         pictureTitle={`${candidate.firstname[0]}${candidate.lastname[0]}`}
//         subtitle={candidate.headline}
//         onPress={onPress}
//       />
//     );
//   }
// }

// export default graphql(JOB_OFFER_REGISTRATION_QUERY, {
//   skip: props => !props.jobOfferRegistrationId,
//   options: props => ({
//     variables: {
//       job_offer_registration_id: props.jobOfferRegistrationId,
//     },
//   }),
// })(JobOfferRegistrationSmallCard);

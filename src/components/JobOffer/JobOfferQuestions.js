import React, { Component } from 'react';
import { StyleSheet, ScrollView, View, Text } from 'react-native';

import Line from '../Design/Line';
import { Button } from '../Button';
import { theme } from '../../config/constants/index';
import Question from './JobOfferQuestion';

const styles = StyleSheet.create({
  questionsText: { fontSize: 28, color: theme.fontBlack, textAlign: 'left', marginLeft: 32 },
  questions: { paddingTop: 33, backgroundColor: 'white' },
  questionsButton: { backgroundColor: theme.primaryColor },
  questionsButtonTitle: { color: 'white' },
  bottomButton: { marginBottom: 40 },
});

class Questions extends Component {
  props: {
    questions: Array<String>,
    answers: Array<String>,
    onPress: Function,
    editable: Boolean,
  };

  state = {
    questions: [],
    answers: [],
  };

  componentWillReceiveProps(nextProps) {
    const { questions, answers = [] } = nextProps;
    this.setState({ questions, answers });
  }

  updateAnswer(yesNo, index) {
    const answers = [...this.state.answers];
    if (answers[index] === yesNo) return;
    answers[index] = yesNo;
    this.setState({ answers });
  }

  render() {
    const {
      questions,
      onPress,
      editable = true,
    } = this.props;

    const questionsLength = questions.length;
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.questions}
      >
        <Text style={styles.questionsText}>Questions</Text>
        {questions.map((question, i) =>
          (<View key={question}>
            <Question
              question={question}
              answer={this.state.answers[i]}
              onPress={yesNo => this.updateAnswer(yesNo, i)}
              editable={editable}
            />
            {questionsLength !== i + 1 && <Line color='#afafaf' />}
          </View>
          ),
        )}
        {editable && <Button
          title='SUIVANT'
          onPress={() => onPress(this.state.answers)}
          buttonStyle={styles.bottomButton}
        />}
      </ScrollView>
    );
  }
}

export default Questions;

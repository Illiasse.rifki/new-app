// import React, { Component } from 'react';
// import { StyleSheet, FlatList, View, Text } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import JobOfferRegistrationCard from './JobOfferRegistrationCard';
// import ErrorComponent from '../Elements/ErrorComponent';
// import LoadingComponent from '../Elements/LoadingComponent';

// import { theme } from '../../config/constants/index';

// const styles = StyleSheet.create({
//   main: {
//     flex: 1,
//     // position: 'relative',
//   },
//   container: {
//     flex: 1,
//   },
//   title: {
//     fontSize: 28,
//     lineHeight: 32,
//     textAlign: 'left',
//     fontWeight: 'bold',
//     color: theme.fontBlack,
//     marginTop: 34,
//   },
//   listContentStyle: {
//     marginBottom: 96,
//   },
//   listContentHorizontal: {
//     marginBottom: 16,
//   },
//   card: {
//     marginBottom: 16,
//   },
//   cardHorizontal: {
//     marginRight: 16,
//     width: 310,
//   },
//   noJobCard: {
//     backgroundColor: 'white',
//     borderRadius: 4,
//     shadowColor: 'rgba(0, 0, 0, 0.08)',
//     shadowOffset: { height: 2, width: 0 },
//     shadowOpacity: 0.8,
//     padding: 32,
//   },
//   noJobCardText: {
//     fontSize: 16,
//     textAlign: 'center',
//     color: '#afafaf',
//     fontStyle: 'italic',
//   },
// });

// class JobOffersList extends Component {
//   state = {
//     refreshing: false,
//   };

//   props: {
//     navigation: {
//       navigate: Function,
//     },
//     listStyle: {},
//     light: boolean,
//     jobsRegistration: [],
//     onJobOfferRegistrationPress: Function,
//     onLoadMore: Function,
//     onRefresh: Function,
//     horizontal: boolean,
//     title: string,
//     data: {
//       loading: boolean,
//       error: {},
//       viewer: {
//         candidate: {
//           tags: [string],
//         },
//       },
//     },
//   };

//   render() {
//     if (this.props.data.error) {
//       return <ErrorComponent message="Erreur lors de la récupération des jobs" />;
//     } else if (this.props.data.loading) {
//       return <LoadingComponent />;
//     }
//     const { jobsRegistration, title } = this.props;
//     return (
//       <View style={styles.main}>
//         {title && <Text style={styles.title}>{title}</Text>}
//         {
//           <FlatList
//             style={[styles.container, this.props.listStyle]}
//             data={jobsRegistration}
//             ListEmptyComponent={
//               <View style={styles.noJobCard}>
//                 <Text style={styles.noJobCardText}>
//                 Aucune offre pour ces critères
//                 </Text>
//               </View>}
//             contentContainerStyle={this.props.horizontal ? styles.listContentHorizontal : styles.listContentStyle}
//             keyExtractor={jobOfferRegistration => jobOfferRegistration.job_offer_registration_id}
//             keyboardShouldPersistTaps={'always'}
//             showsHorizontalScrollIndicator={false}
//             onRefresh={this.props.onRefresh}
//             refreshing={this.state.refreshing}
//             onEndReached={this.props.onLoadMore}
//             horizontal={this.props.horizontal}
//             renderItem={({ item: jobOfferRegistration }) => (
//               <JobOfferRegistrationCard
//                 light={this.props.light}
//                 onPress={() => this.props.onJobOfferRegistrationPress(jobOfferRegistration.job_offer)}
//                 containerStyle={this.props.horizontal ? styles.cardHorizontal : styles.card}
//                 candidateTags={this.props.data.viewer.candidate.tags}
//                 jobOfferRegistration={jobOfferRegistration}
//               />
//             )}
//           />
//         }
//       </View>
//     );
//   }
// }

// const VIEWER_QUERY = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         tags
//       }
//     }
//   }
// `;

// export default graphql(VIEWER_QUERY)(JobOffersList);

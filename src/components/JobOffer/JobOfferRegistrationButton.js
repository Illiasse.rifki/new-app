// import React from 'react';
// import { Text, StyleSheet, View } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import { Button as RaisedButton } from '../Button';

// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';

// import { theme } from '../../config/constants';

// const JOB_OFFER_REGISTRATION_QUERY = gql`
//   query jobOfferRegistration($job_offer_id: String) {
//     jobOfferRegistration(job_offer_id: $job_offer_id) {
//       job_offer_registration_id
//       status
//       apply_date(format: "D/M/Y à H:m")
//       candidate {
//         phone
//         last_experience {
//           role
//         }
//         last_education {
//           diploma
//         }
//       }
//     }
//   }
// `;

// const styles = StyleSheet.create({
//   postulateButton: {
//     marginTop: 0,
//     marginBottom: 0,
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   text: {
//     color: theme.fontBlack,
//     fontSize: 14,
//     textAlign: 'center',
//   },
//   incompleteProfileContainer: {
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   incompleteProfile: {
//     textAlign: 'center',
//     marginBottom: 16,
//     lineHeight: 20,
//   },
//   strong: {
//     fontSize: 16,
//     fontWeight: 'bold',
//   },
// });

// type Props = {
//   navigate: Function,
//   jobOfferId: String,
//   data: {
//     loading: boolean,
//     error: {},
//     refetch: Function,
//     fetchMore: Function,
//     jobOfferRegistration: {
//       status: String,
//     },
//   },
// };

// const textRegistration = {
//   preselected: 'présélectionnée',
//   refused: 'refusée',
//   pending: 'en attente',
// };

// const JobOfferRegistrationButton = (props: Props) => {
//   const { navigate, data, jobOfferId } = props;
//   if (!data || data.error) {
//     return <ErrorComponent message="Problème lors de la récupération la postulation." />;
//   }
//   if (data.loading) return <LoadingComponent />;

//   const {
//     jobOfferRegistration: {
//       status,
//       job_offer_registration_id: jobOfferRegistrationId,
//       apply_date: applyDate,
//       candidate,
//     },
//   } = data;

//   if (status === 'no_apply' || status === 'incomplete') {
//     const isIncompleteCandidate =
//       !(candidate && candidate.phone && candidate.last_experience && candidate.last_education);
//     return isIncompleteCandidate ?
//       <View style={styles.incompleteProfileContainer}>
//         <Text style={styles.incompleteProfile}>
//           <Text style={styles.strong}>Votre profil n'est pas complet !</Text>
//           {'\nMettez le à jour pour pouvoir postuler.'}
//         </Text>
//         <RaisedButton
//           onPress={() => navigate('UpdateProfileForJobOffer', { status, jobOfferId, jobOfferRegistrationId, name: 'Formulaire' })}
//           buttonStyle={styles.postulateButton}
//           title="Mettre à jour & postuler"
//         />
//       </View>
//       : (
//         <RaisedButton
//           onPress={() => navigate('PostulateJobOffer', { status, jobOfferId, jobOfferRegistrationId, name: 'Formulaire' })}
//           buttonStyle={styles.postulateButton}
//           title="Postuler"
//         />
//       );
//   }
//   return <Text style={styles.text}>{`Vous avez postulé le ${applyDate}\nCandidature - ${textRegistration[status]}`}</Text>;
// };

// export default graphql(JOB_OFFER_REGISTRATION_QUERY, {
//   options: props => ({
//     variables: {
//       job_offer_id: props.jobOfferId,
//     },
//   }),
// })(JobOfferRegistrationButton);

import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import { Icon } from 'react-native-elements';


type Props = {
  containerStyle: {},
  rating: number,
};

const styles = StyleSheet.create({
  jobOfferRating: {
    flexDirection: 'row',
  },
});

const JobOfferRatings = (props: Props) => {
  const ties = [];
  for (let i = 0; i < 5; i += 1) {
    ties.push(<Icon
      color={i >= props.rating ? '#afafaf' : '#1c1c1c'}
      key={i}
      name='tie'
      type='material-community'
      size={28}
    />);
  }
  return (
    <View style={[styles.jobOfferRating, props.containerStyle]}>
      {ties}
    </View>
  );
};

export default JobOfferRatings;
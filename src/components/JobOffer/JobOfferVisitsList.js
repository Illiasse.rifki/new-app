import React from 'react';
import { StyleSheet, ScrollView, Text, FlatList, RefreshControl } from 'react-native';

import { theme } from '../../config/constants/index';

import JobOfferVisitSmallCard from '../../components/JobOffer/JobOfferVisitSmallCard';
import EmptyCard from '../Card/EmptyCard';

const styles = StyleSheet.create({
  main: { backgroundColor: '#f5f5f5' },
  mainContent: { marginVertical: 33.5, marginHorizontal: 16 },
  title: { color: theme.fontBlack, fontSize: 28, textAlign: 'left', fontWeight: 'bold', marginBottom: 0 },
  visitsNb: {
    color: theme.fontBlack,
    fontSize: 14,
    textAlign: 'left',
    fontWeight: '100',
    lineHeight: 24,
    marginBottom: 17,
    borderRadius: 4,
    backgroundColor: 'white',
    paddingVertical: 4,
    paddingHorizontal: 8,
    marginTop: 8,
  },
});

type tJobOfferVisitsList = {
    navigate: Function,
    jobOfferRegistrations: any,
    jobOfferViews: int,
    jobOfferTitle: string,
};

const JobOfferVisitsList = (props: tJobOfferVisitsList) => {
  const uniqVisits = props.jobOfferRegistrations ? props.jobOfferRegistrations.length : 0;
  return (
    <ScrollView
      style={styles.main}
      contentContainerStyle={styles.mainContent}
      refreshControl={<RefreshControl
        onRefresh={props.onRefresh}
        refreshing={props.refreshing}
      />}
    >
      <Text style={styles.title}>Liste des visiteurs</Text>
      <Text style={styles.visitsNb} >{`${props.jobOfferViews} visite${props.jobOfferViews > 1 ? 's' : ''} total dont ${uniqVisits} visiteur${uniqVisits > 1 ? 's' : ''} unique${uniqVisits > 1 ? 's' : ''}`}</Text>
      {
        <FlatList
          data={props.jobOfferRegistrations}
          ListEmptyComponent={
            <EmptyCard
              title="Aucune visite sur l'offre"
              subtitle="Tirer vers le bas pour rafraichir"
            />
          }
          contentContainerStyle={styles.listContentStyle}
          keyExtractor={jobOfferReg => jobOfferReg.job_offer_registration_id}
          keyboardShouldPersistTaps={'always'}
          renderItem={({ item: jobOfferReg }) => (
            <JobOfferVisitSmallCard
              key={jobOfferReg.job_offer_registration_id}
              jobOfferRegistrationId={jobOfferReg.job_offer_registration_id}
              onPress={() => props.navigate('RecruiterJobOfferVisitProfile', {
                jobOfferRegistrationId: jobOfferReg.job_offer_registration_id,
                jobOfferRegistration: jobOfferReg,
                jobOfferTitle: props.jobOfferTitle,
                userId: jobOfferReg.candidate.user_id,
              })}
            />
          )}
        />
      }
    </ScrollView>
  );
};

export default JobOfferVisitsList;

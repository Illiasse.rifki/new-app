// @flow
import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, Animated, Easing } from 'react-native';
import { Icon } from 'react-native-elements';

import { theme } from '../../config/constants/index';
import defaultBusinessHeader from '../../assets/defaultBusinessHeader.jpg';
import defaultBusinessLogo from '../../assets/defaultBusiness.png';
import Tags from '../Content/Tags';
import JobOfferFavorite from './JobOfferFavorite';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.8,
  },
  jobOfferCoverImage: {
    width: '100%',
    height: 112,
    resizeMode: 'cover',
  },
  jobOfferCoverContainer: {
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    overflow: 'hidden',
    position: 'relative',
  },
  favoriteIconContainer: {
    position: 'absolute',
    zIndex: 90,
    top: 16,
    right: 16,
    marginRight: 0,
  },
  jobOfferTextContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 17.5,
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 10.5,
  },
  jobOfferTextContainerLight: {
    alignItems: 'flex-start',
  },
  jobOfferTitle: {
    fontSize: 18,
    textAlign: 'center',
    fontWeight: '500',
    lineHeight: 24,
    color: theme.fontBlack,
  },
  jobOfferTitleLight: {
    textAlign: 'left',
  },
  jobOfferSubtitle: {
    fontSize: 14,
    textAlign: 'center',
    fontWeight: '300',
    letterSpacing: 0.5,
    lineHeight: 24,
    color: '#afafaf',
  },
  jobOfferSubtitleLight: {
    textAlign: 'left',
  },
  jobOfferCompanyName: {
    fontSize: 14,
    fontWeight: '500',
    letterSpacing: 0.5,
    color: '#33cbcc',
    lineHeight: 24,
    marginLeft: 7,
  },
  jobOfferFooter: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingBottom: 10.5,
    paddingLeft: 16,
    paddingRight: 16,
  },
  jobOfferLogoContainer: {
    marginTop: 8,
    flexDirection: 'row',
  },
  logo: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: '#f5f5f5',
    borderRadius: 3,
    backgroundColor: 'white',
  },
  nbJobOffers: {
    fontSize: 14,
    lineHeight: 24,
    color: '#afafaf',
  },
  goto: {
    fontSize: 14,
    color: theme.primaryColor,
    letterSpacing: 0.5,
    fontWeight: '500',
  },
  gotoContainer: {
    justifyContent: 'center',
  },
  tags: {
    fontSize: 14,
    color: '#afafaf',
    letterSpacing: 0.5,
    fontWeight: '500',
  },
  tagsIcon: {
    alignSelf: 'center',
  },
  tagsButtonContainer: {
    flexDirection: 'row',
    marginRight: 8,
    alignItems: 'center',
  },
  tagsContainer: {
    borderTopWidth: 1,
    borderTopColor: '#f5f5f5',
    paddingHorizontal: 16,
    paddingBottom: 19.5,
  },
  jobOfferRating: {
    flexDirection: 'row',
    marginTop: 12,
  },
  noTags: {
    marginTop: 19.5,
    color: '#afafaf',
    fontSize: 12,
    textAlign: 'center',
  },
});

const displayRating = (rating) => {
  const ties = [];
  for (let i = 0; i < 5; i += 1) {
    ties.push(<Icon
      color={i >= rating ? '#afafaf' : '#1c1c1c'}
      key={i}
      name='tie'
      type='material-community'
      size={28}
    />);
  }
  return ties;
};

class JobOfferCard extends Component {
  state = {
    toggleTags: false,
    opacityTags: new Animated.Value(0),
    heightTags: new Animated.Value(0),
    maxHeight: 0,
  };

  onToggleTags = () => {
    const initialValue = this.state.toggleTags ? this.state.maxHeight : 0;
    const finalValue = this.state.toggleTags ? 0 : this.state.maxHeight;
    this.setState({ toggleTags: !this.state.toggleTags });
    this.state.heightTags.setValue(initialValue);
    Animated.timing(
      this.state.heightTags,
      {
        toValue: finalValue,
        duration: 300,
        easing: Easing.linear,
      },
    ).start();
  };

  setMaxHeightTags = (event) => {
    this.setState({ maxHeight: event.nativeEvent.layout.height });
  };

  props: {
    jobOffer: {},
    light: boolean,
    candidateTags: [string],
    onPress: Function,
    onPressFavorite: Function,
    containerStyle: {},
  };

  render() {
    let { jobOffer } = this.props;
    if (!jobOffer) {
      jobOffer = { company: { name: 'inconnu' } };
    }
    if (!jobOffer.company) return null;
    const headerPicture = (jobOffer.company && jobOffer.company.header != null ?
      { uri: jobOffer.company.header } : defaultBusinessHeader);
    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        style={[styles.container, this.props.containerStyle]}>
        <View style={styles.jobOfferCoverContainer}>
          <Image
            source={jobOffer.cover_picture ?
              { uri: jobOffer.cover_picture } : headerPicture}
            style={styles.jobOfferCoverImage}
          />
          <JobOfferFavorite
            jobOfferId={jobOffer.job_offer_id}
            style={styles.favoriteIconContainer}
          />
        </View>

        <View style={[styles.jobOfferTextContainer,
          this.props.light && styles.jobOfferTextContainerLight]}>
          <Text numberOfLines={2} ellipsizeMode="tail" style={[styles.jobOfferTitle, this.props.light && styles.jobOfferTitleLight]}>{jobOffer.title}</Text>
          <Text style={[styles.jobOfferSubtitle, this.props.light && styles.jobOfferSubtitleLight]}>
            {`Publié ${jobOffer.date_publish}${(jobOffer.location_name && jobOffer.location_name !== 'Inconnu') ? ` - ${jobOffer.location_name}${jobOffer.location_departement && jobOffer.location_departement !== 0 ? ` (${jobOffer.location_departement})` : ''}` : ''}`}
          </Text>
          {
            !this.props.light &&
            <View style={styles.jobOfferLogoContainer}>
              <Image
                source={jobOffer.company.logo ? { uri: jobOffer.company.logo } : defaultBusinessLogo}
                style={styles.logo}
              />
              <Text style={styles.jobOfferCompanyName}>{jobOffer.company.name.toUpperCase()}</Text>
            </View>
          }
          {
            !this.props.light &&
            <View style={styles.jobOfferRating}>
              {
                displayRating(jobOffer.rating)
              }
            </View>
          }
        </View>
        <View style={styles.jobOfferFooter}>
          <TouchableOpacity
            hitSlop={{ top: 25, left: 25, bottom: 25, right: 25 }}
            onPress={this.onToggleTags} style={styles.tagsButtonContainer}>
            <Text style={styles.tags}>TAGS</Text>
            <Icon name={this.state.toggleTags ? 'chevron-up' : 'chevron-down'} type="evilicon" color="#afafaf" style={styles.tagsIcon} />
          </TouchableOpacity>
          <View style={styles.gotoContainer}>
            <Text style={styles.goto}>VOIR</Text>
          </View>
        </View>
        {
          this.state.toggleTags &&
          <View
            style={[styles.tagsContainer]}
            onLayout={this.setMaxHeightTags}
          >
            {
              (!jobOffer.tags || jobOffer.tags.length === 0) &&
              <Text style={styles.noTags}>Aucun Tags associés</Text>
            }
            <Tags
              tags={jobOffer.tags}
              hasTags={this.props.candidateTags}
            />
          </View>
        }
      </TouchableOpacity>
    );
  }
}

export default JobOfferCard;

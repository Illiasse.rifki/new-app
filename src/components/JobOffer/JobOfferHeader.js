import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { theme } from '../../config/constants/index';
import defaultBusinessHeader from '../../assets/defaultBusinessHeader.jpg';
import defaultBusinessLogo from '../../assets/defaultBusiness.png';
import JobOfferRatings from './JobOfferRating';

type Props = {
  containerStyle: {},
  isRecruiter: boolean,
  navigate: Function,
  jobOffer: {},
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainSection: {
    backgroundColor: 'white',
    paddingTop: 0,
    paddingRight: 0,
    paddingLeft: 0,
    paddingBottom: 32,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 8,
  },
  header: {
    width: '100%',
    height: 181,
    opacity: 0.5,
    resizeMode: 'cover',
  },
  headerRecruiter: {
    width: '100%',
    height: 181,
    opacity: 1,
    resizeMode: 'cover',
  },
  headerTextContainer: {
    paddingLeft: 16,
    paddingRight: 16,
  },
  logo: {
    width: 112,
    height: 112,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: -56,
    borderRadius: 5,
    backgroundColor: 'white',
  },
  headerBg: {
    backgroundColor: theme.primaryColor,
  },
  jobOfferTitle: {
    fontSize: 18,
    textAlign: 'center',
    fontWeight: '500',
    lineHeight: 24,
    color: theme.fontBlack,
    marginTop: 25,
  },
  jobOfferSubtitle: {
    fontSize: 14,
    textAlign: 'center',
    fontWeight: '300',
    letterSpacing: 0.5,
    lineHeight: 24,
    color: '#afafaf',
  },
  jobOfferLocation: {
    fontSize: 14,
    textAlign: 'center',
    fontWeight: '300',
    letterSpacing: 0.5,
    lineHeight: 24,
    color: '#afafaf',
  },
  jobOfferLocationRecruiter: {
    fontSize: 18,
    textAlign: 'center',
    lineHeight: 24,
    fontWeight: '300',
    marginTop: 5,
    marginBottom: 5,
    color: theme.fontBlack,
  },
  jobOfferCompanyButton: {
    marginTop: 7.5,
  },
  jobOfferCompany: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  jobOfferCompanyName: {
    color: theme.primaryColor,
    fontSize: 14,
    letterSpacing: 0.5,
    marginLeft: 5,
    fontWeight: '500',
  },
  ratings: {
    marginTop: 12,
    justifyContent: 'center',
  },
});

const JobOfferHeader = (props: Props) => {
  const { navigate, jobOffer, isRecruiter } = props;
  if (!jobOffer) {
    return null;
  }
  const headerPicture = (jobOffer.company && jobOffer.company.header != null ?
    { uri: jobOffer.company.header } : defaultBusinessHeader);
  return (
    <View
      style={styles.mainSection}
    >
      {
        isRecruiter ?
          <Image
            source={jobOffer.cover_picture ?
              { uri: jobOffer.cover_picture } : headerPicture}
            style={styles.headerRecruiter}
          /> :
          <View>
            <View style={styles.headerBg}>
              <Image
                source={jobOffer.cover_picture ?
                  { uri: jobOffer.cover_picture } : headerPicture}
                style={styles.header}
              />
            </View>
            <Image
              source={jobOffer.company && jobOffer.company.logo ?
                { uri: jobOffer.company.logo } : defaultBusinessLogo}
              style={styles.logo}
            />
          </View>
      }
      <View style={styles.headerTextContainer}>
        <Text style={styles.jobOfferTitle}>{jobOffer.title}</Text>
        <Text style={isRecruiter ? styles.jobOfferLocationRecruiter : styles.jobOfferLocation}>
          {`Publié ${jobOffer.date_publish}${(jobOffer.location_name && jobOffer.location_name !== 'Inconnu') ? ` - ${jobOffer.location_name}${jobOffer.location_departement && jobOffer.location_departement !== 0 ? ` (${jobOffer.location_departement})` : ''}` : ''}`}
        </Text>
        {
          isRecruiter ?
            <Text style={styles.jobOfferSubtitle}>
              {`${jobOffer.view_counter} vues - ${jobOffer.nb_registrations} candidatures`}
            </Text> :
            <View>
              <TouchableOpacity
                onPress={() => jobOffer.company && navigate('CompanyProfile', { companyId: jobOffer.company.company_id })}
                style={styles.jobOfferCompanyButton}
              >
                <View style={styles.jobOfferCompany}>
                  <Icon
                    name="business"
                    type="material-icons"
                    color={theme.primaryColor}
                    size={16}
                  />
                  <Text style={styles.jobOfferCompanyName}>
                    {jobOffer.company && jobOffer.company.name.toUpperCase()}
                  </Text>
                </View>
              </TouchableOpacity>
              <JobOfferRatings
                containerStyle={styles.ratings}
                rating={jobOffer.rating}
              />
            </View>
        }
      </View>
    </View>
  );
};

export default JobOfferHeader;

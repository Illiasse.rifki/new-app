import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

import { Icon } from 'react-native-elements';
import { theme } from '../../config/constants/index';
import defaultBusinessHeader from '../../assets/defaultBusinessHeader.jpg';

type Props = {
  containerStyle: {},
  jobOfferRegistration: {
    cover_picture: any,
    visibility_mode: string,
    title: string,
    nb_registrations: any,
    view_counter: any,
  },
  onPress: Function,
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { height: 1, width: 0 },
    shadowOpacity: 0.8,
  },
  jobOfferImage: {
    height: 96,
    width: 96,
    resizeMode: 'cover',
  },
  imageViewContainer: {
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    overflow: 'hidden',
    position: 'relative',
  },
  jobOfferTextContainer: {
    alignSelf: 'center',
    flex: 1,
    justifyContent: 'space-around',
    marginLeft: 14.5,
  },
  jobOfferStatus: { fontSize: 14 },
  jobOfferCompany: {
    color: '#afafaf',
    fontSize: 13,
    marginBottom: 2,
  },
  jobOfferTitle: {
    fontSize: 18,
    fontWeight: '500',
    lineHeight: 20,
    marginBottom: 2,
  },
  jobOfferSubtitle: {
    color: '#afafaf',
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 24,
  },
  newRegistrationsContainer: {
    position: 'absolute',
    flex: 1,
    top: 20,
    left: 20,
    borderRadius: 30,
    padding: 5,
    backgroundColor: theme.green,
    height: 56,
    width: 56,
    alignItems: 'center',
    justifyContent: 'center',
  },
  newRegistrations: {
    fontSize: 18,
    color: 'white',
    backgroundColor: 'transparent',
  },
  icon: { marginRight: 8, alignSelf: 'center' },
  nbCandidates: { color: '#afafaf', fontSize: 14 },
});

const status = {
  preselected: { value: 'Préselectionné', style: { color: theme.green } },
  pending: { value: 'En attente', style: { color: theme.primaryColor } },
  refused: { value: 'Refusé', style: { color: theme.red } },
};

const JobOfferRegistrationCandidateSmallCard = (props: Props) => {
  const { jobOfferRegistration } = props;
  const { job_offer: jobOffer } = jobOfferRegistration;
  const displayStatus = status[jobOfferRegistration.status] || {
    value: jobOfferRegistration.status,
    style: { color: '#afafaf' },
  };
  return (
    <TouchableOpacity
      style={[styles.container, props.containerStyle]}
      onPress={props.onPress}
    >
      <View style={styles.imageViewContainer}>
        <Image
          style={styles.jobOfferImage}
          source={jobOffer.cover_picture ? { uri: jobOffer.cover_picture } : defaultBusinessHeader}
        />
      </View>
      <View style={styles.jobOfferTextContainer}>
        <Text
          style={styles.jobOfferTitle}
          numberOfLines={2}
          ellipsizeMode="tail"
        >{jobOffer.title}</Text>
        <Text numberOfLines={1} style={styles.jobOfferCompany}>{jobOffer.company.name}</Text>
        <Text style={[styles.jobOfferSubtitle, displayStatus.style]}>{displayStatus.value}</Text>
      </View>
      <Icon
        type="entypo"
        name="chevron-thin-right"
        color="#afafaf"
        size={20}
        style={styles.icon}
      />
    </TouchableOpacity>
  );
};

export default JobOfferRegistrationCandidateSmallCard;

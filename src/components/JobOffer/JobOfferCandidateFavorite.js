// import React, { Component } from 'react';
// import { Icon } from 'react-native-elements';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';
// import { StyleSheet } from 'react-native';

// import LoadingComponent from '../../components/Elements/LoadingComponent';

// const JOB_OFFER_REGISTRATION_MUTATION = gql`
//   mutation($job_offer_registration_id: String!, $is_favorite: Boolean) {
//     editJobOfferRegistration(job_offer_registration_id: $job_offer_registration_id, is_favorite_candidate: $is_favorite) {
//       jobOfferRegistration {
//         job_offer_registration_id
//         is_favorite: is_favorite_candidate
//       }
//       error {
//         code
//         message
//       }
//     }
//   }
// `;

// const QUERY_JOB_OFFER_REGISTRATION = gql`
//   query JobOfferRegistration($job_offer_registration_id: String!) {
//     jobOfferRegistration(job_offer_registration_id: $job_offer_registration_id) {
//       job_offer_registration_id
//       is_favorite: is_favorite_candidate
//     }
//   }
// `;

// const styles = StyleSheet.create({
//   icon: { margin: 0, bottom: 20, marginBottom: -18 },
// });

// type tJobOfferFavorite = {
//   jobOfferRegistrationId: String,
//   data: {
//     refetch: Function,
//     loading: Boolean,
//     error: any,
//     jobOfferRegistration: {
//       is_favorite: Boolean,
//     },
//   },
//   size: number,
//   mutation: Function,
//   style: any,
//   show: Boolean,
// };

// class JobOfferCandidateFavorite extends Component {
//   props: tJobOfferFavorite;

//   componentDidUpdate() {
//     const { data } = this.props;
//     if (data && !data.loading && !data.error && !data.jobOfferRegistration) {
//       this.props.data.refetch();
//     }
//   }

//   mutation = () => {
//     const { data, jobOfferRegistrationId } = this.props;
//     const {
//       is_favorite: favorite,
//     } = data.jobOfferRegistration;
//     this.props.mutation({
//       variables: {
//         job_offer_registration_id: jobOfferRegistrationId,
//         is_favorite: !favorite,
//       },
//       refetchQueries: [{
//         query: QUERY_JOB_OFFER_REGISTRATION,
//         variables: {
//           job_offer_registration_id: jobOfferRegistrationId,
//         },
//       }],
//     }).then(() =>
//       data.refetch().catch(e =>
//         console.warn('Favorite refetch failed', e.toString()))).catch(e =>
//       console.warn('Favorite refetch failed', e.toString()));
//   }

//   render() {
//     const { data, style, show = true } = this.props;

//     if (!data || data.error) return null;
//     if (data.loading || !data.jobOfferRegistration) return <LoadingComponent />;

//     const {
//       is_favorite: favorite,
//     } = data.jobOfferRegistration;

//     if (show) {
//       return (<Icon
//         name={favorite ? 'md-heart' : 'md-heart-outline'}
//         type='ionicon'
//         onPress={this.mutation}
//         color={'white'}
//         size={30}
//         containerStyle={[styles.icon, style]}
//       />);
//     }
//     return (favorite && (<Icon
//       name={'md-heart'}
//       type='ionicon'
//       color={'#33cbcc'}
//       size={24}
//       containerStyle={[styles.icon, style]}
//     />));
//   }
// }

// const JobOfferCandidateFavoriteGQL = compose(
//   graphql(QUERY_JOB_OFFER_REGISTRATION, {
//     options: ({ jobOfferRegistrationId }) => ({
//       variables: {
//         job_offer_registration_id: jobOfferRegistrationId,
//       },
//     }),
//   }),
//   graphql(JOB_OFFER_REGISTRATION_MUTATION, { name: 'mutation' }),
// )(JobOfferCandidateFavorite);

// export default JobOfferCandidateFavoriteGQL;

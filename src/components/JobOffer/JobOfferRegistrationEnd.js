import React from 'react';
import { StyleSheet, ScrollView, View, Text, Image } from 'react-native';

import { Button } from '../Button';

import jobOfferValidate from '../../assets/jobOfferValidate/jobOfferValidate.png';

import {
  JOB_OFFER_REGISTRATION_QUERY,
  JOB_OFFER_REGISTRATION_BUTTON_QUERY,
  JOB_OFFER_REGISTRATIONS_LIST,
} from '../../queries/JobOfferRegistration';

const styles = StyleSheet.create({
  jobOfferRegistrationEnd: {
    paddingVertical: 32,
    paddingHorizontal: 32,
  },
  jobOfferRegistrationEndScroll: {
    backgroundColor: 'white',
  },
  jobOfferRegistrationEndButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  jobOfferRegistrationEndText: {
    fontSize: 28,
    lineHeight: 40,
    textAlign: 'center',
    marginTop: 18,
    marginBottom: 62,
    fontWeight: 'bold',
  },
  jobOfferValidate: {
    height: 272,
    width: 310,
  },
  buttonContainer: { marginLeft: 0, marginRight: 0 },
  buttonStyleXS: { marginTop: 0, marginBottom: 0, width: 147, borderColor: 'rgba(0, 0, 0, 0.08)', borderWidth: 1 },
  buttonStyle: { marginTop: 0, marginBottom: 16, borderColor: 'rgba(0, 0, 0, 0.08)', borderWidth: 1 },
});

type tJobOfferRegistrationEnd = {
  navigate: Function,
  goBack: Function,
  mutation: Function,
  refetch: Function,
  jobOfferRegistrationId: String,
  jobOfferId: String,
  alertWithType: Function,
};

const JobOfferRegistrationEnd = (props: tJobOfferRegistrationEnd) => {
  const {
    navigate,
    mutation,
    jobOfferRegistrationId,
    jobOfferId,
    alertWithType,
    goBack,
    refetch,
  } = props;
  const postulate = () => mutation({
    variables: {
      jobOfferRegistrationId,
      status: 'pending',
    },
    refetchQueries: [
      {
        query: JOB_OFFER_REGISTRATION_QUERY,
        variables: {
          jobOfferRegistrationId,
        },
      },
      {
        query: JOB_OFFER_REGISTRATION_BUTTON_QUERY,
        variables: {
          jobOfferId,
        },
      },
      {
        query: JOB_OFFER_REGISTRATIONS_LIST,
      },
    ],
  }).then(refetch).then(() => navigate('Validation', {
    headerTitle: 'Candidature envoyée',
    title: 'Votre candidature a bien été envoyée',
    subtitle: 'Vous pourrez suivre l’évolution de votre statut à tout moment à partir de votre tableau de bord de l’accueil.',
    buttonName: 'C\'est compris',
    goTo: 'JobOffers',
  })).catch(e => alertWithType('error', 'Erreur', `Impossible de terminer la postulation. ${e.toString()}`));
  return (
    <ScrollView style={styles.jobOfferRegistrationEndScroll} contentContainerStyle={styles.jobOfferRegistrationEnd}>
      <Image source={jobOfferValidate} style={styles.jobOfferValidate} />
      <Text style={styles.jobOfferRegistrationEndText}>Êtes-vous sûr de vouloir postuler ?</Text>
      <Button
        alt
        title='Vérifier votre profil'
        onPress={() => navigate('CandidateProfile')}
        containerStyle={styles.buttonContainer}
        buttonStyle={styles.buttonStyle}
      />
      <View style={styles.jobOfferRegistrationEndButtons}>
        <Button
          alt
          title='Annuler'
          onPress={() => goBack()}
          containerStyle={styles.buttonContainer}
          buttonStyle={styles.buttonStyleXS}
        />
        <Button
          // disabled={} if loading
          title='Postuler'
          onPress={postulate}
          containerStyle={styles.buttonContainer}
          buttonStyle={styles.buttonStyleXS}
        />
      </View>
    </ScrollView>
  );
};

export default JobOfferRegistrationEnd;

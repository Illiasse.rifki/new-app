import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

import { Button } from '../Button';
import { theme } from '../../config/constants/index';

const styles = StyleSheet.create({
  question: { marginTop: 30, marginBottom: 35, marginHorizontal: 32 },
  questionText: { color: theme.fontBlack, fontSize: 18, textAlign: 'left' },
  questionAnswer: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center' },
  button: { width: 140, height: 40, marginTop: 14, marginBottom: 0 },
});

type tQuestion = {
  question: String,
  answer: String,
  onPress: Function,
  editable: Boolean,
};

const Question = (props: tQuestion) => {
  const { question, answer, onPress, editable = true } = props;

  return (
    <View style={styles.question}>
      <Text style={styles.questionText}>{question}</Text>
      <View style={styles.questionAnswer}>
        <Button
          alt={answer !== 'Non'}
          title='Non'
          onPress={editable ? () => answer !== 'Non' && onPress('Non') : null}
          buttonStyle={styles.button}
        />
        <Button
          alt={answer !== 'Oui'}
          title='Oui'
          onPress={editable ? () => answer !== 'Oui' && onPress('Oui') : null}
          buttonStyle={styles.button}
        />
      </View>
    </View>
  );
};

export default Question;

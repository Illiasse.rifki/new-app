import React from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Text,
} from 'react-native';
import JobOfferSmallCard from './JobOfferSmallCard';

type Props = {
  containerStyle: {},
  onJobOfferPress: Function,
  jobs: [],
  rating: number,
  title: string,
};

const styles = StyleSheet.create({
  container: {

  },
  card: {
    marginBottom: 8,
  },
  title: {
    fontSize: 28,
    color: '#1c1c1c',
    fontWeight: 'bold',
    marginBottom: 17,
  },
  noJobCard: {
    backgroundColor: 'white',
    borderRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.8,
    padding: 32,
  },
  noJobCardText: {
    fontSize: 16,
    textAlign: 'left',
    lineHeight: 24,
    color: '#afafaf',
    fontStyle: 'italic',
  },
  noJobCardTextTip: {
    fontSize: 14,
    fontWeight: '100',
    textAlign: 'left',
    lineHeight: 20,
    color: '#afafaf',
    fontStyle: 'italic',
  },
});

const JobOfferRatings = (props: Props) => {
  return (
    <View style={[styles.container, props.containerStyle]}>
      {
        props.title && <Text style={styles.title}>{props.title}</Text>
      }
      <FlatList
        data={props.jobs}
        ListEmptyComponent={
          <View style={styles.noJobCard}>
            <Text style={styles.noJobCardText}>
              Aucune offres en cours
            </Text>
            <Text style={styles.noJobCardTextTip}>
              Vous pouvez ajouter de nouvelles offres depuis l'interface web
            </Text>
          </View>}
        contentContainerStyle={styles.listContentStyle}
        keyExtractor={jobOffer => jobOffer.job_offer_id}
        keyboardShouldPersistTaps={'always'}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item: jobOffer }) => (
          <JobOfferSmallCard
            onPress={() => props.onJobOfferPress(jobOffer)}
            containerStyle={styles.card}
            jobOffer={jobOffer}
          />
        )}
      />
    </View>
  );
};

export default JobOfferRatings;

import React from 'react';
import { StyleSheet, ScrollView, Text, FlatList, View, RefreshControl } from 'react-native';

import { theme } from '../../config/constants/index';

import JobOfferRegistrationSmallCard from '../../components/JobOffer/JobOfferRegistrationSmallCard';

const styles = StyleSheet.create({
  main: { backgroundColor: '#f5f5f5' },
  mainContent: { marginVertical: 33.5, marginHorizontal: 16 },
  title: { color: theme.fontBlack, fontSize: 28, textAlign: 'left', fontWeight: 'bold', marginBottom: 17 },
  noRegCard: {
    marginVertical: 4,
    backgroundColor: 'white',
    borderRadius: 4,
    height: 96,
    shadowRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { height: 1, width: 0 },
    elevation: 1,
    shadowOpacity: 0.8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  noRegCardText: {
    fontSize: 16,
    textAlign: 'left',
    lineHeight: 24,
    color: '#afafaf',
    fontStyle: 'italic',
  },
});

type tJobOfferRegistrationList = {
  navigate: Function,
  jobOfferRegistrations: any,
  jobOfferId: string,
  emptyText: string,
};

const JobOfferRegistrationList = (props: tJobOfferRegistrationList) => (
  <ScrollView
    style={styles.main}
    contentContainerStyle={styles.mainContent}
    refreshControl={<RefreshControl
      refreshing={props.refreshing}
      onRefresh={props.onRefresh}
    />}
  >
    <Text style={styles.title}>Liste des candidatures</Text>
    <FlatList
      style={[styles.container, props.listStyle]}
      data={props.jobOfferRegistrations.sort((a, b) => (a.candidate.lastname > b.candidate.lastname ? 1 : -1))}
      ListEmptyComponent={
        <View style={styles.noRegCard}>
          <Text style={styles.noRegCardText} >
            {props.emptyText || 'Aucune candidatures pour ces critères'}
          </Text>
        </View>}
      keyExtractor={jobOffer => jobOffer.job_offer_registration_id}
      renderItem={({ item: jobOfferReg }) => (
        <JobOfferRegistrationSmallCard
          light
          key={jobOfferReg.job_offer_registration_id}
          jobOfferRegistrationId={jobOfferReg.job_offer_registration_id}
          onPress={() => props.navigate('RecruiterJobOfferRegistration', {
            jobOfferRegistrationId: jobOfferReg.job_offer_registration_id,
            jobOfferId: props.jobOfferId,
            userId: jobOfferReg.candidate.user_id,
          })}
        />
      )}
    />
  </ScrollView>
);

export default JobOfferRegistrationList;

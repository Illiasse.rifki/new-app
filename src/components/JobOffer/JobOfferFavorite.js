// import React from 'react';
// import { Icon } from 'react-native-elements';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';
// import { StyleSheet } from 'react-native';

// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import { JOB_OFFER_LIST_FAV } from '../../screens/job-offer/JobOffersList';

// import { JOB_OFFER_REGISTRATION_MUTATION } from '../../mutations/JobOfferRegistration';

// const JOB_OFFER_QUERY = gql`
//   query jobOffer($jobOfferId: String!) {
//     jobOffer(job_offer_id: $jobOfferId) {
//       job_offer_id
//       favorite: is_favorite_offer
//     }
//   }
// `;

// const styles = StyleSheet.create({
//   icon: { marginRight: 24, backgroundColor: 'transparent' },
// });

// type tJobOfferFavorite = {
//   jobOfferId: String,
//   data: {
//     loading: Boolean,
//     error: any,
//     jobOffer: {
//       favorite: Boolean,
//     },
//   },
//   size: number,
//   mutation: Function,
//   style: any,
// };

// const JobOfferFavorite = (props: tJobOfferFavorite) => {
//   const { data, jobOfferId, style } = props;
//   const err = { message: 'Props jobOfferId of JobOfferFavorite is undefined' };

//   if (!jobOfferId) throw err;
//   if (!data || data.error || !data.jobOffer) return null;
//   if (data.loading) return <LoadingComponent />;

//   const {
//     favorite,
//   } = data.jobOffer;

//   const mutation = () => props.mutation({
//     variables: {
//       jobOfferId,
//       favoriteOffer: !favorite,
//     },
//     refetchQueries: [{
//       query: JOB_OFFER_QUERY,
//       variables: {
//         jobOfferId,
//       },
//     }, {
//       query: JOB_OFFER_LIST_FAV,
//     }],
//   }).then(() =>
//     data.refetch().catch(e =>
//       console.warn('Favorite refetch failed', e.toString()))).catch(e =>
//     console.warn('Favorite refetch failed', e.toString()));

//   return (<Icon
//     underlayColor="transparent"
//     name={favorite ? 'md-heart' : 'md-heart-outline'}
//     type='ionicon'
//     onPress={mutation}
//     color='white'
//     size={24}
//     containerStyle={[styles.icon, style]}
//   />);
// };

// const JobOfferFavoriteGQL = compose(
//   graphql(JOB_OFFER_QUERY, {
//     options: ({ jobOfferId }) => ({
//       variables: {
//         jobOfferId,
//       },
//     }),
//   }),
//   graphql(JOB_OFFER_REGISTRATION_MUTATION, { name: 'mutation' }),
// )(JobOfferFavorite);

// export default JobOfferFavoriteGQL;

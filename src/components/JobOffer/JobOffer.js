// import React from 'react';
// import {
//   View,
//   Text,
//   StyleSheet,
// } from 'react-native';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';

// import { theme } from '../../config/constants/index';
// import CardEditable from '../Card/CardEditable';
// import ErrorComponent from '../Elements/ErrorComponent';
// import LoadingComponent from '../Elements/LoadingComponent';
// import Tags from '../Content/Tags';
// import Qualities from '../Content/Qualities';
// import { FlatButton, Button as RaisedButton } from '../Button';
// import JobOfferHeader from './JobOfferHeader';
// import JobOfferRegistrationButton from './JobOfferRegistrationButton';

// type Props = {
//   containerStyle: {},
//   isRecruiter: boolean,
//   navigate: Function,
//   data: {
//     error: string,
//     singleCompany: {
//       editable: boolean,
//       firstname: string,
//       lastname: string,
//       wish_raw_salary: string,
//       wish_variable_salary: string,
//       work_city: [],
//       company_size: [],
//     },
//     loading: boolean,
//   },
//   viewer: {
//     loading: boolean,
//     error: {},
//     viewer: {},
//   },
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   mainSection: {
//     backgroundColor: 'white',
//     paddingLeft: 0,
//     paddingTop: 0,
//     paddingRight: 0,
//     paddingBottom: 32,
//     marginBottom: 24,
//     shadowColor: 'rgba(0, 0, 0, 0.08)',
//     shadowOffset: { width: 1, height: 2 },
//     shadowOpacity: 0.8,
//     shadowRadius: 8,
//   },
//   emptySectionText: {
//     color: '#afafaf',
//     marginTop: 8,
//     marginHorizontal: 0,
//     fontSize: 14,
//   },
//   tags: {
//     marginBottom: 8,
//     marginLeft: 8,
//     marginRight: 8,
//   },
//   noData: {
//     marginLeft: 8,
//     marginRight: 8,
//     marginTop: 8,
//   },
//   emptyComponentText: {
//     color: '#afafaf',
//     marginTop: 8,
//     marginHorizontal: 8,
//     fontSize: 14,
//   },
//   sectionTitle: {
//     marginTop: 2,
//     marginBottom: 0,
//   },
//   sectionDescriptionTitle: {
//     marginTop: 2,
//     marginLeft: 0,
//     marginBottom: 4,
//   },
//   section: {
//     paddingLeft: 32,
//     paddingRight: 32,
//   },
//   description: {
//     fontSize: 14,
//     lineHeight: 24,
//     color: theme.fontBlack,
//   },
//   postulateButton: {
//     marginTop: 0,
//     marginBottom: 0,
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   buttonContainer: {
//     marginTop: 24,
//     paddingLeft: 16,
//     paddingRight: 16,
//     marginBottom: 24,
//   },
//   lastSection: {
//     marginBottom: 0,
//   },
// });

// const JobOffer = (props: Props) => {
//   if (props.data.error || props.viewer.error) {
//     return (<ErrorComponent message={'Erreur lors de récuperation de l\'offre'}/>);
//   } else if (props.data.loading || props.viewer.loading) {
//     return (<LoadingComponent/>);
//   }
//   const { navigate, isRecruiter } = props;
//   const { jobOffer } = props.data;
//   const { candidate } = props.viewer.viewer;
//   if (!jobOffer) {
//     return (<LoadingComponent/>);
//   }
//   const postulateButton = isRecruiter ?
//     (<View style={styles.buttonContainer}>
//       <RaisedButton
//         onPress={() => navigate('RecruiterJobOfferRegistrationStack', { jobOfferId: jobOffer.job_offer_id })}
//         buttonStyle={styles.postulateButton}
//         title={`Traiter les candidatures (${jobOffer.nb_registrations})`}
//       />
//       {
//         jobOffer.company.sponsor &&
//         <FlatButton
//           onPress={() => navigate('JobOfferVisits', { jobOfferId: jobOffer.job_offer_id })}
//           buttonStyle={styles.postulateButton}
//           title="Voir les visites"
//         />
//       }
//     </View>) :
//     (<View style={styles.buttonContainer}>
//       <JobOfferRegistrationButton jobOfferId={jobOffer.job_offer_id} navigate={navigate} />
//     </View>);

//   return (
//     <View style={[styles.container, props.containerStyle]}>
//       <JobOfferHeader
//         isRecruiter={isRecruiter}
//         jobOffer={jobOffer}
//         navigate={props.navigate}
//       />
//       {postulateButton}
//       <CardEditable
//         title="Descriptif"
//         titleStyle={styles.sectionDescriptionTitle}
//         containerStyle={styles.section}
//       >
//         {
//           jobOffer.description ?
//             <Text style={styles.description}>{jobOffer.description}</Text> :
//             <Text style={styles.emptySectionText}>Aucune description ajoutée</Text>
//         }
//       </CardEditable>
//       <CardEditable
//         title="Profil recherché"
//         titleStyle={styles.sectionDescriptionTitle}
//         containerStyle={styles.section}
//       >
//         {
//           jobOffer.looking_for ?
//             <Text style={styles.description}>{jobOffer.looking_for}</Text> :
//             <Text style={styles.emptySectionText}>Aucune description ajoutée</Text>
//         }
//       </CardEditable>
//       <CardEditable
//         title="Tags requis"
//         titleStyle={styles.sectionTitle}
//       >
//         <Tags
//           emptyComponent={
//             <Text style={styles.emptyComponentText}>Aucun tags requis</Text>
//           }
//           hasTags={candidate && candidate.tags}
//           tags={jobOffer.tags}/>
//       </CardEditable>
//       <CardEditable
//         title="Qualités requises"
//         titleStyle={styles.sectionTitle}
//         containerStyle={styles.lastSection}
//       >
//         <Qualities
//           emptyComponent={
//             <Text style={styles.emptyComponentText}>Aucune qualités spécifiquement requises</Text>
//           }
//           hasQualities={candidate && candidate.qualities}
//           qualities={jobOffer.qualities}
//         />
//       </CardEditable>
//       {postulateButton}
//     </View>
//   );
// };

// const VIEWER_QUERY = isRecruiter => gql`
//   query {
//     viewer {
//       id
//       candidate {
//         tags
//         qualities
//       }
//     }
//   }
// `;

// const JOB_OFFER_QUERY = isRecruiter => gql`
//   query jobOffer($job_offer_id: String!) {
//     jobOffer(job_offer_id: $job_offer_id) {
//       job_offer_id
//       title
//       date_publish(duration: true)
//       cover_picture
//       location_name
//       location_departement
//       rating
//       company {
//         ${isRecruiter ? `
//         sponsor
//         ` : ''}
//         company_id
//         logo
//         header
//         name
//       }
//       tags
//       qualities
//       looking_for
//       description
//       ${isRecruiter ? `
//       view_counter
//       nb_registrations(filter: "pending")
//       ` : ''}
//     }
//   }
// `;

// export default isRecruiter => compose(
//   graphql(JOB_OFFER_QUERY(isRecruiter), {
//     options: ownProps => ({
//       variables: {
//         job_offer_id: ownProps.jobOfferId,
//       },
//     }),
//   }),
//   graphql(VIEWER_QUERY(isRecruiter), { name: 'viewer' }),
// )(JobOffer);

// export { JOB_OFFER_QUERY };

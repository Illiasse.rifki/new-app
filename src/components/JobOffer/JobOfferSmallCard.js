import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { Icon } from 'react-native-elements';

import { theme } from '../../config/constants/index';
import defaultBusinessHeader from '../../assets/defaultBusinessHeader.jpg';

type Props = {
  containerStyle: {},
  jobOffer: {
    cover_picture: any,
    visibility_mode: string,
    title: string,
    nb_registrations: any,
    view_counter: any,
  },
  onPress: Function,
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { height: 1, width: 0 },
    shadowOpacity: 0.8,
  },
  jobOfferImage: {
    height: 96,
    width: 96,
    resizeMode: 'cover',
  },
  imageViewContainer: {
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    overflow: 'hidden',
    position: 'relative',
  },
  jobOfferTextContainer: {
    alignSelf: 'center',
    flex: 1,
    justifyContent: 'space-around',
    marginLeft: 14.5,
  },
  jobOfferStatus: { fontSize: 14 },
  jobOfferTitle: {
    fontSize: 18,
    fontWeight: '500',
    lineHeight: 20,
    marginBottom: 2,
  },
  jobOfferSubtitle: {
    color: '#afafaf',
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 24,
  },
  newRegistrationsContainer: {
    position: 'absolute',
    flex: 1,
    top: 20,
    left: 20,
    borderRadius: 30,
    padding: 5,
    backgroundColor: theme.green,
    height: 56,
    width: 56,
    alignItems: 'center',
    justifyContent: 'center',
  },
  newRegistrations: {
    fontSize: 18,
    color: 'white',
    backgroundColor: 'transparent',
  },
  icon: { marginRight: 8, alignSelf: 'center' },
  nbCandidates: { color: '#afafaf', fontSize: 14 },
});

const JobOfferSmallCard = (props: Props) => {
  const { jobOffer } = props;
  return (
    <TouchableOpacity
      style={[styles.container, props.containerStyle]}
      onPress={props.onPress}
    >
      <View style={styles.imageViewContainer}>
        <Image
          style={styles.jobOfferImage}
          source={jobOffer.cover_picture ? { uri: jobOffer.cover_picture } : defaultBusinessHeader}
        />
        {
          jobOffer.new_registrations > 0 &&
          <View style={styles.newRegistrationsContainer}>
            <Text style={styles.newRegistrations}>+{jobOffer.new_registrations}</Text>
          </View>
        }
      </View>
      <View style={styles.jobOfferTextContainer}>
        {
          jobOffer.visibility_mode === 'publish' &&
          <Text style={styles.jobOfferSubtitle}>{`${jobOffer.nb_registrations} candidatures - ${jobOffer.view_counter} visites`}</Text>
        }
        <Text
          style={styles.jobOfferTitle}
          numberOfLines={2}
          ellipsizeMode="tail"
        >{jobOffer.title}</Text>
        <Text style={styles.jobOfferSubtitle}>{`${jobOffer.nb_registrations} candidatures - ${jobOffer.view_counter} visites`}</Text>
      </View>
      <Icon
        type="entypo"
        name="chevron-thin-right"
        color="#afafaf"
        size={20}
        style={styles.icon}
      />
    </TouchableOpacity>
  );
};

export default JobOfferSmallCard;

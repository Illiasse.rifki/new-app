// @flow
import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { theme } from '../../config/constants/index';

type Props = {
  containerStyle: {},
  referenceKeys: [{
    key: string,
    value: string,
  }],
  limit: number,
  keys: [string]
};

const styles = StyleSheet.create({
  item: {
    fontSize: 14,
    color: theme.gray,
    lineHeight: 20,
  },
});

const ListKeyValueDisplay = (props: Props) => {
  let itemDisplayCounter = 0;
  const limit = props.limit ? props.limit : 999999;
  return (<View style={props.containerStyle}>
    {
      props.referenceKeys.map((item) => {
        if (props.keys.indexOf(item.key) !== -1 && itemDisplayCounter < limit) {
          itemDisplayCounter += 1;
          return (
            <Text
              key={item.key}
              style={styles.item}
            >
              - {item.value}
            </Text>
          );
        }
        return null;
      })
    }
    {
      itemDisplayCounter >= limit ? (
        <Text style={styles.item}>
          ...
        </Text>
      ) : null
    }
  </View>);
};

export default ListKeyValueDisplay;

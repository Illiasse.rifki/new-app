// import React, { Component } from 'react';
// import { View, StyleSheet, FlatList, TouchableHighlight } from 'react-native';
// import { graphql } from 'react-apollo';

// import LoadingComponent from './LoadingComponent';
// import ErrorComponent from './ErrorComponent';

// const styles = StyleSheet.create({
//   itemContainer: {
//     borderBottomWidth: 1,
//     borderColor: '#afafaf',
//     paddingLeft: 7,
//     paddingRight: 7,
//     paddingTop: 5,
//     paddingBottom: 5,
//   },
//   container: {
//     borderBottomWidth: 1,
//     borderLeftWidth: 1,
//     borderRightWidth: 1,
//     borderColor: '#afafaf',
//     borderBottomLeftRadius: 4,
//     borderBottomRightRadius: 4,
//     shadowColor: 'rgba(0, 0, 0, 0.08)',
//     shadowOffset: { width: 0.5, height: 4 },
//     shadowOpacity: 0.8,
//     shadowRadius: 2,
//   },
//   loader: {
//     paddingTop: 8,
//     paddingBottom: 8,
//     paddingLeft: 8,
//     paddingRight: 8,
//     minHeight: 0,
//     justifyContent: 'flex-start',
//   },
//   loaderText: {
//     color: '#afafaf',
//     fontSize: 12,
//     fontStyle: 'italic',
//     marginLeft: 10,
//   },
// });

// class Suggestions extends Component {
//   state = {};

//   props: {
//     value: string,
//     excludes: [{
//       user_id: string,
//     }],
//     show: boolean,
//     data: {
//       loading: boolean,
//       error: any,
//       search: Array<{
//         name: string,
//       }>,
//     },
//     renderItem: Function,
//     onPressItem: Function,
//     itemContainerStyle: {},
//     keyProp: string,
//     listContainer: {},
//     loaderText: string,
//   };

//   render() {
//     if (!this.props.show || !this.props.data || !this.props.data.search) {
//       return null;
//     }
//     const data = this.props.data.search;
//     return (
//       <View>
//         {
//           this.props.data.loading && <LoadingComponent
//             transparent
//             size={18}
//             type="FadingCircle"
//             containerStyle={[styles.container, this.props.listContainer, styles.loader]}
//             text={this.props.loaderText}
//             textStyle={styles.loaderText}
//           />
//         }
//         {
//           this.props.data.error && <ErrorComponent transparent message="Error during searching"/>
//         }
//         {
//           !this.props.data.loading && data.length > 0 &&
//             <FlatList
//               style={[styles.container, this.props.listContainer]}
//               data={data}
//               keyExtractor={item => item[this.props.keyProp]}
//               keyboardShouldPersistTaps={'always'}
//               renderItem={item => (
//                 <TouchableHighlight style={[styles.itemContainer, this.props.itemContainerStyle]}
//                   onPress={() => {
//                     this.props.onPressItem(item.item, item.index);
//                   }}
//                   underlayColor='white'
//                 >
//                   {this.props.renderItem(item.item)}
//                 </TouchableHighlight>
//               )}
//             />
//         }
//       </View>);
//   }
// }

// export default (query, customOptions = {}) => graphql(query, {
//   options: ownProps => ({
//     variables: {
//       value: ownProps.value,
//     },
//   }),
//   skip: (props) => {
//     if (!props.show) {
//       return true;
//     }
//     if (!(props.value && props.value.length > 1)) {
//       return true;
//     }
//     return false;
//   },
//   ...customOptions,
// })(Suggestions);

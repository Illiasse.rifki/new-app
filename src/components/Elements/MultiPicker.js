import React, { PureComponent } from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import { ListItem, CheckBox } from 'react-native-elements';

type Proposition = {
  label: string,
  name: string
}

const styles = StyleSheet.create({
  container: {
    flex: 0.5,
    minHeight: 100,
    // maxHeight: 200,
  },
});


export default class MultiPicker extends PureComponent {
  static defaultProps = {
    viewStyle: {},
  }

  onCheckBoxPress = (item, checked) => () => {
    const { value } = this.props;
    if (this.props.onSelect) {
      const newValue = checked
        ? value.filter(e => e !== item.name)
        : [...value, item.name];
      this.props.onSelect(newValue);
    }
  }

  isSelected = item => this.props.value.indexOf(item.name) !== -1

  props: {
    propositions: [Proposition],
    onSelect: Function,
    value: [string],
    viewStyle: ?Object
  }

  renderItem = ({ item }: {item: Proposition}) => {
    const checked = this.isSelected(item);
    return (
      <CheckBox
        key={[item.name, item.label].concat()}
        title={item.label}
        checked={checked}
        onPress={this.onCheckBoxPress(item, checked)}/>
    );
  }


  render() {
    const { propositions, viewStyle } = this.props;
    return (
      <View style={[styles.container, viewStyle]}>
        <FlatList
          keyExtractor={item => item.name}
          data={propositions}
          renderItem={this.renderItem} />
      </View>
    );
  }
}

import React, { Component } from 'react';
import { View, StyleSheet, FlatList, Text, TouchableHighlight } from 'react-native';
import Field from './../Design/Field';

const styles = StyleSheet.create({
  itemContainer: {
    borderBottomWidth: 1,
    borderColor: '#afafaf',
    paddingLeft: 7,
    paddingRight: 7,
    paddingTop: 5,
    paddingBottom: 5,
  },
  container: {
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: '#afafaf',
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { width: 0.5, height: 4 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
});

class Autocomplete extends Component {
  static defaultProps = {
    hasToBeInListIf: (item, text) => {
      return item.toLowerCase().match(text);
    },
    renderItem: item => (<Text>{item}</Text>),
    regexText: text => (`([^"]*${text.toLowerCase()}[^"]*)`),
  };

  constructor(props) {
    super(props);
    this.state = {
      showList: false,
    };
  }

  props: {
    data: any,
    label: string,
    labelStyle: string,
    listContainer: {},
    defaultValue: any,
    regexText: Function,
    hasToBeInListIf: Function,
    renderItem: Function,
    keyProp: string,
    itemContainerStyle: {},
    containerStyle: {},
    onChangeText: Function,
    onPressItem: Function,
    onEndEditing: Function,
  };

  getSuggestionList(text) {
    const regexText = this.props.regexText(text);
    return this.props.data.filter(item => (this.props.hasToBeInListIf(item, regexText)));
  }

  toggleList(state) {
    this.setState({ showList: state });
  }

  render() {
    return (
      <View style={this.props.containerStyle}>
        <Field
          label={this.props.label}
          labelStyle={this.props.labelStyle}
          value={this.props.defaultValue}
          onChangeText={(text) => {
            const showList = text !== '';
            const suggestionList = this.getSuggestionList(text);
            this.setState({ suggestionList, showList });
            this.props.onChangeText(text);
          }}
          textInputOptions={{
            onEndEditing: () => {
              this.toggleList(false);
            },
            returnKeyType: 'done',
          }}
        />
        { this.state.showList ? (
          <View>
            <FlatList
              style={[styles.container, this.props.listContainer]}
              data={this.state.suggestionList}
              keyExtractor={item => item[this.props.keyProp]}
              keyboardShouldPersistTaps={'always'}
              renderItem={item => (
                <TouchableHighlight style={[styles.itemContainer, this.props.itemContainerStyle]}
                                    onPress={() => {
                                      this.props.onPressItem(item.item, item.index);
                                      this.toggleList(false);
                                    }}
                                    underlayColor='white'
                >
                  {this.props.renderItem(item.item)}
                </TouchableHighlight>
              )}
            />
          </View>) : null
        }
      </View>
    );
  }
};

export default Autocomplete;

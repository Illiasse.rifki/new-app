// @flow
import React from 'react';
import { View, StyleSheet, Text, Switch } from 'react-native';
import { theme } from '../../config/constants/index';

type Props = {
  inactiveColorBackground?: string,
  activeColorBackground?: string,
  inactiveColor?: string,
  activeColor?: string,
  onPress?: Function,
  label?: string,
  value?: boolean,
  containerStyle: {},
};

const styles = StyleSheet.create({
  background: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    flex: 1,
    fontSize: 18,
    lineHeight: 24,
    color: theme.fontBlack,
  },
});

const FormSwitch = (props: Props) => (
  <View style={[styles.background, props.containerStyle]}>
    <Text style={styles.label}>{props.label}</Text>
    <Switch
      inactiveButtonColor={props.inactiveColor || theme.gray}
      activeButtonColor={props.activeColor || theme.primaryColor}
      inactiveBackgroundColor={props.inactiveColorBackground || '#919191'}
      activeBackgroundColor={props.activeColorBackground || '#21b0b0'}
      onValueChange={props.onPress}
      value={props.value}
      buttonRadius={11}
      switchWidth={35}
      switchHeight={16}
    />
  </View>
);

export default FormSwitch;

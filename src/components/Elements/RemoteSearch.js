import React, { Component } from 'react';
import { View, Text, } from 'react-native';
import gql from 'graphql-tag';

import Field from './../Design/Field';
import suggestionsBuilder from './Suggestions';

class RemoteSearch extends Component {
  static defaultProps = {
    renderItem: item => (<Text>{item}</Text>),
  };

  static companiesQuery = gql`
    query searchCompanies($value: String!) {
      search: searchCompanies(value: $value, limit: 7) {
        company_id
        name
        logo
      }
    }
  `;

  static schoolsQuery = gql`
    query searchSchools($value: String!) {
      search: searchSchools(value: $value, limit: 7) {
        school_id
        name
        logo
      }
    }
  `;

  constructor(props) {
    super(props);
    this.state = {
      showList: false,
    };
    this.Suggestions = suggestionsBuilder(props.query);
  }

  props: {
    data: any,
    label: string,
    labelStyle: string,
    listContainer: {},
    defaultValue: any,
    regexText: Function,
    hasToBeInListIf: Function,
    renderItem: Function,
    keyProp: string,
    itemContainerStyle: {},
    containerStyle: {},
    onChangeText: Function,
    onPressItem: Function,
    onEndEditing: Function,
    loaderText: string,
  };

  toggleList(state) {
    this.setState({ showList: state });
  }

  onChange = (value) => {
    if (this.state.timeout) {
      clearTimeout(this.state.timeout);
    }
    this.setState({
      value,
      timeout: setTimeout(() => {
        this.setState({ valueToSearch: value, show: true, showList: value !== '' });
      }, 300),
    });
    this.props.onChangeText(value);
  };

  render() {
    const { Suggestions } = this;
    return (
      <View style={this.props.containerStyle}>
        <Field
          label={this.props.label}
          labelStyle={this.props.labelStyle}
          value={this.props.defaultValue}
          onChangeText={this.onChange}
          textInputOptions={{
            onEndEditing: () => {
              this.toggleList(false);
            },
            returnKeyType: 'done',
          }}
        />
        <Suggestions
          value={this.state.valueToSearch}
          show={this.state.showList}
          onPressItem={(item, i) => {
            this.props.onPressItem(item, i);
            this.toggleList(false);
          }}
          loaderText={this.props.loaderText}
          listContainer={this.props.listContainer}
          itemContainerStyle={this.props.itemContainerStyle}
          renderItem={this.props.renderItem}
          keyProp={this.props.keyProp}
      />
      </View>
    );
  }
};

export default RemoteSearch;

// @flow
import React from 'react';
import { View, StyleSheet, Text, ActivityIndicator } from 'react-native';
import { pure } from 'recompose';
import { theme } from '../../config/constants/index';

type Props = {
  type?: string,
  transparent: string,
  color: string,
  size: number,
  containerStyle: {},
  text: string,
  textStyle: {},
};

const getStyles = transparent => StyleSheet.create({
  background: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: transparent ? 'transparent' : '#f5f5f5',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 32,
    paddingRight: 32,
    minHeight: 200,
  },
  msg: {
    fontSize: 24,
    lineHeight: 35,
    color: theme.fontBlack,
    textAlign: 'center',
  },
});

const LoadingComponent = (props: Props) => {
  const styles = getStyles(props.transparent);
  return (<View style={[styles.background, props.containerStyle]}>
    <ActivityIndicator size={props.size ? "small" : "large"} color={props.color || theme.primaryColor} />
    {
      props.text && <Text style={props.textStyle}>{props.text}</Text>
    }
  </View>);
};

export default pure(LoadingComponent);

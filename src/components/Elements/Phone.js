// @flow
import React from 'react';
import { StyleSheet } from 'react-native';
import { theme } from '../../config/constants/index';
import Url from '../Content/Url';

type Props = {
  phoneNumber: string,
  display: string,
  containerStyle: {},
  textStyle: {},
};

const phoneIcon = {
  name: 'phone',
    type: 'font-awesome',
    size: 20,
    color: theme.primaryColor,
};

const Phone = (props: Props) => (
  <Url
    icon={phoneIcon}
    containerStyle={props.containerStyle}
    textStyle={props.textStyle}
    name={props.display}
    url={`tel:${props.phoneNumber}`}
  />
);

export default Phone;

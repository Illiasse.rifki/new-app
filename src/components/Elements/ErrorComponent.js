// @flow
import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { theme } from '../../config/constants/index';

type Props = {
  message?: string,
  transparent: boolean,
};

const getStyles = transparent => StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: transparent ? 'transparent' : '#f5f5f5',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 32,
    paddingRight: 32,
  },
  msg: {
    fontSize: 24,
    lineHeight: 35,
    color: theme.fontBlack,
    textAlign: 'center',
  },
});

const ErrorComponent = (props: Props) => {
  const styles = getStyles(props.transparent);
  return (
    <View style={styles.background}>
      <Text style={styles.msg}>{props.message || 'Désolé... une erreur est survenu :('}</Text>
    </View>
  );
};

export default ErrorComponent;

import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableHighlight } from 'react-native';

class MultiButtonGroup extends Component {
  static defaultProps = {
    buttons: [],
  };

  getStyle() {
    return StyleSheet.create({
      container: {
        flex: 1,
        borderWidth: 1,
        flexDirection: 'row',
        borderRadius: 2,
        height: 40,
      },
      textStyle: {
        alignSelf: 'center',
        fontSize: 16,
      },
      textContainer: {
        flex: 1,
        justifyContent: 'center',
        borderRightWidth: this.props.innerBorderStyle.width || 1,
        borderLeftWidth: this.props.innerBorderStyle.width || 1,
        borderColor: this.props.innerBorderStyle.color || 'black',
      },
      selectedTextStyle: {
        color: 'white',
      },
      selectedTextContainer: {
        backgroundColor: this.props.selectedBackgroundColor || 'black',
      },
    });
  }

  props: {
    buttons: [],
    containerStyle: {},
    selectedBackgroundColor: string,
    textStyle: {},
    selectedTextStyle: {},
    selectedIf: Function,
    selectedIndexes: [],
    renderButton: Function,
    keyExtractor: Function,
    onPress: Function,
    innerBorderStyle: {
      width: number,
      color: string,
    },
  };

  render() {
    const styles = this.getStyle();

    return (
      <View style={[styles.container, this.props.containerStyle]}>
        {
          this.props.buttons.map((b) => {
            const textStyle = [styles.textStyle, this.props.textStyle];
            const textContainer = [styles.textContainer];
            if (this.props.selectedIndexes.find(s => this.props.selectedIf(b, s))) {
              textStyle.push(styles.selectedTextStyle);
              textStyle.push(this.props.selectedTextStyle);
              textContainer.push(styles.selectedTextContainer);
            }
            return (
              <TouchableHighlight
                key={this.props.keyExtractor(b)}
                style={textContainer}
                onPress={() => { this.props.onPress(b); }}
                underlayColor={'white'}
              >
                <Text style={textStyle}>
                  {
                    this.props.renderButton ? this.props.renderButton(b) : b
                  }
                </Text>
              </TouchableHighlight>
            );
          })
        }
      </View>
    );
  }
}

export default MultiButtonGroup;

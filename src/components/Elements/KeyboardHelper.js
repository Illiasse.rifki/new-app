import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, Platform, Keyboard, LayoutAnimation, UIManager, TouchableOpacity } from 'react-native';
import { theme } from '../../config/constants';
import { Icon } from '../Pure/react-native-elements';

const styles = {
  main: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 38,
    backgroundColor: 'white',
    zIndex: 5000,
    borderTopWidth: 1,
    borderTopColor: '#e1e1e1',
    flexDirection: 'row',
  },
  left: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 8,
  },
  right: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 8,
  },
  iconStyle: {
    color: '#AFAFAF',
  },
  iconContainer: {
    padding: 8,
  },
  button: {
    color: theme.primaryColor,
  },
};

class KeyboardHelper extends Component {
  static defaultProps = {
    backgroundColor: '#f5f5f5',
    buttonLabel: 'OK',
  };

  constructor(props) {
    super(props);
    this.state = {
      bottom: 0,
    };
    // Enable `LayoutAnimation` for Android.
    if (UIManager.setLayoutAnimationEnabledExperimental) {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  componentDidMount() {
    let keyboardShowEvent = 'keyboardWillShow';
    let keyboardHideEvent = 'keyboardWillHide';

    if (Platform.OS === 'android') {
      keyboardShowEvent = 'keyboardDidShow';
      keyboardHideEvent = 'keyboardDidHide';
    }

    this.keyboardShowListener = Keyboard.addListener(keyboardShowEvent, e => this.keyboardShow(e));
    this.keyboardHideListener = Keyboard.addListener(keyboardHideEvent, e => this.keyboardHide(e));
  }

  componentWillUnmount() {
    this.keyboardShowListener.remove();
    this.keyboardHideListener.remove();
  }

  keyboardShow(e) {
    if (this.state.show) return;
    this.setState({ show: true });

    setTimeout(() => {
      LayoutAnimation.easeInEaseOut();
      this.setState({
        bottom: Platform.OS !== 'ios' ? 0 : e.endCoordinates.height,
      });
    }, 100);
  }

  keyboardHide() {
    if (!this.state.show) return;
    setTimeout(() => {
      LayoutAnimation.easeInEaseOut();
      this.setState({
        bottom: 0,
        show: false,
      });
    }, 100);
  }

  prevRef = () => {
    const { inputRefs, currentRef } = this.props;
    const findRef = inputRefs.findIndex(r => r === currentRef);
    if (findRef > 0) {
      const prevRef = inputRefs[findRef - 1];
      if (prevRef) prevRef.focus();
    }
  };

  nextRef = () => {
    const { inputRefs, currentRef } = this.props;
    const findRef = inputRefs.findIndex(r => r === currentRef);
    if (findRef < (inputRefs.length - 1)) {
      const nextRef = inputRefs[findRef + 1];
      if (nextRef) nextRef.focus();
    }
  };

  isLastRef = () => {
    const { inputRefs } = this.props;
    if (!inputRefs) return true;
    const findRef = inputRefs.findIndex(r => r === this.props.currentRef);
    return findRef === (inputRefs.length - 1);
  };

  props: {
    backgroundColor: string,
    navigationButton: any,
  };

  render() {
    const { backgroundColor, inputRefs, navigationButton } = this.props;
    const { bottom } = this.state;
    if (!this.state.show) return null;
    const isLastRef = this.isLastRef();
    return (
      <View style={[styles.main, { backgroundColor, bottom }]} >
        {
          inputRefs && inputRefs.length > 0 &&
          <View style={styles.left}>
            <TouchableOpacity style={styles.iconContainer} onPress={this.prevRef}>
              <Icon iconStyle={styles.iconStyle} name="keyboard-arrow-up" type="material-icons" />
            </TouchableOpacity>
            <TouchableOpacity style={styles.iconContainer} onPress={this.nextRef}>
              <Icon iconStyle={styles.iconStyle} name="keyboard-arrow-down" type="material-icons" />
            </TouchableOpacity>
          </View>
        }
        <TouchableOpacity
          onPress={isLastRef ? navigationButton.onPress : this.nextRef}
          style={styles.right}
        >
          <Text style={styles.button}>{isLastRef ? navigationButton.label : 'Suivant'}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  inputRefs: state.keyboardHelper.inputRefs,
  currentRef: state.keyboardHelper.currentRef,
  navigationButton: state.keyboardHelper.navigationButton,
});

export default connect(mapStateToProps)(KeyboardHelper);

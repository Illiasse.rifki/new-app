import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import Info from '../Info';

describe('Info Modal', () => {
  const tree = renderer.create(<Info />);

  test('render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});

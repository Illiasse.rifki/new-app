import styled from 'styled-components';

const Dot = styled.View`
  background-color: transparent;
  width: 13px;
  height: 13px;
  border-radius: 7px;
  border-width: 1px;
  border-color: white;
  margin-left: 10px;
  margin-right: 10px;
`;

const ActiveDot = styled(Dot)`
  background-color: white;
`;

export default Dot;
export { Dot, ActiveDot };

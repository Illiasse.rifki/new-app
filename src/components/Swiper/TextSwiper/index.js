import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Swiper from 'react-native-swiper';

// blocks
import { ActiveDot, Dot } from './Dots';
import Element from './Element';

type Props = {
  texts: [string],
};

const TextSwiper = (props: Props) => {
  return (
    <Swiper
      autoplay
      autoplayTimeout={6}
      dot={<Dot />}
      activeDot={<ActiveDot />}
    >
      {
        props.texts.map(text => (
          <Element key={text.trim().substring(0, 6)}>
            <Element.Text>
              {text}
            </Element.Text>
          </Element>
        ))
      }
    </Swiper>);
};

export default TextSwiper;
import styled from 'styled-components';

const Element = styled.View`
  flex: 1;
  align-items: center;
  padding: 0px 15px;
`;

const Text = styled.Text`
  color: white;
  text-align: center;
  font-family: System;
  font-size: 15px;
  line-height: 24px;
`;

Element.Text = Text;

export default Element;
// @flow
import React, { Component } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { FormInput, Icon, List } from 'react-native-elements';

import { theme } from '../../config/constants/index';
import EventCandidateCard from '../GQLLinked/EventCandidateCard';
import SearchField from '../Design/SearchField';
import EmptyCard from '../Card/EmptyCard';
import { FiltersButton } from '../../screens/candidates/Filters';

const styles = StyleSheet.create({
  list: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
  },
  container: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    height: '100%',
  },
  title: {
    paddingLeft: 32,
    paddingRight: 32,
    lineHeight: 24,
    fontSize: 18,
    fontWeight: '500',
    color: theme.fontBlack,
  },
  titleView: {
    marginTop: 0,
    marginBottom: 7,
  },
  listItem: {
    borderBottomColor: '#d8d8d8',
  },
  lastListItem: {
    borderBottomColor: 'transparent',
  },
  listItemTitle: {
    fontSize: 16,
    color: theme.fontBlack,
    width: 302,
    lineHeight: 20,
  },
  listItemTitleSelected: {
    color: theme.primaryColor,
    width: 302,
    fontSize: 16,
    lineHeight: 20,
  },
  selectAll: {
    borderBottomColor: 'transparent',
  },
  selectAllContainer: {
    marginTop: 28,
  },
  searchStyle: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
    // justifyContent: 'flex-start',

  },
});

class CandidatesList extends Component {
  static defaultProps = {
    candidates: [],
    enableSearch: true,
  };

  props: {
    containerStyle: {},
    emptyText: string,
    arriveDateMarker: ?boolean,
    enableSearch: boolean,
    value: any,
    onPress: Function,
    candidates: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      search: '',
    };
  }


  searchCandidates = (text, crs) => {
    const regexText = `([^"]*${text.toLowerCase()}[^"]*)`;
    return crs.filter(({ candidate }) => candidate && (
      candidate.fullname
        .toLowerCase()
        .match(regexText)
      || candidate.headline
        .toLowerCase()
        .match(regexText)
    )).sort((a, b) => (a.candidate.lastname < b.candidate.lastname ? -1 : 1));
  };

  candidateKeyExtractor = cr => cr.candidate.user_id;

  renderCandidate = ({ item: c }) => (
    <EventCandidateCard
      arriveDateMarker={this.props.arriveDateMarker}
      arriveDate={c.arrive_date}
      picture={c.candidate.picture}
      pictureTitle={`${c.candidate.lastname[0]} ${c.candidate.firstname[0]}`}
      onPress={() => this.props.onPress(c)}
      title={`${c.candidate.lastname.toUpperCase()} ${c.candidate.firstname.toLowerCase().replace(/(?:^|\s)\S/g, a => a.toUpperCase())}`}
      subtitle={c.candidate.headline}
      event_registration_info_id={c.info ? c.info.event_registration_info_id : null}
      userId={c.candidate.user_id}
    />);

  render() {
    return <View style={[styles.container, this.props.containerStyle]}>
      <View style={styles.searchStyle}>
        {/* {
          this.props.enableSearch &&
        <FiltersButton />
        } */}
        {
          this.props.enableSearch &&
        <SearchField
          loading={this.state.loading}
          placeholder="Recherche par nom, prénom, intitulés…"
          value={this.state.search}
          onChangeText={search => this.setState({ search })}
        />
        }
      </View>
      <List containerStyle={styles.list}>
        <FlatList
          ListEmptyComponent={
            <EmptyCard
              title={this.props.emptyText || 'Aucun candidat pour ces critères'}
            />
          }
          data={this.searchCandidates(this.state.search, this.props.candidates)}
          keyExtractor={this.candidateKeyExtractor}
          renderItem={data => this.renderCandidate(data)}
        />
      </List>
    </View>;
  }
}

export default CandidatesList;

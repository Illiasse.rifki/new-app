import React from 'react';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { View, StyleSheet } from 'react-native';
import { googleAPIKEY, theme } from '../../config/constants/index';

type Props = {
  containerStyle: {},
  searchRef: Function,
  onPress: Function,
  onChangeText: Function,
  value: any,
};

const getStyle = () => {
  return StyleSheet.create({
    container: {
      flex: 1,
    },
  });
};

const GeolocalisationField = (props: Props) => {
  const styles = getStyle();
  return (
    <View style={[styles.container, props.containerStyle]}>
      <GooglePlacesAutocomplete
        ref={props.searchRef}
        placeholder='Ville (Ex: Paris, Lyon...)'
        onPress={props.onPress}
        getDefaultValue={props.value}
        minLength={2}
        fetchDetails
        query={{
          key: googleAPIKEY,
          language: 'fr',
          types: '(cities)',
        }}
        listViewDisplayed='true'
        enablePoweredByContainer={false}
        textInputProps={{
          autoCorrect: false,
          onChangeText: props.onChangeText,
        }}
        styles={{
          container: {
            flex: 1,
          },
          textInputContainer: {
            marginTop: 8,
            borderTopWidth: 0,
            borderBottomWidth: 0,
            backgroundColor: 'white',
          },
          textInput: {
            backgroundColor: 'transparent',
            marginLeft: 0,
            marginRight: 0,
            marginTop: 0,
            marginBottom: 0,
            paddingBottom: 0,
            fontSize: 16,
            color: theme.fontBlack,
            paddingLeft: 0,
            paddingRight: 0,
            borderRadius: 0,
            borderTopWidth: 0,
            borderBottomWidth: 1,
            borderBottomColor: '#c9c9c9',
          },
          listView: {
            zIndex: 1000,
            marginTop: -14,
            backgroundColor: 'white',
            borderLeftWidth: 1,
            borderRightWidth: 1,
            borderBottomWidth: 1,
            borderColor: '#c9c9c9',

          },
        }}
        returnKeyType='done'

      />
    </View>
  );
};

export default GeolocalisationField;

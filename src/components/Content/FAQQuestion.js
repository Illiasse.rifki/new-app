// @flow
import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import { theme } from '../../config/constants/index';

const styles = StyleSheet.create({
  faqElementContainer: {
    borderBottomWidth: 1,
    borderBottomColor: '#afafaf',
    backgroundColor: 'white',
    paddingLeft: 32,
    paddingTop: 10,
    paddingRight: 16,
    paddingBottom: 8.5,
  },
  faqElementHeader: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  faqElementText: {
    fontSize: 14,
    flex: 1,
    color: '#1c1c1c',
    lineHeight: 24,
    textAlign: 'left',
    fontWeight: '500',
  },
  faqElementTextActive: {
    fontSize: 14,
    flex: 1,
    color: theme.primaryColor,
    lineHeight: 24,
    textAlign: 'left',
    fontWeight: '500',
  },
  faqElementIcon: { marginLeft: 16 },
  faqElementTextBottom: {
    marginTop: 6,
    marginBottom: 3,
    lineHeight: 20,
    color: '#1c1c1c',
    fontSize: 14,
  },
});

class FAQQuestion extends Component {
  props: {
    question: string,
    answer: string,
    containerStyle: {},
    textStyle: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      visibility: false,
    };
  }

  render() {
    const { visibility } = this.state;
    return (<TouchableOpacity
      style={[styles.faqElementContainer, this.props.containerStyle]}
      onPress={() => this.setState({ visibility: !this.state.visibility })}
    >
      <View style={styles.faqElementHeader}>
        <Text style={[visibility ? styles.faqElementTextActive :
          styles.faqElementText, this.props.textStyle]}>
          {this.props.question}
        </Text>
        <Icon
          iconStyle={styles.faqElementIcon}
          color="#afafaf"
          size={20}
          type="entypo"
          name={visibility ? 'chevron-thin-up' : 'chevron-thin-down'}
        />
      </View>
      {visibility ? (
        <Text style={styles.faqElementTextBottom}>
          {this.props.answer}
        </Text>
      ) : null}
    </TouchableOpacity>);
  }
}

export default FAQQuestion;

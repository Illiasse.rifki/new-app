import React from 'react';
import DatePicker from 'react-native-datepicker';
import { View, StyleSheet, Platform } from 'react-native';
import { theme } from '../../config/constants/index';

type Props = {
  containerStyle: {},
  onPress: Function,
  minDate: string,
  maxDate: string,
  onDateChange: Function,
  placeholder: string,
  date: string,
  value: any,
};

const getStyle = () => {
  return StyleSheet.create({
    container: {
      flex: 1,
    },
    birthContainer: {
      width: '100%',
      marginTop: 8,
      marginLeft: 0,
      marginRight: 0,
    },
  });
};

const CustomDatePicker = (props: Props) => {
  const styles = getStyle();
  return (
    <View style={[styles.container, props.containerStyle]}>
      <DatePicker
        placeholder={props.placeholder}
        style={styles.birthContainer}
        onDateChange={props.onDateChange}
        date={props.date}
        format='DD-MM-YYYY'
        minDate={props.minDate}
        maxDate={props.maxDate}
        confirmBtnText='Confirmer'
        cancelBtnText='Annuler'
        customStyles={{
          dateInput: {
            borderTopColor: 'transparent',
            borderLeftColor: 'transparent',
            borderRightColor: 'transparent',
            borderRightWidth: 0,
            borderLeftWidth: 0,
            borderBottomColor: Platform.OS === 'ios' ? '#c9c9c9' : '#979797',
          },
          dateText: {
            fontSize: 16,
            width: '100%',
            color: theme.fontBlack,
            marginBottom: -8,
            textAlign: 'left',
          },
          placeholderText: {
            fontSize: 16,
            width: '100%',
            marginBottom: -8,
            textAlign: 'left',
          },
        }}
        showIcon={false}
      />
    </View>
  );
};

export default CustomDatePicker;

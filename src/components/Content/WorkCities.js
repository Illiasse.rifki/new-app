import React from 'react';
import { View } from 'react-native';
import UserWorkCity from '../GQLLinked/UserWorkCity';

type Props = {
  containerStyle: {},
  searchCities: [],
  onPress: Function,
  onAddCity: Function,
  onDeleteCity: Function,
  searchRef: Function,
  userId: string,
};

const WorkCities = (props: Props) => (
  <View style={[props.containerStyle]}>
    {
      props.searchCities.map((city, i) => (
        <UserWorkCity
          key={city.work_city_id}
          userId={props.userId}
          workCityId={city.work_city_id}
          onDeleteCity={() => {
            if (props.onDeleteCity) {
              props.onDeleteCity(city, i);
            }
          }}
        />))
    }
    <UserWorkCity
      userId={props.userId}
      searchRef={props.searchRef}
      onAddCity={props.onAddCity}
    />
  </View>
);

export default WorkCities;

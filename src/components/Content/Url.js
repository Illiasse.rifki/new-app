// @flow
import React, { Component } from 'react';
import { Icon } from 'react-native-elements';
import {
  Text,
  Linking,
  StyleSheet,
  View,
  TouchableOpacity,
} from 'react-native';
import { theme } from '../../config/constants/index';

const styles = StyleSheet.create({
  url: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  urlText: {
    color: theme.primaryColor,
  },
  iconView: {
    marginRight: 8,
  },
});

class Url extends Component {
  props: {
    containerStyle: {},
    textStyle: {},
    url: string,
    name: string,
    icon: {},
    children: any,
  };

  handleUrlPressed = () => {
    Linking.openURL(this.props.url);
  };

  render() {
    return (
      <TouchableOpacity onPress={this.handleUrlPressed}>
        {
          this.props.children ? this.props.children :
            (<View style={[styles.url, this.props.containerStyle]}>
              {
                this.props.icon &&
                <View style={styles.iconView}>
                  <Icon
                    {...this.props.icon}
                  />
                </View>
              }
              <Text style={[styles.urlText, this.props.textStyle]}>
                {this.props.name || this.props.url}
              </Text>
            </View>)
        }
      </TouchableOpacity>
    );
  }
}

export default Url;

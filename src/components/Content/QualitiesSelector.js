// @flow
import React from 'react';
import { View, StyleSheet, TouchableHighlight, Text } from 'react-native';
import { Icon } from 'react-native-elements';
import { theme } from '../../config/constants/index';
import { qualities } from './Qualities';

type Props = {
  selectedQualities?: [string],
  onSelect: Function,
  containerStyle: {},
};

const styles = StyleSheet.create({
  qualitiesContainer: {
    marginTop: 2.5,
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    justifyContent: 'space-around',
    flexDirection: 'row',
    paddingBottom: 36,
  },
  qualityTextStyle: {
    fontSize: 16,
    color: theme.gray,
    marginTop: 10,
  },
  qualityTextStyleSelected: {
    fontSize: 16,
    color: 'black',
    marginTop: 10,
  },
  qualityIconContainerStyle: {
    width: 88,
    height: 88,
    borderColor: theme.gray,
    borderWidth: 2,
    borderRadius: 10,
    borderStyle: 'dashed',
    padding: 10.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  qualityIconContainerStyleSelected: {
    width: 88,
    height: 88,
    borderColor: theme.green,
    borderWidth: 2,
    borderRadius: 10,
    backgroundColor: theme.green,
    borderStyle: 'solid',
    padding: 10.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  qualityContainerStyle: {
    marginTop: 16,
    padding: 6,
    width: '100%',
    alignItems: 'center',
  },
  qualityView: {
    width: '45%',
  },
});

const isQualitySelected = (selectedQualities, quality) => {
  return selectedQualities.indexOf(quality) !== -1;
};

const QualitiesSelector = (props: Props) => (
  <View style={[styles.qualitiesContainer, props.containerStyle]}>
    {
      qualities.map(quality => (
        <TouchableHighlight
          underlayColor='#FFFFFF'
          key={quality.key}
          style={styles.qualityView}
          onPress={() => { props.onSelect(quality.key); }}
        >
          <View style={styles.qualityContainerStyle} >
            <Icon
              name={quality.icon.name}
              type={quality.icon.type}
              size={61}
              color={isQualitySelected(props.selectedQualities, quality.key) ?
                '#ffffff' :
                '#afafaf'}
              containerStyle={isQualitySelected(props.selectedQualities, quality.key) ?
                styles.qualityIconContainerStyleSelected :
                styles.qualityIconContainerStyle}
            />
            <Text style={isQualitySelected(props.selectedQualities, quality.key) ?
              styles.qualityTextStyleSelected :
              styles.qualityTextStyle}
            >
              {quality.value}
            </Text>
          </View>
        </TouchableHighlight>
      ))
    }
  </View>
);

export default QualitiesSelector;

// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Badge } from 'react-native-elements';
import { theme, tags } from '../../config/constants/index';

type Props = {
  tags?: [string],
  hasTags?: [string],
  emptyComponent: any,
  containerStyle: {},
};

const styles = StyleSheet.create({
  tags: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  tagView: {
    width: '47%',
  },
  tagTextStyle: {
    fontSize: 16,
    color: theme.fontBlack,
  },
  tagContainerStyle: {
    marginTop: 16,
    backgroundColor: '#f5f5f5',
    width: '100%',
    height: 40,
  },
  selectedTagTextStyle: {
    fontSize: 16,
    color: 'white',
  },
  selectedTagContainerStyle: {
    marginTop: 16,
    backgroundColor: theme.green,
    width: '100%',
    height: 40,
  },
});

const Tags = (props: Props) => (
  <View style={[styles.tags, props.containerStyle]}>
    {
      (!props.tags || props.tags.length === 0) && props.emptyComponent
    }
    {
      props.tags && props.tags.length > 0 && props.tags.map((tag) => {
        let tagContainerStyleVar = styles.selectedTagContainerStyle;
        let tagTextStyleVar = styles.selectedTagTextStyle;
        if (props.hasTags && props.hasTags.indexOf(tag) === -1) {
          tagContainerStyleVar = styles.tagContainerStyle;
          tagTextStyleVar = styles.tagTextStyle;
        }
        return (
          <View style={styles.tagView}
            key={tag}
          >
            <Badge
              value={tags.find(t => t.key === tag).value}
              textStyle={tagTextStyleVar}
              containerStyle={tagContainerStyleVar}
            />
          </View>
        );
      })
    }
  </View>
);

export { tags };
export default Tags;

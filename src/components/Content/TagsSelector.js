// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Badge } from 'react-native-elements';
import { theme } from '../../config/constants/index';
import { tags } from './Tags';

type Props = {
  selectedTags?: [string],
  onSelect: Function,
  containerStyle: {},
};

const styles = StyleSheet.create({
  tagsContainer: {
    marginTop: 2.5,
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  tagTextStyle: {
    fontSize: 16,
    color: theme.fontBlack,
  },
  tagView: {
    width: '48%',
  },
  tagTextStyleSelected: {
    fontSize: 16,
    color: 'white',
  },
  tagContainerStyle: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 16,
    backgroundColor: '#f5f5f5',
    width: '100%',
    height: 40,
  },
  tagContainerStyleSelected: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 16,
    backgroundColor: theme.green,
    width: '100%',
    height: 40,
  },
});

const isTagSelected = (selectedTags, tag) => {
  return selectedTags.indexOf(tag) !== -1;
};

const TagsSelector = (props: Props) => (
  <View style={[styles.tagsContainer, props.containerStyle]}>
    {
      tags.map(tag => (
        <View style={styles.tagView}
          key={tag.key}
        >
          <Badge
            value={tag.value}
            textStyle={isTagSelected(props.selectedTags, tag.key) ?
              styles.tagTextStyleSelected :
              styles.tagTextStyle}
            containerStyle={isTagSelected(props.selectedTags, tag.key) ?
              styles.tagContainerStyleSelected :
              styles.tagContainerStyle}
            onPress={() => { props.onSelect(tag.key); }}
          />
        </View>
      ))
    }
  </View>
);

export default TagsSelector;

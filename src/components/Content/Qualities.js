// @flow
import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Icon } from 'react-native-elements';
import { theme, qualities } from '../../config/constants/index';

type Props = {
  qualities?: [string],
  containerStyle: {},
  emptyComponent: any,
  hasQualities: [string],
};

const styles = StyleSheet.create({
  qualities: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  qualityContainer: {
    marginTop: 16,
    padding: 6,
    width: '45%',
    alignItems: 'center',
  },
  qualityTextStyle: {
    fontSize: 16,
    color: '#afafaf',
    marginTop: 10,
  },
  selectedQualityTextStyle: {
    fontSize: 16,
    color: theme.fontBlack,
    marginTop: 10,
  },
  qualityIconContainerStyle: {
    width: 88,
    height: 88,
    borderColor: '#afafaf',
    borderWidth: 2,
    borderRadius: 50,
    backgroundColor: theme.secondaryColor,
    borderStyle: 'dashed',
    padding: 10.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  selectedQualityIconContainerStyle: {
    width: 88,
    height: 88,
    borderColor: theme.green,
    borderWidth: 2,
    borderRadius: 10,
    backgroundColor: theme.green,
    borderStyle: 'solid',
    padding: 10.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const Qualities = (props: Props) => ((!props.qualities || props.qualities.length === 0) ?
    (props.emptyComponent || null) :
    <View style={[styles.qualities, props.containerStyle]}>
      {
        props.qualities && props.qualities.map((qualityKey) => {
          const quality = qualities.find(q => q.key === qualityKey) || {
            key: qualityKey,
            icon: {
              name: 'md-compass',
              type: 'ionicon',
            },
            value: qualityKey,
          };
          let qualityIconContainerStyleVar = styles.selectedQualityIconContainerStyle;
          let qualityTextStyleVar = styles.selectedQualityTextStyle;
          let qualityIconColor = 'white';
          if (props.hasQualities && props.hasQualities.indexOf(quality.key) === -1) {
            qualityIconContainerStyleVar = styles.qualityIconContainerStyle;
            qualityTextStyleVar = styles.qualityTextStyle;
            qualityIconColor = '#afafaf';
          }
          return (
            <View key={quality.key} style={styles.qualityContainer}>
              <Icon
                name={quality.icon.name}
                type={quality.icon.type}
                size={61}
                color={qualityIconColor}
                containerStyle={qualityIconContainerStyleVar}
              />
              <Text style={qualityTextStyleVar}>{quality.value}</Text>
            </View>
          );
        })
      }
    </View>);

export { qualities };
export default Qualities;

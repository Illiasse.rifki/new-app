// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { theme, companySize } from '../../config/constants/index';
import MultiButtonGroup from '../Elements/MultiButtonGroup';

type Props = {
  containerStyle: {},
  selectedSizes: [],
  onCompanySizesChange: Function,
};

const styles = StyleSheet.create({
  companySizeButtonsStyle: {
    marginTop: 10,
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 0,
    backgroundColor: theme.secondaryColor,
    borderColor: '#afafaf',
  },
});

const selectCompanySize = (selectedIndex, oldIndexes) => {
  const index = oldIndexes.indexOf(selectedIndex);
  const newCompanySize = oldIndexes.slice();
  if (index === -1) {
    newCompanySize.push(selectedIndex);
  } else {
    newCompanySize.splice(index, 1);
  }
  return newCompanySize;
};

const CompanySizesPicker = (props: Props) => (
  <View style={props.containerStyle}>
    <MultiButtonGroup
      buttons={companySize}
      containerStyle={styles.companySizeButtonsStyle}
      onPress={(selectedIndex) => {
        const newSizes = selectCompanySize(selectedIndex, props.selectedSizes);
        props.onCompanySizesChange(newSizes);
      }}
      selectedIndexes={props.selectedSizes}
      selectedIf={(button, selected) => button.value === selected.value}
      selectedBackgroundColor={theme.primaryColor}
      renderButton={b => b.text}
      keyExtractor={b => b.value}
      textStyle={{ color: theme.fontBlack, fontWeight: '400' }}
      innerBorderStyle={{ color: 'transparent' }}
      selectedTextStyle={{ color: theme.secondaryColor }}
    />
  </View>
);

export default CompanySizesPicker;

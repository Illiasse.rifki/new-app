// @flow
import React, { Component } from 'react';
import { StyleSheet, Image, Text, View, TouchableOpacity, } from 'react-native';
import { Callout, Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import ClusteredMapView from 'react-native-maps-super-cluster';

import { mapStyle, sectors, theme } from '../../config/constants';
import markerImg from '../../assets/marker.png';

const INIT_REGION = {
  latitude: 41.8962667,
  longitude: 11.3340056,
  latitudeDelta: 20,
  longitudeDelta: 20,
};

const styles = StyleSheet.create({
  map: {
    flex: 1,
  },
  marker: {
    width: 34.6, // 104
    height: 43.6, // 131
  },
  clusterContainer: {
    width: 32,
    height: 32,
    borderWidth: 1,
    borderRadius: 20,
    borderColor: theme.primaryColor,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.primaryColor,
  },
  clusterText: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
  },
  callout: {
    paddingTop: 4,
    paddingBottom: 4,
    width: 250,
  },
  jobTitle: {
    fontWeight: '600',
    fontSize: 18,
  },
  jobContract: {
    marginTop: 4,
    fontSize: 11,
    fontWeight: '100',
  },
  checkOffer: {
    color: theme.primaryColor,
    fontSize: 12,
    marginTop: 6,
  },
  sectors: {
    marginTop: 4,
  },
  sector: {
    color: '#afafaf',
    fontSize: 10,
    lineHeight: 14,
    marginRight: 4,
  },
  elementCluster: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    marginBottom: 4,
    minHeight: 20,
  },
  clusterJobTitleContainer: {
    flex: 5,
  },
  clusterJobTitle: {
  },
  clusterJobContractContainer: {
    flex: 1,
  },
  clusterJobContract: {
    fontWeight: 'bold',
    color: theme.primaryColor,
  },
});

class Map extends Component {
  state = {
    jobs: [],
  };

  props: {
    jobs: any,
    width: number,
    navigate: Function,
  };

  componentDidMount() {
    const { props } = this;
    if (props.jobs.length) {
      this.setState({ jobs: props.jobs });
      this.clusterMap.getMapRef().fitToCoordinates(
        props.jobs.map(j => ({ latitude: j.location.latitude, longitude: j.location.longitude })),
        {
          edgePadding: { top: 20, right: 20, bottom: 20, left: 20 },
        });
    }
  }

  componentWillReceiveProps(props) {
    if (props.jobs.length) {
      this.setState({ jobs: props.jobs });
      this.clusterMap.getMapRef().fitToCoordinates(
        props.jobs.map(j => ({ latitude: j.location.latitude, longitude: j.location.longitude })),
        {
          edgePadding: { top: 20, right: 20, bottom: 20, left: 20 },
        });
    }
  }

  renderMarker = job => (
    <Marker
      key={job.job_offer_id}
      coordinate={job.location}
      title={job.title}
      onPress={() => {
        this.clusterMap.getMapRef().animateToCoordinate(job.location, 300);
      }}
    >
      <Image resizeMode='cover' style={styles.marker} source={markerImg} />
      <Callout
        onPress={() => {
          this.props.navigate('JobOffer', { jobOfferId: job.job_offer_id });
        }}
      >
        <View
          style={styles.callout}
        >

          <Text numberOfLines={2} style={styles.jobTitle}>{job.title}</Text>
          <Text style={styles.sectors}>
            {
              job.sectors && job.sectors.map((s, i) => {
                const sector = sectors.find(f => f.key === s);
                return (
                  <Text key={s} style={styles.sector}>
                    {sector.value || s}{(i !== job.sectors.length - 1) && ','}
                  </Text>
                );
              })
            }
          </Text>
          {
            job.contract_type && <Text numberOfLines={1} style={styles.jobContract}>{`TYPE: ${job.contract_type.toUpperCase()}`}</Text>

          }
          <Text style={styles.checkOffer}>{'> Voir Offre'}</Text>
        </View>
      </Callout>
    </Marker>
  );

  renderCluster = (cluster) => {
    const { pointCount, coordinate, clusterId } = cluster;
    const clusteringEngine = this.clusterMap.getClusteringEngine();
    const clusteredPoints = clusteringEngine.getLeaves(clusterId, 100);
    return (<Marker
      identifier={`cluster-${clusterId}`}
      coordinate={coordinate}
      onPress={() => {
        this.clusterMap.getMapRef().animateToCoordinate(coordinate, 300);
      }}
    >
      <View style={styles.clusterContainer}>
        <Text style={styles.clusterText} >{pointCount}</Text>
      </View>
      <Callout>
        <View style={styles.callout}>
          {
            clusteredPoints && clusteredPoints.map(({ properties: { item: job } }) => (
              <View style={styles.elementCluster} key={job.job_offer_id}>
                {job.contract_type && <View style={styles.clusterJobContractContainer}>
                  <Text style={styles.clusterJobContract}>{job.contract_type.toUpperCase()}</Text>
                </View>}
                <View style={styles.clusterJobTitleContainer}>
                  <Text numberOfLines={2} style={styles.clusterJobTitle}>{job.title}</Text>
                </View>
              </View>
            ))
          }
        </View>
      </Callout>
    </Marker>);
  };

  render() {
    return (
      <ClusteredMapView
        initialRegion={INIT_REGION}
        width={this.props.width}
        ref={(ref) => { this.clusterMap = ref; }}
        data={this.state.jobs}
        provider={PROVIDER_GOOGLE}
        customMapStyle={mapStyle}
        style={styles.map}
        renderMarker={this.renderMarker}
        renderCluster={this.renderCluster}
      />);
  }
}

export default Map;

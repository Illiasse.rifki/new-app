// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Badge } from 'react-native-elements';
import { theme } from '../../config/constants/index';

const contrats = ['cdd', 'cdi', 'ctt'];

type Props = {
  selectedContrats?: [string],
  onSelect: Function,
  containerStyle: {},
};

const styles = StyleSheet.create({
  contratsContainer: {
    marginTop: 2.5,
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  contratTextStyle: {
    fontSize: 16,
    color: theme.fontBlack,
  },
  contratView: {
    width: '48%',
  },
  contratTextStyleSelected: {
    fontSize: 16,
    color: 'white',
  },
  contratContainerStyle: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 16,
    backgroundColor: '#f5f5f5',
    width: '100%',
    height: 40,
  },
  contratContainerStyleSelected: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 16,
    backgroundColor: theme.green,
    width: '100%',
    height: 40,
  },
});

const isContratSelected = (selectedContrats, contrat) =>
  selectedContrats.indexOf(contrat) !== -1;

const ContratsSelector = (props: Props) => (
  <View style={[styles.contratsContainer, props.containerStyle]}>
    {
      contrats.map(contrat => (
        <View style={styles.contratView}
          key={contrat}
        >
          <Badge
            value={contrat.toUpperCase()}
            textStyle={isContratSelected(props.selectedContrats, contrat) ?
              styles.contratTextStyleSelected :
              styles.contratTextStyle}
            containerStyle={isContratSelected(props.selectedContrats, contrat) ?
              styles.contratContainerStyleSelected :
              styles.contratContainerStyle}
            onPress={() => { props.onSelect(contrat); }}
          />
        </View>
      ))
    }
  </View>
);

export default ContratsSelector;

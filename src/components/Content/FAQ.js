// @flow
import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Icon } from 'react-native-elements';
import { theme } from '../../config/constants/index';
import FAQQuestion from "./FAQQuestion";

type Props = {
  faq: [{
    question: string,
    answer: string,
  }],
  title: string,
  containerStyle: {},
  questionsContainerStyle: {},
  questionContainerStyle: {},
  questionTextStyle: {},
};

const styles = StyleSheet.create({
  faqContainer: {
    backgroundColor: 'white',
    paddingTop: 37,
    paddingBottom: 28,
  },
  faqTitle: {
    paddingLeft: 32,
    paddingRight: 32,
    fontWeight: 'bold',
    fontSize: 28,
    color: theme.fontBlack,
    letterSpacing: 0,
  },
  faqQuestionsContainer: {
    marginTop: 13,
    borderTopWidth: 1,
    borderTopColor: '#afafaf',
  },
});

const FAQ = (props: Props) => (
  <View style={[styles.faqContainer, props.containerStyle]}>
    {
      props.title ? (<Text style={styles.faqTitle}>{props.title}</Text>) : null
    }
    <View style={[styles.faqQuestionsContainer, props.questionsContainerStyle]}>
      {
        props.faq.map(f => (
          <FAQQuestion
            key={f.question}
            question={f.question}
            answer={f.answer}
            containerStyle={props.questionContainerStyle}
            textStyle={props.questionTextStyle}
          />
        ))
      }
    </View>
  </View>
);

export default FAQ;

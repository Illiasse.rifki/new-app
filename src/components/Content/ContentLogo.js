import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';

import ReadMore from 'react-native-read-more-text';

import { theme } from '../../config/constants/index';

import defaultBusiness from '../../assets/defaultBusiness.png';

type Props = {
  containerStyle: {},
  content: {
    image?: string,
    title?: string,
    subtitle?: string,
    thirdLine?: string,
    description?: string,
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 12,
  },
  headerImage: {
    flex: 3,

  },
  headerInfos: {
    flex: 9,
  },
  headerInfosTitle: {
    fontWeight: '500',
    fontSize: 18,
    lineHeight: 24,
    color: theme.fontBlack,
  },
  headerInfosSubtitle: {
    fontSize: 16,
    lineHeight: 16,
    color: theme.fontBlack,
  },
  headerInfosThirdLine: {
    color: theme.gray,
    fontSize: 14,
  },
  description: {
    fontSize: 14,
    lineHeight: 24,
    color: theme.fontBlack,
  },
  image: {
    width: 64,
    height: 64,
    resizeMode: 'contain',
    borderWidth: 1,
    borderColor: '#f5f5f5',
    borderRadius: 4,
  },
  more: {
    fontSize: 16,
    textAlign: 'center',
    fontWeight: '500',
    color: theme.primaryColor,
    marginTop: 8,
  },
});

const ContentLogo = (props: Props) => (
  <View style={[styles.container, props.containerStyle]}>
    <View style={styles.header}>
      <View style={styles.headerImage}>
        <Image source={props.content.image ? { uri: props.content.image } :
          defaultBusiness} style={styles.image} />
      </View>
      <View style={styles.headerInfos}>
        <Text style={styles.headerInfosTitle}>{props.content.title}</Text>
        <Text style={styles.headerInfosSubtitle}>
          {props.content.subtitle}
        </Text>
        <Text style={styles.headerInfosThirdLine}>{props.content.thirdLine}</Text>
      </View>
    </View>
    <View>
      <ReadMore
        numberOfLines={5}
        renderTruncatedFooter={press => <Text style={styles.more} onPress={press}>Voir plus</Text>}
        renderRevealedFooter={press => <Text style={styles.more} onPress={press}>Réduire</Text>}
      >
        <Text style={styles.description}>{props.content.description}</Text>
      </ReadMore>
    </View>
  </View>
);

export default ContentLogo;

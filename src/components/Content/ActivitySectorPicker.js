// @flow
import React, { Component } from 'react';
import { View, StyleSheet, FlatList, Text } from 'react-native';
import { trendySectors, workedSectors, theme } from '../../config/constants';
import { FormInput, Icon, List, ListItem } from 'react-native-elements';

const styles = StyleSheet.create({
  list: {
    borderColor: 'transparent',
  },
  title: {
    paddingLeft: 32,
    paddingRight: 32,
    lineHeight: 24,
    fontSize: 18,
    fontWeight: '500',
    color: theme.fontBlack,
  },
  titleView: {
    marginTop: 0,
    marginBottom: 7,
  },
  searchInput: {
    width: '100%',
    fontSize: 14,
    color: theme.gray,
    fontWeight: '500',
  },
  searchInputContainer: {
    marginRight: 0,
    marginLeft: 10,
    flex: 1,
    borderBottomColor: 'transparent',
  },
  searchInputView: {
    shadowColor: 'rgba(0, 0, 0, 0.32)',
    shadowOffset: { width: 1, height: 0.5 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    marginTop: 16,
    marginRight: 32,
    marginLeft: 32,
    paddingLeft: 10,
    borderRadius: 2,
    borderBottomColor: 'transparent',
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  listItem: {
    borderBottomColor: '#d8d8d8',
  },
  lastListItem: {
    borderBottomColor: 'transparent',
  },
  listItemTitle: {
    fontSize: 16,
    color: theme.fontBlack,
    width: 302,
    lineHeight: 20,
  },
  listItemTitleSelected: {
    color: theme.primaryColor,
    width: 302,
    fontSize: 16,
    lineHeight: 20,
  },
  selectAll: {
    borderBottomColor: 'transparent',
  },
  selectAllContainer: {
    marginTop: 28,
  },
});

class ActivitySectorPicker extends Component {
  static defaultProps = {
    sectors: workedSectors,
    trendySectors,
  };

  static getLeftIconSingleName(isSelectedSector) {
    return isSelectedSector ? 'circle' : 'circle-thin';
  }

  static getLeftIconMultiName(isSelectedSector) {
    return isSelectedSector !== false ? 'check-square' : 'square-o';
  }

  props: {
    sectors: [],
    trendySectors: [],
    containerStyle: {},
    enableSearch: boolean,
    enableSelectAll: boolean,
    showTrendySectors: boolean,
    value: any,
    multi: boolean,
    onSelectActivity: Function,
  };

  constructor(props) {
    super(props);
    this.state = {
      search: '',
    };
    this.isSelectedSector = props.multi ? this.isSelectedMultiSector : this.isSelectedSingleSector;
    this.getLeftIconName = props.multi ? ActivitySectorPicker.getLeftIconMultiName : ActivitySectorPicker.getLeftIconSingleName;
    this.onSelectActivity = props.multi ? this.onSelectActivityMulti : this.onSelectActivitySingle;
  }

  searchActivitySector = (text, sectors) => {
    const regexText = `([^"]*${text.toLowerCase()}[^"]*)`;
    return sectors.filter(sector => sector.value.toLowerCase().match(regexText));
  };

  isSelectedSingleSector = activitySector => activitySector === this.props.value;

  isSelectedMultiSector = (activitySector) => {
    const idx = this.props.value.indexOf(activitySector);
    return idx === -1 ? false : idx;
  };

  onSelectActivitySingle = (key) => {
    this.props.onSelectActivity(key);
  };

  onSelectActivityMulti = (key) => {
    const idx = this.isSelectedMultiSector(key);
    const wantSectors = this.props.value.slice();
    if (idx === false) {
      wantSectors.push(key);
    } else {
      wantSectors.splice(idx, 1);
    }
    this.props.onSelectActivity(wantSectors);
  };

  selectAllSectors = () => {
    const isAllSelected = this.props.value.length === this.props.sectors.length;
    this.props.onSelectActivity(isAllSelected ? [] : this.props.sectors.slice().map(s => s.key));
  };

  sectorKeyExtractor = sector => sector.key;

  renderSector = ({ item: sector, index: i }, sectors) => {
    const isSelectedSector = this.isSelectedSector(sector.key);
    return (
      <ListItem
        containerStyle={sectors.length - 1 === i ?
          styles.lastListItem : styles.listItem}
        title={sector.value}
        titleNumberOfLines={2}
        rightIcon={{ color: 'transparent' }}
        leftIcon={{
          name: this.getLeftIconName(isSelectedSector),
          type: 'font-awesome',
          size: 26,
          color: isSelectedSector !== false ? theme.primaryColor : '#d8d8d8',
        }}
        titleStyle={isSelectedSector ?
          styles.listItemTitleSelected : styles.listItemTitle}
        onPress={() => {
          this.onSelectActivity(sector.key);
        }}
      />);
  };

  renderSelectAll = () => {
    const isAllSelected = this.props.value.length === this.props.sectors.length;
    return (
      <ListItem
        containerStyle={styles.selectAll}
        title='Sélectionner tous les secteurs'
        titleNumberOfLines={2}
        rightIcon={{ color: 'transparent' }}
        leftIcon={{
          name: isAllSelected ? 'check-square' : 'square-o',
          type: 'font-awesome',
          size: 26,
          color: isAllSelected ? theme.primaryColor : '#d8d8d8',
        }}
        titleStyle={isAllSelected === false ?
          styles.listItemTitle : styles.listItemTitleSelected}
        onPress={this.selectAllSectors}
      />
    );
  };

  render() {
    const isSearching = this.state.search !== '';
    return <View style={this.props.containerStyle}>
      {
        this.props.enableSearch &&
          <View style={styles.searchInputView}>
            <Icon name='search' color={theme.gray}/>
            <FormInput
              placeholder='Nom du secteur d’activité'
              inputStyle={styles.searchInput}
              containerStyle={styles.searchInputContainer}
              onChangeText={search => this.setState({ search })}
              value={this.state.search}
              underlineColorAndroid='#979797'
            />
          </View>
      }
      {
        this.props.multi && this.props.enableSelectAll && !isSearching &&
          <View style={styles.selectAllContainer}>
            {this.renderSelectAll()}
          </View>
      }
      {
        this.props.showTrendySectors && !isSearching &&
        <List containerStyle={styles.list}>
          <Text style={[styles.title, styles.titleView]} >
            Secteurs les plus populaires
          </Text>
          <FlatList
            data={this.searchActivitySector(this.state.search, this.props.trendySectors)}
            keyExtractor={this.sectorKeyExtractor}
            renderItem={data => this.renderSector(data, this.props.trendySectors)}
          />
        </List>
      }
      <List containerStyle={styles.list}>
        {
          this.props.showTrendySectors && !isSearching &&
          <Text style={[styles.title, styles.titleView]}>
            Les autres secteurs
          </Text>
        }
        <FlatList
          data={this.searchActivitySector(this.state.search, this.props.sectors)}
          keyExtractor={this.sectorKeyExtractor}
          renderItem={data => this.renderSector(data, this.props.sectors)}
        />
      </List>
    </View>;
  }
}

export default ActivitySectorPicker;

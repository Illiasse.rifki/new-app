// import React, { Component } from 'react';
// import {
//   View, StyleSheet, Text, ScrollView, FlatList,
//   TouchableOpacity,
//   Platform,
// } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';
// import { NavigationActions } from 'react-navigation';
// import { Avatar } from 'react-native-elements';
// import { Icon } from '../Pure/react-native-elements';
// import Url from './Url';
// import global from '../../config/global';

// const styles = StyleSheet.create({
//   icon: {
//     color: 'white',
//     paddingLeft: 10,
//   },
//   container: {
//     flex: 1,
//     backgroundColor: '#f5f5f5',
//   },
//   whoContainer: {
//     height: 64,
//     flexDirection: 'row',
//     backgroundColor: '#33cbcc',
//   },
//   whoPicture: {
//     flex: 2,
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   whoInfos: {
//     flex: 6,
//     justifyContent: 'center',
//   },
//   whoFullname: {
//     fontSize: 18,
//     lineHeight: 24,
//     color: 'white',
//     fontWeight: '600',

//   },
//   whoHeadline: {
//     fontSize: 14,
//     lineHeight: 20,
//     color: 'white',
//     fontWeight: '100',
//   },
//   flatlist: {

//   },
//   itemText: {
//     color: 'black',
//     fontSize: 16,
//   },
//   itemContainer: {
//     paddingVertical: 15,
//     backgroundColor: 'white',
//     marginVertical: 2,
//     paddingLeft: 16,
//     paddingRight: 8,
//     flexDirection: 'row',
//     alignItems: 'center',
//   },
//   footerContainer: {
//     height: 42,
//     flexDirection: 'row',
//     alignItems: 'center',
//     justifyContent: 'flex-end',
//   },
//   footerIcon: {
//     marginRight: 8,
//   },
//   sectionHeadingStyle: {
//     fontSize: 18,
//     lineHeight: 24,
//     fontWeight: '100',
//     marginVertical: 8,
//     marginLeft: 8,
//   },
//   iconList: {
//     marginRight: 8,
//     width: 32,
//   },
// });

// class BizzeoDrawer extends Component {
//   static Icon = props => (
//     <Icon
//       name='menu'
//       type='ionicons'
//       color={props.color || 'white'}
//       iconStyle={styles.icon}
//       onPress={props.onPress}
//     />
//   );

//   static navigationList = [
//     { key: 'CandidateSettings', name: 'Configuration du compte' },
//     { key: 'QRCodeCandidate', name: 'Votre QRCode' },
//     // { key: 'Favorites', name: 'Modifier vos favoris' },
//     { key: 'LegalMentions', name: 'Mentions légales' },
//     { key: 'CandidateAccountInfos', name: 'Identifiants et coordonnées' },
//     // { key: 'FAQCandidate', name: 'Foire Aux Questions' },
//   ];

//   handleOnClickDisconnect = () => {
//     global.authStockage.setToken('access-token', null);
//     global.authStockage.setToken('recruiter-access-token', null);
//     const resetAction = NavigationActions.reset({
//       index: 0,
//       key: null,
//       actions: [NavigationActions.navigate({ routeName: 'OnBoarding' })],
//     });
//     this.props.navigation.dispatch(resetAction);
//     this.setState({ disconnect: true });
//   };

//   render() {
//     let candidate = { fullname: '', firstname: '', lastname: '' };
//     if (!this.props.data.error
//       && !this.props.data.loading
//       && this.props.data.viewer
//       && this.props.data.viewer.candidate) {
//       candidate = this.props.data.viewer.candidate;
//     }
//     const { navigation } = this.props;
//     return (
//       <View style={styles.container}>
//         <View style={styles.whoContainer}>
//           <View style={styles.whoPicture}>
//             <Avatar
//               medium
//               rounded
//               source={candidate && candidate.picture && { uri: candidate.picture }}
//               title={candidate && !candidate.picture ? `${candidate.firstname[0]}${candidate.lastname[0]}` : ''}
//             />
//           </View>
//           <View style={styles.whoInfos}>
//             <Text style={styles.whoFullname} numberOfLines={1} ellipsizeMode="tail" >{candidate.fullname}</Text>
//             <Text style={styles.whoHeadline} numberOfLines={1} ellipsizeMode="tail" >{candidate.headline}</Text>
//           </View>
//         </View>
//         <ScrollView>
//           <View>
//             <TouchableOpacity
//               style={styles.itemContainer}
//               onPress={() => {
//                 navigation.navigate('Home');
//               }}
//             >
//               <Icon name="home" type="entypo" containerStyle={styles.iconList} />
//               <Text numberOfLines={1} ellipsizeMode="tail" style={styles.itemText}>Accueil</Text>
//             </TouchableOpacity>
//             <TouchableOpacity
//               style={styles.itemContainer}
//               onPress={() => {
//                 navigation.navigate('ProfileInfos');
//               }}
//             >
//               <Icon name="user-o" type="font-awesome" containerStyle={styles.iconList} />
//               <Text numberOfLines={1} ellipsizeMode="tail" style={styles.itemText}>Mon profil</Text>
//             </TouchableOpacity>
//             <TouchableOpacity
//               style={styles.itemContainer}
//               onPress={() => {
//                 navigation.navigate('SearchCriteria');
//               }}
//             >
//               <Icon name="business-center" type="material-icons" containerStyle={styles.iconList} />
//               <Text numberOfLines={1} ellipsizeMode="tail" style={styles.itemText}>Poste recherché</Text>
//             </TouchableOpacity>
//           </View>
//           <View>
//             <Text style={styles.sectionHeadingStyle}>
//               Aide & Réglages
//             </Text>
//             <FlatList
//               style={styles.flatlist}
//               data={BizzeoDrawer.navigationList}
//               keyExtractor={item => item.key}
//               keyboardShouldPersistTaps={'always'}
//               ListFooterComponent={
//                 <TouchableOpacity
//                   style={styles.itemContainer}
//                   onPress={this.handleOnClickDisconnect}
//                 >
//                   <Text numberOfLines={1} ellipsizeMode="tail" style={styles.itemText}>Se déconnecter</Text>
//                 </TouchableOpacity>
//               }
//               renderItem={({ item }) => (
//                 <TouchableOpacity
//                   style={styles.itemContainer}
//                   onPress={() => {
//                     navigation.navigate(item.key);
//                   }}
//                 >
//                   <Text numberOfLines={1} ellipsizeMode="tail" style={styles.itemText}>{item.name}</Text>
//                 </TouchableOpacity>
//               )}
//             />
//           </View>
//         </ScrollView>
//         <View style={styles.footerContainer}>
//           <Url url="https://www.facebook.com/bizzeo.co/">
//             <Icon name="facebook" type="entypo" color="#3b5998" style={styles.footerIcon}/>
//           </Url>
//           <Url url="https://www.linkedin.com/company/bizzeo-co/">
//             <Icon name="linkedin-with-circle" type="entypo" color="#0077B5" style={styles.footerIcon}/>
//           </Url>
//         </View>
//       </View>
//     );
//   }
// }

// const VIEWER = gql`
// query {
//   viewer {
//     id
//     candidate {
//       firstname
//       lastname
//       fullname
//       picture
//       headline
//     }
//   }
// }
// `;

// export default graphql(VIEWER)(BizzeoDrawer);

import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import Validate from '../Validate';

describe('Validate footer menu', () => {
  test('renders correctly', () => {
    const tree = renderer.create(<Validate />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});

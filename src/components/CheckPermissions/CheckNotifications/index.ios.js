import React, { Component } from 'react';
import { AppState } from 'react-native';
import Expo from 'expo';
import styled from 'styled-components';
import RaisedButton from '../../Button/RaisedButton';
import { theme } from '../../../config/constants';


const Container = styled.View`
  background-color: transparent;
  padding: 32px;
`;

const ActiveNotificationText = styled.Text`
  font-size: 15px;
  color: ${theme.primaryColor};
`;

const GoToSettingText = styled.Text`
  padding: 8px;
  line-height: 20px;
  alignSelf: center;
  text-align: center;
  color: ${theme.primaryColor};
`;

class CheckNotificationsIOS extends Component {
  state = {
    init: false,
    notifications: false,
  };

  shouldComponentUpdate = () => {
    if (this._mounted) return true;
    return false;
  };

  componentDidMount() {
    this._mounted = true;
    AppState.addEventListener('change', () => {
      this.checkNotification();
    });
    this.checkNotification();
  }

  componentWillUnmount() {
    this._mounted = false;
  }


  props: {
    onGetNotificationStatus: Function,
  };

  render() {
    const { init, notifications } = this.state;
    if (!init || notifications === 'authorized') return null;
    const canIOpenSettings = true;
    return (
      <Container style={{ display: 'none' }}>
        <ActiveNotificationText>
          Vous devez activer vos notifications pour pouvoir les parametrer.
        </ActiveNotificationText>
        {
          canIOpenSettings ?
            <RaisedButton
              title="Activer Notifications"
              onPress={this.handleActivateNotifications}
            />
            :
            <GoToSettingText>
              Paramètres > Bizzeo > Notifications > Autoriser Notifications
            </GoToSettingText>
        }
      </Container>
    );
  }
}

export default CheckNotificationsIOS;

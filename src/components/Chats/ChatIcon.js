// @flow
import React, { Component } from 'react';
import { Icon } from 'react-native-elements';

import { client } from '../../utils/Chat';

class ChatIcon extends Component {
  state = { roomUsed: false };
  props: {
    userId: string,
  };

  async roomUsed() {
    try {
      const used = await client.roomUsed(this.props.userId);
      this.setState({ roomUsed: used });
    } catch (e) {
      this.setState({ roomUsed: false });
    }
  }

  componentDidMount() {
    this.roomUsed();
  }

  render() {
    const { roomUsed } = this.state;
    if (!roomUsed) return null;
    return (
      <Icon color='#33cbcc' name='chat' />
    );
  }
}

export default ChatIcon;

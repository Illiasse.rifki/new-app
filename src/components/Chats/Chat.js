// // @flow
// import React from 'react';
// import { Avatar, Icon } from 'react-native-elements';
// import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';
// import LoadingComponent from '../Elements/LoadingComponent';
// import ErrorComponent from '../Elements/ErrorComponent';
// import DefaultUserPic from '../../assets/carrevolutis.jpg';

// const styles = StyleSheet.create({
//   chatTextTitle: {
//     color: 'black',
//     fontSize: 18,
//     textAlign: 'left',
//     fontWeight: '500',
//     lineHeight: 25.5,
//   },
//   chatTextResume: {
//     color: '#afafaf',
//     fontSize: 14,
//     textAlign: 'left',
//     maxHeight: 32,
//     lineHeight: 16,
//     fontWeight: '400',
//   },
//   chatViewRow: {
//     backgroundColor: 'transparent',
//     flexDirection: 'row',
//     alignItems: 'center',
//     paddingLeft: 24,
//     paddingRight: 24,
//     paddingTop: 13.5,
//     paddingBottom: 13.5,
//     height: 91,
//     borderBottomWidth: 1,
//     borderColor: '#f5f5f5',
//   },
//   lastChatViewRow: {
//     backgroundColor: 'transparent',
//     flexDirection: 'row',
//     paddingLeft: 24,
//     paddingRight: 24,
//     paddingTop: 13.5,
//     paddingBottom: 13.5,
//     height: 91,
//     alignItems: 'center',
//   },
//   avatar: {
//     width: 64,
//     height: 64,
//     borderRadius: 4,
//   },
//   chatViewContent: {
//     flex: 5,
//     flexDirection: 'column',
//     justifyContent: 'center',
//     backgroundColor: 'transparent',
//   },
//   chatViewPic: {
//     flex: 2,
//   },
//   online: { position: 'absolute', left: 28.5, top: 28.5 },
// });

// const AssistanceIcon = require('../../assets/admin_icon.png');

// const Chat = (props: any) => {
//   const { data } = props;
//   if (data && data.error) {
//     return <ErrorComponent />;
//   }
//   if (data && data.loading) {
//     return <LoadingComponent />;
//   }
//   const { content, online } = props;
//   const {
//     last_read: lastRead,
//     last_message: lastMessage,
//     _id: chatId,
//     isAssistance,
//   } = content;
//   const chatStyle = props.isLast ? styles.lastChatViewRow : styles.chatViewRow;
//   let contentPic = null;
//   let contentName = null;
//   let companyId = null;
//   if (isAssistance) {
//     contentPic = <Avatar
//       avatarStyle={styles.avatar}
//       rounded
//       width={64}
//       height={64}
//       source={AssistanceIcon}
//     />;
//     contentName = 'Assistance Bizzeo';
//   } else {
//     const user = props.data ? { ...props.data.user_chat_data } : {
//       fullname: 'Unknown',
//     };
//     contentPic = (
//       <Avatar
//         avatarStyle={styles.avatar}
//         rounded
//         width={64}
//         height={64}
//         source={user.picture ? { uri: user.picture } : DefaultUserPic}
//       />
//     );
//     contentName = user.company_id ? `${user.company_name} - ${user.fullname}` : user.fullname;
//     companyId = user.company_id;
//   }

//   const read = lastRead > lastMessage.created_at
//     ? null : { fontWeight: 'bold' };

//   const dayTime = ((new Date()).getHours() >= 9) &&
//             ((new Date()).getHours() < 19);
//   const checkOnline = (isAssistance && dayTime) || (!isAssistance && online);
//   return (
//     <TouchableOpacity
//       onPress={() => props.onPress(contentName, companyId)}
//       disabled={props.disabled}
//       key={chatId}
//     >
//       <View style={chatStyle}>
//         <View style={styles.chatViewPic}>
//           {contentPic}
//           {
//             checkOnline &&
//             < Icon size={64}
//               containerStyle={styles.online}
//               name='dot-single'
//               type='entypo'
//               color='#33cc80'
//             />
//           }
//         </View>
//         <View style={styles.chatViewContent}>
//           <Text style={styles.chatTextTitle}>{contentName}</Text>
//           <Text numberOfLines={2} style={[styles.chatTextResume, read]}>
//             {lastMessage.content}
//           </Text>
//         </View>
//       </View>
//     </TouchableOpacity>
//   );
// };

// const GET_USER_CHAT_DATA = gql`
//   query UserChatData($user_id: String!) {
//     user_chat_data(user_id: $user_id) {
//       fullname
//       picture
//       company_name
//       company_id
//     }
//   }
// `;

// export default graphql(GET_USER_CHAT_DATA, {
//   skip: ownProps => ownProps.userId === undefined,
//   options: ownProps => ({
//     variables: {
//       user_id: ownProps.userId,
//     },
//   }),
// })(Chat);

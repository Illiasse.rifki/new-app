// @flow
import React from 'react';
import {
  View,
  StyleSheet,
  Text,
} from 'react-native';

const styles = StyleSheet.create({
  triangleCorner: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderRightWidth: 20,
    borderTopWidth: 10,
    borderRightColor: 'transparent',
  },
  chatLeft: { backgroundColor: '#f5f5f5', padding: 20, borderRadius: 4 },
  chatRight: { backgroundColor: '#33cbcc', padding: 20, borderRadius: 4 },
  chatText: { textAlign: 'left' },
  cornerRight: {
    borderTopColor: '#33cbcc',
    transform: [{ rotateY: '180deg' }],
    alignSelf: 'flex-end',
    marginTop: -1,
  },
  cornerLeft: {
    borderTopColor: '#f5f5f5',
    marginTop: -1,
  },
  chatBoxRight: { marginTop: 10, marginRight: 20, marginLeft: 60 },
  chatBoxLeft: { marginTop: 10, marginLeft: 20, marginRight: 60 },
});

const isLastMsgFrom = (index, array, id) => {
  let last = -1;

  array.forEach((v, i) => {
    if (v.user === id) last = i;
  });

  if (last === index) return true;
  return false;
};

const Message = (props: { message: any, index: any, array: any, target: any, user: any }) => {
  const { message, array, index, user } = props;
  const isMe = message.user === user._id;
  return (
    <View style={isMe ? styles.chatBoxRight : styles.chatBoxLeft}>
      <View style={isMe ? styles.chatRight : styles.chatLeft}>
        <Text
          style={[
            styles.chatText,
            isMe ? {
              color: 'white',
            } : {
              color: 'black',
            },
          ]}
        >
          {message.content}
        </Text>
      </View>
      {isLastMsgFrom(index, array, message.user) && (
        <View
          style={[
            styles.triangleCorner,
            isMe ? styles.cornerRight : styles.cornerLeft,
          ]}
        />
      )}
    </View>
  );
};

export default Message;

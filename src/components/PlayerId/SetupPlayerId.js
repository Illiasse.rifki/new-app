import React from "react";
// import gql from "graphql-tag";
// import OneSignal from "react-native-onesignal";
import { graphql } from "@apollo/client/react/hoc";
import { gql } from "@apollo/client";

class SetupPlayerId extends React.Component {
  onIds = (ids) => {
    const playerId = ids.userId;
    const variables = {
      player_id: playerId,
    };
    this.props
      .setPlayerId({
        variables,
      })
      .then((ret) => {
        if (ret.data.setPlayerId.error) {
          console.warn("pas de notif");
        }
      });
  };

  // componentWillMount() {
  //   OneSignal.addEventListener("ids", this.onIds);
  // }

  // componentWillUnmount() {
  //   OneSignal.removeEventListener("ids", this.onIds);
  // }

  props: {
    setPlayerId: Function,
  };

  render() {
    return null;
  }
}

const SET_PLAYER_ID = gql`
  mutation setPlayerId($player_id: String!) {
    setPlayerId(player_id: $player_id) {
      error {
        message
      }
      updated
    }
  }
`;

export default graphql(SET_PLAYER_ID, { name: "setPlayerId" })(SetupPlayerId);

import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

import PartnerLogo from './PartnerLogo';


const styles = StyleSheet.create({
  container: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});

export default class PartnersLogos extends Component {
  static defaultProps = {
    partnersIds: [],
  };

  props: {
    partnersIds: Array<number>,
  };

  render = () => (
    <View style={styles.container}>
      {this.props.partnersIds.map(c => <PartnerLogo partner_id={c} key={c} />)}
    </View>
  );
}

// import React, { PureComponent } from 'react';
// import { View, Text, StyleSheet, Image } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import { theme } from '../../config/constants/index';

// const styles = StyleSheet.create({
//   container: {
//     marginTop: 16,
//     padding: 6,
//     width: '45%',
//     alignItems: 'center',
//   },
//   text: {
//     fontSize: 18,
//     fontWeight: 'bold',
//     color: theme.fontBlack,
//     marginTop: 10,
//   },
//   imageContainer: {
//     width: 88,
//     height: 88,
//     borderColor: theme.grey,
//     borderWidth: 1,
//     borderStyle: 'solid',
//     borderRadius: 2,
//     padding: 10.5,
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   image: {
//     resizeMode: 'contain',
//     width: 87,
//     height: 87,
//   },
// });

// class PartnerLogo extends PureComponent {
//   static defaultProps = {
//     onPress: () => null,
//   };

//   props: {
//     partner_id: string,
//     partnerById: {
//       loading: boolean,
//       partnerById: {
//         partner_id: string,
//         name: string,
//         logo: string,
//       },
//     },
//     onPress: Function,
//   };

//   render = () => {
//     if (this.props.partnerById.loading) {
//       return null;
//     }
//     return (
//       <View style={styles.container}>
//         <View style={styles.imageContainer}>
//           <Image style={styles.image} source={{ uri: this.props.partnerById.partnerById.logo }} />
//         </View>
//         <Text style={styles.text}>{this.props.partnerById.partnerById.name}</Text>
//       </View>
//     );
//   };
// }

// const PARTNER_BY_ID_QUERY = gql`
//   query partnerById($partner_id: String!) {
//     partnerById(partner_id: $partner_id) {
//       partner_id
//       logo
//       name
//     }
//   }
// `;

// export default graphql(PARTNER_BY_ID_QUERY, {
//   name: 'partnerById',
//   options: ownProps => ({
//     variables: {
//       partner_id: ownProps.partner_id,
//     },
//   }),
// })(PartnerLogo);

// source https://github.com/BrunoWilkinson/CurrencyConverted/blob/master/app/components/Alert/index.js

// https://github.com/testshallpass/react-native-dropdownalert/issues/30

import AlertProvider from './AlertProvider';
import connectAlert from './connectAlert';

export { AlertProvider, connectAlert };

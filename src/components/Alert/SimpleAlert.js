// @flow
import { Alert } from 'react-native';

export default class SimpleAlert {
  static error(msg) {
    Alert.alert(
      'Erreur',
      msg,
      [{ text: 'OK' }],
    );
  }

  static info(msg, title, onPress) {
    const okButton = {
      text: 'OK',
      onPress: onPress !== undefined ? onPress : undefined,
    };
    Alert.alert(
      title || 'Info',
      msg,
      [okButton],
    );
  }
}

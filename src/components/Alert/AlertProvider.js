// source https://github.com/BrunoWilkinson/CurrencyConverted/blob/master/app/components/Alert/AlertProvider.js
import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { View } from "react-native";
import DropdownAlert from "react-native-dropdownalert";

import { theme } from "../../config/constants/index";

class AlertProvider extends Component {
  // static childContextTypes = {
  //   alertWithType: PropTypes.func,
  //   alert: PropTypes.func,
  // };

  props: {
    children: any,
  };

  getChildContext() {
    return {
      alert: (...args) => this.dropdown.alert(...args),
      alertWithType: (...args) => this.dropdown.alertWithType(...args),
    };
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {React.Children.only(this.props.children)}
        <DropdownAlert
          errorColor={theme.red}
          infoColor={theme.primaryColor}
          successColor={theme.green}
          inactiveStatusBarStyle="light-content"
          ref={(ref) => {
            this.dropdown = ref;
          }}
        />
      </View>
    );
  }
}

export default AlertProvider;

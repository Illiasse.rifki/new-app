import 'react-native';
import 'styled-components/native';
import React from 'react';
import renderer from 'react-test-renderer';

import Logo from '../Logo';

describe('Logo Component', () => {
  test('render correctly', () => {
    const tree = renderer.create(<Logo />);
    expect(tree).toMatchSnapshot();
  });
});

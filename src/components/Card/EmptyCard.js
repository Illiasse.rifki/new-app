import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Icon } from 'react-native-elements';

type Props = {
  containerStyle: {},
  title?: string,
  subtitle?: string,
};

const styles = StyleSheet.create({
  emptyCard: {
    backgroundColor: 'white',
    borderRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.8,
    padding: 32,
    marginVertical: 4,
  },
  emptyCardText: {
    fontSize: 16,
    textAlign: 'left',
    lineHeight: 24,
    color: '#afafaf',
    fontStyle: 'italic',
  },
  emptyCardTextTip: {
    fontSize: 14,
    fontWeight: '100',
    textAlign: 'left',
    lineHeight: 20,
    color: '#afafaf',
    fontStyle: 'italic',
  },
});

const EmptyCard = (props: Props) => (
  <View style={[styles.emptyCard, props.containerStyle]}>
    <Text style={styles.emptyCardText}>
      {props.title}
    </Text>
    {
      props.subtitle &&
      <Text style={styles.emptyCardTextTip}>
        {props.subtitle}
      </Text>
    }
  </View>
);

export default EmptyCard;

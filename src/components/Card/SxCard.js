import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { theme } from '../../config/constants/index';

type Props = {
  style?: {},
  onPress?: Function,
  title?: string,
  number: string,
  label: string,
};

const styles = StyleSheet.create({
  sxcard: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingBottom: 8.5,
    paddingTop: 10.5,
    paddingHorizontal: 8.5,
    borderRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { height: 1, width: 0 },
    shadowOpacity: 0.8,
    height: 112,
  },
  sxcardTitle: {
    fontSize: 14,
  },
  sxcardNumber: {
    fontSize: 28,
    color: theme.fontBlack,
    fontWeight: 'bold',
  },
  sxcardLink: {
    fontSize: 14,
    fontWeight: '500',
    color: theme.primaryColor,
    letterSpacing: 0.5,
  },
});

const SxCard = (props: Props) => (
  <TouchableOpacity style={[styles.sxcard, props.style]} onPress={props.onPress}>
    <Text style={styles.sxcardTitle}>{props.title}</Text>
    <Text style={styles.sxcardNumber}>{props.number}</Text>
    <Text style={styles.sxcardLink}>{props.label || 'VOIR'}</Text>
  </TouchableOpacity>
);

export default SxCard;

import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import QualityCard from '../Quality';

describe('Quality Card', () => {
  test('renders correctly', () => {
    const tree = renderer.create(<QualityCard />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});

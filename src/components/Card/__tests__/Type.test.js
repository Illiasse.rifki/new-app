import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import Type from '../Type';

describe('Type Card', () => {
  const tree = renderer.create(<Type />).toJSON();

  test('render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});

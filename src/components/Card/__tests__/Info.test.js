import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import InfoCard from '../Info';

describe('Info Card', () => {
  test('renders correctly', () => {
    const tree = renderer.create(<InfoCard />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});

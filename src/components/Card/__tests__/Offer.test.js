import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import OfferCard from '../Offer';

describe('Offer Card', () => {
  test('renders correctly', () => {
    const tree = renderer.create(<OfferCard />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});

import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import MapCard from '../Map';

describe('Map Card', () => {
  test('renders correctly', () => {
    const tree = renderer.create(<MapCard />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});

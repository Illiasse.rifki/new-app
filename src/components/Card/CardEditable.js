import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Icon } from 'react-native-elements';

type Props = {
  containerStyle: {},
  onPress?: Function,
  children?: {},
  title?: string,
  titleStyle: {},
  editable?: boolean,
};

const getStyle = () => {
  return StyleSheet.create({
    cardBackground: {
      flex: 1,
      position: 'relative',
      backgroundColor: 'white',
      paddingLeft: 24,
      paddingRight: 24,
      paddingTop: 32,
      paddingBottom: 32,
      marginBottom: 16,
    },
    cardTitle: {
      fontWeight: 'bold',
      fontSize: 28,
      lineHeight: 40,
      marginTop: 8,
      marginLeft: 8,
      backgroundColor: 'transparent',
    },
    editView: {
      position: 'absolute',
      right: 18,
      top: 18,
    },
  });
};

const CardEditable = (props: Props) => {
  const styles = getStyle();
  return (
    <View style={[styles.cardBackground, props.containerStyle]}>
      {
        props.title ? (
          <Text style={[styles.cardTitle, props.titleStyle]}>
            {props.title}
          </Text>) : null
      }
      {props.children}
      {props.editable ?
        (<Icon
          containerStyle={styles.editView}
          name='edit'
          size={30}
          onPress={props.onPress}
        />) : null
      }
      {props.rightIcon ?
        (<Icon
          containerStyle={styles.editView}
          name={props.rightIcon.name}
          type={props.rightIcon.type}
          size={30}
          onPress={props.rightIcon.onPress}
        />) : null
      }
    </View>);
};

export default CardEditable;

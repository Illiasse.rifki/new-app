import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { Icon, Avatar } from 'react-native-elements';

import { pure } from 'recompose';

const styles = StyleSheet.create({
  box: {
    backgroundColor: 'white',
    borderRadius: 4,
    marginVertical: 4,
    paddingRight: 12.5,
    flexDirection: 'row',
    alignItems: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { width: 0.5, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 4,
    zIndex: 2000,
    height: 96,
  },
  name: {
    color: '#1c1c1c',
    textAlign: 'left',
    fontWeight: '500',
    fontSize: 18,
  },
  subtitle: {
    color: '#afafaf',
    fontSize: 14,
    marginTop: 5,
    height: 20,
  },
  comment: {
    color: '#1c1c1c',
    fontSize: 14,
    marginTop: 5,
    height: 20,
    fontWeight: '600',
  },
  offerTitle: {
    color: '#afafaf',
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 5,
    height: 20,
  },
  textContainer: {
    flex: 1,
    paddingHorizontal: 14.5,
  },
  avatar: {
    width: 96,
    height: 96,
    borderRadius: 4,
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
  },
  avatarContainer: {
    width: 96,
    height: 96,
    borderRadius: 4,
  },
  favorite: {
    marginRight: 10,
  },
  dot: {
    borderStyle: 'solid',
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 25,
    height: 16,
    width: 16,
  },
});

type tUserSmallCard = {
  onPress: Function,
  arriveDate: ?string,
  arriveDateMarker: ?boolean,
  title: string,
  subtitle: string,
  pictureTitle: string,
  picture: string,
  favorite: Boolean,
  comment: ?string,
  chat: Boolean,
  offerTitle: string,
};

const Dot = props => <View style={[styles.dot, { backgroundColor: props.color }]} />;

const GreenDot = () => <Dot color='green'/>;

const RedDot = () => <Dot color='red'/>;

const UserSmallCard = (props: tUserSmallCard) => {
  const {
    arriveDate,
    arriveDateMarker,
    picture,
    pictureTitle,
    title,
    subtitle,
    favorite,
    chat,
    comment,
    offerTitle,
    onPress,
  } = props;
  return (
    <TouchableOpacity
      onPress={onPress}
      style={styles.box}
    >
      <Avatar large title={picture ? undefined : pictureTitle}
        avatarStyle={styles.avatar}
        containerStyle={styles.avatarContainer}
        width={96}
        height={96}
        source={picture ? {
          uri: picture,
        } : undefined } />
      <View style={styles.textContainer}>
        {offerTitle && <Text style={styles.offerTitle} numberOfLines={1}>{offerTitle}</Text>}
        <Text style={styles.name} numberOfLines={1}>{title}</Text>
        <Text style={styles.subtitle} numberOfLines={1}>{subtitle}</Text>
        {comment ? <Text style={styles.comment}
          ellipsizeMode={'tail'}
          numberOfLines={1}>{comment.split('\n').join(' ')}</Text> : null}
      </View>
      {favorite && <Icon
        name={'md-heart'}
        type='ionicon'
        color={'#33cbcc'}
        size={24}
        style={styles.favorite}
      />}
      {chat && <Icon color='#33cbcc' name='chat' />}
      {arriveDateMarker && (arriveDate ? <GreenDot /> : <RedDot />) }
      <Icon
        name='chevron-thin-right'
        size={21}
        type='entypo'
        color='#afafaf'
      />
    </TouchableOpacity>
  );
};

export default pure(UserSmallCard);

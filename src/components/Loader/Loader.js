import React, { Component } from 'react';
import { View, StyleSheet, Animated, AsyncStorage } from 'react-native';

const styles = StyleSheet.create({
  background: {
    flexDirection: 'row',
    backgroundColor: 'transparent',
    height: 8,
  },
});

class Loader extends Component {
  static defaultProps = {
    width: 0,
  };

  props: {
    width: number,
    start: number,
  };

  constructor(props) {
    super(props);
    this.props.width = this.props.width || 0;
    this.state = {
      current: new Animated.Value(0),
      visible: !(typeof this.props.start === 'undefined' || this.props.start === null),
    };
    this.animateLoader = Animated.timing(
      this.state.current,
      {
        toValue: this.props.width / 100,
        duration: 1000,
      },
    );
  }

  componentDidMount() {
    this.getStartProgress();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.start !== this.props.start) {
      if (typeof this.props.start === 'undefined' || this.props.start === null) {
        return;
      }
      this.state.current.setValue(this.props.start / 100);
      this.animateLoader.start();
    }
    if (prevProps.width !== this.props.width) {
      AsyncStorage.setItem('@Carrevolutis:loaderProgress', this.props.width.toString());
    }
  }

  async getStartProgress() {
    let value = this.props.start;
    if (typeof value === 'undefined' || value === null) {
      try {
        value = Number(await AsyncStorage.getItem('@Carrevolutis:loaderProgress'));
      } catch (e) {
        value = 0;
      }
    }
    this.state.current.setValue(value / 100);
    this.setState({
      visible: true,
    });
    AsyncStorage.setItem('@Carrevolutis:loaderProgress', this.props.width.toString());
    this.animateLoader.start();
  }

  getProgressBarStyle = () => {
    return {
      backgroundColor: 'black',
      flex: this.state.current,
    };
  };

  getEmptyBarStyle = () => {
    return {
      backgroundColor: '#f5f5f5',
      flex: 100 - this.state.current,
    };
  };

  render() {
    if (!this.state.visible) { return null; }
    return (
      <View style={styles.background}>
        <Animated.View
          style={this.getProgressBarStyle()}
        >
        </Animated.View>
        <Animated.View
          style={this.getEmptyBarStyle()}
        >
        </Animated.View>
      </View>
    );
  }
}

export default Loader;

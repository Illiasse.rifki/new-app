// import React from 'react';
// import styled from 'styled-components';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';

// import RaisedButton from '../Button/RaisedButton';
// import NumberField from '../FormGenerator/blocks/NumberField';

// import { EVENT_REGISTRATION_BY_EVENT_ID } from '../Events/RegistrationButton/Candidate/index';
// import { connectAlert } from '../Alert/index';

// const Container = styled.View`
//   margin: 24px 32px 0 32px;
//   background-color: white;
//   padding: 16px 16px 0 16px;
// `;

// const Info = styled.Text`
//   font-size: 16px;
//   text-align: center;
//   line-height: 24px;
// `;

// class AskPhone extends React.Component {
//   state = {
//     phone: ''
//   };

//   handlePhone = phone => this.setState({ phone });

//   onPress = async () => {
//     const { alertWithType, successText } = this.props;
//     if (!this.state.phone) {
//       alertWithType('error', 'Erreur', 'Vous devez remplir un numéro de téléphone')
//       return ;
//     }
//     try {
//       const res = await this.props.editCandidate({
//         variables: {
//           user_id: this.props.data.viewer.id,
//           phone: this.state.phone,
//         },
//       });
//       const { error } = res.data.editCandidate;
//       if (error) {
//         throw error;
//       }
//       if (this.props.refetch) {
//         this.props.refetch();
//       }
//       alertWithType('success', 'Numéro de téléphone mise à jour', successText || 'Vous pouvez continuer');
//     } catch (e) {
//       alertWithType('error', 'Erreur', 'Erreur lors de la mise à jour du numéro de téléphone');
//     }
//   };

//   props: {
//     refetch: Function,
//     successText: string,
//     infoText: string,
//     editCandidate: Function,
//     alertWithType: Function,
//   };

//   render() {
//     return (
//       <Container>
//         <Info>
//           {this.props.infoText || "Un numéro de téléphone est indispensable"}
//         </Info>
//         <NumberField
//           value={this.state.phone}
//           onChangeText={this.handlePhone}
//         />
//         <RaisedButton
//           title="Valider"
//           onPress={this.onPress}
//         />
//       </Container>
//     );
//   }
// }

// const VIEWER = gql`
//   query {
//     viewer {
//       id
//     }
//   }
// `;

// const EDIT_CANDIDATE = gql`
//   mutation editCandidate($user_id: String!, $phone: String) {
//     editCandidate(user_id: $user_id, phone: $phone) {
//       error {
//         message
//       }
//     }
//   }
// `;

// export default compose(
//   graphql(VIEWER),
//   graphql(EDIT_CANDIDATE, { name: 'editCandidate' })
// )(connectAlert(AskPhone));

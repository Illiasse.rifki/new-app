import React from 'react';
import { FormInput } from 'react-native-elements';
import { InputProps } from './index';
import { themeElements } from '../../../config/constants/index';

// @flow
export default function Password(props: InputProps) {
  return (
    <FormInput
      {...props}
      name="password"
      autoCapitalize="none"
      secureTextEntry
      placeholder="Mot de passe (6 caractères mini.)"
      inputStyle={themeElements.formInputText}
      containerStyle={themeElements.formInputContainer}
      onChangeText={(password) => {
        props.onChangeText('password', password);
      }}
      value={props.value}
      autoCorrect={false}
      underlineColorAndroid="#979797"
    />
  );
}

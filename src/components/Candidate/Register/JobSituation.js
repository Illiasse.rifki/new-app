import React from 'react';
import { FormInput, Button, Text, Header } from 'react-native-elements';
import { Picker, Modal, View } from 'react-native';
import { PickerProps } from './index';
import { JobSituationStatus } from '../../../config/constants/jobSituation';
import { themeElements, theme } from '../../../config/constants/index';

// @flow
export default class JobSituation extends React.PureComponent<PickerProps> {
  state = {
    modal: false,
  };

  mapLabel = () => {
    const { value } = this.props;
    if (!value) return '';
    try {
      return JobSituationStatus.find(e => e.value === value).label;
    } catch (error) {
      console.error(error);
      return 'Error';
    }
  };

  onFocus = () => {
    this.setState({ modal: true });
  };

  onBlur = () => {
    this.setState({ modal: false });
  };

  render() {
    const { modal } = this.state;
    const props = this.props;
    return (
      <View>
        <FormInput
          placeholder={'Situation actuelle'}
          inputStyle={themeElements.formInputText}
          containerStyle={themeElements.formInputContainer}
          onFocus={this.onFocus}
          value={this.mapLabel()}
        />
        <Modal
          animationType="slide"
          transparent={false}
          visible={modal}
          onRequestClose={this.onBlur}
        >
          <Header
            backgroundColor={theme.primaryColor}
            centerComponent={{
              text: 'Situation actuelle',
              style: { color: 'white', fontSize: 18, marginTop: 8 },
            }}
          />
          <Picker
            {...props}
            selectedValue={props.value}
            onValueChange={jobSituation => props.onValueChange('jobSituation', jobSituation)}
            name="jobSituation"
          >
            <Picker.Item key={'empty-item-picker'} label={''} value={''} />
            {JobSituationStatus.map(e => (
              <Picker.Item key={e.label} label={e.label} value={e.value} />
            ))}
          </Picker>
          <View style={{ flex: 1 }} />
          <Button
            style={{ marginVertical: 16 }}
            title="Retour"
            backgroundColor={theme.primaryColor}
            color="white"
            raised
            onPress={this.onBlur}
          />
        </Modal>
      </View>
    );
  }
}

import React from 'react';
import { FormInput } from 'react-native-elements';
import { InputProps } from './index';
import { themeElements } from '../../../config/constants/index';

// @flow
export default function Lastname(props: InputProps) {
  return (
    <FormInput
      {...props}
      name="lastname"
      placeholder="Nom"
      inputStyle={themeElements.formInputText}
      containerStyle={themeElements.formInputContainer}
      onChangeText={(lastname) => {
        props.onChangeText('lastname', lastname);
      }}
      value={props.value}
      autoCorrect={false}
      underlineColorAndroid="#979797"
    />
  );
}

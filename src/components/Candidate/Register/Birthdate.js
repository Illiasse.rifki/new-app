import React from 'react';
import { Platform, StyleSheet } from 'react-native';
import DatePicker from 'react-native-datepicker';
import { InputDateProps } from './index';
import { theme } from '../../../config/constants/index';

// @flow

const styles = StyleSheet.create({
  dateText: {
    fontSize: 16,
    width: '100%',
    color: theme.fontBlack,
    marginBottom: -8,
    textAlign: 'left',
  },
  dateInput: {
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderTopColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: Platform.OS === 'ios' ? '#c9c9c9' : '#979797',
  },
  datePlaceholder:
    Platform.OS === 'android'
      ? {
        fontSize: 16,
        width: '100%',
        marginBottom: -8,
        textAlign: 'left',
        color: 'rgb(140, 140, 140)',
      }
      : {
        fontSize: 16,
        width: '100%',
        marginBottom: -8,
        textAlign: 'left',
      },
});

export default function Birthdate(props: InputDateProps) {
  const { styles: propsStyles, value, maxDate, onDateChange } = props;
  return (
    <DatePicker
      placeholder="Date de naissance"
      style={propsStyles.birthContainer}
      onDateChange={birthdate => onDateChange('birthdate', birthdate)}
      date={value}
      format="DD-MM-YYYY"
      minDate="01-01-1950"
      maxDate={maxDate}
      confirmBtnText="Confirmer"
      cancelBtnText="Annuler"
      customStyles={{
        dateInput: styles.dateInput,
        dateText: styles.dateText,
        placeholderText: styles.datePlaceholder,
      }}
      showIcon={false}
    />
  );
}

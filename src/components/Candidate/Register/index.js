// @flow

export type InputProps = {
  placeholder: string,
  inputStyle: {},
  containerStyle: {},
  name: string,
  value: string,
  onChangeText: Function,
  autoCorrect: boolean,
  underlineColorAndroid: string,
};

export type InputDateProps =
  | InputProps
  | {
      styles: {},
      onDateChange: Function,
      value: string,
      maxDate: string,
    };

export type PickerProps = {
  onValueChange: Function,
  value: string,
};

export type CheckBoxProps = {
  onPress: Function,
  checked: boolean,
};

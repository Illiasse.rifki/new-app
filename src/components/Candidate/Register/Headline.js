import React from 'react';
import { FormInput } from 'react-native-elements';
import { InputProps } from './index';
import { themeElements } from '../../../config/constants/index';

// @flow
export default function Headline(props: InputProps) {
  return (
    <FormInput
      {...props}
      name="headline"
      required
      autoCapitalize="none"
      placeholder="Fonction actuelle ou recherchée"
      inputStyle={themeElements.formInputText}
      containerStyle={themeElements.formInputContainer}
      onChangeText={(headline) => {
        props.onChangeText('headline', headline);
      }}
      value={props.value}
      autoCorrect={false}
      underlineColorAndroid="#979797"
    />
  );
}

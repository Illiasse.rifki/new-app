import React from 'react';
import { FormInput } from 'react-native-elements';
import { InputProps } from './index';
import { themeElements } from '../../../config/constants/index';

// @flow
export default function Firstname(props: InputProps) {
  return (
    <FormInput
      {...props}
      name="firstname"
      placeholder="Prénom"
      inputStyle={themeElements.formInputText}
      containerStyle={themeElements.formInputContainer}
      onChangeText={(firstname) => {
        props.onChangeText('firstname', firstname);
      }}
      value={props.value}
      autoCorrect={false}
      underlineColorAndroid="#979797"
    />
  );
}

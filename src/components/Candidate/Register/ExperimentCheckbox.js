import React from 'react';
import { View, Text, TouchableHighlight, StyleSheet } from 'react-native';
import { CheckBox } from 'react-native-elements';
import { CheckBoxProps } from './index';
import { theme } from '../../../config/constants/index';

// @flow

const styles = StyleSheet.create({
  containerView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 8,
  },
});

export default function ExperimentCheckbox(props: CheckBoxProps) {
  return (
    <View style={styles.containerView}>
      <CheckBox
        checked={props.checked}
        checkedIcon="check-square"
        checkedColor={theme.primaryColor}
        onPress={props.onPress}
        fontSize={24}
      />
      <View>
        <TouchableHighlight onPress={props.onPress} underlayColor="#FFF">
          <Text>{"J'ai plus de 2 ans d'expérience"}</Text>
        </TouchableHighlight>
      </View>
    </View>
  );
}

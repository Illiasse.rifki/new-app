import React from 'react';
import { FormInput } from 'react-native-elements';
import { InputProps } from './index';
import { themeElements } from '../../../config/constants/index';

// @flow
export default function Email(props: InputProps) {
  return (
    <FormInput
      {...props}
      name="email"
      autoCapitalize="none"
      keyboardType="email-address"
      placeholder="Email"
      inputStyle={themeElements.formInputText}
      containerStyle={themeElements.formInputContainer}
      onChangeText={(email) => {
        props.onChangeText('email', email);
      }}
      value={props.value}
      autoCorrect={false}
      underlineColorAndroid="#979797"
    />
  );
}

import React from 'react';
import { FormInput } from 'react-native-elements';
import { InputProps } from './index';
import { themeElements } from '../../../config/constants/index';

// @flow
export default function Phone(props: InputProps) {
  return (
    <FormInput
      {...props}
      name="email"
      autoCapitalize="none"
      returnKeyType="done"
      keyboardType="phone-pad"
      placeholder="Téléphone portable"
      inputStyle={themeElements.formInputText}
      containerStyle={themeElements.formInputContainer}
      onChangeText={(phone) => {
        props.onChangeText('phone', phone);
      }}
      value={props.value}
      autoCorrect={false}
      underlineColorAndroid="#979797"
    />
  );
}

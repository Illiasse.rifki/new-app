import React from 'react';
import { FormInput } from 'react-native-elements';
import { InputProps } from './index';
import { themeElements } from '../../../config/constants/index';

// @flow
export default function Confirmation(props: InputProps) {
  return (
    <FormInput
      {...props}
      name="password-confirmation"
      autoCapitalize="none"
      secureTextEntry
      placeholder="Confirmez votre mot de passe"
      inputStyle={themeElements.formInputText}
      containerStyle={themeElements.formInputContainer}
      onChangeText={(confirmation) => {
        props.onChangeText('confirmation', confirmation);
      }}
      value={props.value}
      autoCorrect={false}
      underlineColorAndroid="#979797"
    />
  );
}

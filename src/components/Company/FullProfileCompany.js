// import React, { Component } from 'react';
// import {
//   View,
//   Text,
//   Image,
//   StyleSheet,
//   TouchableOpacity,
//   Animated,
// } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';
// import 'moment/locale/fr';
// import { theme } from '../../config/constants/index';
// import CardEditable from '../Card/CardEditable';
// import ErrorComponent from '../Elements/ErrorComponent';
// import LoadingComponent from '../Elements/LoadingComponent';
// import defaultBusinessHeader from '../../assets/defaultBusinessHeader.jpg';
// import defaultBusinessLogo from '../../assets/defaultBusiness.png';
// import CompanyAbout from './CompanyAbout';
// import CompanyQualities from './CompanyQualities';
// import CompanyOffers from './CompanyOffers';
// import CompanyEvents from './CompanyEvents';

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   mainSection: {
//     paddingLeft: 0,
//     paddingTop: 0,
//     paddingRight: 0,
//     paddingBottom: 17,
//     marginBottom: 0,
//   },
//   header: {
//     width: '100%',
//     height: 181,
//     resizeMode: 'cover',
//     opacity: 0.5,
//     borderRadius: 10,
//     backgroundColor: 'white',
//   },
//   logo: {
//     width: 112,
//     height: 112,
//     backgroundColor: 'white',
//     borderRadius: 4,
//     resizeMode: 'contain',
//     alignSelf: 'center',
//     marginTop: -56,
//   },
//   companyName: {
//     fontSize: 18,
//     lineHeight: 24,
//     fontWeight: '600',
//     alignSelf: 'center',
//     color: theme.fontBlack,
//     marginTop: 25.5,
//   },
//   emptySectionText: {
//     padding: 8,
//     lineHeight: 24,
//   },
//   noData: {
//     marginLeft: 8,
//     marginRight: 8,
//     marginTop: 8,
//   },
//   nbJobOffers: {
//     fontSize: 14,
//     lineHeight: 24,
//     textAlign: 'center',
//     color: '#1C1C1C',
//     letterSpacing: 0.5,
//     marginTop: 1.5,
//   },
//   headerBg: {
//     backgroundColor: theme.primaryColor,
//   },
//   companyNavBar: {
//     flexDirection: 'row',
//     marginBottom: 16,
//     backgroundColor: 'white',
//   },
//   companyNavBarButton: {
//     flex: 1,
//     borderBottomWidth: 4,
//     borderBottomColor: '#ffffff',
//   },
//   companyNavBarButtonActive: {
//     flex: 1,
//     borderBottomWidth: 4,
//     borderBottomColor: '#33cbcc',
//   },
//   companyNavBarButtonLabel: {
//     textAlign: 'center',
//     fontSize: 14,
//     lineHeight: 48,
//     color: '#afafaf',
//     fontWeight: '500',
//   },
//   companyNavBarButtonLabelActive: {
//     textAlign: 'center',
//     fontSize: 14,
//     lineHeight: 48,
//     color: '#1c1c1c',
//     fontWeight: '500',
//   },
// });

// class FullProfileCompany extends Component {
//   static navbar = [
//     { key: 'about', label: 'À propos' },
//     { key: 'qualities', label: 'Qualités' },
//     // { key: 'offers', label: 'Offres' },
//     { key: 'events', label: 'Évènements' },
//   ];

//   state = {
//     display: 'about',
//   };

//   displayNbAvailableOffers = (nbJobOffers) => {
//     return " ";
//   };

//   props: {
//     containerStyle: {},
//     companyId: string,
//     navigate: Function,
//     data: {
//       error: string,
//       singleCompany: {
//         editable: boolean,
//         firstname: string,
//         lastname: string,
//         wish_raw_salary: string,
//         wish_variable_salary: string,
//         work_city: [],
//         company_size: [],
//       },
//       loading: boolean,
//     },
//   };

//   render() {
//     if (this.props.data.error) {
//       return (<ErrorComponent message={'Erreur lors de récuperation de la page entreprise'} />);
//     } else if (this.props.data.loading) {
//       return (<LoadingComponent />);
//     }
//     const { navigate } = this.props;
//     const company = this.props.data.singleCompany;
//     if (!company) {
//       return (<LoadingComponent />);
//     }
//     return (
//       <View style={[styles.container, this.props.containerStyle]}>
//         <CardEditable
//           containerStyle={styles.mainSection}
//         >
//           <View style={styles.headerBg}>
//             <Image
//               source={company.header ? { uri: company.header } : defaultBusinessHeader}
//               style={styles.header}
//             />
//           </View>
//           <Image
//             source={company.logo ? { uri: company.logo } : defaultBusinessLogo}
//             style={styles.logo}
//           />
//           <Text style={styles.companyName}>{company.name}</Text>
//           <Text style={styles.nbJobOffers}>{
//             this.displayNbAvailableOffers(company.nb_job_offers).toUpperCase()}
//           </Text>
//         </CardEditable>
//         <Animated.View style={styles.companyNavBar}>
//           {
//             FullProfileCompany.navbar.map(b => (
//               <TouchableOpacity
//                 key={b.key}
//                 style={this.state.display === b.key ?
//                   styles.companyNavBarButtonActive : styles.companyNavBarButton}
//                 onPress={() => this.setState({ display: b.key })}
//               >
//                 <Text
//                   style={this.state.display === b.key ?
//                     styles.companyNavBarButtonLabelActive : styles.companyNavBarButtonLabel}
//                 >
//                   {b.label}
//                 </Text>
//               </TouchableOpacity>
//             ))
//           }
//         </Animated.View>
//         {
//           this.state.display === 'about' &&
//           <CompanyAbout
//             companyId={this.props.companyId}
//             navigate={navigate}
//           />
//         }
//         {
//           this.state.display === 'qualities' &&
//           <CompanyQualities
//             companyId={this.props.companyId}
//           />
//         }
//         {
//           this.state.display === 'offers' &&
//           <CompanyOffers
//             companyId={this.props.companyId}
//             navigate={navigate}
//           />
//         }
//         {
//           this.state.display === 'events' &&
//           <CompanyEvents
//             companyId={this.props.companyId}
//             navigate={navigate}
//           />
//         }
//       </View>
//     );
//   }
// }

// const QUERY_COMPANY = gql`
//   query CompanyQuery($company_id: String!) {
//     singleCompany(company_id: $company_id) {
//       company_id
//       header
//       logo
//       editable
//       description
//       name
//       tags
//       qualities
//       nb_job_offers
//     }
//   }
// `;

// export default graphql(QUERY_COMPANY, {
//   options: ownProps => ({
//     variables: {
//       company_id: ownProps.companyId,
//     },
//   }),
// })(FullProfileCompany);
// export { QUERY_COMPANY };

// import React from 'react';
// import {
//   View,
//   StyleSheet,
//   FlatList,
// } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';
// import 'moment/locale/fr';
// import ErrorComponent from '../Elements/ErrorComponent';
// import LoadingComponent from '../Elements/LoadingComponent';
// import EventCard from '../Events/EventCard';
// import EmptyCard from '../Card/EmptyCard';

// type Props = {
//   containerStyle: {},
//   userId: string,
//   navigate: Function,
//   data: {
//     error: string,
//     singleCompany: {
//       editable: boolean,
//       firstname: string,
//       lastname: string,
//       wish_raw_salary: string,
//       wish_variable_salary: string,
//       work_city: [],
//       company_size: [],
//     },
//     loading: boolean,
//   },
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   list: {
//     marginTop: 8,
//     marginLeft: 16,
//     marginRight: 16,
//     marginBottom: 8,
//   },
//   card: {
//     marginRight: 0,
//     marginBottom: 16,
//   },
// });

// const keyEventExtractor = event => event.event_id;

// const renderEventCard = (event, navigate) => (
//   <EventCard
//     navigate={navigate}
//     containerStyle={styles.card}
//     event_id={event.event_id}
//   />
// );

// const CompanyEvents = (props: Props) => {
//   if (props.data.error) {
//     return <ErrorComponent message='Erreur lors de récuperation de la page entreprise' />;
//   } else if (props.data.loading) {
//     return <LoadingComponent />;
//   }
//   const company = props.data.singleCompany;
//   if (!company) {
//     return (<LoadingComponent />);
//   }
//   const { navigate } = props;
//   return (
//     <View style={[styles.container, props.containerStyle]}>
//       <FlatList
//         style={styles.list}
//         data={company.events}
//         keyExtractor={keyEventExtractor}
//         keyboardShouldPersistTaps={'always'}
//         renderItem={({ item }) => renderEventCard(item, navigate)}
//         ListEmptyComponent={<EmptyCard title='Aucun évènement disponible' />}
//       />
//     </View>
//   );
// };

// const QUERY_COMPANY = gql`
//   query CompanyQuery($company_id: String!) {
//     singleCompany(company_id: $company_id) {
//       name
//       events(next: true) {
//         event_id
//       }
//     }
//   }
// `;

// export default graphql(QUERY_COMPANY, {
//   options: ownProps => ({
//     variables: {
//       company_id: ownProps.companyId,
//     },
//   }),
// })(CompanyEvents);

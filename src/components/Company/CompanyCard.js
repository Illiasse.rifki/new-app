// @flow
import React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import { theme } from '../../config/constants/index';
import defaultBusinessHeader from '../../assets/defaultBusinessHeader.jpg';
import defaultBusinessLogo from '../../assets/defaultBusiness.png';

type Props = {
  name: string,
  coverPicture: string,
  logo: string,
  onPress: Function,
  containerStyle: {},
  nbJobOffers: number,
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.8,
  },
  companyCoverImage: {
    width: '100%',
    height: 112,
    resizeMode: 'cover',
  },
  companyCoverContainer: {
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    overflow: 'hidden',
  },
  companyTextContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 8,
    paddingBottom: 12.5,
    paddingLeft: 16,
    paddingRight: 16,
  },
  companyName: {
    fontSize: 18,
    textAlign: 'center',
    fontWeight: '500',
    lineHeight: 24,
    color: theme.fontBlack,
  },
  logo: {
    width: 68,
    height: 68,
    backgroundColor: 'white',
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: -34,
    borderWidth: 1,
    borderColor: '#f5f5f5',
    borderRadius: 7,
  },
  description: {
    fontSize: 14,
    lineHeight: 24,
    color: '#afafaf',
  },
  goto: {
    fontSize: 14,
    color: theme.primaryColor,
    letterSpacing: 0.5,
    marginTop: 10,
    alignSelf: 'center',
  },
});

const displayNbAvailableOffers = (description) => 
  // let display = 'Aucune offre d\'emploi disponible';
  // if (nbJobOffers === 1) display = '1 offre d\'emploi disponible';
  // else if (nbJobOffers >= 2) display = `${nbJobOffers} offres d'emplois disponibles`;
   description
;

const CompanyCard = (props: Props) => (
  <TouchableOpacity onPress={props.onPress} style={[styles.container, props.containerStyle]}>
    <View style={styles.companyCoverContainer}>
      <Image
        source={props.coverPicture ? { uri: props.coverPicture } : defaultBusinessHeader}
        style={styles.companyCoverImage}
      />
      <Image
        source={props.logo ? { uri: props.logo } : defaultBusinessLogo}
        style={styles.logo}

      />
    </View>
    <View style={styles.companyTextContainer}>
      <Text style={styles.companyName}>{props.name}</Text>
      <Text style={styles.description}>{displayNbAvailableOffers(props.description)}</Text>
      <Text style={styles.goto}>VOIR</Text>
    </View>
  </TouchableOpacity>
);

export default CompanyCard;

// import React from 'react';
// import {
//   View,
//   StyleSheet,
// } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';
// import 'moment/locale/fr';

// import CardEditable from '../Card/CardEditable';
// import ErrorComponent from '../Elements/ErrorComponent';
// import LoadingComponent from '../Elements/LoadingComponent';
// import Qualities from '../Content/Qualities';

// type Props = {
//   containerStyle: {},
//   userId: string,
//   navigate: Function,
//   data: {
//     error: string,
//     singleCompany: {
//       qualities: [string]
//     },
//     loading: boolean,
//   },
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
// });

// const CompanyQualities = (props: Props) => {
//   if (props.data.error) {
//     return (<ErrorComponent message={'Erreur lors de récuperation de la page entreprise'} />);
//   } else if (props.data.loading) {
//     return (<LoadingComponent />);
//   }
//   const company = props.data.singleCompany;
//   if (!company) {
//     return (<LoadingComponent />);
//   }
//   return (
//     <View style={[styles.container, props.containerStyle]}>
//       <CardEditable
//         title="Qualités favorites"
//         containerstyle={styles.section}
//       >
//         <Qualities qualities={company.qualities} />
//       </CardEditable>
//     </View>
//   );
// };

// const QUERY_COMPANY = gql`
//   query CompanyQuery($company_id: String!) {
//     singleCompany(company_id: $company_id) {
//       qualities
//     }
//   }
// `;

// export default graphql(QUERY_COMPANY, {
//   options: ownProps => ({
//     variables: {
//       company_id: ownProps.companyId,
//     },
//   }),
// })(CompanyQualities);

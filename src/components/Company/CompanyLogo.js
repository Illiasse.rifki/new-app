// import React, { PureComponent } from 'react';
// import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import { theme } from '../../config/constants/index';
// import defaultBusiness from '../../assets/defaultBusiness.png';

// const logoWidth = 112;

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     display: 'flex',
//     flexDirection: 'column',
//     justifyContent: 'center',
//     width: '45%',
//   },
//   text: {
//     alignSelf: 'center',
//     fontSize: 18,
//     fontWeight: '500',
//     maxWidth: logoWidth,
//     color: theme.fontBlack,
//     marginTop: 10,
//     textAlign: 'center',
//   },
//   imageMutualContainer: {
//     width: logoWidth,
//     height: logoWidth,
//     backgroundColor: 'white',
//     borderRadius: 4,
//     resizeMode: 'contain',
//     borderColor: 'black',
//     borderWidth: 0.5,
//     // This cause warning on react-native < 0.55
//     //    borderStyle: 'solid',
//     justifyContent: 'center',
//     marginTop: 5,
//     flexWrap: 'wrap',
//   },
//   image: {
//     width: logoWidth,
//     height: logoWidth,
//     backgroundColor: 'white',
//     borderRadius: 4,
//     resizeMode: 'contain',
//     alignSelf: 'center',
//   },
// });

// class CompanyLogo extends PureComponent {
//   static defaultProps = {
//     onPress: () => null,
//   };

//   props: {
//     company_id: string,
//     companyById: {
//       loading: boolean,
//       companyById: {
//         company_id: string,
//         name: string,
//         logo: string,
//       },
//     },
//     eventType: String,
//     onPress: Function,
//   };

//   onPressLogo = () => {
//     const company = this.props.companyById.companyById;
//     if (this.props.onPress) {
//       this.props.onPress(company);
//     }
//   };

//   render = () => {
//     if (this.props.companyById.loading) {
//       return null;
//     }

//     const company = this.props.companyById.companyById;
//     return (
//       <View>
//         {this.props.eventType === 'mutual' ?
//           <TouchableOpacity onPress={this.onPressLogo}>
//             <Image style={styles.imageMutualContainer} source={company.logo ? { uri: company.logo } : defaultBusiness} />
//           </TouchableOpacity>
//           :
//           < Image style={styles.image} source={company.logo ? { uri: company.logo } : defaultBusiness} />
//         }
//         <Text style={styles.text}>{company.name}</Text>
//       </View>
//     );
//   };
// }

// const COMPANY_BY_ID_QUERY = gql`
//   query companyById($company_id: String!) {
//               companyById(company_id: $company_id) {
//               company_id
//       logo
//             name
//           }
//         }
//       `;

// export default graphql(COMPANY_BY_ID_QUERY, {
//   name: 'companyById',
//   options: ownProps => ({
//     variables: {
//       company_id: ownProps.company_id,
//     },
//   }),
// })(CompanyLogo);

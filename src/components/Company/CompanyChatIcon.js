// import React from 'react';
// import { Icon } from 'react-native-elements';
// import {
//   TouchableOpacity,
//   StyleSheet,
// } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// type Props = {
//   containerStyle: {},
//   userId: string,
//   navigation: {
//     navigate: Function,
//   },
//   data: {
//     error: string,
//     singleCompany: {
//       editable: boolean,
//       main_recruiter: {
//         user_id: string,
//       },
//     },
//     viewer: {
//       id: string,
//     },
//     loading: boolean,
//   },
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
// });

// const CompanyChatIcon = (props: Props) => {
//   if (props.data.error || props.data.loading) {
//     return null;
//   }
//   const company = props.data.singleCompany;
//   if (!company || !company.sponsor || !company.main_recruiter) {
//     return null;
//   }
//   return (
//     <TouchableOpacity style={[styles.container, props.containerStyle]}
//       onPress={() => props.navigation.navigate('Chat', {
//         target: {
//           id: company.main_recruiter.user_id,
//           fullname: company.name,
//           companyId: company.company_id,
//           type: 'recruiter',
//         },
//         user: {
//           user_id: props.data.viewer.id,
//         },
//       })}
//     >
//       <Icon name='chat' type='entypo' color='white' containerStyle={styles.rightIconStyle} />
//     </TouchableOpacity>
//   );
// };

// const QUERY_COMPANY = gql`
//   query CompanyQuery($company_id: String!) {
//     singleCompany(company_id: $company_id) {
//       company_id
//       sponsor
//       name
//       main_recruiter {
//         user_id
//       }
//     }
//     viewer {
//       id
//     }
//   }
// `;

// export default graphql(QUERY_COMPANY, {
//   options: ownProps => ({
//     variables: {
//       company_id: ownProps.companyId,
//     },
//   }),
// })(CompanyChatIcon);

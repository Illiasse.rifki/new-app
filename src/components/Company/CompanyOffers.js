// import React from 'react';
// import {
//   View,
//   StyleSheet,
// } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';
// import 'moment/locale/fr';

// import ErrorComponent from '../Elements/ErrorComponent';
// import LoadingComponent from '../Elements/LoadingComponent';

// import JobOffersList from '../JobOffer/JobOffersList';

// type Props = {
//   containerStyle: {},
//   userId: string,
//   navigate: Function,
//   data: {
//     error: string,
//     singleCompany: {
//       editable: boolean,
//       firstname: string,
//       lastname: string,
//       wish_raw_salary: string,
//       wish_variable_salary: string,
//       work_city: [],
//       company_size: [],
//     },
//     loading: boolean,
//   },
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   list: {
//     marginTop: 0,
//     marginLeft: 16,
//     marginRight: 16,
//   },
// });

// const CompanyOffers = (props: Props) => {
//   if (props.data.error) {
//     return (<ErrorComponent message={'Erreur lors de récuperation de la page entreprise'} />);
//   } else if (props.data.loading) {
//     return (<LoadingComponent />);
//   }
//   const company = props.data.singleCompany;
//   if (!company) {
//     return (<LoadingComponent />);
//   }
//   const { navigate } = props;
//   return (
//     <View style={[styles.container, props.containerStyle]}>
//       <JobOffersList
//         light
//         jobs={company.job_offers}
//         listStyle={styles.list}
//         onJobOfferPress={(jobOffer) => {
//           navigate('JobOffer', { jobOfferId: jobOffer.job_offer_id });
//         }}
//       />
//     </View>
//   );
// };

// const QUERY_COMPANY = gql`
//   query CompanyQuery($company_id: String!) {
//     singleCompany(company_id: $company_id) {
//       job_offers {
//         job_offer_id
//         title
//         date_publish(duration: true)
//         visibility_mode
//         cover_picture
//         location_name
//         location_departement
//         rating
//         company {
//           logo
//           name
//           header
//         }
//         tags
//         is_favorite: is_favorite_offer
//       }
//     }
//   }
// `;

// export default graphql(QUERY_COMPANY, {
//   options: ownProps => ({
//     variables: {
//       company_id: ownProps.companyId,
//     },
//   }),
// })(CompanyOffers);

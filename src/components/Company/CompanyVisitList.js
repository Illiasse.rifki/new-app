import React, { Component } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';

import CompanyLogo from './CompanyLogo';
import EmptyCard from '../Card/EmptyCard';
import UserSmallCard from '../Card/UserSmallCard';


const styles = StyleSheet.create({
  container: {

  },
});

const CompanyVisitList = (props: Props) => (
  <View style={[styles.container, props.containerStyle]}>
    <FlatList
      data={props.visitors}
      ListEmptyComponent={
        <EmptyCard
          title="Aucune visite sur l'entreprise"
          subtitle="Tirer vers le bas pour rafraichir"
        />
      }
      contentContainerStyle={styles.listContentStyle}
      keyExtractor={visitor => visitor.company_visitor_id}
      keyboardShouldPersistTaps={'always'}
      renderItem={({ item: visitor }) => {
        const { candidate } = visitor;
        if (!candidate) return null;
        return (
          <UserSmallCard
            offerTitle={`${visitor.nbviews} visite${visitor.nbviews > 1 ? 's' : ''}`}
            picture={candidate.picture}
            title={candidate.fullname}
            pictureTitle={`${candidate.firstname[0]}${candidate.lastname[0]}`}
            subtitle={candidate.headline}
            onPress={() => props.navigate('CandidateProfile', { userId: candidate.user_id })}
          />
        );
      }}
    />
  </View>
);

export default CompanyVisitList;

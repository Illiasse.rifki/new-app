// import React, { Component } from 'react';
// import {
//   View,
//   Text,
//   StyleSheet,
// } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';
// import 'moment/locale/fr';
// import { theme } from '../../config/constants/index';
// import CardEditable from '../Card/CardEditable';
// import LoadingComponent from '../Elements/LoadingComponent';
// import Tags from '../Content/Tags';
// import Map from '../Content/Map';
// import ErrorComponent from '../Elements/ErrorComponent';

// type Props = {
//   containerStyle: {},
//   userId: string,
//   navigate: Function,
//   data: {
//     error: string,
//     singleCompany: {
//       editable: boolean,
//       firstname: string,
//       lastname: string,
//       wish_raw_salary: string,
//       wish_variable_salary: string,
//       work_city: [],
//       company_size: [],
//     },
//     loading: boolean,
//   },
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   companyName: {
//     fontSize: 28,
//     lineHeight: 40,
//     fontWeight: 'bold',
//     alignSelf: 'center',
//     color: theme.fontBlack,
//     marginTop: 17.5,
//   },
//   emptySectionText: {
//     padding: 8,
//     lineHeight: 24,
//   },
//   tags: {
//     marginBottom: 8,
//     marginLeft: 8,
//     marginRight: 8,
//   },
//   validateButton: {
//     marginBottom: 24,
//     borderRadius: 4,
//     shadowColor: 'rgba(0, 0, 0, 0.08)',
//     shadowOffset: { width: 0.5, height: 4 },
//     shadowOpacity: 0.8,
//     shadowRadius: 2,
//     width: '90%',
//     height: 40,
//     marginTop: 24,
//     alignSelf: 'center',
//   },
//   experience: {
//     marginTop: 22,
//     paddingLeft: 8,
//     paddingRight: 8,
//     marginBottom: 2,
//   },
//   noData: {
//     marginLeft: 8,
//     marginRight: 8,
//     marginTop: 8,
//   },
//   mapContainer: {
//     flex: 1,
//     marginTop: 17,
//     marginLeft: 8,
//     marginRight: 8,
//     height: 310,
//   },
//   mapContainerFullscreen: {
//     flex: 1,
//     marginTop: 17,
//     marginBottom: 0,
//   },
//   section: {

//   },
//   sectionFullscreen: {
//     position: 'absolute',
//     top: -80,
//     bottom: -40,
//     left: 0,
//     right: 0,
//     paddingLeft: 0,
//     paddingRight: 0,
//     paddingBottom: 0,
//     backgroundColor: 'white',
//   },
//   titleFullscreen: {
//     marginLeft: 16,
//   },
// });

// class CompanyAbout extends Component {
//   state = {
//     mapFullscreen: false,
//   };

//   mapJobOffersLocation = (jobOffers) => {
//     const jobs = jobOffers
//       .filter(j => j.location_lat_lng && j.location_lat_lng.length === 2)
//       .map(j => ({
//         ...j,
//         location: {
//           latitude: j.location_lat_lng[0],
//           longitude: j.location_lat_lng[1],
//         },
//       }));
//     return (jobs);
//   };

//   render() {
//     if (this.props.data.error) {
//       return (<ErrorComponent message={'Erreur lors de récuperation de la page entreprise'} />);
//     } else if (this.props.data.loading) {
//       return (<LoadingComponent />);
//     }
//     const company = this.props.data.singleCompany;
//     if (!company) {
//       return (<LoadingComponent />);
//     }
//     const { navigate } = this.props;
//     const { mapFullscreen } = this.state;
//     return (
//       <View style={[styles.container, this.props.containerStyle]}>
//         <CardEditable
//           title="Qui sommes-nous ?"
//           containerStyle={styles.section}
//         >
//           <Text style={styles.emptySectionText}>
//             {
//               company.description ? company.description : 'Aucune description ajoutée'
//             }
//           </Text>
//         </CardEditable>
//         <CardEditable
//           title="Tags appréciés"
//           containerStyle={styles.section}
//         >
//           <Tags tags={company.tags} />
//         </CardEditable>
//         {/* {
//           company && company.job_offers.length > 0 &&
//           <CardEditable
//             title="Lieu des offres"
//             rightIcon={{
//               name: 'size-fullscreen',
//               type: 'simple-line-icon',
//               onPress: () => this.setState({ mapFullscreen: !mapFullscreen })
//             }}
//             containerStyle={mapFullscreen ? styles.sectionFullscreen : styles.section}
//             titleStyle={mapFullscreen && styles.titleFullscreen}
//           >
//             <View style={mapFullscreen ? styles.mapContainerFullscreen : styles.mapContainer}>
//               <Map
//                 width={mapFullscreen ? undefined : 310}
//                 navigate={navigate}
//                 jobs={this.mapJobOffersLocation(company.job_offers)}
//               />
//             </View>
//           </CardEditable>
//         } */}
//       </View>
//     );
//   }
// }

// const QUERY_COMPANY = gql`
//   query CompanyQuery($company_id: String!) {
//     singleCompany(company_id: $company_id) {
//       description
//       tags
//       job_offers {
//         job_offer_id
//         title
//         location_lat_lng
//         contract_type
//         sectors
//       }
//     }
//   }
// `;

// export default graphql(QUERY_COMPANY, {
//   options: ownProps => ({
//     variables: {
//       company_id: ownProps.companyId,
//     },
//   }),
// })(CompanyAbout);

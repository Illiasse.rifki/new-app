import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

import CompanyLogo from './CompanyLogo';


const styles = StyleSheet.create({
  container: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
});

export default class CompaniesLogo extends Component {
  static defaultProps = {
    companiesIds: [],
  };

  props: {
    companiesIds: Array<number>,
    onPress: Function,
    eventType: String,
  };

  render = () => (
    <View style={styles.container}>
      {this.props.companiesIds.map(c => <CompanyLogo eventType={this.props.eventType} onPress={this.props.onPress} company_id={c} key={c} />)}
    </View>
  );
}

import styled from 'styled-components';
import { theme } from '../../config/constants';

const FieldTitle = styled.Text`
  line-height: 24px;
  font-size: 18px;
  font-weight: 500;
  color: ${theme.fontBlack};
`;

export default FieldTitle;
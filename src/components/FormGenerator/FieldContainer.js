import styled from 'styled-components';

const FieldContainer = styled.View`
  margin-top: 10px;
  margin-bottom: 10px;
`;

export default FieldContainer;
import React, { Component } from 'react';
import { connect } from 'react-redux';

import WorkCities from '../Content/WorkCities';
import Title from './blocks/Title';
import NumberField from './blocks/NumberField';
import CompanySizeField from './blocks/CompanySizeField';
import ActivitySectorField from './blocks/ActivitySectorField';
import TextField from './blocks/TextField';
import { resetKeyboardHelper, setCurrentRef, setInputRefs } from '../../actions/keyboardHelper';

// blocks
import Container from './Container';
import FieldContainer from './FieldContainer';
import FieldTitle from './FieldTitle';


class FormGenerator extends Component {
  fields = {
    title: {
      component: props => <Title {...props} />,
    },
    number: {
      component: (props, index) => <NumberField
        {...props}
        getRef={ref => this.getRef(ref, index) }
      />,
    },
    text: {
      component: (props, index) => <TextField
        {...props}
        getRef={ref => this.getRef(ref, index) }
      />,
    },
    'company-size-picker': {
      component: props => <CompanySizeField {...props} />,
    },
    'activity-sector-picker': {
      component: props => <ActivitySectorField {...props} />,
    },
    'work-cities': {
      component: props => <WorkCities {...props} />,
    },
  };

  componentDidMount() {
    this.props.setInputRefs(this.inputRefs);
  }

  getRef = (ref, index) => { this.inputRefs[index] = ref; };

  setCurrentRef = (index) => {
    const { inputRefs } = this.props;
    this.props.setCurrentRef(inputRefs[index]);
  };

  constructor(props) {
    super(props);
    const size = props.buildFields.length;
    this.inputRefs = new Array(size);
    if (props.smartNavigation) {
      props.resetKeyboardHelper({
        navigationButton: props.navigationButton,
      });
    }
  }

  props: {
    resetKeyboardHelper: any,
    setCurrentRef: any,
    inputRefs: [],
    setInputRefs: [],
    buildFields: [],
    smartNavigation: boolean,
    navigationButton: {
      name: string,
      onPress: Function,
    },
  };

  renderField = (field, data, index) => {
    const fieldsProps = !this.props.smartNavigation ? data.props : {
      ...data.props,
      onFocus: () => this.setCurrentRef(index),
    };

    const fieldComponent = field.component(fieldsProps, index);
    return (
      <FieldContainer style={data.fieldStyle} key={data.key}>
        {
          data.title && <FieldTitle>{data.title}</FieldTitle>
        }
        {fieldComponent}
      </FieldContainer>
    );
  };

  render() {
    const { buildFields } = this.props;
    return (
      <Container>
        {
          buildFields.map((fieldData, index) => {
            const field = this.fields[fieldData.type];
            if (field) return this.renderField(field, fieldData, index);
            return null;
          })
        }
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  inputRefs: state.keyboardHelper.inputRefs,
});

const mapDispatchToProps = dispatch => ({
  setInputRefs: inputRefs => dispatch(setInputRefs(inputRefs)),
  setCurrentRef: currentRef => dispatch(setCurrentRef(currentRef)),
  resetKeyboardHelper: params => dispatch(resetKeyboardHelper(params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FormGenerator);

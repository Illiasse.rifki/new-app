import React, { Fragment } from 'react';
import styled from 'styled-components';
import { theme } from '../../../config/constants';

type Props = {
  title: string,
  subtitle: string,
};

const TitleText = styled.Text`
  line-height: 24px;
  font-size: 18px;
  font-weight: 500;
  color: ${theme.fontBlack};
`;

const SubtitleText = styled.Text`
  line-height: 16px;
  font-size: 14px;
  font-style: italic;
  color: ${theme.fontBlack};
`;

const Title = (props: Props) => {
  return (
    <Fragment>
      <TitleText>{props.title}</TitleText>
      <SubtitleText>{props.subtitle}</SubtitleText>
    </Fragment>
  );
};

export default Title;
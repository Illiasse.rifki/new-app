import React from 'react';
import { StyleSheet, View } from 'react-native';
import styled from 'styled-components';
import { FormLabel } from 'react-native-elements';
import CompanySizesPicker from '../../Content/CompanySizesPicker';
import { theme } from '../../../config/constants';

const Italic = styled.Text`
  font-style: italic;
`;

type Props = {
  label: string,
  info: string,
  selectedSizes: [],
  onCompanySizesChange: Function,
};

const styles = StyleSheet.create({
  formLabelContainer: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
  },
  formLabelContent: {
    marginTop: 0,
    marginRight: 0,
    marginLeft: 0,
    color: '#afafaf',
    letterSpacing: 0.5,
    fontSize: 14,
    lineHeight: 20,
    fontWeight: '500',
  },
  companySizeButtonsStyle: {
    marginTop: 10,
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 0,
    backgroundColor: theme.secondaryColor,
    borderColor: '#afafaf',
  },
});


const CompanySizeField = (props: Props) => {
  return (
    <View>
      <FormLabel
        labelStyle={styles.formLabelContent}
        containerStyle={styles.formLabelContainer}
      >
        {props.label.toUpperCase()}
        <Italic>{props.info}</Italic>
      </FormLabel>
      <CompanySizesPicker
        containerStyle={styles.companySizeButtonsStyle}
        onCompanySizesChange={props.onCompanySizesChange}
        selectedSizes={props.selectedSizes}
      />
    </View>
  );
};

export default CompanySizeField;
import React  from 'react';
import { StyleSheet } from 'react-native';
import styled from 'styled-components';
import { theme } from '../../../config/constants';
import { FormLabel, FormInput } from 'react-native-elements';

type Props = {
  label: string,
  placeholder: string,
  onChangeText: Function,
  value: any,
};

const Container = styled.View`
`;

const styles = StyleSheet.create({
  formLabelContainer: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 17,
  },
  formLabelContent: {
    marginTop: 0,
    marginRight: 0,
    marginLeft: 0,
    color: '#afafaf',
    letterSpacing: 0.5,
    fontSize: 14,
    lineHeight: 20,
    fontWeight: '500',
  },
  formInputText: {
    color: theme.fontBlack,
    width: '100%',
    fontSize: 16,
    marginBottom: -5,
  },
  formInputContainer: {
    marginRight: 0,
    marginLeft: 0,
  },
});

const NumberField = (props: Props) => {
  return (
    <Container>
      {
        props.label &&
        <FormLabel
          labelStyle={styles.formLabelContent}
          containerStyle={styles.formLabelContainer}
        >
          {props.label.toUpperCase()}
        </FormLabel>
      }
      <FormInput
        placeholder={props.placeholder}
        textInputRef={props.getRef}
        onFocus={props.onFocus}
        autoCapitalize="none"
        keyboardType="number-pad"
        inputStyle={styles.formInputText}
        containerStyle={styles.formInputContainer}
        onChangeText={props.onChangeText}
        value={props.value}
        underlineColorAndroid='#979797'
      />
    </Container>
  );
};

export default NumberField;
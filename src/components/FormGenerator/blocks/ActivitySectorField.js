import React from 'react';
import { StyleSheet, View } from 'react-native';
import { sectors } from '../../../config/constants';
import SquareButtonField from '../../Design/SquareButtonField';
import ListKeyValueDisplay from '../../Elements/ListKeyValueDisplay';

type Props = {
  activities: [],
  info: string,
  selectedSizes: [],
  onCompanySizesChange: Function,
};

const styles = StyleSheet.create({
  activitiesView: {
    marginTop: 5,
    marginBottom: 5,
  },
});

const ActivitySectorField = (props: Props) => {
  return (
    <SquareButtonField
      label={'Secteur(s) d’activité(s)'.toUpperCase()}
      labelStyle="light"
      subtitle={
        <ListKeyValueDisplay
          containerStyle={styles.activitiesView}
          referenceKeys={sectors}
          keys={props.activities || []}
          limit={5}
        />
      }
      title={'Modifier vos secteurs'}
      onPress={props.onPress}
    />
  );
};

export default ActivitySectorField;
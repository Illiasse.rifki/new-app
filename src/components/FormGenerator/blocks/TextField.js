import React  from 'react';
import { StyleSheet } from 'react-native';
import Field from '../../Design/Field';

type Props = {
  label: string,
  placeholder: string,
  onChangeText: Function,
  value: any,
  multiline: boolean,
  getRef: Function,
  secureTextEntry: boolean,
};

const TextField = (props: Props) => {
  return (
    <Field
      label={props.label}
      textInputRef={props.getRef}
      onFocus={props.onFocus}
      onChangeText={props.onChangeText}
      placeholder={props.placeholder}
      value={props.value}
      multiline={props.multiline}
      autoCapitalize={props.autoCapitalize}
      secureTextEntry={props.secureTextEntry}
    />
  );
};

export default TextField;
import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import { theme } from '../../config/constants/index';

const styles = StyleSheet.create({
  button: {
    marginBottom: 24,
    marginTop: 19,
    borderRadius: 4,
    shadowRadius: 2,
    shadowOffset: { width: 1, height: 4 },
    shadowColor: 'rgba(0, 0, 0, 0.16)',
    borderWidth: 1,
    borderColor: 'transparent',
    paddingRight: 20,
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
  },
  textStyleAlt: {
    color: theme.fontBlack,
    fontSize: 16,
  },
  textStyle: {
    color: theme.primaryColor,
    fontSize: 16,
  },
});

const bg = 'transparent';

export default class FlatButton extends Component {
  static defaultProps = {
    title: 'Voir',
    onPress: () => null,
  }

  props: {
    title: ?string,
    containerStyle: string,
    onPress: ?Function,
    buttonStyle: string,
  };

  render() {
    return (
      <Button
        containerViewStyle={this.props.containerStyle}
        onPress={this.props.onPress}
        title={this.props.title}
        backgroundColor={bg}
        buttonStyle={[styles.button, this.props.buttonStyle]}
        textStyle={styles.textStyle}
      />
    );
  }
}

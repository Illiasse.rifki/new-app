import React, { PureComponent } from 'react';
import { StyleSheet, Platform } from 'react-native';
import { Button } from 'react-native-elements';
import { theme } from '../../config/constants/index';

const styles = StyleSheet.create({
  button: {
    marginBottom: 24,
    marginTop: 19,
    borderRadius: 4,
    shadowRadius: 2,
    shadowOffset: { width: 0, height: 0.5 },
    shadowColor: 'rgba(0, 0, 0, 0.16)',
    shadowOpacity: 0.8,
    borderWidth: 1,
    borderColor: Platform.OS === 'ios' ? 'transparent' : '#1c1c1c1c',
    paddingRight: 20,
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
  },
  textStyleAlt: {
    color: theme.fontBlack,
    fontSize: 15,
  },
  textStyle: {
    color: 'white',
    fontSize: 15,
  },
});

const bg = theme.primaryColor;
const bgAlt = 'white';

export default class RaisedButton extends PureComponent {
  static defaultProps = {
    title: 'Voir',
    onPress: () => null,
  };

  props: {
    title: ?string,
    containerStyle: string,
    onPress: ?Function,
    buttonStyle: string,
    alt: ?Boolean,
  };

  render = () => {
    const { alt, onPress, title, buttonStyle, containerStyle, disabled } = this.props;
    let { textStyle } = styles;
    let backgroundColor = bg;
    if (alt) {
      textStyle = styles.textStyleAlt;
      backgroundColor = bgAlt;
    }
    return (
      <Button
        disabled={disabled}
        containerViewStyle={containerStyle}
        onPress={onPress}
        title={title}
        backgroundColor={backgroundColor}
        buttonStyle={[styles.button, buttonStyle]}
        textStyle={textStyle}
      />
    );
  };
}

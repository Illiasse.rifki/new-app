import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import { theme } from '../../config/constants/index';

const styles = StyleSheet.create({
  bottomPageButton: {
    backgroundColor: theme.primaryColor,
    shadowColor: 'rgba(0, 0, 0, 0.16)',
    shadowOffset: { width: 0, height: 1.5 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    paddingTop: 13.5,
    paddingBottom: 13.5,
  },
  bottomPageButtonContainer: {
    marginLeft: 0,
    marginRight: 0,
  },
  bottomPageButtonText: {
    color: theme.secondaryColor,
    fontSize: 18,
    letterSpacing: 0.5,
    fontWeight: '500',
  },
  bottomPageButtonLoadingText: {
    color: theme.secondaryColor,
    fontSize: 18,
    letterSpacing: 0.5,
    fontWeight: '500',
  },
  loadingStyle: {
    height: 23,
  },
});

export default class BottomButton extends Component {
  static defaultProps = {
    title: 'SUITE',
    onPress: () => null,
  };

  props: {
    title: ?string,
    disabled: boolean,
    onPress: ?Function,
    loading: boolean,
  };

  render() {
    return (
      <Button
        onPress={this.props.onPress}
        title={this.props.loading ? '' : this.props.title}
        loading={this.props.loading}
        containerViewStyle={styles.bottomPageButtonContainer}
        buttonStyle={styles.bottomPageButton}
        color={this.props.loading ? theme.fontBlack : theme.secondaryColor}
        activityIndicatorStyle={styles.loadingStyle}
        textStyle={styles.bottomPageButtonText}
        disabled={this.props.loading ? true : this.props.disabled}
      />
    );
  }
}

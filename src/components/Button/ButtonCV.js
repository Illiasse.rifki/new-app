// import React, { Component } from "react";
// import { StyleSheet } from "react-native";
// import { withNavigation } from "react-navigation";
// import { Button } from "react-native-elements";
// // import { ReactNativeFile } from "apollo-upload-client";
// import Expo from "expo";
// import { compose } from "recompose";
// // import { graphql } from "react-apollo";
// import gql from "graphql-tag";
// import { theme } from "../../config/constants";
// import { connectAlert } from "../Alert";

// const styles = StyleSheet.create({
//   buttonContainer: {
//     width: "80%",
//     marginTop: 26,
//     marginLeft: 0,
//     marginRight: 0,
//   },
//   buttonCV: {
//     borderRadius: 4,
//     shadowColor: "rgba(0, 0, 0, 0.16)",
//     shadowOffset: {
//       width: 0,
//       height: 1.5,
//     },
//     shadowOpacity: 0.8,
//     shadowRadius: 2,
//   },
//   buttonTextCV: {
//     color: theme.secondaryColor,
//   },
// });

// class ButtonCV extends Component {
//   // static propTypes = {
//   //   onUpload: Function,
//   //   title: String,
//   //   navigation: Object,
//   //   alertWithType: Function,
//   //   mutate: Function,
//   //   data: Object,
//   //   error: Object,
//   // }

//   static defaultProps = {
//     title: "Télécharger mon CV",
//     alertWithType: () => null,
//   };

//   handleUpload = () => {
//     const { navigate } = this.props.navigation;
//     Expo.DocumentPicker.getDocumentAsync({ type: "*/*" })
//       .then((res) => {
//         // const file = new ReactNativeFile({
//         //   uri: res.uri,
//         //   type: "application/pdf",
//         //   name: res.name,
//         // });
//         const file = null;
//         this.props
//           .mutate({
//             variables: {
//               file,
//             },
//           })
//           .then(() =>
//             navigate("CandidateFormPreview", {
//               transition: "noTransition",
//             })
//           )
//           .catch((e) => {
//             this.props.alertWithType(
//               "info",
//               "Information",
//               "Réessayer d'envoyer votre pdf plus tard."
//             );
//           });
//       })
//       .catch((err) => {
//         if (err) {
//           this.props.alertWithType(
//             "info",
//             "Information",
//             "Fichier incompatible."
//           );
//         }
//       });
//   };

//   render() {
//     const { title } = this.props;
//     const { buttonStyle, textStyle, containerViewStyle } = styles;
//     return (
//       <Button
//         title={title}
//         backgroundColor={theme.primaryColor}
//         buttonStyle={buttonStyle}
//         textStyle={textStyle}
//         containerViewStyle={containerViewStyle}
//         onPress={this.handleUpload}
//       />
//     );
//   }
// }

// const MUTATION = gql`
//   mutation ($file: Upload!) {
//     upload(file: $file) {
//       uploaded
//     }
//   }
// `;

// export default compose(
//   connectAlert,
//   withNavigation,
//   graphql(MUTATION)
// )(ButtonCV);

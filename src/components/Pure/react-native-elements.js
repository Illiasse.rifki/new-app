import React from 'react';
import { pure } from 'recompose';
import { Icon } from 'react-native-elements';

const PureIcon = pure((props: props) => <Icon {...props} />);

export { PureIcon as Icon };
import React, { PureComponent } from 'react';
import { View, StyleSheet, Text } from 'react-native';

import { theme } from '../../config/constants/index';
import EventCardSmall from './EventCardSmall';
import ErrorComponent from '../Elements/ErrorComponent';
import LoadingComponent from '../Elements/LoadingComponent';

const styles = StyleSheet.create({
  container: {
  },
  title: {
    fontSize: 28,
    fontWeight: 'bold',
    color: theme.fontBlack,
    marginBottom: 17,
    lineHeight: 32,
  },
});

class EventsRegistration extends PureComponent {
  componentWillReceiveProps(nextProps) {
    if (nextProps.refresh && this.props.data) {
      this.props.data.refetch().catch(e => console.log('catch refetch Events.js', e));
    }
  }

  props: {
    more: boolean,
    navigate: Function,
    data: {
      error: boolean,
      loading: boolean,
      events: [{
        event_registration_id: string,
      }],
    },
    route: string,
    routeName: string,
    isRecruiter: boolean,
    containerStyle: string,
    title: string,
    big: boolean,
    refresh: boolean,
  };

  render() {
    const { error, loading } = this.props.data;

    if (error) {
      return <ErrorComponent message="Problème lors de la récupération la page des events." />;
    } else if (loading) {
      return <LoadingComponent />;
    }

    const registrations = this.props.data.events.filter(e => e.event);
    if (!registrations || registrations.length === 0) {
      return null;
    }

    return (
      <View style={[styles.container, this.props.containerStyle]}>
        { this.props.title ? (<Text style={styles.title}>{this.props.title}</Text>) : null }
        <View>
          {
            registrations.map(e =>
              (
                <EventCardSmall
                  isRecruiter={this.props.isRecruiter}
                  key={e.event.event_id}
                  event_id={e.event.event_id}
                  status={e.status}
                  navigate={this.props.navigate}
                  refresh={this.props.refresh}
                />
              ))
          }
        </View>
      </View>);
  }
}

export default EventsRegistration;

import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';
import EventCard from './EventCard';
import ErrorComponent from '../Elements/ErrorComponent';
import LoadingComponent from '../Elements/LoadingComponent';

const styles = StyleSheet.create({
  title: {
    fontSize: 28,
    fontWeight: 'bold',
    color: 'black',
  },
  background: {
    marginTop: 14,
    paddingBottom: 24,
  //  alignSelf: 'center',
  },
});

class Events extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.refresh && this.props.data) {
      this.props.data.refetch().catch(e => console.log('catch refetch Events.js', e));
    }
  }

  props: {
    navigate: Function,
    title: string,
    containerStyle: string,
    isRecruiter: boolean,
    refresh: boolean,
    data: {
      loading: boolean,
      error: boolean,
      refetch: Function,
      events: Array<{
        event_id: string
      }>,
    }
  };

  render = () => {
    const { error, loading } = this.props.data;
    if (error) {
      return <ErrorComponent message="Problème lors de la récupération d'event" />;
    } else if (loading) {
      return <LoadingComponent size={30} transparent />;
    }

    const { data, isRecruiter } = this.props;
    const { events } = data;
    if (events.length === 0) return null;
    return (
      <View style={this.props.containerStyle}>
        <Text style={styles.title}>{this.props.title}</Text>
        <ScrollView
          style={styles.background}
          horizontal
          showsHorizontalScrollIndicator={false}
        >
          {events.map(e => (
            <EventCard
              isRecruiter={isRecruiter}
              key={e.event_id}
              event_id={e.event ? e.event.event_id : e.event_id}
              navigate={this.props.navigate}
              refresh={this.props.refresh}
            />
          ))}
        </ScrollView>
      </View>
    );
  };
}

export default Events;

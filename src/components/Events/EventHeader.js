// // @flow
// import React, { Component } from 'react';
// import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';

// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import CompanyLogo from '../../components/Company/CompanyLogo';

// import Button from '../Button/RaisedButton';
// import { theme } from '../../config/constants/index';
// import defaultCoverPicture from '../../assets/defaultBusinessHeader.jpg';

// type Props = {
//   event: {
//     event_id: string,
//     status: boolean,
//     name: string,
//     picture: string,
//     detail: string,
//     date_format: string,
//   },
//   navigate: Function,
//   isRecruiter: boolean,
//   containerStyle: {},
//   questionsContainerStyle: {},
//   questionContainerStyle: {},
//   questionTextStyle: {},
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   screenBackground: {
//     backgroundColor: theme.secondaryColor,
//   },
//   typeTextMutual: {
//     fontSize: 14,
//     color: '#1c1c1c',
//     textAlign: 'left',
//     marginBottom: 15,
//     marginRight: 16,
//     marginLeft: 16,
//     lineHeight: 20,
//   },
//   titleMutual: {
//     fontSize: 18,
//     color: '#1c1c1c',
//     fontWeight: '500',
//     marginBottom: 7,
//     marginTop: 25,
//     textAlign: 'center',
//   },
//   titleDedicated: {
//     fontSize: 18,
//     color: '#1c1c1c',
//     fontWeight: '800',
//     margin: 0,
//     marginTop: 10,
//     marginBottom: 10,
//     alignSelf: 'center',
//     textAlign: 'center',
//   },
//   dateDedicated: {
//     fontSize: 14,
//     color: '#1c1c1c',
//     textAlign: 'left',
//     marginBottom: 15,
//     marginRight: 16,
//     marginLeft: 16,
//     lineHeight: 20,
//   },
//   borderDedicated: {
//     alignItems: 'center',
//     alignSelf: 'center',
//     margin: 0,
//     marginTop: 10,
//     borderWidth: 0.4,
//     borderColor: 'grey',
//     width: 250,
//   },
// });

// const EVENT_REGISTRATION_BY_EVENT_ID = gql`
//   query event_by_id($event_id: String!) {
//     event_registration_by_event_id(event_id: $event_id, is_recruiter: true) {
//       event_registration_id
//       status
//     }
//   }
// `;

// class ButtonCandidate extends Component {
//   props: any;

//   shouldComponentUpdate(nextProps, nextState) {
//     const next = JSON.stringify({ props: nextProps, state: nextState });
//     const now = JSON.stringify({ props: this.props, state: this.state });
//     return next !== now;
//   }

//   render() {
//     if (!this.props.data) return null;
//     const eri = this.props.data.event_registration_by_event_id;
//     if (!eri || eri.loading || eri.error || eri.status !== 'accepted') return null;
//     return (
//       <Button
//         title='Voir les candidats séléctionnés'
//         onPress={() => this.props.navigate('CandidatesStack', {
//           event_id: this.props.event_id,
//           name: 'Liste des candidats',
//         })}
//       />
//     );
//   }
// }

// const ButtonCandidates = graphql(EVENT_REGISTRATION_BY_EVENT_ID, {
//   options: props => ({
//     fetchPolicy: 'network-only',
//     variables: {
//       event_id: props.event_id,
//     },
//   }),
// })(ButtonCandidate);

// class EventHeader extends Component {
//   props: Props;

//   shouldComponentUpdate(nextProps, nextState) {
//     const next = JSON.stringify({ props: nextProps, state: nextState });
//     const now = JSON.stringify({ props: this.props, state: this.state });
//     return next !== now;
//   }

//   render() {
//     return (
//       <View style={[styles.container, this.props.containerStyle]}>
//         {/* if the event is dedicated => change logo of the company */}
//         {
//           (this.props.event.type === 'dedicated') ?
//             <TouchableOpacity
//               onPress={() => {
//                 this.props.navigate('CompanyProfile', { companyId: this.props.event.companies[0].company_id });
//               }}
//               style={styles.screenBackground}
//             >
//               <Image style={{ width: '100%', height: 164 }} source={this.props.event.picture ? { uri: this.props.event.picture } : defaultCoverPicture} />
//               <View
//                 style={{ marginTop: -55 }}>
//                 {
//                   this.props.event
//                   && Array.isArray(this.props.event.companies)
//                   && this.props.event.companies[0]
//                   && (
//                     <CompanyLogo
//                       eventType={this.props.event.type}
//                       company_id={this.props.event.companies[0].company_id}
//                       onPress={(company) => {
//                         this.props.navigate('CompanyProfile', { companyId: company.company_id });
//                       }}
//                     />
//                   )
//                 }
//                 <View style={styles.borderDedicated}></View>
//                 <Text style={styles.titleDedicated}>{this.props.event.name}</Text>
//                 <Text style={styles.dateDedicated}>{this.props.event.date_format}</Text>
//               </View>
//             </TouchableOpacity>
//             :
//             <View style={{ backgroundColor: theme.secondaryColor }}>
//               <Image style={{ width: '100%', height: 164 }} source={this.props.event.picture ? { uri: this.props.event.picture } : defaultCoverPicture} />
//               <Text style={styles.titleMutual}>{this.props.event.name} - {this.props.event.date_format}</Text>
//               <Text style={styles.typeTextMutual}>{this.props.event.detail}</Text>
//             </View>
//         }

//         {this.props.isRecruiter && (<ButtonCandidates
//           navigate={this.props.navigate}
//           event_id={this.props.event.event_id}
//         />)}
//       </View>
//     );
//   }
// }

// export default EventHeader;

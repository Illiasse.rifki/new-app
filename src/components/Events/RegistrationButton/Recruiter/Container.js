import styled from 'styled-components';

export default styled.View`
  margin-left: 32px;
  margin-right: 32px;
`;
// import React, { Component, Fragment } from 'react';
// import { View, StyleSheet, Text } from 'react-native';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';

// import { connectAlert } from '../../../Alert/index';
// import LoadingComponent from '../../../Elements/LoadingComponent';
// import ErrorComponent from '../../../Elements/ErrorComponent';
// import RaisedButton from '../../../Button/RaisedButton';

// // blocks
// import Container from './Container';
// import Status from './Status/index';

// const styles = StyleSheet.create({
//   eventButton: {
//     marginTop: 24,
//     marginBottom: 24,
//     marginLeft: 16,
//     marginRight: 16,
//   },
// });

// const EVENT_REGISTRATION_BY_EVENT_ID = gql`
//   query event_by_id($event_id: String!) {
//     event_registration: event_registration_by_event_id(event_id: $event_id, is_recruiter: true) {
//       event_registration_id
//       status
//     }
//   }
// `;

// class RecruiterEventRegistrationButton extends Component {
//   onPress = async () => {
//     this.props.navigate('EventRegistrationRecruiter', { event_id: this.props.eventId });
//   };

//   shouldComponentUpdate(nextProps, nextState) {
//     const next = JSON.stringify({ props: nextProps, state: nextState });
//     const now = JSON.stringify({ props: this.props, state: this.state });
//     return next !== now;
//   }

//   renderStatus = () => {
//     const { event_registration } = this.props.eventById;
//     const { navigate } = this.props;
//     return (
//       <Status
//         navigate={navigate}
//         eventRegistration={event_registration}
//       />
//     );
//   };

//   renderPostulateButton = () => (
//     <RaisedButton
//       buttonStyle={styles.eventButton}
//       title={'Je veux participer'}
//       onPress={this.onPress}
//     />
//   );

//   render() {
//     const { loading, error, event_registration } = this.props.eventById;
//     if (loading) return <LoadingComponent />;
//     if (error) {
//       return <ErrorComponent message="Erreur de récupèration de l'évènement" />;
//     }
//     const { hideIfStatus } = this.props;
//     if (hideIfStatus) return null;

//     if (event_registration) {
//       return (
//         <Container>
//           {this.renderStatus()}
//         </Container>
//       );
//     }

//     return !this.props.isPast && this.renderPostulateButton();
//   }
// }

// const query = graphql(EVENT_REGISTRATION_BY_EVENT_ID, {
//   name: 'eventById',
//   options: ownProps => ({
//     fetchPolicy: 'network-only',
//     variables: {
//       event_id: ownProps.eventId,
//     },
//   }),
// });

// export { EVENT_REGISTRATION_BY_EVENT_ID };
// export default (query)(connectAlert(RecruiterEventRegistrationButton));

import React, { Component } from 'react';
import { theme } from '../../../../../config/constants/index';

// blocks
import Container from './Container';
import Label from './Label';

class RecruiterEventRegistrationButtonStatus extends Component {
  static status = {
    accepted: { value: 'Participation validé', style: { color: theme.green } },
    awaiting: { value: 'Participation en attente', style: { color: theme.primaryColor } },
    pending: { value: 'Participation en attente', style: { color: theme.primaryColor } },
    refused: { value: 'Participation refusée', style: { color: theme.red } },
  };

  props: {
    eventRegistration: {},
  };

  shouldComponentUpdate(nextProps, nextState) {
    const next = JSON.stringify({ props: nextProps, state: nextState });
    const now = JSON.stringify({ props: this.props, state: this.state });
    return next !== now;
  }

  renderStatus = () => {
    const { status } = this.props.eventRegistration;
    const statusInfos = RecruiterEventRegistrationButtonStatus.status[status] || {
      value: status,
      style: { color: 'black '},
    };

    return <Label style={statusInfos.style}>{statusInfos.value}</Label>
  };

  render = () => {
    const { status } = this.props.eventRegistration;
    return status !== 'accepted' && (
      <Container>
        {this.renderStatus()}
      </Container>
    );
  };
}

export default RecruiterEventRegistrationButtonStatus;

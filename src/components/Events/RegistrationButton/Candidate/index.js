// import React, { Component, Fragment } from 'react';
// import { StyleSheet } from 'react-native';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';

// import { connectAlert } from '../../../Alert/index';
// import LoadingComponent from '../../../Elements/LoadingComponent';
// import ErrorComponent from '../../../Elements/ErrorComponent';
// import RaisedButton from '../../../Button/RaisedButton';

// // blocks
// import Container from './Container';
// import Status from './Status/index';
// import TextField from '../../../FormGenerator/blocks/TextField';
// import AskPhone from '../../../Candidate/AskPhone';

// const styles = StyleSheet.create({
//   eventButton: {
//     marginTop: 24,
//     marginBottom: 24,
//     marginLeft: 16,
//     marginRight: 16,
//   },
// });

// const EVENT_REGISTRATION_BY_EVENT_ID = gql`
//   query event_by_id($event_id: String!) {
//     event_registration: event_registration_by_event_id(event_id: $event_id, is_recruiter: false) {
//       event_registration_id
//       status
//       candidate {
//         phone
//         last_experience {
//           experience_id
//         }
//         last_education {
//           education_id
//         }
//       }
//     }
//     viewer {
//       id
//       candidate {
//         phone
//       }
//     }
//   }
// `;

// class CandidateEventRegistrationButton extends Component {
//   onPress = async () => {
//     const { registerEvent, eventId } = this.props;
//     try {
//       const res = await registerEvent({
//         variables: {
//           event_id: eventId,
//         },
//         refetchQueries: [
//           {
//             query: EVENT_REGISTRATION_BY_EVENT_ID,
//             variables: {
//               event_id: eventId,
//               is_recruiter: false,
//             },
//           },
//         ],
//       });
//       const { createEventRegistration } = res.data;
//       const { error, event_registration } = createEventRegistration;
//       if (error) {
//         this.props.alertWithType('warn', 'Info', res.data.createEventRegistration.error.message);
//       } else if (event_registration && event_registration.event_registration_id) {
//         this.props.alertWithType('success', 'OK', 'Votre candidature a bien été prise en compte !');
//         this.props.navigate('EventRegistrationCompleted');
//       }
//     } catch (e) {
//       this.props.alertWithType('error', 'Erreur', 'Erreur lors de la postulation');
//     }
//   };

//   refetchEvent = () => {
//     this.props.eventById.refetch();
//   };

//   renderStatus = () => {
//     const { event_registration } = this.props.eventById;
//     const { navigate } = this.props;

//     return (
//       <Status
//         navigate={navigate}
//         eventRegistration={event_registration}
//       />
//     );
//   };

//   renderPostulateButton = () => {
//     const { phone } = this.props.eventById.viewer.candidate;
//     return (
//       <Fragment>
//         {
//           !phone &&
//             <AskPhone
//               successText="Vous pouvez désormais postuler à l'évènement"
//               infoText="Un numéro de téléphone est indispensable pour participer à l'évènement"
//               refetch={this.refetchEvent}
//             />
//         }
//         <RaisedButton
//           disabled={!phone}
//           buttonStyle={styles.eventButton}
//           title="Postuler à la soirée"
//           onPress={this.onPress}
//         />
//       </Fragment>
//     );
//   };

//   render = () => {
//     const { loading, error, event_registration, viewer } = this.props.eventById;
//     if (loading) return <LoadingComponent />;
//     if (error) {
//       return <ErrorComponent message="Erreur de récupèration de l'évènement" />;
//     }
//     const { hideIfStatus } = this.props;
//     if (hideIfStatus) return null;

//     if (event_registration) {
//       return (
//         <Container>
//           {this.renderStatus()}
//         </Container>
//       );
//     }

//     if (!this.props.isPast && viewer && viewer.candidate) { return this.renderPostulateButton(); }
//     return null;
//   };
// }

// const EVENT_REGISTER_MUTATION = gql`
//   mutation createEventRegistration($event_id: String!) {
//     createEventRegistration(event_id: $event_id, is_recruiter: false) {
//       event_registration {
//         event_registration_id
//       }
//       error {
//         code
//         message
//       }
//     }
//   }
// `;

// const query = graphql(EVENT_REGISTRATION_BY_EVENT_ID, {
//   name: 'eventById',
//   options: ownProps => ({
//     fetchPolicy: 'network-only',
//     variables: {
//       event_id: ownProps.eventId,
//     },
//   }),
// });

// const mutation = graphql(EVENT_REGISTER_MUTATION, {
//   name: 'registerEvent',
// });

// export { EVENT_REGISTRATION_BY_EVENT_ID };
// export default compose(query, mutation)(connectAlert(CandidateEventRegistrationButton));

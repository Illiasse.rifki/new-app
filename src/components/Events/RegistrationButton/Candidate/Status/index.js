import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { theme } from '../../../../../config/constants/index';

// blocks
import Container from './Container';
import PendingIncomplete from './PendingIncomplete';
import PendingComplete from './PendingComplete';
import Label from './Label';

class CandidateEventRegistrationButtonStatus extends Component {
  static status = {
    accepted: { value: 'Candidature acceptée', style: { color: theme.green } },
    awaiting: { value: 'Candidature prise en compte', style: { color: theme.primaryColor } },
    pending: { value: 'Candidature prise en compte', style: { color: theme.primaryColor } },
    refused: { value: 'Candidature refusée', style: { color: theme.red } },
  };

  props: {
    eventRegistration: {},
  };

  renderStatusPending = () => {
    const { candidate } = this.props.eventRegistration;
    const { navigate } = this.props;

    const isProfileIncomplete = candidate && (!candidate.last_experience || !candidate.last_education);
    return isProfileIncomplete ?
      <PendingIncomplete
        candidate={candidate}
        navigate={navigate}
      /> :
      <PendingComplete />;
  };

  renderStatus = () => {
    const { status } = this.props.eventRegistration;
    const statusInfos = CandidateEventRegistrationButtonStatus.status[status] || {
      value: status,
      style: { color: 'black ' },
    };

    return <Label style={statusInfos.style}>{statusInfos.value}</Label>;
  };

  render = () => {
    const { status } = this.props.eventRegistration;

    return (
      <Container>
        {
          status === 'pending' ?
            this.renderStatusPending() :
            this.renderStatus()
        }
      </Container>
    );
  };
}

export default CandidateEventRegistrationButtonStatus;

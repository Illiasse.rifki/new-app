import styled from 'styled-components';
import { theme } from '../../../../../config/constants';

const Label = styled.Text`
  font-size: 18px;
  font-weight: bold;
  text-align: center;
  color: ${theme.primaryColor};
`;

export default Label;
import styled from 'styled-components';

const Title = styled.Text`
  font-size: 14px;
  text-align: center;
  color: #1C1C1C;
  padding-bottom: 10px;
  line-height: 20px;
`;

export default Title;
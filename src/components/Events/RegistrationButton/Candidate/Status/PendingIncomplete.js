import React from 'react';
import styled from 'styled-components';
import RaisedButton from '../../../../Button/RaisedButton';
import { theme } from '../../../../../config/constants';
import Title from './Title';
import Label from './Label';

type Props = {
  candidate: {},
  status: {},
};

const Container = styled.View`
`;

const Strong = styled.Text`
  font-weight: bold;
  font-size: 16px;
`;

const ProfileModificationNeeded = styled.Text`
  color: #ffa064;
  font-size: 12px;
  margin-top: 3px;
  margin-bottom: 3px;
  align-self: center;
  text-align: center;
`;

const Informations = styled.View`
  margin-top: 16px;
`;

const goToProfile = navigate => {
  navigate('CandidateProfile');
};

export default (props: Props) => {
  const { candidate, navigate } = props;
  return (
    <Container>
      <Title>
        <Strong>
          {"Attention, votre profil n'est pas complet! "}
        </Strong>
        Pour que notre équipe puisse vous sélectionner, nous vous remercions de mettre à
        jour votre profil en ajoutant au minimum une expérience et une formation.
      </Title>
      <Label>Candidature en Attente</Label>
      <Informations>
        {!candidate.last_experience && <ProfileModificationNeeded>Une expérience professionnelle minimum est requise</ProfileModificationNeeded>}
        {!candidate.last_education && <ProfileModificationNeeded>Une formation minimum est requise</ProfileModificationNeeded>}
        <RaisedButton
          title="Mettre à jour votre profil"
          onPress={() => goToProfile(navigate)}
        />
      </Informations>
    </Container>
  )
}
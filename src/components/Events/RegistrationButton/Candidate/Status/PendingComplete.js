import React from 'react';
import styled from 'styled-components';
import Title from './Title';
import Label from './Label';

const Container = styled.View`
`;

export default () => {
  return (
    <Container>
      <Title>
        Merci votre inscription a bien été prise en compte.
        L&apos;équipe Bizzeo reviendra vers vous très vite pour
        vous signifier votre sélection ou non à l&apos;événement.
      </Title>
      <Label>Candidature en Attente</Label>
    </Container>
  )
}
import React, { Component } from 'react';

// blocks
import CandidateEventRegistrationButton from './Candidate';
import RecruiterEventRegistrationButton from './Recruiter';


class RegistrationButton extends Component {
  props: {
    containerStyle: {},
    navigate: Function,
    register: Function,
    event_id: string,
    refresh: boolean,
    alertWithType: Function,
    isRecruiter: boolean,
    hideIfStatus: boolean,
    isPast: boolean,
  };

  renderCandidate = () => {
    const { isPast, event_id, hideIfStatus, navigate } = this.props;
    return (
      <CandidateEventRegistrationButton
        isPast={isPast}
        eventId={event_id}
        hideIfStatus={hideIfStatus}
        navigate={navigate}
      />
    );
  };

  renderRecruiter = () => {
    const { isPast, event_id, navigate } = this.props;
    return (
      <RecruiterEventRegistrationButton
        isPast={isPast}
        eventId={event_id}
        navigate={navigate}
      />
    );
  };

  render = () => (this.props.isRecruiter ?
    this.renderRecruiter() :
    this.renderCandidate());
}

export default RegistrationButton;

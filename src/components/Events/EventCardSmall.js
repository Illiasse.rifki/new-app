// import React, { Component } from 'react';
// import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
// import { Icon } from 'react-native-elements';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import ErrorComponent from '../Elements/ErrorComponent';
// import LoadingComponent from '../Elements/LoadingComponent';
// import defaultCoverPicture from '../../assets/defaultBusinessHeader.jpg';

// const styles = StyleSheet.create({
//   inscriptionsBackground: {
//     backgroundColor: 'white',
//     flexDirection: 'row',
//     justifyContent: 'space-around',
//     borderRadius: 4,
//     shadowColor: 'rgba(0, 0, 0, 0.08)',
//     shadowOffset: { height: 1, width: 0 },
//     shadowOpacity: 0.8,
//     marginBottom: 8,
//   },
//   inscriptionImage: {
//     height: 96,
//     width: 96,
//   },
//   imageViewContainer: {
//     borderTopLeftRadius: 4,
//     borderBottomLeftRadius: 4,
//     overflow: 'hidden',
//   },
//   inscriptionTextContainer: {
//     alignSelf: 'center',
//     flex: 1,
//     justifyContent: 'space-around',
//     marginLeft: 14.5,
//   },
//   inscriptionStatus: { fontSize: 14 },
//   inscriptionName: { fontSize: 18, fontWeight: '500', lineHeight: 24, marginBottom: 2 },
//   inscriptionIcon: { marginRight: 8, alignSelf: 'center' },
//   nbCandidates: { color: '#afafaf', fontSize: 14 },
// });

// class EventCardSmall extends Component {
//   static defaultProps = {
//     navigate: () => null,
//   };

//   static status = {
//     accepted: { value: 'Candidature acceptée', style: { color: '#33cc80' } },
//     pending: { value: 'En cours de traitement', style: { color: '#afafaf' } },
//     refused: { value: 'Candidature refusée', style: { color: '#cc4733' } },
//   };

//   constructor(props) {
//     super(props);
//     this.state = { more: false };
//   }

//   componentWillReceiveProps(nextProps) {
//     if (nextProps.refresh && this.props.eventById) {
//       this.props.eventById.refetch().catch(e => console.log('catch refetch Events.js', e));
//     }
//   }

//   goToEvent = () => {
//     const { navigate } = this.props;
//     const event = this.props.eventById.event_by_id;
//     const { event_id, name } = event;
//     navigate(`${this.props.isRecruiter ? 'Recruiter' : 'Candidate'}Event`, { event_id, name });
//   };

//   props: {
//     navigate: Function,
//     status: string,
//     isRecruiter: boolean,
//     refresh: boolean,
//     eventById: {
//       loading: boolean,
//       error: {},
//       event_by_id: {
//         event_id: string,
//         image: string,
//         name: string,
//       },
//     },
//   };

//   render() {
//     const { error, loading } = this.props.eventById;
//     if (error) {
//       return <ErrorComponent message="Problème lors de la récupération la page d'accueil." />;
//     } else if (loading) {
//       return <LoadingComponent />;
//     }
//     const event = this.props.eventById.event_by_id || {};
//     const { nb_candidates } = event;
//     const { status, isRecruiter } = this.props;
//     const statusStyle = EventCardSmall.status[status];
//     const inscriptionStatus = (
//       <Text style={[styles.inscriptionStatus, statusStyle && statusStyle.style]}>
//         {statusStyle && statusStyle.value}
//       </Text>
//     );
//     const nbCandidatesComponent = (
//       <Text style={styles.nbCandidates}>
//         {`${nb_candidates || 'Aucun'} candidat${nb_candidates > 1 ? 's' : ''} présent${nb_candidates > 1 ? 's' : ''}`}
//       </Text>
//     );
//     return (
//       <TouchableOpacity
//         key={event.event_id}
//         style={styles.inscriptionsBackground}
//         onPress={this.goToEvent}
//       >
//         <View style={styles.imageViewContainer}>
//           <Image style={styles.inscriptionImage} source={event.picture ? { uri: event.picture } : defaultCoverPicture} />
//         </View>
//         <View style={styles.inscriptionTextContainer}>
//           <Text style={styles.inscriptionName}>{event.name}, {event.date_format}</Text>
//           { isRecruiter && status === 'accepted' ? nbCandidatesComponent : inscriptionStatus }
//         </View>
//         <Icon
//           type="entypo"
//           name="chevron-thin-right"
//           color="#afafaf"
//           size={20}
//           style={styles.inscriptionIcon}
//         />
//       </TouchableOpacity>
//     );
//   }
// }

// const EVENT_BY_ID_QUERY = gql`
//   query event_by_id($event_id: String!) {
//     event_by_id(event_id: $event_id) {
//       event_id
//       name
//       date_format
//       description
//       passed
//       picture
//       nb_candidates
//     }
//   }
// `;

// export default graphql(EVENT_BY_ID_QUERY, {
//   name: 'eventById',
//   options: ownProps => ({
//     variables: {
//       event_id: ownProps.event_id,
//     },
//   }),
// })(EventCardSmall);

// import React, { Component } from 'react';
// import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import Button from '../Button/RaisedButton';
// import { theme } from '../../config/constants/index';
// import ErrorComponent from '../Elements/ErrorComponent';
// import LoadingComponent from '../Elements/LoadingComponent';
// import defaultCoverPicture from '../../assets/defaultBusinessHeader.jpg';

// const styles = StyleSheet.create({
//   eventsBackground: {
//     backgroundColor: 'white',
//     borderRadius: 4,
//     shadowColor: 'rgba(0, 0, 0, 0.08)',
//     shadowOffset: { height: 2, width: 0 },
//     shadowOpacity: 0.8,
//     marginRight: 8,
//     minWidth: 310,
//     maxWidth: 310,
//   },
//   eventImage: {
//     height: 128,
//     width: '100%',
//     resizeMode: 'cover',
//   },
//   eventImageContainer: {
//     borderTopLeftRadius: 4,
//     borderTopRightRadius: 4,
//     overflow: 'hidden',
//   },
//   eventTextContainer: {
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
//   eventName: {
//     fontSize: 18,
//     marginBottom: 7.5,
//     textAlign: 'center',
//     marginTop: 25,
//     fontWeight: '500',
//     color: theme.fontBlack,
//   },
//   eventDescription: {
//     fontSize: 14,
//     lineHeight: 20,
//     width: 278,
//     textAlign: 'left',
//     minHeight: 80,
//     color: theme.fontBlack,
//   },
// });

// class EventCard extends Component {
//   state = {};

//   componentWillReceiveProps(nextProps) {
//     if (nextProps.refresh && this.props.eventById) {
//       this.props.eventById.refetch().catch(e => console.log('catch refetch Events.js', e));
//     }
//   }

//   props: {
//     isRecruiter: boolean,
//     refresh: boolean,
//     eventById: {
//       error: boolean,
//       loading: boolean,
//       event_by_id: {
//         event_id: string,
//         description: string,
//       },
//     },
//     containerStyle: {},
//     navigate: Function,
//   };

//   goToEvent = () => {
//     const event = this.props.eventById.event_by_id;
//     const { isRecruiter } = this.props;
//     const { event_id, name } = event;
//     this.props.navigate(`${isRecruiter ? 'Recruiter' : 'Candidate'}Event`, {
//       event_id,
//       name,
//     });
//   };

//   render() {
//     const { error, loading } = this.props.eventById;
//     if (error) {
//       return <ErrorComponent message="Problème lors de la récupération la page d'accueil." />;
//     } else if (loading) {
//       return <LoadingComponent />;
//     }
//     const event = this.props.eventById.event_by_id;
//     if (!event) return null;
//     return (
//       <TouchableOpacity
//         onPress={this.goToEvent}
//         key={event.event_id}
//         style={[styles.eventsBackground, this.props.containerStyle]}
//       >
//         <View style={styles.eventImageContainer}>
//           <Image style={styles.eventImage} source={event.picture ? { uri: event.picture } : defaultCoverPicture} />
//         </View>
//         <View style={styles.eventTextContainer}>
//           <Text numberOfLines={2} ellipsizeMode="middle" style={styles.eventName}>{event.name} - {event.date_format}</Text>
//           <Text numberOfLines={4} ellipsizeMode="middle" style={styles.eventDescription}>
//             {event.detail ||
//               'Aucune description'}
//           </Text>
//           <Button alt={event.passed} title="Découvrir la soirée" onPress={this.goToEvent} />
//         </View>
//       </TouchableOpacity>
//     );
//   }
// }

// const EVENT_BY_ID_QUERY = gql`
//   query event_by_id($event_id: String!) {
//     event_by_id(event_id: $event_id) {
//       event_id
//       name
//       description
//       passed
//       picture
//       detail
//       date_format
//     }
//   }
// `;

// export default graphql(EVENT_BY_ID_QUERY, {
//   name: 'eventById',
//   options: ownProps => ({
//     variables: {
//       event_id: ownProps.event_id,
//     },
//   }),
// })(EventCard);

// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';

// import Events from '../Events/Events';
// import EventsRegistration from '../Events/EventsRegistration';

// const REGISTERED_EVENTS_CANDIDATE = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         next_event_registrations {
//           event {
//             event_id
//           }
//         }
//       }
//     }
//   }
// `;

// const NextEventsRegisteredCandidate = graphql(REGISTERED_EVENTS_CANDIDATE, {
//   props: ({ data }) => ({
//     data: {
//       ...data,
//       events: (data.viewer &&
//         data.viewer.candidate && data.viewer.candidate.next_event_registrations
//         ? data.viewer.candidate.next_event_registrations.filter(e => e.event).map(e => ({ event_id: e.event.event_id }))
//         : []),
//     },
//   }),
// })(Events);

// const REGISTERED_EVENTS_RECRUITER = gql`
//   query {
//     viewer {
//       id
//       recruiter {
//         next_event_registrations {
//           event {
//             event_id
//           }
//         }
//       }
//     }
//   }
// `;

// const NextEventsRegisteredRecruiter = graphql(REGISTERED_EVENTS_RECRUITER, {
//   props: ({ data }) => ({
//     data: {
//       ...data,
//       events: (data.viewer &&
//         data.viewer.recruiter && data.viewer.recruiter.next_event_registrations
//         ? data.viewer.recruiter.next_event_registrations.filter(e => e.event).map(e => ({ event_id: e.event && e.event.event_id }))
//         : []),
//     },
//   }),
// })(Events);

// const PAST_REGISTERED_EVENTS_RECRUITER = gql`
//   query {
//     viewer {
//       id
//       recruiter {
//         past_event_registrations {
//           event {
//             event_id
//           }
//         }
//       }
//     }
//   }
// `;

// const PastEventsRegisteredRecruiter = graphql(PAST_REGISTERED_EVENTS_RECRUITER, {
//   props: ({ data }) => ({
//     data: {
//       ...data,
//       events: (data.viewer &&
//         data.viewer.recruiter && data.viewer.recruiter.past_event_registrations
//         ? data.viewer.recruiter.past_event_registrations.filter(e => e.event).map(e => ({ event_id: e.event.event_id }))
//         : []),
//     },
//   }),
// })(Events);

// const CURRENT_EVENTS_REGISTRATION_ID = gql`
//   query {
//     viewer {
//       id
//       recruiter {
//         current_event_registrations {
//           event {
//             event_id
//           }
//         }
//       }
//     }
//   }
// `;

// const CurrentEventsRegisteredRecruiter = graphql(CURRENT_EVENTS_REGISTRATION_ID, {
//   props: ({ data }) => ({
//     isRecruiter: true,
//     data: {
//       ...data,
//       events: (data.viewer &&
//         data.viewer.recruiter && data.viewer.recruiter.current_event_registrations
//         ? data.viewer.recruiter.current_event_registrations : []),
//     },
//   }),
// })(EventsRegistration);

// export {
//   NextEventsRegisteredRecruiter,
//   PastEventsRegisteredRecruiter,
//   CurrentEventsRegisteredRecruiter,
// };
// export { NextEventsRegisteredCandidate };

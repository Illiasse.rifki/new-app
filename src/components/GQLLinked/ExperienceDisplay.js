// import React from 'react';
// import { View, StyleSheet } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';
// import moment from 'moment';
// import ErrorComponent from '../Elements/ErrorComponent';
// import ContentLogo from '../Content/ContentLogo';

// type Props = {
//   containerStyle: {},
//   experienceId: string,
//   data: {
//     experience: {
//       description: string,
//       start: string,
//       end: string,
//       role: string,
//     },
//     loading: boolean,
//     error: boolean,
//   }
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
// });

// const ExperienceDisplay = (props: Props) => {
//   if (props.data.error) {
//     return (<ErrorComponent message={'Erreur lors de récuperation de l\'experience'}/>);
//   } else if (props.data.loading) {
//     return null;
//   }
//   const { experience } = props.data;
//   if (!experience)
//    return null;

//   const startDate = moment(new Date(experience.start));
//   const startDateFormat = startDate.format('MMM Y');
//   const endDate = (!experience.end || experience.end === 'maintenant') ? moment() : moment(new Date(experience.end));

//   const endDateFormat =
//     (!experience.end || experience.end === 'maintenant') ? 'maintenant' : endDate.format('MMM Y');
//   const timeIn = startDate.to(endDate, true);
//   return (
//     <View style={[styles.container, props.containerStyle]}>
//       <ContentLogo
//         content={{
//           image: experience.company && experience.company.logo,
//           title: experience.role,
//           subtitle: experience.company && experience.company.name,
//           thirdLine: `De ${startDateFormat} à ${endDateFormat} - ${timeIn}`,
//           description: experience.description,
//         }}
//       />
//     </View>
//   );
// };

// const QUERY_EXPERIENCE = gql`
//   query ExperienceQuery($experience_id: String!) {
//     experience(experience_id: $experience_id) {
//       company {
//         name
//         logo
//       }
//       role
//       start
//       end
//       description
//     }
//   }
// `;

// export default graphql(QUERY_EXPERIENCE, {
//   options: ownProps => ({
//     variables: {
//       experience_id: ownProps.experienceId,
//     },
//   }),
// })(ExperienceDisplay);

// import React from 'react';
// import { View, StyleSheet } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';
// import moment from 'moment';
// import ErrorComponent from '../Elements/ErrorComponent';
// import ContentLogo from '../Content/ContentLogo';

// type Props = {
//   containerStyle: {},
//   experienceId: string,
//   data: {
//     education: {
//       description: string,
//       start: string,
//       end: string,
//       diploma: string,
//     },
//     loading: boolean,
//     error: boolean,
//   }
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
// });

// const EducationDisplay = (props: Props) => {
//   if (props.data.error) {
//     return (<ErrorComponent message={'Erreur lors de récuperation de l\'experience'}/>);
//   } else if (props.data.loading) {
//     return null;
//   }

//   const { education } = props.data;
//   const startDate = moment(new Date(education.start));
//   const startDateFormat = startDate.format('MMM Y');
//   const endDate = (!education.end || education.end === 'maintenant') ? moment() : moment(new Date(education.end));
//   const endDateFormat =
//     (!education.end || education.end === 'maintenant') ? 'maintenant' : endDate.format('MMM Y');
//   const timeIn = startDate.to(endDate, true);

//   return (
//     <View style={[styles.container, props.containerStyle]}>
//       <ContentLogo
//         content={{
//           image: education.school && education.school.logo,
//           title: education.school && education.school.name,
//           subtitle: education.diploma,
//           thirdLine: `De ${startDateFormat} à ${endDateFormat} - ${timeIn}`,
//           description: education.description,
//         }}
//       />
//     </View>
//   );
// };

// const QUERY_EDUCATION = gql`
//   query EducationQuery($education_id: String!) {
//     education(education_id: $education_id) {
//       school {
//         name
//         logo
//       }
//       diploma
//       start
//       end
//       description
//     }
//   }
// `;

// export default graphql(QUERY_EDUCATION, {
//   options: ownProps => ({
//     variables: {
//       education_id: ownProps.educationId,
//     },
//   }),
// })(EducationDisplay);

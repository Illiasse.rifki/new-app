// import React from 'react';
// import { View, Text, StyleSheet } from 'react-native';
// import Expo from 'expo';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';
// import 'moment/locale/fr';
// import { theme } from '../../config/constants';
// import CardEditable from '../Card/CardEditable';
// import ErrorComponent from '../Elements/ErrorComponent';
// import LoadingComponent from '../Elements/LoadingComponent';
// import Tags from '../Content/Tags';
// import Qualities from '../Content/Qualities';
// import ExperienceDisplay from './ExperienceDisplay';
// import LanguageDisplay from './LanguageDisplay';
// import EducationDisplay from './EducationDisplay';
// import ProfilePicture from './ProfilePicture';
// import ButtonCV from '../Button/ButtonCV';
// import { Button } from '../Button';

// type Props = {
//   preview: boolean,
//   containerStyle: {},
//   userId: string,
//   editable: boolean,
//   navigate: Function,
//   alertWithType: Function,
//   compatibility: string,
//   data: {
//     error: string,
//     refetch: any,
//     singleCandidate: {
//       editable: boolean,
//       firstname: string,
//       lastname: string,
//       wish_raw_salary: string,
//       wish_variable_salary: string,
//       work_city: [],
//       company_size: [],
//     },
//     loading: boolean,
//   },
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   mainSectionBackground: {
//     flex: 1,
//     position: 'relative',
//     backgroundColor: 'white',
//     paddingLeft: 24,
//     paddingRight: 24,
//     paddingTop: 32,
//     paddingBottom: 32,
//     marginBottom: 16,
//     shadowColor: 'rgba(0, 0, 0, 0.08)',
//     shadowOffset: { width: 1, height: 4.5 },
//     shadowOpacity: 0.8,
//     shadowRadius: 2,
//   },
//   mainSectionBackgroundInfo: {
//     flex: 1,
//     position: 'relative',
//     backgroundColor: '#fffbf0',
//     paddingLeft: 24,
//     paddingRight: 24,
//     paddingTop: 32,
//     paddingBottom: 32,
//     marginBottom: 16,
//     shadowColor: 'rgba(0, 0, 0, 0.08)',
//     shadowOffset: { width: 1, height: 4.5 },
//     shadowOpacity: 0.8,
//     shadowRadius: 2,
//   },
//   personalInfos: {
//     flex: 1,
//     alignItems: 'center',
//   },
//   personalInfosName: {
//     fontSize: 28,
//     lineHeight: 40,
//     fontWeight: 'bold',
//     color: theme.fontBlack,
//   },
//   personalInfosRole: {
//     fontSize: 18,
//     lineHeight: 24,
//     fontWeight: '500',
//     color: theme.fontBlack,
//     textAlign: 'center',
//   },
//   personalInfosCity: {
//     fontSize: 14,
//     lineHeight: 24,
//     color: theme.gray,
//   },
//   tags: {
//     marginBottom: 8,
//     marginLeft: 8,
//     marginRight: 8,
//   },
//   validateButton: {
//     marginBottom: 24,
//     borderRadius: 4,
//     shadowColor: 'rgba(0, 0, 0, 0.08)',
//     shadowOffset: { width: 0.5, height: 4 },
//     shadowOpacity: 0.8,
//     shadowRadius: 2,
//     width: '90%',
//     height: 40,
//     marginTop: 24,
//     alignSelf: 'center',
//   },
//   CV: {
//     flex: 1,
//     alignItems: 'center',
//   },
//   textCV: {
//     paddingBottom: 8,
//     textAlign: 'center',
//   },
//   experience: {
//     marginTop: 22,
//     paddingLeft: 8,
//     paddingRight: 8,
//     marginBottom: 2,
//   },
//   phoneNumber: { fontWeight: '800' },
//   contactEmail: { fontWeight: '800' },
//   noData: {
//     marginLeft: 8,
//     marginRight: 8,
//     marginTop: 8,
//   },
//   contactInformations: { width: '100%', paddingVertical: 10, marginTop: 16, borderRadius: 2 },
//   compatibility: { width: '100%', paddingVertical: 10, marginTop: 16, borderRadius: 2 },
//   compatibilityText: { color: 'white', textAlign: 'center' },
//   Proche: { backgroundColor: 'orange' },
//   Incompatible: { backgroundColor: '#cc4733' },
//   Compatible: { backgroundColor: theme.green },
// });

// const renderCV = (
//   cv: String,
//   cv_type: String,
//   userId: String,
//   refetch: Function,
//   editable: Boolean,
// ) => {
//   const noCV = cv === null;
//   const importCV = editable ? "Vous n'avez pas encore importé votre CV" : 'Aucun CV';
//   return noCV ? (
//     <View>
//       <Text style={styles.textCV}>{importCV}</Text>
//       {editable && <ButtonCV onUpload={refetch} userId={userId} />}
//     </View>
//   ) : (
//     <View>
//       {editable && <Text style={styles.textCV}>{'CV ajouté'}</Text>}
//       <Button
//         title={'Télécharger le CV'}
//         onPress={() =>
//           Expo.FileSystem.downloadAsync(
//             cv,
//             `${Expo.FileSystem.documentDirectory}/cv-${userId}.${cv_type}`,
//           ).catch(err => console.warn(err))
//         }
//       />
//       {editable && <ButtonCV title={'Modifier votre CV'} onUpload={refetch} userId={userId} />}
//     </View>
//   );
// };

// const renderExperiences = (experiences) => {
//   const hasExperiences = experiences.length;
//   return hasExperiences && experiences ? (
//     experiences.map(experience => (
//       <ExperienceDisplay
//         containerStyle={styles.experience}
//         key={experience.experience_id}
//         experienceId={experience.experience_id}
//       />
//     ))
//   ) : (
//     <View style={styles.noData}>
//       <Text>Aucune expérience ajouté</Text>
//     </View>
//   );
// };

// const renderEducation = (education) => {
//   const hasEducation = education.length;

//   return hasEducation ? (
//     education.map(formation => (
//       <EducationDisplay
//         containerStyle={styles.experience}
//         key={formation.education_id}
//         educationId={formation.education_id}
//       />
//     ))
//   ) : (
//     <View style={styles.noData}>
//       <Text>Aucune formation ajoutée</Text>
//     </View>
//   );
// };

// const renderLanguages = (languages) => {
//   const hasLanguages = languages.length;

//   return hasLanguages ? (
//     languages.map(language => (
//       <LanguageDisplay
//         containerStyle={styles.experience}
//         key={language.language_id}
//         languageId={language.language_id}
//       />
//     ))
//   ) : (
//     <View style={styles.noData}>
//       <Text>Aucune langue ajoutée</Text>
//     </View>
//   );
// };

// const Profile = (props: Props) => {
//   if (props.data.error) {
//     return <ErrorComponent message={"Erreur lors de récuperation de l'experience"} />;
//   } else if (props.data.loading) {
//     return <LoadingComponent />;
//   }
//   let editable = true;
//   if (props.editable === false) editable = false;
//   const candidate = props.data.singleCandidate;
//   if (!candidate) {
//     // props.data.refetch().catch(e => console.log('cacth error refetch profile.js', e));
//     return null;
//   }
//   return (
//     <View style={[styles.container, props.containerStyle]}>
//       {props.preview && (
//         <CardEditable editable={false} containerStyle={styles.mainSectionBackgroundInfo}>
//           <Text style={styles.personalInfosRole}>{"C'est presque fini !"}</Text>
//           <Text style={styles.personalInfosRole}>
//             Complétez votre profil avec vos expériences et vos formations
//           </Text>
//           <Text style={styles.personalInfosRole}>pour que les recruteurs vous contactent !</Text>
//         </CardEditable>
//       )}
//       <CardEditable
//         editable={editable === false ? false : candidate.editable}
//         containerStyle={styles.mainSectionBackground}
//         onPress={() => {
//           props.navigate('CandidateEditProfil');
//         }}
//       >
//         <View style={styles.personalInfos}>
//           <ProfilePicture
//             editable={editable === false ? false : candidate.editable}
//             alertWithType={props.alertWithType}
//             userId={props.userId}
//           />
//           <Text
//             style={styles.personalInfosName}
//           >{`${candidate.lastname.toUpperCase()} ${candidate.firstname
//               .toLowerCase()
//               .replace(/(?:^|\s)\S/g, a => a.toUpperCase()) || ''}`}</Text>
//           <Text style={styles.personalInfosRole}>{candidate.headline}</Text>
//           <Text style={styles.personalInfosCity}>
//             {candidate.work_city
//               ? candidate.work_city.reduce(
//                 (s, city, i) => `${s} ${i > 0 ? '-' : ''} ${city.name}`,
//                 '',
//               )
//               : null}
//           </Text>
//         </View>
//       </CardEditable>
//       {props.compatibility && (
//         <CardEditable>
//           <View style={styles.personalInfos}>
//             <Text style={styles.personalInfosName}>Prétention salariale</Text>
//             <View style={[styles.compatibility, styles[props.compatibility]]}>
//               <Text style={styles.compatibilityText}>{props.compatibility}</Text>
//             </View>
//           </View>
//         </CardEditable>
//       )}
//       <CardEditable>
//         <View style={styles.personalInfos}>
//           <Text style={styles.personalInfosName}>Coordonnées</Text>
//           <View style={[styles.contactInformations]}>
//             {candidate.phone && (
//               <View>
//                 <Text style={styles.phoneNumber}>Téléphone :</Text>
//                 <Text>{candidate.phone}</Text>
//               </View>
//             )}
//             {candidate.contact_email && (
//               <View>
//                 <Text style={styles.contactEmail}>Mail:</Text>
//                 <Text>{candidate.contact_email}</Text>
//               </View>
//             )}
//           </View>
//         </View>
//       </CardEditable>

//       {/* <CardEditable
//         title="CV"
//       >
//         <View style={styles.CV}>
//           {
//                        renderCV(candidate.cv, candidate.cv_type, candidate.user_id,
//                          props.data.refetch, editable === false ? false : candidate.editable)
//           }
//         </View>
//       </CardEditable> */}
//       <CardEditable
//         editable={editable === false ? false : candidate.editable}
//         title="Expérience"
//         onPress={() => {
//           props.navigate('CandidateProfileExperiences', { userId: props.userId });
//         }}
//       >
//         <View style={styles.experiences}>{renderExperiences(candidate.experience)}</View>
//       </CardEditable>
//       <CardEditable
//         editable={editable === false ? false : candidate.editable}
//         title="Formation"
//         onPress={() => {
//           props.navigate('CandidateProfileEducation', { userId: props.userId });
//         }}
//       >
//         <View style={styles.qualities}>{renderEducation(candidate.education)}</View>
//       </CardEditable>
//       <CardEditable
//         editable={editable === false ? false : candidate.editable}
//         title="Langues parlées"
//         onPress={() => {
//           props.navigate('CandidateProfileLanguages', { userId: props.userId });
//         }}
//       >
//         <View style={styles.qualities}>{renderLanguages(candidate.language)}</View>
//       </CardEditable>
//       <CardEditable
//         editable={editable === false ? false : candidate.editable}
//         onPress={() => {
//           props.navigate('CandidateFormTags', {
//             edit: true,
//             buttonLabel: 'VALIDER',
//             userId: props.userId,
//           });
//         }}
//         title="Tags"
//       >
//         <Tags containerStyle={styles.tags} tags={candidate.tags} />
//       </CardEditable>
//       <CardEditable
//         editable={editable === false ? false : candidate.editable}
//         title="Qualités"
//         onPress={() => {
//           props.navigate('CandidateFormQualities', {
//             edit: true,
//             buttonLabel: 'VALIDER',
//             userId: props.userId,
//           });
//         }}
//       >
//         <Qualities qualities={candidate.qualities} />
//       </CardEditable>
//     </View>
//   );
// };

// const QUERY_PROFILE_CANDIDATE = gql`
//   query SingleCandidateQuery($user_id: String) {
//     singleCandidate(user_id: $user_id) {
//       editable
//       contact_email
//       phone
//       firstname
//       lastname
//       tags
//       qualities
//       headline
//       work_city {
//         name
//       }
//       experience(order_by_date: true) {
//         experience_id
//         role
//       }
//       education(order_by_date: true) {
//         education_id
//       }
//       language {
//         language_id
//       }
//     }
//   }
// `;

// export default graphql(QUERY_PROFILE_CANDIDATE, {
//   options: ownProps => ({
//     variables: {
//       user_id: ownProps.userId,
//     },
//   }),
// })(Profile);

// export { QUERY_PROFILE_CANDIDATE };

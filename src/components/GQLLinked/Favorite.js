// import React from 'react';
// import { compose, graphql } from 'react-apollo';
// import gql from 'graphql-tag';
// import { StyleSheet } from 'react-native';
// import { Icon } from 'react-native-elements';

// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';

// const styles = StyleSheet.create({
//   icon: {
//     marginRight: 24,
//     marginBottom: -1,
//     backgroundColor: 'transparent',
//   },
// });

// const EVENT_REGISTRATION_INFO = gql`
//   query($event_registration_info_id: String!){
//     eventRegistrationInfo(event_registration_info_id: $event_registration_info_id) {
//       event_registration_info_id
//       favorite: is_favorite
//     }
//   }
// `;

// const EDIT_EVENT_REGISTRATION_INFO = gql`
//   mutation($event_registration_info_id: String!, $is_favorite: Boolean!) {
//     editEventRegistrationInfo(event_registration_info_id: $event_registration_info_id, is_favorite: $is_favorite) {
//       event_registration_info {
//         event_registration_info_id
//       }
//       error {
//         code
//         message
//       }
//     }
//   }
// `;

// type Props = {
//   editable?: boolean,
//   data: any,
//   event_registration_info_id: string,
//   color?: string,
//   edit: Function,
// };

// const Favorite = (props: Props) => {
//   const { event_registration_info_id, data, editable } = props;
//   if (data && !data.loading && !data.eventRegistrationInfo) props.data.refetch();
//   if (
//     !event_registration_info_id ||
//     !data ||
//     data.loading ||
//     !data.eventRegistrationInfo
//   ) return (<LoadingComponent transparent color='white' />);
//   if (data.error) return (<ErrorComponent message={data.error} />);

//   const { favorite = false } = data.eventRegistrationInfo;

//   const edit = async () => {
//     try {
//       await props.edit(
//         {
//           variables: {
//             event_registration_info_id,
//             is_favorite: !favorite,
//           },
//           refetchQueries: [{
//             query: EVENT_REGISTRATION_INFO,
//             variables: {
//               event_registration_info_id,
//             },
//           }],
//         },
//       );
//     } catch (e) {
//       console.warn('catched on props.edit', e);
//       setTimeout(() => props.data.refetch(), 1000);
//     }
//   };

//   if (!editable && !favorite) return null;

//   return (
//     <Icon
//       name={favorite ? 'heart' : 'heart-outlined'}
//       color={'white'}
//       type='entypo'
//       onPress={editable ? edit : null }
//       containerStyle={styles.icon}
//     />
//   );
// };

// export default compose(
//   graphql(EVENT_REGISTRATION_INFO, {
//     options: ({ event_registration_info_id }) => ({
//       skip: !event_registration_info_id,
//       variables: {
//         event_registration_info_id,
//       },
//     }),
//   }),
//   graphql(EDIT_EVENT_REGISTRATION_INFO, { name: 'edit' }),
// )(Favorite);

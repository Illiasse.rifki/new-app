// import React, { Component } from 'react';
// import { View, StyleSheet, Image } from 'react-native';
// import { Button, CheckBox } from 'react-native-elements';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';
// import moment from 'moment';
// import { theme, themeElements } from '../../config/constants/index';
// import Field from '../../components/Design/Field';
// import DateField from '../../components/Design/DateField';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import connectAlert from '../../components/Alert/connectAlert';
// import School, { QUERY_SCHOOLS } from './School';
// import defaultBusinessImg from '../../assets/defaultBusiness.png';

// const QUERY_PROFILE_CANDIDATE = gql`
//   query SingleCandidateQuery($user_id: String) {
//     singleCandidate(user_id: $user_id) {
//       editable
//       firstname
//       lastname
//       tags
//       qualities
//       headline
//       work_city {
//         name
//       }
//       experience(order_by_date: true) {
//         experience_id
//         role
//       }
//       education(order_by_date: true) {
//         education_id
//       }
//       language {
//         language_id
//       }
//     }
//   }
// `;

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   titleView: {
//     flexDirection: 'row',
//     paddingLeft: 32,
//     paddingRight: 19,
//     marginTop: 22,
//     marginBottom: 30,
//   },
//   mainTitle: {
//     flex: 1,
//   },
//   sectionBackground: {
//     flex: 1,
//     position: 'relative',
//     backgroundColor: 'white',
//     paddingLeft: 24,
//     paddingRight: 24,
//     paddingTop: 32,
//     paddingBottom: 32,
//     marginBottom: 16,
//   },
//   formInputContainer: {
//     marginTop: -5,
//     marginRight: 0,
//     marginLeft: 0,
//   },
//   imageView: {
//     alignItems: 'center',
//   },
//   fieldView: {
//     marginTop: 20.5,
//     marginLeft: 24.25,
//     marginRight: 24.25,
//   },
//   datesView: {
//     marginTop: 28.5,
//     marginBottom: 0,
//     flexDirection: 'row',
//   },
//   dateView: {
//     flex: 1,
//     marginLeft: 24.25,
//     marginRight: 24.25,
//     flexDirection: 'column',
//   },
//   buttonView: {
//     marginBottom: 70,
//   },
//   checkBox: {
//     backgroundColor: 'white',
//     borderWidth: 0,
//     marginLeft: 0,
//     marginRight: 0,
//     marginBottom: 0,
//     marginTop: 0,
//   },
// });

// const QUERY_EDUCATION = gql`
//   query EducationQuery($education_id: String!) {
//     education(education_id: $education_id) {
//       school {
//         school_id
//         name
//         logo
//       }
//       diploma
//       start
//       end
//       description
//     }
//   }
// `;

// const VIEWER_CANDIDATE = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         education {
//           education_id
//         }
//         experience {
//           experience_id
//         }
//       }
//     }
//   }
// `;

// const format = 'YYYY';

// class EducationEdit extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {};
//   }

//   componentWillMount() {
//     if (this.props.data && !this.props.data.loading) {
//       const { education } = this.props.data;
//       const end = (!education.end || education.end === 'maintenant') ? undefined : moment(new Date(education.end)).format(format);
//       this.setState({
//         schoolName: education.school.name,
//         schoolId: education.school.school_id,
//         schoolLogo: education.school.logo,
//         diploma: education.diploma,
//         start: moment(new Date(education.start)).format(format),
//         dateEndNow: !end,
//         end,
//         description: education.description,
//       });
//     }
//   }

//   componentDidUpdate(prevProps) {
//     if (prevProps.data
//       && prevProps.data.loading
//       && !this.props.data.loading
//       && this.props.data.education
//     ) {
//       const { education } = this.props.data;
//       const end = (!education.end || education.end === 'maintenant') ? undefined : moment(new Date(education.end)).format(format);

//       this.setState({
//         schoolName: education.school.name,
//         schoolId: education.school.school_id,
//         schoolLogo: education.school.logo,
//         diploma: education.diploma,
//         start: moment(new Date(education.start)).format(format),
//         dateEndNow: !end,
//         end,
//         description: education.description,
//       });
//     }
//   }

//   getRefetchQueries() {
//     const refetchQueries = this.props.refetchQueries.slice();
//     refetchQueries.push({
//       query: QUERY_SCHOOLS,
//     });
//     refetchQueries.push({
//       query: QUERY_PROFILE_CANDIDATE,
//       variables: {
//         user_id: this.props.userId,
//       },
//     });
//     refetchQueries.push({
//       query: VIEWER_CANDIDATE,
//     });
//     return refetchQueries;
//   }

//   props: {
//     alertWithType: Function,
//     containerStyle: {},
//     educationId: string,
//     userId: string,
//     addEducation: Function,
//     editEducation: Function,
//     deleteEducation: Function,
//     onAddEducation: Function,
//     onEditEducation: Function,
//     onDeleteEducation: Function,
//     refetchQueries: [];
//     data: {
//       education: {
//         description: string,
//         start: string,
//         end: string,
//         diploma: string,
//         school: {
//           school_id: string,
//           name: string,
//           logo: string,
//         },
//       },
//       loading: boolean,
//       error: boolean,
//     },
//   };

//   hasFieldsFill() {
//     if (!this.state.schoolName
//       || !this.state.start
//       || !this.state.diploma
//     ) {
//       this.props.alertWithType(
//         'error',
//         'Champs de formulaires incomplets',
//         'Vous devez completer au moins le nom de l\'école, votre diplome, date de commencement, de fin.',
//       );
//       return false;
//     }
//     return true;
//   }

//   handleAddEducationButton = async () => {
//     if (!this.hasFieldsFill()) {
//       return;
//     }
//     const variables = {
//       start: moment(this.state.start, format).toDate(),
//       diploma: this.state.diploma,
//       end: this.state.end && !this.state.dateEndNow ? moment(this.state.end, format).toDate() : undefined,
//       description: this.state.description,
//     };
//     if (this.state.schoolId) {
//       variables.school_id = this.state.schoolId;
//     } else {
//       variables.school_name = this.state.schoolName;
//     }

//     const ret = await this.props.addEducation({
//       refetchQueries: this.getRefetchQueries(),
//       variables,
//     });
//     if (ret.data.createEducation.error) {
//       this.props.alertWithType('error', 'Erreur', "Erreur lors de l'ajout de l'education");
//       return;
//     }
//     if (this.props.onAddEducation) {
//       this.props.alertWithType(
//         'success',
//         'Ajout effectué',
//         'La formation a été ajouté avec succès',
//       );
//       this.props.onAddEducation();
//     }
//   };

//   handleEditEducationButton = async () => {
//     if (!this.hasFieldsFill()) {
//       return;
//     }
//     const variables = {
//       education_id: this.props.educationId,
//       start: moment(this.state.start, format).toDate(),
//       diploma: this.state.diploma,
//       end: this.state.end && !this.state.dateEndNow ? moment(this.state.end, format).toDate() : undefined,
//       description: this.state.description,
//     };
//     if (this.state.schoolId) {
//       variables.school_id = this.state.schoolId;
//     } else {
//       variables.school_name = this.state.schoolName;
//     }

//     const ret = await this.props.editEducation({
//       refetchQueries: [{
//         query: QUERY_EDUCATION,
//         variables: {
//           education_id: this.props.educationId,
//         },
//       }, {
//         query: QUERY_SCHOOLS,
//       }],
//       variables,
//     });
//     if (ret.data.editEducation.error) {
//       this.props.alertWithType(
//         'error',
//         'Erreur',
//         "Erreur lors de la modification de l'education",
//       );
//       return;
//     }
//     if (this.props.onEditEducation) {
//       this.props.alertWithType(
//         'success',
//         'Modification effectuée',
//         'La formation a été modifié avec succès',
//       );
//       this.props.onEditEducation();
//     }
//   };

//   handleDeleteEducationButton = async () => {
//     const ret = await this.props.deleteEducation({
//       refetchQueries: this.getRefetchQueries(),
//       variables: {
//         education_id: this.props.educationId,
//       },
//     });
//     if (ret.data.deleteEducation.error) {
//       this.props.alertWithType(
//         'error',
//         'Erreur',
//         "Erreur lors de la suppression de l'education",
//       );
//       return;
//     }
//     if (this.props.onDeleteEducation) {
//       this.props.alertWithType(
//         'success',
//         'Suppression effectuée',
//         'La formation a été supprimé avec succès',
//       );
//       this.props.onDeleteEducation();
//     }
//   };

//   render() {
//     if (this.props.data && this.props.data.error) {
//       return (<ErrorComponent message="Problème lors de la récupération du profil." />);
//     }

//     return (
//       <View style={[styles.container, this.props.containerStyle]}>
//         <View style={styles.imageView}>
//           <Image
//             source={this.state.schoolLogo ? { uri: this.state.schoolLogo } :
//               defaultBusinessImg}
//             style={{ width: 64, height: 64 }}
//             resizeMode="contain"
//           />
//         </View>
//         <School
//           containerStyle={styles.fieldView}
//           label="Nom de l'école"
//           value={this.state.schoolName}
//           onChangeText={(text) => {
//             if (text !== this.state.schoolName) {
//               this.setState({ schoolName: text, schoolId: null, schoolLogo: null });
//             }
//           }}
//           onValidate={(school) => {
//             this.setState({
//               schoolName: school.name,
//               schoolId: school.school_id,
//               schoolLogo: school.logo,
//             });
//           }}
//         />
//         <Field
//           containerStyle={styles.fieldView}
//           label="Diplome"
//           value={this.state.diploma}
//           onChangeText={(diploma) => {
//             this.setState({ diploma });
//           }}
//         />
//         <View style={styles.datesView}>
//           <DateField
//             containerStyle={styles.dateView}
//             label="Date de début"
//             placeholder="Date"
//             dateOptions={{ format }}
//             date={this.state.start}
//             onDateChange={(start) => {
//               this.setState({ start });
//             }}
//           />
//           <View style={styles.dateView}>
//             <DateField
//               label="Date de Fin"
//               placeholder="Date"
//               dateOptions={{ format }}
//               date={this.state.end}
//               disabled={this.state.dateEndNow}
//               onDateChange={(end) => {
//                 this.setState({ end });
//               }}
//             />
//             <CheckBox
//               title='Maintenant'
//               checkedColor={theme.primaryColor}
//               checked={this.state.dateEndNow}
//               containerStyle={styles.checkBox}
//               onPress={() => this.setState({ dateEndNow: !this.state.dateEndNow })}
//             />
//           </View>
//         </View>
//         <Field
//           containerStyle={styles.fieldView}
//           label="Description"
//           onChangeText={(description) => {
//             this.setState({ description });
//           }}
//           value={this.state.description}
//           inputStyle={{ height: 72 }}
//           textInputOptions={{
//             multiline: true,
//             maxHeight: 72,
//             returnKeyType: 'done',
//             blurOnSubmit: true,
//           }}
//         />
//         {
//           !this.props.educationId ? (
//             <Button
//               title="Ajouter la formation"
//               color={theme.secondaryColor}
//               backgroundColor={theme.primaryColor}
//               buttonStyle={themeElements.validateButton}
//               containerViewStyle={{ marginTop: 28.5 }}
//               onPress={this.handleAddEducationButton}
//             />) : (
//             <View style={styles.buttonView}>
//               <Button
//                 title="Valider la formation"
//                 color={theme.secondaryColor}
//                 backgroundColor={theme.primaryColor}
//                 buttonStyle={themeElements.validateButton}
//                 containerViewStyle={{ marginTop: 28.5 }}
//                 onPress={this.handleEditEducationButton}
//               />
//               <Button
//                 title="Supprimer la formation"
//                 color={'#cc4733'}
//                 backgroundColor={'white'}
//                 containerViewStyle={{ marginTop: 0 }}
//                 onPress={this.handleDeleteEducationButton}
//               />
//             </View>
//           )
//         }
//       </View>
//     );
//   }
// }

// const ADD_EDUCATION = gql`
//   mutation(
//     $school_id: String
//     $school_name: String
//     $start: String!
//     $diploma: String
//     $end: String
//     $description: String) {
//     createEducation(
//       school_id: $school_id
//       school_name: $school_name
//       start: $start
//       diploma: $diploma
//       end: $end
//       description: $description
//     ) {
//       education {
//         education_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const EDIT_EDUCATION = gql`
//   mutation(
//     $education_id: String!
//     $school_id: String
//     $school_name: String
//     $start: String!
//     $diploma: String
//     $end: String
//     $description: String) {
//     editEducation(
//       education_id: $education_id
//       school_id: $school_id
//       school_name: $school_name
//       start: $start
//       diploma: $diploma
//       end: $end
//       description: $description
//     ) {
//       education {
//         education_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const DELETE_EDUCATION = gql`
//   mutation(
//     $education_id: String!
//   ) {
//     deleteEducation(
//       education_id: $education_id
//     ) {
//       education {
//         education_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// export default compose(
//   graphql(QUERY_EDUCATION, {
//     skip: ownProps => !ownProps.educationId,
//     options: ownProps => ({
//       variables: {
//         education_id: ownProps.educationId,
//       },
//     }),
//   }),
//   graphql(ADD_EDUCATION, { name: 'addEducation' }),
//   graphql(EDIT_EDUCATION, { name: 'editEducation' }),
//   graphql(DELETE_EDUCATION, { name: 'deleteEducation' }),
// )(connectAlert(EducationEdit));

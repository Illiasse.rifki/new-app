// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import EventsRegistration from '../Events/EventsRegistration';

// const NEXT_EVENTS_REGISTRATION_RECRUITER = gql`
//   query {
//     viewer {
//       id
//       recruiter {
//         user_id
//         next_event_registrations {
//           event_registration_id
//           status
//           event {
//             event_id
//           }
//         }
//       }
//     }
//   }
// `;

// const NextEventsRegistrationRecruiter = graphql(NEXT_EVENTS_REGISTRATION_RECRUITER, {
//   props: ({ data }) => ({
//     data: {
//       ...data,
//       events: (data.viewer &&
//         data.viewer.recruiter && data.viewer.recruiter.next_event_registrations
//         ? data.viewer.recruiter.next_event_registrations : []),
//     },
//   }),
// })(EventsRegistration);

// const NEXT_EVENTS_REGISTRATION_ID = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         user_id
//         next_event_registrations {
//           event_registration_id
//           status
//           event {
//             event_id
//           }
//         }
//       }
//     }
//   }
// `;

// const NextEventsRegistration = graphql(NEXT_EVENTS_REGISTRATION_ID, {
//   props: ({ data }) => ({
//     data: {
//       ...data,
//       events: (data.viewer &&
//         data.viewer.candidate && data.viewer.candidate.next_event_registrations
//         ? data.viewer.candidate.next_event_registrations
//         : []),
//     },
//   }),
// })(EventsRegistration);

// const PAST_EVENTS_REGISTRATION_ID = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         user_id
//         past_event_registrations {
//           event_registration_id
//           status
//           event {
//             event_id
//           }
//         }
//       }
//     }
//   }
// `;

// const PastEventsRegistration = graphql(PAST_EVENTS_REGISTRATION_ID, {
//   props: ({ data }) => ({
//     data: {
//       ...data,
//       events: (data.viewer &&
//         data.viewer.candidate && data.viewer.candidate.past_event_registrations
//         ? data.viewer.candidate.past_event_registrations : []),
//     },
//   }),
// })(EventsRegistration);

// const CURRENT_EVENTS_REGISTRATION_ID = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         user_id
//         current_event_registrations {
//           event_registration_id
//           status
//           event {
//             event_id
//           }
//         }
//       }
//     }
//   }
// `;

// const CurrentEventsRegistration = graphql(CURRENT_EVENTS_REGISTRATION_ID, {
//   props: ({ data }) => ({
//     data: {
//       ...data,
//       events: (data.viewer &&
//         data.viewer.candidate && data.viewer.candidate.current_event_registrations
//         ? data.viewer.candidate.current_event_registrations : []),
//     },
//   }),
// })(EventsRegistration);

// export { NextEventsRegistrationRecruiter };
// export { NextEventsRegistration, PastEventsRegistration, CurrentEventsRegistration };

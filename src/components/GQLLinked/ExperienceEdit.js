// import React, { Component } from 'react';
// import { View, StyleSheet, Image, Text } from 'react-native';
// import { Button, CheckBox } from 'react-native-elements';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';
// import moment from 'moment';
// import { theme, themeElements } from '../../config/constants/index';
// import Field from '../../components/Design/Field';
// import DateField from '../../components/Design/DateField';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import connectAlert from '../../components/Alert/connectAlert';
// import Company, { QUERY_COMPANIES } from './Company';
// import defaultBusinessImg from '../../assets/defaultBusiness.png';

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   titleView: {
//     flexDirection: 'row',
//     paddingLeft: 32,
//     paddingRight: 19,
//     marginTop: 22,
//     marginBottom: 30,
//   },
//   mainTitle: {
//     flex: 1,
//   },
//   sectionBackground: {
//     flex: 1,
//     position: 'relative',
//     backgroundColor: 'white',
//     paddingLeft: 24,
//     paddingRight: 24,
//     paddingTop: 32,
//     paddingBottom: 32,
//     marginBottom: 16,
//   },
//   formInputContainer: {
//     marginTop: -5,
//     marginRight: 0,
//     marginLeft: 0,
//   },
//   imageView: {
//     alignItems: 'center',
//   },
//   fieldView: {
//     marginTop: 20.5,
//     marginLeft: 24.25,
//     marginRight: 24.25,
//   },
//   datesView: {
//     marginTop: 28.5,
//     marginBottom: 0,
//     flexDirection: 'row',
//   },
//   dateView: {
//     flex: 1,
//     marginLeft: 24.25,
//     marginRight: 24.25,
//     flexDirection: 'column',
//   },
//   buttonView: {
//     marginBottom: 70,
//   },
//   checkBox: {
//     backgroundColor: 'white',
//     borderWidth: 0,
//     marginLeft: 0,
//     marginRight: 0,
//     marginBottom: 0,
//     marginTop: 0,
//   },
// });

// const VIEWER_CANDIDATE = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         education {
//           education_id
//         }
//         experience {
//           experience_id
//         }
//       }
//     }
//   }
// `;

// const QUERY_PROFILE_CANDIDATE = gql`
//   query SingleCandidateQuery($user_id: String) {
//     singleCandidate(user_id: $user_id) {
//       editable
//       firstname
//       lastname
//       tags
//       qualities
//       headline
//       work_city {
//         name
//       }
//       experience(order_by_date: true) {
//         experience_id
//         role
//       }
//       education(order_by_date: true) {
//         education_id
//       }
//       language {
//         language_id
//       }
//     }
//   }
// `;

// const QUERY_EXPERIENCE = gql`
//   query ExperienceQuery($experience_id: String!) {
//     experience(experience_id: $experience_id) {
//       company {
//         company_id
//         name
//         logo
//       }
//       role
//       start
//       end
//       description
//     }
//   }
// `;

// const format = 'MM-YYYY';

// class ExperienceEdit extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       dateEndNow: false,
//     };
//   }

//   componentWillMount() {
//     if (this.props.data && !this.props.data.loading) {
//       const { experience } = this.props.data;
//       const end = (!experience.end || experience.end === 'maintenant') ? undefined : moment(new Date(experience.end)).format(format);
//       this.setState({
//         companyName: experience.company ? experience.company.name : '',
//         companyId: experience.company ? experience.company.company_id : '',
//         companyLogo: experience.company ? experience.company.logo : '',
//         role: experience.role,
//         start: moment(new Date(experience.start)).format(format),
//         end,
//         dateEndNow: !end,
//         description: experience.description,
//       });
//     }
//   }

//   componentDidUpdate(prevProps) {
//     if (prevProps.data
//       && prevProps.data.loading
//       && !this.props.data.loading
//       && this.props.data.experience
//     ) {
//       const { experience } = this.props.data;
//       const end = (!experience.end || experience.end === 'maintenant') ? undefined : moment(new Date(experience.end)).format(format);
//       this.setState({
//         companyName: experience.company ? experience.company.name : '',
//         companyId: experience.company ? experience.company.company_id : '',
//         companyLogo: experience.company ? experience.company.logo : '',
//         role: experience.role,
//         start: moment(new Date(experience.start)).format(format),
//         end,
//         dateEndNow: !end,
//         description: experience.description,
//       });
//     }
//   }

//   getRefetchQueries() {
//     const refetchQueries = this.props.refetchQueries.slice();
//     refetchQueries.push({
//       query: QUERY_COMPANIES,
//     });
//     refetchQueries.push({
//       query: QUERY_PROFILE_CANDIDATE,
//       variables: {
//         user_id: this.props.userId,
//       },
//     });
//     refetchQueries.push({
//       query: VIEWER_CANDIDATE,
//     });
//     return refetchQueries;
//   }

//   props: {
//     alertWithType: Function,
//     containerStyle: {},
//     experienceId: string,
//     userId: string,
//     addExperience: Function,
//     editExperience: Function,
//     deleteExperience: Function,
//     onAddExperience: Function,
//     onEditExperience: Function,
//     onDeleteExperience: Function,
//     refetchQueries: [];
//     data: {
//       experience: {
//         description: string,
//         start: string,
//         end: string,
//         role: string,
//         company: {
//           company_id: string,
//           name: string,
//           logo: string,
//         },
//       },
//       loading: boolean,
//       error: boolean,
//     },
//   };

//   hasFieldsFill() {
//     if (!this.state.companyName
//       || !this.state.start
//       || !this.state.role
//     ) {
//       this.props.alertWithType(
//         'error',
//         'Champs de formulaires incomplets',
//         'Vous devez completer au moins le nom de l\'entreprise, votre position, date de commencement, de fin.',
//       );
//       return false;
//     }
//     return true;
//   }

//   handleAddExperienceButton = async () => {
//     if (!this.hasFieldsFill()) {
//       return;
//     }
//     const variables = {
//       start: moment(this.state.start, format).toDate(),
//       role: this.state.role,
//       end: this.state.end && !this.state.dateEndNow ? moment(this.state.end, format).toDate() : undefined,
//       description: this.state.description,
//     };
//     if (this.state.companyId) {
//       variables.company_id = this.state.companyId;
//     } else {
//       variables.company_name = this.state.companyName;
//     }

//     const ret = await this.props.addExperience({
//       refetchQueries: this.getRefetchQueries(),
//       variables,
//     });
//     if (ret.data.createExperience.error) {
//       this.props.alertWithType(
//         'error',
//         'Erreur',
//         "Erreur lors de l'ajout de l'experience",
//       );
//       return;
//     }
//     if (this.props.onAddExperience) {
//       this.props.alertWithType(
//         'success',
//         'Ajout effectuée',
//         "L'expérience a été ajouté avec succès",
//       );
//       this.props.onAddExperience();
//     }
//   };

//   handleEditExperienceButton = async () => {
//     if (!this.hasFieldsFill()) {
//       return;
//     }
//     const variables = {
//       experience_id: this.props.experienceId,
//       start: moment(this.state.start, format).toDate(),
//       role: this.state.role,
//       end: this.state.end && !this.state.dateEndNow ? moment(this.state.end, format).toDate() : undefined,
//       description: this.state.description,
//     };
//     if (this.state.companyId) {
//       variables.company_id = this.state.companyId;
//     } else {
//       variables.company_name = this.state.companyName;
//     }

//     const ret = await this.props.editExperience({
//       refetchQueries: [{
//         query: QUERY_EXPERIENCE,
//         variables: {
//           experience_id: this.props.experienceId,
//         },
//       }, {
//         query: QUERY_COMPANIES,
//       }],
//       variables,
//     });
//     if (ret.data.editExperience.error) {
//       this.props.alertWithType(
//         'error',
//         'Erreur',
//         "Erreur lors de la modification de l'experience",
//       );
//       return;
//     }
//     if (this.props.onEditExperience) {
//       this.props.alertWithType(
//         'success',
//         'Modificiation effectuée',
//         "L'expérience a été modifiée avec succès",
//       );
//       this.props.onEditExperience();
//     }
//   };

//   handleDeleteExperienceButton = async () => {
//     const ret = await this.props.deleteExperience({
//       refetchQueries: this.getRefetchQueries(),
//       variables: {
//         experience_id: this.props.experienceId,
//       },
//     });
//     if (ret.data.deleteExperience.error) {
//       this.props.alertWithType(
//         'error',
//         'Erreur',
//         "Erreur lors de la suppression de l'experience",
//       );
//       return;
//     }
//     if (this.props.onDeleteExperience) {
//       this.props.alertWithType(
//         'success',
//         'Suppression effectuée',
//         "L'expérience a été supprimé avec succès",
//       );
//       this.props.onDeleteExperience();
//     }
//   };

//   render() {
//     if (this.props.data && this.props.data.error) {
//       return (<ErrorComponent message="Problème lors de la récupération du profil." />);
//     }
//     return (
//       <View style={[styles.container, this.props.containerStyle]}>
//         <View style={styles.imageView}>
//           <Image
//             source={this.state.companyLogo ? { uri: this.state.companyLogo } : defaultBusinessImg}
//             style={{ width: 64, height: 64 }}
//             resizeMode="contain"
//           />
//         </View>
//         <Company
//           containerStyle={styles.fieldView}
//           label="Nom de l'entreprise"
//           value={this.state.companyName}
//           onChangeText={(text) => {
//             if (text !== this.state.companyName) {
//               this.setState({ companyName: text, companyId: null, companyLogo: null });
//             }
//           }}
//           onValidate={(company) => {
//             this.setState({
//               companyName: company.name,
//               companyId: company.company_id,
//               companyLogo: company.logo,
//             });
//           }}
//         />
//         <Field
//           containerStyle={styles.fieldView}
//           label="Poste dans l'entreprise"
//           value={this.state.role}
//           onChangeText={(role) => {
//             this.setState({ role });
//           }}
//         />
//         <View style={styles.datesView}>
//           <DateField
//             containerStyle={styles.dateView}
//             label="Date de début"
//             placeholder="Date"
//             dateOptions={{ format }}
//             date={this.state.start}
//             onDateChange={(start) => {
//               this.setState({ start });
//             }}
//           />
//           <View style={styles.dateView}>
//             <DateField
//               label="Date de Fin"
//               placeholder="Date"
//               dateOptions={{ format }}
//               date={this.state.end}
//               onDateChange={(end) => {
//                 this.setState({ end });
//               }}
//               disabled={this.state.dateEndNow}
//             />
//             <CheckBox
//               title='Maintenant'
//               checkedColor={theme.primaryColor}
//               checked={this.state.dateEndNow}
//               containerStyle={styles.checkBox}
//               onPress={() => this.setState({ dateEndNow: !this.state.dateEndNow })}
//             />
//           </View>
//         </View>
//         <Field
//           containerStyle={styles.fieldView}
//           label="Description"
//           onChangeText={(description) => {
//             this.setState({ description });
//           }}
//           value={this.state.description}
//           inputStyle={{ height: 80 }}
//           multiline
//           textInputOptions={{
//             multiline: true,
//             maxHeight: 72,
//             returnKeyType: 'done',
//             blurOnSubmit: true,
//           }}
//         />
//         {
//           !this.props.experienceId ? (
//             <Button
//               title="Ajouter l'expérience"
//               color={theme.secondaryColor}
//               backgroundColor={theme.primaryColor}
//               buttonStyle={themeElements.validateButton}
//               containerViewStyle={{ marginTop: 28.5 }}
//               onPress={this.handleAddExperienceButton}
//             />) : (
//             <View style={styles.buttonView}>
//               <Button
//                 title="Valider l'expérience"
//                 color={theme.secondaryColor}
//                 backgroundColor={theme.primaryColor}
//                 buttonStyle={themeElements.validateButton}
//                 containerViewStyle={{ marginTop: 28.5 }}
//                 onPress={this.handleEditExperienceButton}
//               />
//               <Button
//                 title="Supprimer l’expérience"
//                 color={'#cc4733'}
//                 backgroundColor={'white'}
//                 containerViewStyle={{ marginTop: 0 }}
//                 onPress={this.handleDeleteExperienceButton}
//               />
//             </View>
//           )
//         }
//       </View>
//     );
//   }
// }

// const ADD_EXPERIENCE = gql`
//   mutation(
//     $company_id: String
//     $company_name: String
//     $start: String!
//     $role: String
//     $end: String
//     $description: String) {
//     createExperience(
//       company_id: $company_id
//       company_name: $company_name
//       start: $start
//       role: $role
//       end: $end
//       description: $description
//     ) {
//       experience {
//         experience_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const EDIT_EXPERIENCE = gql`
//   mutation(
//     $experience_id: String!
//     $company_id: String
//     $company_name: String
//     $start: String!
//     $role: String
//     $end: String
//     $description: String) {
//     editExperience(
//       experience_id: $experience_id
//       company_id: $company_id
//       company_name: $company_name
//       start: $start
//       role: $role
//       end: $end
//       description: $description
//     ) {
//       experience {
//         experience_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const DELETE_EXPERIENCE = gql`
//   mutation(
//     $experience_id: String!
//   ) {
//     deleteExperience(
//       experience_id: $experience_id
//     ) {
//       experience {
//         experience_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// export default compose(
//   graphql(QUERY_EXPERIENCE, {
//     skip: ownProps => !ownProps.experienceId,
//     options: ownProps => ({
//       variables: {
//         experience_id: ownProps.experienceId,
//       },
//     }),
//   }),
//   graphql(ADD_EXPERIENCE, { name: 'addExperience' }),
//   graphql(EDIT_EXPERIENCE, { name: 'editExperience' }),
//   graphql(DELETE_EXPERIENCE, { name: 'deleteExperience' }),
// )(connectAlert(ExperienceEdit));

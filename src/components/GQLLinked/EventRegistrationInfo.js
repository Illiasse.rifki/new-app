// // @flow
// import React, { Component } from 'react';
// import { View, StyleSheet, Text } from 'react-native';
// import { compose, graphql } from 'react-apollo';
// import gql from 'graphql-tag';
// import moment from 'moment';
// import { Button } from 'react-native-elements';
// import { QUERY_EVENT_CANDIDATES as QUERY_PRESENT } from '../../screens/candidates/Present';
// import { QUERY_EVENT_CANDIDATES as QUERY_ANNOTATED } from '../../screens/candidates/Annotated';
// import { QUERY_EVENT_CANDIDATES as QUERY_SELECTED } from '../../screens/candidates/Selected';
// import { QUERY_EVENT_CANDIDATES as QUERY_TREAT } from '../../screens/candidates/Treat';
// import { theme } from '../../config/constants/index';

// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import LoadingComponent from '../../components/Elements/LoadingComponent';

// const styles = StyleSheet.create({
//   container: {
//     backgroundColor: '#f5f5f5',
//     paddingVertical: 24,
//     paddingHorizontal: 32,
//   },
//   selectText: {
//     color: 'white',
//     fontSize: 16,
//   },
//   group: {
//     flexDirection: 'row',
//     flex: 1,
//   },
//   groupText: {
//     color: '#1c1c1c',
//     fontSize: 16,
//   },
//   waiting: {
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
//   BtnStyle: {
//     borderRadius: 4,
//     shadowRadius: 2,
//     shadowOffset: { width: 1, height: 4 },
//     shadowColor: 'rgba(0, 0, 0, 0.16)',
//     borderWidth: 1,
//     borderColor: 'rgba(0, 0, 0, 0.06)',
//   },
//   chat: {
//     flex: 1,
//   },
//   comment: {
//     flex: 1,
//   },
// });

// class EventRegistrationInfo extends Component {
//   props: {
//     event_registration_info_id: string,
//     user_id: string,
//     event_registration_id: string,
//     event_id: string,
//     refetch: Function,
//     data: {
//       error: any,
//       loading: boolean,
//       refetch: Function,
//       eventRegistrationInfo: any,
//     },
//     navigate: Function,
//     edit: any,
//     create: any,
//   };

//   async componentWillMount() {
//     // worst fail safe ever
//     await this.props.refetch();
//   }

//   componentDidMount() {
//     if (!this.props.event_registration_info_id) {
//       this.createEventRegistrationInfo();
//     }
//   }

//   createEventRegistrationInfo() {
//     const { event_registration_id, event_id } = this.props;
//     this.props.create({
//       variables: {
//         event_registration_id,
//         event_id,
//         selected: false,
//         comment: '',
//       },
//     })
//       .then(({
//         data: {
//           createEventRegistrationInfo: {
//             event_registration_info: {
//               event_registration_info_id,
//             },
//           },
//         },
//       }) => {
//         this.props.setParams({ event_registration_info_id });
//       })
//       .catch(error => console.warn('catch creat event reg', error));
//   }

//   editEventRegistrationInfo = async () => {
//     const { event_registration_info_id } = this.props;
//     const { selected, comment } = this.props.data.eventRegistrationInfo;
//     const v = { event_id: this.props.event_id };
//     try {
//       await this.props.edit({
//         variables: { event_registration_info_id, selected: !selected },
//         refetchQueries: [
//           {
//             query: QUERY_PRESENT,
//             variables: v,
//           },
//           {
//             query: QUERY_ANNOTATED,
//             variables: v,
//           },
//           {
//             query: QUERY_SELECTED,
//             variables: v,
//           },
//           {
//             query: QUERY_TREAT,
//             variables: v,
//           },
//           {
//             query: EVENT_REGISTRATION_INFO,
//             variables: { event_registration_info_id },
//           },
//         ],
//       });
//     } catch (e) {
//       console.warn('Edit or Refetch failed', e);
//     }
//   }

//   renderWait = () => (
//     <View style={styles.container}>
//       <LoadingComponent />
//       <View style={styles.waiting}>
//         <Text>
//           {
//             'Récuperation des infos'
//           }
//         </Text>
//       </View>
//     </View>
//   )

//   render() {
//     const { data, event_id, user_id } = this.props;
//     if (!data) return this.renderWait();

//     if (data && data.loading) return <LoadingComponent />;
//     if (data && data.error) return <ErrorComponent />;
//     if (!this.props.event_registration_info_id || !data) return null;
//     const past = data.eventRegistrationInfo.event_registration.event.passed;

//     const [title, backgroundColor] = data.eventRegistrationInfo.selected ?
//       ['Deselectionner', theme.red] : ['Selectionner', theme.primaryColor];
//     const { event, candidate } = data.eventRegistrationInfo.event_registration;

//     const onPress = this.props.event_registration_info_id ? this.editEventRegistrationInfo : this.createEventRegistrationInfo;
//     if (!past) { return (null); }
//     return (
//       <View style={styles.container}>
//         {
//           past &&
//           <Button
//             onPress={this.editEventRegistrationInfo}
//             title={title}
//             textStyle={styles.selectText}
//             backgroundColor={backgroundColor}
//             buttonStyle={[styles.BtnStyle, { marginBottom: 16 }]}
//           />
//         }
//         <View style={styles.group}>
//           {
//             past &&
//             <View style={styles.comment}>
//               <Button
//                 onPress={() => this.props.navigate('Comment', {
//                   user_id,
//                   event_id,
//                   event_registration_id: this.props.event_registration_id,
//                   event_registration_info_id: this.props.event_registration_info_id,
//                 })}
//                 backgroundColor={theme.secondaryColor}
//                 title='Annoter'
//                 textStyle={styles.groupText}
//                 buttonStyle={styles.BtnStyle}
//               />
//             </View>
//           }
//         </View>
//       </View>
//     );
//   }
// }

// const EVENT_REGISTRATION_INFO = gql`
//   query CandidateInfo($event_registration_info_id: String!){
//     eventRegistrationInfo(event_registration_info_id: $event_registration_info_id) {
//       event_registration_info_id
//       recruiter_id
//       event_registration {
//         candidate {
//           fullname
//         }
//         event {
//           name
//           passed
//           location
//           date_format
//         }
//       }
//       selected
//       comment
//     }
//   }
// `;

// const CREATE_EVENT_REGISTRATION_INFO = gql`
//   mutation createEventRegistrationInfo($recruiter_id: String, $event_registration_id: String!, $event_id: String!, $selected: Boolean, $comment: String) {
//     createEventRegistrationInfo(recruiter_id: $recruiter_id, event_registration_id: $event_registration_id, event_id: $event_id, selected: $selected, comment: $comment) {
//       event_registration_info {
//         event_registration_info_id
//         selected
//         comment
//         event_registration {
//           candidate {
//             fullname
//           }
//         }
//       }
//       error {
//         code
//         message
//       }
//     }
//   }
// `;

// const EDIT_EVENT_REGISTRATION_INFO = gql`
//   mutation editEventRegistrationInfo($event_registration_info_id: String!, $selected: Boolean, $comment: String) {
//     editEventRegistrationInfo(event_registration_info_id: $event_registration_info_id, selected: $selected, comment: $comment) {
//       event_registration_info {
//         event_registration_info_id
//         selected
//         comment
//       }
//       error {
//         code
//         message
//       }
//     }
//   }
// `;

// export default compose(
//   graphql(EVENT_REGISTRATION_INFO, {
//     skip: props => !props.event_registration_info_id,
//     options: props => ({
//       variables: {
//         event_registration_info_id: props.event_registration_info_id,
//       },
//     }),
//   }),
//   graphql(CREATE_EVENT_REGISTRATION_INFO, { name: 'create' }),
//   graphql(EDIT_EVENT_REGISTRATION_INFO, { name: 'edit' }),
// )(EventRegistrationInfo);

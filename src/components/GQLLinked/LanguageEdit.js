// import React, { Component } from 'react';
// import { View, StyleSheet } from 'react-native';
// import { Button } from 'react-native-elements';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';
// import { theme, themeElements, languages, levels } from '../../config/constants/index';
// import DataPicker from '../../components/Design/DataPicker';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import connectAlert from '../../components/Alert/connectAlert';

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     height: '100%',
//     marginBottom: 20,
//   },
//   mainTitle: {
//     flex: 1,
//   },
//   fieldView: {
//     marginTop: 28.5,
//     marginLeft: 24.25,
//     marginRight: 24.25,
//   },
//   buttonView: {
//     marginBottom: 70,
//   },
// });

// const QUERY_LANGUAGE = gql`
//   query LanguageQuery($language_id: String!) {
//     language(language_id: $language_id) {
//       language_id
//       name
//       level
//     }
//   }
// `;

// class LanguageEdit extends Component {
//   state= {
//     name: languages[0].language,
//     code: languages[0].code,
//     level: levels[0].label,
//     level_code: levels[0].code,
//   }

//   componentDidUpdate(prevProps) {
//     if (prevProps.data
//       && prevProps.data.loading
//       && !this.props.data.loading
//       && this.props.data.language
//     ) {
//       this.setState({
//         name: this.props.data.language.name,
//         level: this.props.data.language.level,
//       });
//     }
//   }

//   props: {
//     containerStyle: {},
//     languageId: string,
//     userId: string,
//     addLanguage: Function,
//     editLanguage: Function,
//     deleteLanguage: Function,
//     onAddLanguage: Function,
//     onEditLanguage: Function,
//     onDeleteLanguage: Function,
//     refetchQueries: [];
//     alertWithType: Function,
//     data: {
//       language: {
//         name: string,
//         level: string,
//       },
//       loading: boolean,
//       error: boolean,
//     },
//   };

//   hasFieldsFill = () => {
//     if (!this.state.name
//       || !this.state.level
//     ) {
//       this.props.alertWithType(
//         'error',
//         'Vous devez completer tous les champs disponibles',
//         'Champs de formulaires incomplets',
//       );
//       return false;
//     }
//     return true;
//   }

//   handleAddLanguageButton = async () => {
//     if (!this.hasFieldsFill()) {
//       return;
//     }
//     const variables = {
//       user_id: this.props.userId,
//       name: this.state.name,
//       code: this.state.code,
//       level_code: this.state.level_code,
//       level: this.state.level,
//     };

//     const ret = await this.props.addLanguage({
//       refetchQueries: this.props.refetchQueries,
//       variables,
//     });
//     if (ret.data.createLanguage.error) {
//       this.props.alertWithType(
//         'error',
//         'Erreur',
//         "Erreur lors de l'ajout de la langue",
//       );
//       return;
//     }
//     if (this.props.onAddLanguage) {
//       this.props.alertWithType(
//         'success',
//         'Ajout effectué',
//         'La langue a été ajoutée avec succès',
//       );
//       this.props.onAddLanguage();
//     }
//   };

//   handleEditLanguageButton = async () => {
//     if (!this.hasFieldsFill()) {
//       return;
//     }
//     const variables = {
//       language_id: this.props.languageId,
//       name: this.state.name,
//       code: this.state.code,
//       level_code: this.state.level_code,
//       level: this.state.level,
//     };

//     const ret = await this.props.editLanguage({
//       refetchQueries: [{
//         query: QUERY_LANGUAGE,
//         variables: {
//           language_id: this.props.languageId,
//         },
//       }],
//       variables,
//     });
//     if (ret.data.editLanguage.error) {
//       this.props.alertWithType(
//         'error',
//         'Erreur',
//         'Erreur lors de la modification de la langue',
//       );
//       return;
//     }
//     if (this.props.onEditLanguage) {
//       this.props.alertWithType(
//         'success',
//         'Modificiation effectuée',
//         'La langue a été modifiée avec succès',
//       );
//       this.props.onEditLanguage();
//     }
//   };

//   handleDeleteLanguageButton = async () => {
//     const ret = await this.props.deleteLanguage({
//       refetchQueries: this.props.refetchQueries,
//       variables: {
//         language_id: this.props.languageId,
//       },
//     });
//     if (ret.data.deleteLanguage.error) {
//       this.props.alertWithType(
//         'error',
//         'Erreur',
//         'Erreur lors de la suppression de la langue',
//       );
//       return;
//     }
//     if (this.props.onDeleteLanguage) {
//       this.props.alertWithType(
//         'success',
//         'Suppression effectuée',
//         'La langue a été supprimé avec succès',
//       );
//       this.props.onDeleteLanguage();
//     }
//   };

//   render() {
//     if (this.props.data && this.props.data.error) {
//       return (<ErrorComponent message='Problème lors de la récupération de la langue.' />);
//     }
//     return (
//       <View style={[styles.container, this.props.containerStyle]}>
//         <DataPicker
//           containerStyle={styles.fieldView}
//           label='Langue'
//           data={languages.map(language => language.language)}
//           onConfirm={(name) => {
//             const languageName = name || null;
//             if (languageName) {
//               const v = languages.find(l => l.language === languageName);
//               if (v) {
//                 this.setState({ name: v.language, code: v.code });
//               }
//             }
//           }}
//           value={this.state.name}
//           placeholder='Selectionner votre langue'
//         />
//         <DataPicker
//           containerStyle={styles.fieldView}
//           label='Niveau de langue'
//           data={levels.map(l => l.label)}
//           onConfirm={(level) => {
//             const levelName = level || null;
//             if (levelName) {
//               const v = levels.find(l => l.label === levelName);
//               if (v) {
//                 this.setState({ level: v.label, level_code: v.code });
//               }
//             }
//           }}
//           value={this.state.level}
//           placeholder='Selectionner votre niveau de langue'
//         />
//         {
//           !this.props.languageId ? (
//             <Button
//               title="Ajouter la langue"
//               color={theme.secondaryColor}
//               backgroundColor={theme.primaryColor}
//               buttonStyle={themeElements.validateButton}
//               containerViewStyle={{ marginTop: 38.5 }}
//               onPress={this.handleAddLanguageButton}
//             />) : (
//             <View style={styles.buttonView}>
//               <Button
//                 title="Valider la langue"
//                 color={theme.secondaryColor}
//                 backgroundColor={theme.primaryColor}
//                 buttonStyle={themeElements.validateButton}
//                 containerViewStyle={{ marginTop: 38.5 }}
//                 onPress={this.handleEditLanguageButton}
//               />
//               <Button
//                 title="Supprimer la langue"
//                 color={'#cc4733'}
//                 backgroundColor={'white'}
//                 containerViewStyle={{ marginTop: 0 }}
//                 onPress={this.handleDeleteLanguageButton}
//               />
//             </View>
//           )
//         }
//       </View>
//     );
//   }
// }

// const ADD_LANGUAGE = gql`
//   mutation(
//     $user_id: String!
//     $name: String!
//     $code: String!
//     $level: String!
//     $level_code: String!
//   ) {
//     createLanguage(
//       user_id: $user_id
//       name: $name
//       code: $code
//       level: $level
//       level_code: $level_code
//     ) {
//       language {
//         language_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const EDIT_LANGUAGE = gql`
//   mutation(
//     $language_id: String!
//     $name: String
//     $code: String
//     $level: String
//     $level_code: String
//   ) {
//     editLanguage(
//       language_id: $language_id
//       name: $name
//       code: $code
//       level: $level
//       level_code: $level_code
//     ) {
//       language {
//         language_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const DELETE_LANGUAGE = gql`
//   mutation(
//     $language_id: String!
//   ) {
//     deleteLanguage(
//       language_id: $language_id
//     ) {
//       language {
//         language_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// export default compose(
//   graphql(QUERY_LANGUAGE, {
//     skip: ownProps => !ownProps.languageId,
//     options: ownProps => ({
//       variables: {
//         language_id: ownProps.languageId,
//       },
//     }),
//   }),
//   graphql(ADD_LANGUAGE, { name: 'addLanguage' }),
//   graphql(EDIT_LANGUAGE, { name: 'editLanguage' }),
//   graphql(DELETE_LANGUAGE, { name: 'deleteLanguage' }),
// )(connectAlert(LanguageEdit));

// import React from "react";
// import { View, StyleSheet, Text } from "react-native";
// import { graphql } from "react-apollo";
// import gql from "graphql-tag";
// import ErrorComponent from "../Elements/ErrorComponent";
// import { theme } from "../../config/constants/index";

// type Props = {
//   containerStyle: {},
//   languageId: string,
//   data: {
//     language: {
//       name: string,
//       level: string,
//     },
//     loading: boolean,
//     error: boolean,
//   },
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   name: {
//     color: theme.fontBlack,
//     fontSize: 18,
//     fontWeight: "500",
//     lineHeight: 24,
//   },
//   level: {
//     color: theme.gray,
//     fontSize: 14,
//     fontWeight: "400",
//     lineHeight: 24,
//   },
// });

// const LanguageDisplay = (props: Props) => {
//   if (props.data.error) {
//     return (
//       <ErrorComponent message={"Erreur lors de récuperation de la langue"} />
//     );
//   } else if (props.data.loading) {
//     return null;
//   }

//   const { language } = props.data;
//   return (
//     <View style={[styles.container, props.containerStyle]}>
//       <Text style={styles.name}>{language.name}</Text>
//       <Text style={styles.level}>{language.level}</Text>
//     </View>
//   );
// };

// const QUERY_LANGUAGE = gql`
//   query LanguageQuery($language_id: String!) {
//     language(language_id: $language_id) {
//       name
//       level
//     }
//   }
// `;

// export default graphql(QUERY_LANGUAGE, {
//   options: (ownProps) => ({
//     variables: {
//       language_id: ownProps.languageId,
//     },
//   }),
// })(LanguageDisplay);

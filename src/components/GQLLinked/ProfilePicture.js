// // @flow
// import React, { Component } from 'react';
// import { Avatar, Button } from 'react-native-elements';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';
// import Expo from 'expo';
// import { StyleSheet, View, Picker, Modal, Alert } from 'react-native';

// import LoadingComponent from '../../components/Elements/LoadingComponent';
// import ErrorComponent from '../../components/Elements/ErrorComponent';
// import { connectAlert } from '../../components/Alert';
// import { theme } from '../../config/constants/index';

// const GRANTED = 'granted';

// const QUERY_PICTURE = gql`
//   query SingleCandidateQuery($user_id: String) {
//     viewer {
//       id
//       recruiter {
//         user_id
//       }
//       candidate {
//         user_id
//       }
//     }
//     singleCandidate(user_id: $user_id) {
//       user_id
//       picture
//       firstname
//       lastname
//       years_experience
//     }
//   }
// `;

// const styles = StyleSheet.create({
//   container: {
//     borderWidth: 1.5,
//     borderColor: theme.primaryColor,
//     borderRadius: 5,
//   },
//   containerModal: {
//     height: '100%',
//     width: '100%',
//     display: 'flex',
//     flexDirection: 'column',
//     justifyContent: 'flex-end',
//   },
//   image: {
//     borderRadius: 5,
//   },
//   loading: {
//     minWidth: 110,
//     minHeight: 110,
//     flex: 0,
//     paddingLeft: 0,
//     paddingRight: 0,
//   },
//   buttonGallery: {
//     backgroundColor: '#26A69A',
//   },
//   buttonCamera: {
//     backgroundColor: '#607d8b',
//   },
//   modalPicker: {
//     backgroundColor: '#3333',
//   },
//   containerModalPicker: {
//     backgroundColor: '#3333',
//     width: '100%',
//     paddingTop: 8,
//     paddingBottom: 8,
//     position: 'absolute',
//     bottom: 0,
//   },
// });

// type Image = {
//   cancelled: Boolean,
//   uri: String,
//   width: Number,
//   height: Number,
//   exif: ?{},
//   base64: ?String,
// };

// function BorderExperience(props: { experimented: ?boolean, children: any, style: Object }) {
//   const color =
//     props.experimented !== undefined
//       ? props.experimented
//         ? '#d50000'
//         : '#2962ff'
//       : theme.primaryColor;
//   return (
//     <View
//       style={[
//         {
//           borderColor: color,
//           borderWidth: 4,
//           borderRadius: 5,
//           marginBottom: 14,
//           borderStyle: 'solid',
//         },
//         props.style,
//       ]}
//     >
//       {props.children}
//     </View>
//   );
// }

// class ProfilePicture extends Component {
//   state = {
//     loading: false,
//     picture: null,
//     type: null,
//     modalVisible: false,
//   };

//   props: {
//     data: {
//       refetch: Function,
//       loading: boolean,
//       singleCandidate: {
//         user_id: string,
//         picture: string,
//         firstname: string,
//         lastname: string,
//       },
//     },
//     avatarStyle: {},
//     defaultPicture: string,
//     editPicture: any,
//     edit: boolean,
//     userId: string,
//     alertWithType: Function,
//     size: number,
//     containerStyle: {},
//     onChange: Function,
//   };

//   ensurePermission = async (permission) => {
//     const { status } = await Expo.Permissions.getAsync(permission);
//     if (status !== GRANTED) {
//       const { status: askStatus } = await Expo.Permissions.askAsync(permission);
//       if (askStatus !== GRANTED) {
//         return false;
//       }
//     }
//     return true;
//   };

//   handleEdit = async (type: 'camera' | 'gallery') => {
//     await this.ensurePermission(Expo.Permissions.CAMERA);
//     await this.ensurePermission(Expo.Permissions.CAMERA_ROLL);
//     const options = {
//       base64: true,
//       quality: 1,
//       mediaType: Expo.ImagePicker.MediaTypeOptions.Images,
//     };
//     let promise;
//     if (type === 'camera') {
//       promise = Expo.ImagePicker.launchCameraAsync(options);
//       // title: 'Sélectionner votre photo de profil',
//       // mediaType: 'photo',
//       // noData: true,
//       // takePhotoButtonTitle: 'Prendre une photo',
//       // chooseFromLibraryButtonTitle: 'Sélectionner à partir de vos images',
//       // storageOptions: {
//       //   skipBackup: true,
//       // },
//     } else if (type === 'gallery') {
//       promise = Expo.ImagePicker.launchImageLibraryAsync(options);
//     } else {
//       this.setState({ modalVisible: false });
//       return;
//     }
//     promise
//       .then(async ({ cancelled, uri, width, height, exif, base64 }: Image) => {
//         this.setState({ modalVisible: false });
//         if (cancelled) {
//           this.props.alertWithType('info', 'Info', "Vous avez annulé le changement d'image.");
//         }
//         /* else if (response.error) {
//         this.props.alertWithType('error', 'Error', `Impossible de récupérer l'image. ${response.error}`);
//       } */
//         const { singleCandidate: candidate } = this.props.data || {};
//         const filename = `${
//           candidate ? `${candidate.firstname}_${candidate.lastname}_` : ''
//         }${Date.now()}.jpg`;
//         const getMax = (a, b) => (a > b ? a : b);
//         const getMin = (a, b) => (a < b ? a : b);
//         const longestSide = getMax(width, height) === width ? 'width' : 'height';
//         const shortestSide = longestSide === 'width' ? 'height' : 'width';
//         const map = {
//           width: {
//             origin: 'originX',
//             value: 'width',
//           },
//           height: {
//             origin: 'originY',
//             value: 'height',
//           },
//         };
//         try {
//           const resizedImage: Image = await Expo.ImageManipulator.manipulate(
//             uri,
//             [
//               {
//                 crop: {
//                   [map[longestSide].origin]: (getMax(height, width) - getMin(height, width)) / 2,
//                   [map[shortestSide].origin]: 0,
//                   [map[longestSide].value]: getMin(height, width),
//                   [map[shortestSide].value]: getMin(height, width),
//                 },
//                 resize: {
//                   width: 300,
//                   height: 300,
//                 },
//               },
//             ],
//             { format: 'jpeg', base64: true },
//           );
//           const picture = JSON.stringify({
//             data: resizedImage.base64,
//             filename,
//           });
//           if (!this.props.userId) {
//             // This ugly s*** is not mine
//             // It just appear I've my name because of eslint refactoring.
//             // @maxime
//             // this.state.picture = uri; // Dafuck
//             this.setState({ picture: uri });
//             this.props.onChange(picture);
//             return;
//           }
//           this.setState({ loading: true });
//           const { data: d } = await this.props.editPicture({
//             variables: {
//               user_id: this.props.userId,
//               picture,
//             },
//           });
//           this.setState({ loading: false });
//           if (!d) this.props.alertWithType('error', 'Error', "Impossible de récupérer l'image.");
//           else this.props.data.refetch();
//         } catch (e) {
//           this.setState({ loading: false });
//           this.props.alertWithType('error', 'Error', `Impossible de récupérer l'image. ${e}`);
//         }
//       })
//       .catch((err) => {
//         console.warn('something wrong happen');
//         console.error(err);
//       });
//   };

//   isExperimented = () => {
//     const { data } = this.props;
//     if (data && !data.loading && data.singleCandidate && data.viewer) {
//       const candidate = data.singleCandidate;
//       const exp = candidate.years_experience;
//       if (data.viewer.recruiter !== null) {
//         try {
//           const expNumber = parseInt(exp, 10);
//           return expNumber > 2;
//         } catch (error) {
//           return false;
//         }
//       }
//     }
//     return undefined;
//   };

//   render() {
//     const { data } = this.props;
//     let title = '';
//     let pic = null;
//     let loading = false;
//     let candidate = {};
//     if (data) {
//       if (data.error) {
//         return <ErrorComponent message={"Erreur de chargement de l'image, réessayer plus tard."} />;
//       }
//       loading = data.loading;
//       if (!loading) {
//         candidate = data.singleCandidate;
//         pic = candidate.picture;
//         title = !pic ? `${candidate.firstname[0]}${candidate.lastname[0]}` : '';
//       }
//     }
//     return (
//       <BorderExperience experimented={this.isExperimented()} style={this.props.containerStyle}>
//         {loading || this.state.loading ? (
//           <LoadingComponent containerStyle={styles.loading} transparent />
//         ) : (
//           <React.Fragment>
//             <Avatar
//               activeOpacity={1}
//               width={this.props.size || 110}
//               height={this.props.size || 110}
//               title={title}
//               source={
//                 (this.state.picture && { uri: this.state.picture }) ||
//                 (pic && { uri: pic }) ||
//                 this.props.defaultPicture
//               }
//               avatarStyle={[styles.image, this.props.avatarStyle]}
//               onPress={() => {
//                 if (this.props.edit) {
//                   this.setState({ modalVisible: true });
//                 }
//               }}
//             />
//             <Modal
//               animationType="slide"
//               style={styles.modalPicker}
//               visible={this.state.modalVisible}
//               onRequestClose={() => this.setState({ modalVisible: false })}
//             >
//               <View style={[styles.containerModalPicker]}>
//                 <Button
//                   raised
//                   buttonStyle={styles.buttonCamera}
//                   icon={{
//                     type: 'font-awesome',
//                     name: 'camera',
//                     size: 15,
//                     color: 'white',
//                   }}
//                   color="white"
//                   title={'Prendre une photo'}
//                   onPress={() => this.handleEdit('camera')}
//                 />
//                 <Button
//                   raised
//                   buttonStyle={styles.buttonGallery}
//                   icon={{
//                     name: 'image',
//                     type: 'font-awesome',
//                     size: 15,
//                     color: 'white',
//                   }}
//                   style={{ marginTop: 8 }}
//                   color="white"
//                   title={'Choisir depuis la galerie'}
//                   onPress={() => this.handleEdit('gallery')}
//                 />
//               </View>
//             </Modal>
//           </React.Fragment>
//         )}
//       </BorderExperience>
//     );
//   }
// }

// const EDIT_PICTURE = gql`
//   mutation($user_id: String!, $picture: String!) {
//     editCandidate(user_id: $user_id, picture: $picture) {
//       candidate {
//         user_id
//         picture
//         firstname
//         lastname
//       }
//       error {
//         code
//         message
//       }
//     }
//   }
// `;

// export default compose(
//   graphql(QUERY_PICTURE, {
//     skip: props => !props.userId,
//     options: props => ({
//       variables: {
//         user_id: props.userId,
//       },
//     }),
//   }),
//   graphql(EDIT_PICTURE, { name: 'editPicture' }),
// )(connectAlert(ProfilePicture));

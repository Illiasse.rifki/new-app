// import React, { Component } from 'react';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import { client } from '../../utils/Chat';

// import ErrorComponent from '../Elements/ErrorComponent';
// import LoadingComponent from '../Elements/LoadingComponent';

// import UserSmallCard from '../Card/UserSmallCard';

// type tEventCandidateCard = {
//   title: string,
//   subtitle: string,
//   picture: any,
//   pictureTitle: string,
//   userId: string,
//   arriveDate: ?string,
//   arriveDateMarker: ?boolean,
//   onPress: Function,
//   data: {
//     loading: Boolean,
//     error: any,
//     info: {
//       event_registration_info_id: string,
//       is_favorite: Boolean,
//     },
//   },
// };

// const EventCandidateCard = (props: tEventCandidateCard) => {
//   const {
//     data = {},
//     title,
//     subtitle,
//     picture,
//     pictureTitle,
//     userId,
//     onPress,
//     arriveDate,
//     arriveDateMarker,
//   } = props;
//   if (data && data.loading) return <LoadingComponent />;
//   if (data && data.error) return <ErrorComponent message={data.error} />;

//   const { eventInfo } = data;
//   return <UserSmallCard
//     arriveDateMarker={arriveDateMarker}
//     arriveDate={arriveDate}
//     title={title}
//     subtitle={subtitle}
//     comment={eventInfo ? eventInfo.comment : null}
//     favorite={eventInfo ? eventInfo.is_favorite : false}
//     chat={client.roomUsed(userId)}
//     pictureTitle={pictureTitle}
//     picture={picture}
//     onPress={onPress}
//   />;
// };

// const EVENT_INFO_QUERY = gql`
//   query($event_registration_info_id: String!) {
//     eventInfo: eventRegistrationInfo(event_registration_info_id: $event_registration_info_id) {
//       event_registration_info_id
//       comment
//       is_favorite
//     }
//   }
// `;

// export default graphql(EVENT_INFO_QUERY, {
//   skip: props => !props.event_registration_info_id,
//   options: props => ({
//     variables: {
//       event_registration_info_id: props.event_registration_info_id,
//     },
//   }),
// })(EventCandidateCard);

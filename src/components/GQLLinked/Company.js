import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import gql from 'graphql-tag';

import RemoteSearch from '../../components/Elements/RemoteSearch';
import { theme } from '../../config/constants/index';
import defaultBusinessHeader from '../../assets/defaultBusiness.png';

const styles = StyleSheet.create({
  companySuggestionText: {
    color: theme.gray,
    fontSize: 16,
  },
  companyItemContainer: {
    paddingLeft: 3,
    paddingTop: 3,
    paddingRight: 3,
    paddingBottom: 3,
    borderColor: 'transparent',
  },
  company: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  companyImg: { width: 45, height: 45 },
  companyTextView: {
    paddingLeft: 10,
  },
});

type Props = {
  containerStyle: {},
  value: any,
  label: string,
  labelStyle: string,
  onValidate: Function,
  onChangeText: Function,
  data: {
    companies: {
      company_id: string,
      name: string,
    },
    loading: boolean,
    error: boolean,
  },
};

const Company = (props: Props) => (
  <View style={props.containerStyle}>
    <RemoteSearch
      label={props.label}
      labelStyle={props.labelStyle}
      loaderText="Recherche d'entreprise en cours..."
      defaultValue={props.value}
      itemContainerStyle={styles.companyItemContainer}
      renderItem={item => (
        <View style={styles.company}>
          <Image
            source={item.logo ? { uri: item.logo } : defaultBusinessHeader}
            style={styles.companyImg}
          />
          <View style={styles.companyTextView} >
            <Text style={styles.companySuggestionText}>{item.name}</Text>
          </View>
        </View>
      )}
      query={RemoteSearch.companiesQuery}
      onPressItem={(item) => { props.onValidate(item); }}
      onChangeText={props.onChangeText}
      keyProp="company_id"
    />
  </View>
);


const QUERY_COMPANIES = gql`
  query {
    companies {
      company_id
      name
      logo
    }
  }
`;
export default Company;
export { QUERY_COMPANIES };

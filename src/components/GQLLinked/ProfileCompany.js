// import React from 'react';
// import {
//   View,
//   Text,
//   Image,
//   StyleSheet,
// } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';
// import 'moment/locale/fr';

// import { theme } from '../../config/constants/index';
// import CardEditable from '../Card/CardEditable';
// import ErrorComponent from '../Elements/ErrorComponent';
// import LoadingComponent from '../Elements/LoadingComponent';
// import Tags from '../Content/Tags';
// import Qualities from '../Content/Qualities';
// import defaultBusinessHeader from '../../assets/defaultBusinessHeader.jpg';
// import defaultBusinessLogo from '../../assets/defaultBusiness.png';
// import { Button } from '../../components/Button';

// type Props = {
//   containerStyle: {},
//   userId: string,
//   navigate: Function,
//   data: {
//     error: string,
//     singleCompany: {
//       editable: boolean,
//       firstname: string,
//       lastname: string,
//       wish_raw_salary: string,
//       wish_variable_salary: string,
//       work_city: [],
//       company_size: [],
//     },
//     loading: boolean,
//   },
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   mainSection: {
//     paddingLeft: 0,
//     paddingTop: 0,
//     paddingRight: 0,
//     paddingBottom: 30,
//   },
//   header: {
//     width: '100%',
//     height: 181,
//     resizeMode: 'cover',
//   },
//   logo: {
//     width: 112,
//     height: 112,
//     resizeMode: 'stretch',
//     alignSelf: 'center',
//     marginTop: -56,
//     backgroundColor: 'white',
//     borderRadius: 10,
//   },
//   companyName: {
//     fontSize: 28,
//     lineHeight: 40,
//     fontWeight: 'bold',
//     alignSelf: 'center',
//     color: theme.fontBlack,
//     marginTop: 17.5,
//   },
//   emptySectionText: {
//     padding: 8,
//     lineHeight: 24,
//   },
//   tags: {
//     marginBottom: 8,
//     marginLeft: 8,
//     marginRight: 8,
//   },
//   validateButton: {
//     marginBottom: 24,
//     borderRadius: 4,
//     shadowColor: 'rgba(0, 0, 0, 0.08)',
//     shadowOffset: { width: 0.5, height: 4 },
//     shadowOpacity: 0.8,
//     shadowRadius: 2,
//     width: '90%',
//     height: 40,
//     marginTop: 24,
//     alignSelf: 'center',
//   },
//   experience: {
//     marginTop: 22,
//     paddingLeft: 8,
//     paddingRight: 8,
//     marginBottom: 2,
//   },
//   noData: {
//     marginLeft: 8,
//     marginRight: 8,
//     marginTop: 8,
//   },
// });

// const ProfileCompany = (props: Props) => {
//   if (props.data.error) {
//     return (<ErrorComponent message={'Erreur lors de récuperation de la page entreprise'} />);
//   } else if (props.data.loading) {
//     return (<LoadingComponent />);
//   }
//   const { navigate } = props;
//   const company = props.data.singleCompany;
//   if (!company) {
//     return (<LoadingComponent />);
//   }
//   return (
//     <View style={[styles.container, props.containerStyle]}>
//       <CardEditable
//         // editable={company.editable} // edit photo / banner in later version
//         containerStyle={styles.mainSection}
//       >
//         <Image
//           source={company.header ? { uri: company.header } : defaultBusinessHeader}
//           style={styles.header}
//         />
//         <Image
//           source={company.logo ? { uri: company.logo } : defaultBusinessLogo}
//           style={styles.logo}
//         />
//         <Text style={styles.companyName}>{company.name}</Text>
//       </CardEditable>
//       {
//         company.sponsor &&
//         <Button
//           title="Voir les visites"
//           onPress={() => navigate('CompanyVisitors')}
//         />
//       }
//       <CardEditable
//         editable={company.editable}
//         title="Qui sommes-nous ?"
//         containerStyle={styles.section}
//         onPress={() => { navigate('CompanyDescription'); }}
//       >
//         <Text style={styles.emptySectionText}>
//           {
//             company.description ? company.description : 'Aucune description ajoutée'
//           }
//         </Text>
//       </CardEditable>
//       <CardEditable
//         editable={company.editable}
//         title="Tags appréciés"
//         containerStyle={styles.section}
//         onPress={() => { navigate('CompanyTags', { companyId: company.company_id }); }}
//       >
//         <Tags tags={company.tags} />
//       </CardEditable>
//       <CardEditable
//         editable={company.editable}
//         title="Qualités favorites"
//         containerstyle={styles.section}
//         onPress={() => { navigate('CompanyQualities', { companyId: company.company_id }); }}
//       >
//         <Qualities qualities={company.qualities} />
//       </CardEditable>
//     </View>
//   );
// };

// const QUERY_COMPANY = gql`
//   query CompanyQuery($company_id: String!) {
//     singleCompany(company_id: $company_id) {
//       sponsor
//       company_id
//       header
//       logo
//       editable
//       description
//       name
//       tags
//       qualities
//     }
//   }
// `;

// export default graphql(QUERY_COMPANY, {
//   options: ownProps => ({
//     variables: {
//       company_id: ownProps.companyId,
//     },
//   }),
// })(ProfileCompany);
// export { QUERY_COMPANY };

// import React from 'react';
// import { View, StyleSheet, Text, Image } from 'react-native';
// import { graphql } from 'react-apollo';
// import gql from 'graphql-tag';

// import RemoteSearch from '../../components/Elements/RemoteSearch';
// import { theme } from '../../config/constants/index';
// import defaultBusinessImage from '../../assets/defaultBusiness.png';

// const styles = StyleSheet.create({
//   schoolItemContainer: {
//     paddingLeft: 3,
//     paddingTop: 3,
//     paddingRight: 3,
//     paddingBottom: 3,
//     borderColor: 'transparent',
//   },
//   school: {
//     flexDirection: 'row',
//     alignItems: 'center',
//   },
//   schoolImg: { width: 45, height: 45 },
//   schoolTextView: {
//     paddingLeft: 10,
//   },
//   schoolSuggestionText: {
//     color: theme.gray,
//     fontSize: 16,
//   },
// });

// type Props = {
//   containerStyle: {},
//   value: any,
//   label: string,
//   labelStyle: string,
//   onValidate: Function,
//   onChangeText: Function,
//   data: {
//     schools: {
//       school_id: string,
//       name: string,
//     },
//     loading: boolean,
//     error: boolean,
//   },
// };

// const School = (props: Props) => (
//   <View style={props.containerStyle}>
//     <RemoteSearch
//       label={props.label}
//       labelStyle={props.labelStyle}
//       loaderText="Recherche d'écoles en cours..."
//       defaultValue={props.value}
//       itemContainerStyle={styles.schoolItemContainer}
//       renderItem={item => (
//         <View style={styles.school}>
//           <Image
//             source={item.logo ? { uri: item.logo } : defaultBusinessImage}
//             style={styles.schoolImg}
//           />
//           <View style={styles.schoolTextView} >
//             <Text style={styles.schoolSuggestionText}>{item.name}</Text>
//           </View>
//         </View>
//       )}
//       query={RemoteSearch.schoolsQuery}
//       onPressItem={(item) => { props.onValidate(item); }}
//       onChangeText={props.onChangeText}
//       keyProp='school_id'
//     />
//   </View>
// );

// const QUERY_SCHOOLS = gql`
//   query {
//     schools {
//       school_id
//       name
//       logo
//     }
//   }
// `;
// export default graphql(QUERY_SCHOOLS)(School);
// export { QUERY_SCHOOLS };

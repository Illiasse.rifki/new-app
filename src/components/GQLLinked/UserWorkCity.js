// import React from 'react';
// import { View, StyleSheet } from 'react-native';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';

// import GeolocalisationField from '../Content/GeolocalisationField';

// type Props = {
//   containerStyle: {},
//   onPress: Function,
//   onAddCity: Function,
//   onDeleteCity: Function,
//   value: any,
//   searchRef: Function,
//   userId: string,
//   workCityId: string,
//   editCity: Function,
//   addCity: Function,
//   removeCity: Function,
//   data: {
//     work_city: {
//       description: string,
//     },
//     loading: boolean,
//     error: boolean,
//   }
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
// });

// const CITIES = gql`
//   query {
//     viewer {
//       id
//       candidate {
//         work_city {
//           work_city_id
//         }
//       }
//     }
//   }
// `;

// const UserWorkCity = (props: Props) => {
//   let customProps = {};
//   if (!props.data) {
//     customProps = {
//       value: () => (''),
//       onPress: (data, details) => {
//         props.addCity({
//           refetchQueries: [{
//             query: CITIES,
//           }],
//           variables: {
//             user_id: props.userId,
//             name: details.name,
//             description: data.description,
//             lat: '0',
//             long: '0',
//           },
//         }).then((ret) => {
//           if (props.onAddCity) {
//             props.onAddCity(ret.data.createWorkCity.work_city.work_city_id);
//           }
//         });
//       },
//     };
//   } else {
//     if (props.data.error || props.data.loading) {
//       return null;
//     }
//     customProps = {
//       value: () => (props.data.work_city ? props.data.work_city.description : ''),
//       onPress: (data, details) => {
//         props.editCity({
//           variables: {
//             work_city_id: props.workCityId,
//             name: details.name,
//             description: data.description,
//             lat: '0',
//             long: '0',
//           },
//         });
//       },
//     };
//   }
//   return (
//     <View style={[styles.container, props.containerStyle]}>
//       <GeolocalisationField
//         {...customProps}
//         searchRef={props.searchRef}
//         onChangeText={(text) => {
//           if (text === '' && props.onDeleteCity) {
//             if (props.workCityId) {
//               props.removeCity({
//                 refetchQueries: [{
//                   query: CITIES,
//                 }],
//                 variables: {
//                   work_city_id: props.workCityId,
//                 },
//               });
//             }
//             if (props.onDeleteCity) {
//               props.onDeleteCity();
//             }
//           }
//         }}
//       />
//     </View>
//   );
// };

// const QUERY_CITY = gql`
//   query WorkCityQuery($work_city_id: String!) {
//     work_city(work_city_id: $work_city_id) {
//       description
//     }
//   }
// `;

// const EDIT_CITY = gql`
//   mutation(
//     $work_city_id: String!,
//     $name: String,
//     $description: String,
//   )   {
//     editWorkCity(
//       work_city_id: $work_city_id
//       name: $name,
//       description: $description
//     ) {
//       work_city {
//         work_city_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const ADD_CITY = gql`
//   mutation(
//     $user_id: String!,
//     $name: String!,
//     $description: String!,
//     $lat: String!,
//     $long: String!,
//   )   {
//     createWorkCity(
//       user_id: $user_id,
//       name: $name,
//       description: $description
//       lat: $lat,
//       long: $long,
//     ) {
//       work_city {
//         work_city_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const REMOVE_CITY = gql`
//   mutation(
//     $work_city_id: String!,
//   )   {
//     deleteWorkCity(
//       work_city_id: $work_city_id
//     ) {
//       work_city {
//         work_city_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// export default compose(
//   graphql(QUERY_CITY, {
//     skip: ownProps => !ownProps.workCityId,
//     options: ownProps => ({
//       variables: {
//         work_city_id: ownProps.workCityId,
//       },
//     }),
//   }),
//   graphql(EDIT_CITY, { name: 'editCity' }),
//   graphql(ADD_CITY, { name: 'addCity' }),
//   graphql(REMOVE_CITY, { name: 'removeCity' }),
// )(UserWorkCity);

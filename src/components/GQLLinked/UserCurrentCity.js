// import React from 'react';
// import { View, StyleSheet } from 'react-native';
// import { graphql, compose } from 'react-apollo';
// import gql from 'graphql-tag';

// import GeolocalisationField from '../Content/GeolocalisationField';

// type Props = {
//   containerStyle: {},
//   onPress: Function,
//   onChangeCity: Function,
//   value: any,
//   userId: string,
//   currentCityId: string,
//   editCity: Function,
//   addCity: Function,
//   addWorkCity: Function,
//   createWorkCityOnAdd: boolean,
//   data: {
//     current_city: {
//       description: string,
//     },
//     loading: boolean,
//     error: boolean,
//   }
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
// });

// const UserCurrentCity = (props: Props) => {
//   let customProps = {};
//   if (!props.data) {
//     customProps = {
//       value: () => (''),
//       onPress: (data, details) => {
//         if (props.createWorkCityOnAdd) {
//           props.addWorkCity({
//             variables: {
//               user_id: props.userId,
//               name: details.name,
//               description: data ? data.description : null,
//               lat: '0',
//               long: '0',
//             },
//           });
//         }
//         props.addCity({
//           variables: {
//             user_id: props.userId,
//             name: details.name,
//             description: data ? data.description : null,
//             lat: '0',
//             long: '0',
//           },
//         }).then((ret) => {
//           props.onChangeCity(ret.data.createCurrentCity.current_city.current_city_id);
//         });
//       },
//     };
//   } else {
//     if (props.data.error || props.data.loading) {
//       return null;
//     }
//     customProps = {
//       value: () => (props.data && props.data.current_city
//         && props.data.current_city.description
//         ? props.data.current_city.description
//         : ''),
//       onPress: (data, details) => {
//         if (props.createWorkCityOnAdd) {
//           props.addWorkCity({
//             variables: {
//               user_id: props.userId,
//               name: details.name,
//               description: data ? data.description : null,
//               lat: '0',
//               long: '0',
//             },
//           });
//         }
//         props.editCity({
//           variables: {
//             current_city_id: props.currentCityId,
//             name: details.name,
//             description: data ? data.description : null,
//             lat: '0',
//             long: '0',
//           },
//         });
//       },
//     };
//   }
//   return (
//     <View style={[styles.container, props.containerStyle]}>
//       <GeolocalisationField
//         {...customProps}
//       />
//     </View>
//   );
// };

// const QUERY_CITY = gql`
//   query CurrentCityQuery($current_city_id: String!) {
//     current_city(current_city_id: $current_city_id) {
//       description
//     }
//   }
// `;

// const EDIT_CITY = gql`
//   mutation(
//     $current_city_id: String!,
//     $name: String,
//     $description: String,
//   )   {
//     editCurrentCity(
//       current_city_id: $current_city_id
//       name: $name,
//       description: $description
//     ) {
//       current_city {
//         current_city_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const ADD_CITY = gql`
//   mutation(
//     $user_id: String!,
//     $name: String!,
//     $description: String!,
//     $lat: String!,
//     $long: String!,
//   )   {
//     createCurrentCity(
//       user_id: $user_id,
//       name: $name,
//       description: $description
//       lat: $lat,
//       long: $long,
//     ) {
//       current_city {
//         current_city_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// const ADD_WORK_CITY = gql`
//   mutation(
//     $user_id: String!,
//     $name: String!,
//     $description: String!,
//     $lat: String!,
//     $long: String!,
//   )   {
//     createWorkCity(
//       user_id: $user_id,
//       name: $name,
//       description: $description
//       lat: $lat,
//       long: $long,
//     ) {
//       work_city {
//         work_city_id
//       }
//       error {
//         message
//       }
//     }
//   }
// `;

// export default compose(
//   graphql(QUERY_CITY, {
//     skip: ownProps => !ownProps.currentCityId,
//     options: ownProps => ({
//       variables: {
//         current_city_id: ownProps.currentCityId,
//       },
//     }),
//   }),
//   graphql(EDIT_CITY, { name: 'editCity' }),
//   graphql(ADD_CITY, { name: 'addCity' }),
//   graphql(ADD_WORK_CITY, { name: 'addWorkCity' }),
// )(UserCurrentCity);

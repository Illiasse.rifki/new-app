import { SET_FIELD, FLUSH_FORM } from '../actions/candidateForm';

const initialState = {
  firstname: '',
  lastname: '',
  email: '',
  birthdate: '', // DD-MM-YYYY Date
  password: '',
  confirmation: '',
  jobSituation: '',
  phone: '',
  headline: '',
  progress: 0,
  profilPicture: null, // string url
  cguAccepted: false,
  experimented: false,
  tags: [],
  qualities: [],
  graduationLevel: null, // enum
  city: {
    name: '',
    description: '',
    lat: 0,
    long: 0,
  },
  yearsStudy: '0',
  activitySector: '', // string
  companySize: [],
  searchCities: [],
  wantSectors: [], // string array
  companyBenefits: [], // string array
  annualFixedSalary: '',
  annualVariableSalary: '',
  annualFixedSalaryWanted: '',
  annualVariableSalaryWanted: '',
  experiences: [],
  languages: [],
  training: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FLUSH_FORM: {
      return initialState;
    }
    case SET_FIELD: {
      const newState = {
        ...state,
      };
      newState[action.key] = action.value;
      return newState;
    }
    default: {
      return state;
    }
  }
};

export default reducer;

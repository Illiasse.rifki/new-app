import { SET_CURRENT_REF, SET_INPUT_REFS, RESET_KEYBOARD_HELPER } from '../config/constants/actionsTypes';

const initialState = {
  inputRefs: [],
  currentRef: null,
  navigationButton: {
    label: 'OK',
    onPress: undefined,
  },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_INPUT_REFS: {
      return { ...state, inputRefs: action.data };
    }
    case SET_CURRENT_REF: {
      return { ...state, currentRef: action.data };
    }
    case RESET_KEYBOARD_HELPER: {
      return { ...state, navigationButton: action.data.navigationButton };
    }
    default: {
      return state;
    }
  }
};

export default reducer;

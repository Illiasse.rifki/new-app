import { combineReducers } from 'redux';
import candidateForm from './candidateForm';
import keyboardHelper from './keyboardHelper';

export default combineReducers({ candidateForm, keyboardHelper });

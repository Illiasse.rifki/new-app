import gql from 'graphql-tag';

export const JOB_OFFER_REGISTRATION_QUERY = gql`
  query jobOfferRegistration($jobOfferRegistrationId: String!) {
    jobOfferRegistration(job_offer_registration_id: $jobOfferRegistrationId) {
      jobOfferRegistrationId: job_offer_registration_id
      jobOffer: job_offer {
        jobOfferId: job_offer_id
        questions
      }
      favorite: is_favorite_offer
      nbviews
      status
    }
  }
`;

export const JOB_OFFER_REGISTRATION_BUTTON_QUERY = gql`
  query jobOfferRegistration($jobOfferId: String) {
    jobOfferRegistration(job_offer_id: $jobOfferId) {
      jobOfferRegistration: job_offer_registration_id
      status
      applyDate: apply_date(format: "D/M/Y à H:m")
    }
  }
`;

export const JOB_OFFER_REGISTRATIONS_LIST = gql`
  query {
    viewer {
      id
      candidate {
        jobOffersRegistrations: job_offers_registrations {
          jobOfferRegistrationId: job_offer_registration_id
          status
          jobOffer: job_offer {
            jobOfferId: job_offer_id
            company {
              name
            }
            title
            cover_picture
          }
        }
      }
    }
  }
`;

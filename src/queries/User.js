import gql from 'graphql-tag';

export const getUserById = gql`
  query {
    user(id: 1) {
      id
      firstname
      lastname
    }
  }
`;

export const getAllUsers = gql`
  query {
    users {
      id
      firstname
      lastname
    }
  }
`;

import { AsyncStorage } from 'react-native';

export default class AuthStockage {
  constructor() {
    this.init = false;
  }

  async load(tokens) {
    const promises = [];
    tokens.forEach((token) => {
      promises.push(AsyncStorage.getItem(`@Bizzeo:${token}`));
    });
    return Promise.all(promises).then((items) => {
      items.forEach((item, i) => {
        this[tokens[i]] = item;
      });
      this.init = true;
    });
  }

  isInit() {
    return this.init;
  }

  setToken(name, token) {
    this[name] = token;
    if (!token) {
      AsyncStorage.removeItem(`@Bizzeo:${name}`);
    } else {
      AsyncStorage.setItem(`@Bizzeo:${name}`, token);
    }
  }

  removeToken = () => {
    AsyncStorage.removeItem('@Bizzeo:access-token');
    AsyncStorage.removeItem('@Bizzeo:recruiter-access-token');
    AsyncStorage.removeItem('@Bizzeo:signed-as');
  }

  getToken(name) {
    return this[name];
  }
}

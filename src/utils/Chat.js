// @flow
import io from 'socket.io-client/dist/socket.io';
import { AsyncStorage } from 'react-native';
import { CHAT } from '../config/constants/index';

class ChatClient {
  async set(args) {
    if (!this.socket) {
      const accessToken = await AsyncStorage.getItem('@Bizzeo:access-token');
      const recruiterAccessToken = await AsyncStorage.getItem('@Bizzeo:recruiter-access-token');
      // const { accessToken, recruiterAccessToken } = args;
      const query = {};

      if (!accessToken && !recruiterAccessToken) return;

      if (accessToken) query['access-token'] = accessToken;
      if (recruiterAccessToken) query['recruiter-access-token'] = recruiterAccessToken;

      const socket = io(CHAT, {
        transports: ['websocket'],
        query,
      });
      this.socket = socket;
      this.chatOpen = true;
    }
  }

  chatOpen = false;

  on = (chan, callback) => this.socket.on(chan, callback);
  off = (chan, callback) => this.socket.off(chan, callback);
  once = (chan, callback) => this.socket.once(chan, callback);
  emit = (chan, message, callback) => this.socket.emit(chan, message, callback);
  disconnect = () => {
    this.chatOpen = false;
    this.socket.disconnect();
  }
  destroy = () => this.socket.destroy();

  getRoom = room_id => new Promise((res) => {
    this.socket.emit('get_room', { room_id });
    this.socket.once('get_room', res);
  });
  getRooms = () => new Promise((res) => {
    this.socket.emit('rooms');
    this.socket.once('rooms', res);
  });
  getMessages = () => new Promise((res) => {
    this.socket.emit('messages');
    this.socket.once('rooms', res);
  });

  roomUsed = (userId) => {
    if (!this.rooms || this.rooms.length === 0) return false;
    const i = this.rooms.findIndex(room => room.users.findIndex(user => user.id === userId) !== -1 && Object.keys(room.last_message).length !== 0);
    return i !== -1;
  }

  sendMessage = (room_id, message) => new Promise((res) => {
    this.socket.emit('message', { room_id, message });
    this.socket.once('message', res);
  });
  createRoom = (user_id, cb) => {
    this.socket.emit('create_room', { user_id }, cb);
    // this.socket.once('room', res);
  };
  sendRead = room_id => new Promise((res) => {
    this.socket.emit('read', { room_id });
  });
  sendTyping = room_id => this.socket.emit('typing', { room_id });
}

const client = new ChatClient();

export default (options = {}) => {
  client.set(options);
  return client;
};

export { client };

import gql from 'graphql-tag';

export const JOB_OFFER_REGISTRATION_MUTATION = gql`
  mutation($jobOfferId: String, $jobOfferRegistrationId: String, $applyQuestions: [String], $applyAnswers: [String], $status: String, $favoriteOffer: Boolean, $favoriteCandidate: Boolean) {
    editJobOfferRegistration(job_offer_id: $jobOfferId, job_offer_registration_id: $jobOfferRegistrationId, apply_questions: $applyQuestions, apply_answers: $applyAnswers, status: $status, is_favorite_offer: $favoriteOffer, is_favorite_candidate: $favoriteCandidate) {
      jobOfferRegistration {
        job_offer_registration_id
      }
      error {
        code
        message
      }
    }
  }
`; 
